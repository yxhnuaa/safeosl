#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "clang/Driver/Options.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Tooling/RefactoringCallbacks.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Lex/Lexer.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Decl.h"
#include "llvm/ADT/APSInt.h"


#define MAX 10000
using namespace clang::tooling;
using namespace clang::ast_matchers;
using namespace llvm;
using namespace clang;
using namespace std;

static int mut_ref_var_count = 0;
static unsigned int for_variable_count = 0;
static int mut_var_count = 0;
static int heap_var_count = 0;
static string file_name;

string mut_var[MAX];
string mut_ref_var[MAX];
string heap_var[MAX];
unsigned int for_variable[MAX];  //for语句中声明的变量

//查找是否为可变引用
bool read_mut_ref_var(string varname){
     for(int i = 0; i < mut_ref_var_count; i++){
      if(varname == mut_ref_var[i])
        return true;
     }
     return false;
}

//查找是否是for语句中声明的变量
bool find_in_for_var(unsigned int linenumber){
 for(unsigned int i = 0; i < for_variable_count; i++){
      if( linenumber == for_variable[i])
          return true;
      }
          return false;
}
//确定copy属性
bool find_heap_var(string varname){
     for(int i = 0; i < heap_var_count; i++){
      if(varname == heap_var[i])
        return true;
     }
     return false;
}

//返回声明变量的名字
  std::string  return_varname(const VarDecl *vd){
           return vd -> getNameAsString();
  }

  //返回声明语句的源码范围
  SourceRange return_endloc(const VarDecl *vd, const SourceManager &sm, LangOptions lopt){
          SourceRange decl_range = vd -> getSourceRange(); 
          SourceLocation decl_begin = decl_range.getBegin();
          SourceLocation decl_start_end = decl_range.getEnd();
          SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(decl_start_end, 0, sm, lopt).getLocWithOffset(1);
          SourceRange range = SourceRange(decl_begin, decl_end_end);
          return range;
  }
 
 //返回赋值语句的源码范围
SourceRange return_assign_endloc(const Stmt *st, const SourceManager &sm, LangOptions lopt){
          SourceLocation begin = st->getBeginLoc();
          SourceLocation end = st->getEndLoc();
          SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(end, 0, sm, lopt).getLocWithOffset(1);
          SourceRange range = SourceRange(begin, decl_end_end);
          return range;
  }

class VisitVarDecl : public MatchFinder::MatchCallback {

private:
  Rewriter& rewriter_;
  LangOptions lopt;

public:
  explicit VisitVarDecl(Rewriter &r): rewriter_(r){}

  //改写源码
 void do_replacement(const SourceManager &sm, SourceRange range, string newcontent){
         Replacement repl(sm, CharSourceRange(range,false), newcontent);
         repl.apply(rewriter_);         
 } 

//处理elseif语句
void hander_elseif(const IfStmt *elseif_Stmt, const SourceManager &sm, LangOptions lopt){
       const Expr *condition = elseif_Stmt->getCond();
       SourceLocation else_if_end = clang::Lexer::getLocForEndOfToken(condition -> getEndLoc(), 0, sm, lopt).getLocWithOffset(1);
       SourceLocation else_if_begin = condition -> getBeginLoc().getLocWithOffset(-8);
       string elseif_text = " , ";
       do_replacement(sm, SourceRange(else_if_begin,else_if_end), elseif_text);
}

//处理else语句
void hander_else(const Stmt *else_stmt, const SourceManager &sm, LangOptions lopt) {
         SourceLocation else_end = clang::Lexer::getLocForEndOfToken(else_stmt -> getBeginLoc(), 0, sm, lopt).getLocWithOffset(-5);
         string else_text = " , ";
         do_replacement(sm, SourceRange(else_end,else_stmt->getBeginLoc()), else_text);
         //分支体后添加；
         SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(else_stmt->getEndLoc(), 1, sm, lopt);
         rewriter_.InsertTextAfterToken(add_comma, ";");
} 

//处理二元运算符的操作表达式
const std::string handler_op_stmt(Expr *expr, const SourceManager &sm, LangOptions lopt){
    
    const clang::CastExpr *explicit_stmt = dyn_cast<clang::CastExpr>(expr->IgnoreImplicit());
    CharSourceRange CSR;
    //变量在左边,是否含有*号
    if(explicit_stmt){    //处理形如(int)(*p_global>=MIN)或(int)p
         const Expr *sub_left_var = explicit_stmt-> getSubExpr();
         const clang::BinaryOperator *sub_UO = dyn_cast<clang::BinaryOperator>(sub_left_var->IgnoreParenImpCasts());
         if(sub_UO) {   //处理形如(int)(*p_global>=MIN)
             Expr *left_var = sub_UO->getLHS();
             CSR = CharSourceRange(left_var->getSourceRange(), true);
         }
         else{//处理形如(int)p
             CSR = CharSourceRange(sub_left_var->getSourceRange(), true);
         }
    }
    else{    //处理*p_global>=MIN或p
         const clang::BinaryOperator *sub_UO = dyn_cast<clang::BinaryOperator>(expr->IgnoreParenImpCasts());
         if(sub_UO){   //*p_global>=MIN
             Expr *left_var = sub_UO->getLHS();
             CSR = CharSourceRange(left_var->getSourceRange(), true); 
         }
         else{   //处理形如p
             CSR = CharSourceRange(expr->getSourceRange(), true);
         }
    }
    return Lexer::getSourceText(CSR, sm, lopt).str();  
}

//赋值语句左边表达式是数组元素，根据不同类型返回替换内容 a[i]
std::string arraySubscript_content(const ArraySubscriptExpr *expr, const SourceManager &sm, LangOptions lopt, int flag, string other){
    const clang::DeclRefExpr *array_DR = clang::dyn_cast<clang::DeclRefExpr>(expr->getBase()->IgnoreImpCasts());
    string newcontent;
    if(array_DR){
       string array_name = array_DR -> getDecl()->getNameAsString();  //数组名
       const Expr *idx = expr -> getIdx();                            //数组索引
       CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
       const std::string idx_value = Lexer::getSourceText(idx_CSR, sm, lopt).str(); 
       const BinaryOperator *complex_idx = clang::dyn_cast<BinaryOperator>(idx->IgnoreParenImpCasts());
       if(flag == 1){
         if(complex_idx)
           newcontent = "transfer newResource(copy) " + array_name + ".(" + idx_value + ");      // array element assignment";
         else
           newcontent = "transfer newResource(copy) " + array_name + "." + idx_value + ";      // array element assignment";
       }
       else if(flag == 2){
          if(complex_idx)
            newcontent = "transfer newResource(copy) *(" + array_name + ".(" + idx_value + "));   // array element assignment";
          else
            newcontent = "transfer newResource(copy) *(" + array_name + "." + idx_value + ");   // array element assignment";
       }  
       else if(flag == 3){
           if(complex_idx)
             newcontent = "transfer " + other + " " + array_name + ".(" + idx_value + ");     // array element assignment";
           else
             newcontent = "transfer " + other + " " + array_name + "." + idx_value + ";     // array element assignment";
       }
       else if(flag == 4){
          if(complex_idx)
            newcontent = "transfer newResource() " + array_name + ".(" + idx_value + ");     //" + other + " function to array element";
          else
            newcontent = "transfer newResource() " + array_name + "." + idx_value + ";     //" + other + " function to array element";
       }
       else if(flag == 5){
          if(complex_idx)
            newcontent = array_name + ".(" + idx_value + ")"+ other;
          else
            newcontent = array_name + "." + idx_value + other;
       }
       else if(flag == 6){
           if(complex_idx)
             newcontent = "transfer " + array_name + ".(" + idx_value + ") " + other + ";";
           else
             newcontent = "transfer " + array_name + "." + idx_value + " " + other + ";";
       }
       else if(flag == 7){
           if(complex_idx)
             newcontent = "transfer *(" + array_name + ".(" + idx_value + ")) " + other + ";";
           else
             newcontent = "transfer *(" + array_name + "." + idx_value + ") " + other + ";";
      }
       else if(flag == 8){
            if(complex_idx)
              newcontent = "decl " + other + ":#own(copy); transfer *(" + array_name + ".(" + idx_value + ")) " + other + ";";
            else
              newcontent = "decl " + other + ":#own(copy); transfer *(" + array_name + "." + idx_value + ") " + other + ";";
      } 
       else if(flag == 9){
            if(complex_idx)
              newcontent = "decl " + other + ":#own(copy); transfer " + array_name + ".(" + idx_value + ") " + other + ";";
            else
              newcontent = "decl " + other + ":#own(copy); transfer " + array_name + "." + idx_value + " " + other + ";";
      } 
       else if(flag == 10){
            if(complex_idx)
              newcontent = other + " " + array_name + ".(" + idx_value + ");";
            else
              newcontent = other + " " + array_name + "." + idx_value + ";";
      }
       else if(flag == 11){
            if(complex_idx)
              newcontent = "decl " + other + "; transfer " + array_name + ".(" + idx_value + ") " + other + ";";
            else
              newcontent = "decl " + other + "; transfer " + array_name + "." + idx_value + " " + other + ";";
      } 
       else if(flag == 12){
            if(complex_idx)
              newcontent = "transfer newResource() " + array_name + ".(" + idx_value + ");      // array element assignment";
            else
              newcontent = "transfer newResource() " + array_name + "." + idx_value + ";      // array element assignment";
      }
    }
    return newcontent;
}

//赋值语句左边表达式是成员访问，根据不同类型返回替换内容
std::string member_access_content(const MemberExpr *expr, int flag, string other){
    const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(expr->getBase()->IgnoreImpCasts());
    string newcontent;
    if(struct_union_DR){
       string struct_union_name = struct_union_DR -> getDecl()->getNameAsString();  //结构体/联合体变量名
       string member_name = expr->getMemberDecl()->getNameAsString(); //成员名
       int member_idx = member_id(struct_union_DR, member_name);   //获取成员变量在struct/union中定义的序号
       if(flag == 1)
         newcontent = "transfer newResource(copy) " + struct_union_name + "." + to_string(member_idx) + ";    // member variable assign";
       else if(flag == 2)
         newcontent = "transfer newResource(copy) *(" + struct_union_name + "." + to_string(member_idx) + ");    // member variable assign";
       else if(flag == 3)
         newcontent = "transfer " + other + " " + struct_union_name + "." + to_string(member_idx) + ";    // member variable assign";
       else if(flag == 4)
         newcontent = "transfer newResource()  " + struct_union_name + "." + to_string(member_idx) + ";   //" + other + " function";
       else if(flag == 5)
         newcontent = struct_union_name + "." + to_string(member_idx) + other;
       else if(flag == 6)
         newcontent = "transfer " + struct_union_name + "." + to_string(member_idx) + " " + other + ";";
       else if(flag == 7)
         newcontent = "transfer *(" + struct_union_name + "." + to_string(member_idx) + ") " + other + ";";
       else if(flag == 8)
         newcontent = "decl " + other + ":#own(copy); transfer *(" + struct_union_name + "." + to_string(member_idx) + ") " + other + ";";
       else if(flag == 9)
         newcontent = "decl " + other + ":#own(copy); transfer " + struct_union_name + "." + to_string(member_idx) + " " + other + ";";
       else if(flag == 10)
         newcontent = other + " " + struct_union_name + "." + to_string(member_idx) + ";";
       else if(flag == 11)
         newcontent = "decl " + other + "; transfer " + struct_union_name + "." + to_string(member_idx) +  " " + other + ";";
    }
    return newcontent;
}

//获取成员变量在struct/union中定义的序号
int member_id(const DeclRefExpr *dre, string member_name){
  const clang::VarDecl *declaration = clang::dyn_cast<clang::VarDecl>(dre -> getDecl());
  const clang::Type *type = declaration->getType().getTypePtr();
  RecordDecl *struct_union_decl;
  if(type-> isRecordType())  //p4.x = 10;
      struct_union_decl = type->getAsRecordDecl();
  if(type-> isPointerType()){   //p3->x = 19;
      const clang::PointerType *pointer = clang::dyn_cast<clang::PointerType>(type);
          if(pointer){
              if(pointer->getPointeeType().getTypePtr()->isRecordType())
                  struct_union_decl = pointer->getPointeeType().getTypePtr()->getAsRecordDecl();
          } 
  }
  if(struct_union_decl){
      clang::RecordDecl::field_iterator jt;
      int i = 0;
      for(jt = struct_union_decl->field_begin(); jt != struct_union_decl->field_end(); ++jt){
          if(jt->getNameAsString() == member_name)  break;
          else
              ++i;
      }
      return i;
  }
}

//重写函数调用中的参数形式(不含有参数为调用函数的函数调用)
std::string parameters_change(const CallExpr *callexpr, const SourceManager &sm, LangOptions lopt){
     unsigned int argnumber = callexpr->getNumArgs();
     string format;
     if (argnumber >= 1){
      //cout << "%%------%%" << endl;
      for(unsigned int i = 0; i < argnumber; i++){
        const Expr *call_arguments = callexpr -> getArg(i)->IgnoreParenImpCasts();
        CharSourceRange Arg_CSR = CharSourceRange(call_arguments->getSourceRange(), true);
        const std::string Arg = Lexer::getSourceText(Arg_CSR, sm, lopt).str();
        //cout << Arg << endl;
        const DeclRefExpr *simple_type_var = clang::dyn_cast<DeclRefExpr>(call_arguments->IgnoreParenImpCasts());
        const MemberExpr *member_expr = clang::dyn_cast<MemberExpr>(call_arguments->IgnoreParenImpCasts());
        const ArraySubscriptExpr *array_expr = clang::dyn_cast<ArraySubscriptExpr>(call_arguments->IgnoreParenImpCasts());
        const UnaryOperator *star_expr = clang::dyn_cast<UnaryOperator>(call_arguments->IgnoreParenImpCasts());
        //const CallExpr *call_arg = clang::dyn_cast<CallExpr>(call_arguments->IgnoreParenCasts());
        if(simple_type_var)   //单个变量 m
            format.append(simple_type_var->getDecl()->getNameAsString());
        else if(member_expr){    //访问非指针类型成员变量 p3->x
              const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(member_expr->getBase()->IgnoreImpCasts());
              string struct_union_name = struct_union_DR -> getDecl()->getNameAsString();  //结构体/联合体变量名
              string member_name = member_expr->getMemberDecl()->getNameAsString(); //结构体/联合体成员名
              int member_idx = member_id(struct_union_DR, member_name);   //获取成员变量在struct/union中定义的序号
              format.append(struct_union_name).append(".").append(to_string(member_idx));
        }
        else if(array_expr){     //数组元素类型
              const clang::DeclRefExpr *DR = clang::dyn_cast<clang::DeclRefExpr>(array_expr->getBase()->IgnoreImpCasts());
              string array_name = DR -> getDecl()->getNameAsString();  //数组名
              const Expr *idx = array_expr -> getIdx();      //数组索引                          
              CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
              const std::string idx_value = Lexer::getSourceText(idx_CSR, sm, lopt).str(); //变量数组索引
              format.append(array_name).append(".").append(idx_value);
        }
        else if(star_expr){     
              Expr *star_aub_exp = star_expr -> getSubExpr();
              const clang::DeclRefExpr *star_expr_simple = clang::dyn_cast<clang::DeclRefExpr>(star_aub_exp->IgnoreParenImpCasts());
              CharSourceRange Op_CSR = CharSourceRange(SourceRange(star_expr->getBeginLoc(),star_aub_exp->getBeginLoc().getLocWithOffset(-1)), true);
              const std::string Op_name = Lexer::getSourceText(Op_CSR, sm, lopt).str();
             // cout << "///" << Op_name << "///" << endl;
              if(star_expr_simple){       //*ptr 或 &ptr
                 if(Op_name == "*")
                    format.append(Arg);
                 if(Op_name == "&"){
                    const FunctionDecl *fun_definition = callexpr->getDirectCallee();
                    if(fun_definition){
                       string parmeter_name = fun_definition->getParamDecl(i)->getNameAsString(); //形参
                       if(read_mut_ref_var(parmeter_name)) 
                         format.append(parmeter_name).append(" mborrow ").append(star_expr_simple->getDecl()->getNameAsString());
                       else
                         format.append(parmeter_name).append(" borrow ").append(star_expr_simple->getDecl()->getNameAsString());
                    }
                 }
              }
              const MemberExpr *sub_member_expr = clang::dyn_cast<MemberExpr>(star_aub_exp->IgnoreParenImpCasts());
              if(sub_member_expr){    //*( p3->x)或*(p3.x)类型
                   const clang::DeclRefExpr *sub_struct_DR = clang::dyn_cast<clang::DeclRefExpr>(sub_member_expr->getBase()->IgnoreImpCasts());
                   string sub_name = sub_struct_DR -> getDecl()->getNameAsString();  //结构体/联合体变量名
                   string sub_member_name = sub_member_expr->getMemberDecl()->getNameAsString(); //结构体/联合体成员名
                   int member_idx = member_id(sub_struct_DR, sub_member_name); 
                   if(Op_name == "*")
                      format.append("*(").append(sub_name).append(".").append(to_string(member_idx)).append(")");
                   if(Op_name == "&")    //&( p3->x)
                      format.append(sub_name).append(".").append(to_string(member_idx));
              }
              const ArraySubscriptExpr *sub_array_expr = clang::dyn_cast<ArraySubscriptExpr>(star_aub_exp->IgnoreParenImpCasts());
              if(sub_array_expr){       //*(a[1])或&(a[1])类型
                   const clang::DeclRefExpr *DR = clang::dyn_cast<clang::DeclRefExpr>(sub_array_expr->getBase()->IgnoreImpCasts());
                   string array_name = DR -> getDecl()->getNameAsString();  //数组名
                   const Expr *idx = sub_array_expr -> getIdx();      //数组索引                          
                   CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                   const std::string idx_value = Lexer::getSourceText(idx_CSR, sm, lopt).str(); //变量数组索引
                   if(Op_name == "*")   //*(a[1])
                      format.append("*(").append(array_name).append(".").append(idx_value);
                   if(Op_name == "&"){    //&(a[1])
                      const FunctionDecl *fun_definition = callexpr->getDirectCallee();
                      if(fun_definition){
                       string parmeter_name = fun_definition->getParamDecl(i)->getNameAsString(); //形参
                       if(read_mut_ref_var(parmeter_name)) 
                         format.append(parmeter_name).append(" mborrow ").append(array_name).append(".").append(idx_value);
                       else
                         format.append(parmeter_name).append(" borrow ").append(array_name).append(".").append(idx_value);
                      }
                    }
              }   
        }
        /*else if(call_arg){         //参数为函数调用：srand( (unsigned)time(p) ); a(b(c(i)))
             //cout << "||3" << Arg <<"||3" << endl;
          //cout << "**" << call << "**" << endl;
          if(call_arg->getNumArgs()){
             string parameters = parameters_change(call_arg, sm, lopt);
             //cout << "**" << parameters << "**" << endl;
             format.append("call ").append(call_arg->getDirectCallee()->getNameInfo().getAsString()).append("(").append(parameters).append(")");
          }
      }*/
        else if(Arg == ""){  //参数为NULL
             format.append("#uninit");
           }
        else
          format.append(Arg);
      if(i != argnumber - 1)
                format.append(", ");
     }
     return format;
   }
}

virtual void run(const MatchFinder::MatchResult &Result){

    const ASTContext *Context = Result.Context;
    const SourceManager &sourceManager_ = Context->getSourceManager();

       
/*---------------------------------------------------------变量声明转换------------------------------------------------*/
  //0.多个变量的声明的action 
    const DeclStmt *stmt = Result.Nodes.getNodeAs<DeclStmt>("declstmt");
    if(stmt != nullptr && sourceManager_.isWrittenInMainFile(stmt->getBeginLoc())) {
      std::string newcontent;
      string flag_var = "true";
      //const clang::Type *varType;
      if(!stmt->isSingleDecl()) { 
         //cout << "//" << endl;   
         SourceRange range = SourceRange(stmt->getBeginLoc(), stmt->getEndLoc());
         CharSourceRange CSR = CharSourceRange(range, true);
         const std::string vardecl_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();

         clang::DeclStmt::const_decl_iterator it;
         //遍历所有声明的变量
          for(it = stmt->decl_begin(); it != stmt->decl_end(); ++it){
            //const RecordDecl *recorddecl = dyn_cast<RecordDecl>(*it);
            //const EnumDecl *enumdecl = dyn_cast<EnumDecl>(*it);
            const auto *vardecl = dyn_cast<VarDecl>(*it);
            if(vardecl == nullptr){
              flag_var = "false";
              break;
            }
            else if(vardecl != nullptr && sourceManager_.isWrittenInMainFile(vardecl->getLocation())){
               std::string varname = return_varname(vardecl);   //变量名 
               const clang::Type *varType = vardecl->getType().getTypePtr();   //变量类型
               if(vardecl -> hasInit()){     //具有初始值的变量声明
                  //cout << "///" << varname << "///";
                  const Expr *init = vardecl -> getAnyInitializer();
                  CharSourceRange expr_CSR = CharSourceRange(init->getSourceRange(), true);
                  const std::string init_stmt = Lexer::getSourceText(expr_CSR, sourceManager_, lopt).str();
                  const DeclRefExpr *dre = dyn_cast<DeclRefExpr>(init -> IgnoreParenCasts());
                  const CallExpr *call_fun = dyn_cast<CallExpr>(init -> IgnoreParenCasts());
                  const UnaryOperator *uo = dyn_cast<UnaryOperator>(init -> IgnoreParenCasts());
                  if(varType->isBuiltinType()){   //内置类型  int a=b, c=10, k=y(m);
                       if(dre){     //int a=b
                          string old_owner = dre -> getDecl() -> getNameAsString();
                          newcontent.append("decl ").append(varname).append("; transfer ").append(old_owner).append(" ").append(varname).append(";");
                       }
                       else if(call_fun){   //int k=y(m);
                          CharSourceRange CSR = CharSourceRange(call_fun->getSourceRange(), true);
                          const std::string call_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
                          unsigned int argnumber = call_fun->getNumArgs();
                          string call_name = call_fun -> getDirectCallee()->getNameInfo().getAsString();
                          if(argnumber == 0)
                            newcontent.append("decl ").append(varname).append("; call ").append(call_stmt)
                                      .append("; transfer newResource(copy) ").append(varname).append(";");
                          else{
                            string parameters = parameters_change(call_fun, sourceManager_, lopt);
                            newcontent.append("decl ").append(varname).append("; call ").append(call_name).append("(").append(parameters).append(")")
                                      .append("; transfer newResource(copy) ").append(varname).append(";");
                          }
                       }else     //int x = *p;  或者int x = 10;   int = 100 + m;
                          newcontent.append("decl ").append(varname).append("; transfer newResource(copy) ").append(varname).append(";");
                       
                  }
                  const InitListExpr *initializer_list = dyn_cast<InitListExpr>(init);
                  if(varType->isArrayType() && initializer_list)  //数组类型 int f[10]={ 0,1,2,3,4,5,6,7,8,9 };
                      newcontent.append("decl ").append(varname).append("; transfer newResource(copy) ").append(varname).append(";");
                  if(varType->isStructureType()){   //结构体类型
                      if(initializer_list)      //带有初始列表值的结构体声明 Point p1= {0, 1}
                        newcontent.append("decl ").append(varname).append("; transfer newResource(copy) ").append(varname).append(";");
                      if(dre){      //带有初始值变量的结构体声明 Point p2 = p1;
                        string reference_var_name = dre -> getDecl()->getNameAsString();
                        newcontent.append("decl ").append(varname).append("; transfer ").append(reference_var_name).append(" ").append(varname).append(";");
                      }
                  }
                  if(varType->isUnionType()){ //联合体类型，一般不对联合体进行集体赋值，因此不考虑联合体的列表赋值
                      if(dre){      //带有初始值变量的联合体声明 number n2 = n1;
                        string reference_var_name = dre -> getDecl()->getNameAsString();
                        newcontent.append("decl ").append(varname).append("; transfer ").append(reference_var_name).append(" ").append(varname).append(";");
                      }
                  }
                  if(varType->isEnumeralType()){   //枚举类型
                      if(dre){
                      const ValueDecl *val_decl = dre -> getDecl();
                      const EnumConstantDecl *enum_constant = dyn_cast<EnumConstantDecl>(val_decl);
                      if(enum_constant) //带有初始值的枚举变量声明 today = TUE; 
                           newcontent.append("decl ").append(varname).append("; transfer newResource(copy) ").append(varname).append(";");
                      else{      //带有初始值变量的枚举声明 DAY terrowday = today;
                           const std::string reference_var_name = dre -> getDecl()->getNameAsString();
                           newcontent.append("decl ").append(varname).append("; transfer ").append(reference_var_name).append(" ").append(varname).append(";");
                       }
                    }
                  }
                  if(varType->isPointerType()){   //指针类型
                      if(init_stmt == "NULL" || init_stmt == "0") //初始化为NULL或0的指针 int *p = NULL;
                          newcontent.append("decl ").append(varname).append(";").append("transfer #uninit ").append(varname);
                      if(call_fun){ //函数调用的指针声明
                          std::string memory_call_name = call_fun -> getDirectCallee() -> getNameAsString();
                          if(memory_call_name == "malloc" || memory_call_name == "calloc" 
                                      ||memory_call_name == "realloc" ) //带有分配函数的指针声明int *p = malloc(sizeof(int));
                               newcontent.append("decl ").append(varname).append("; transfer newResource() ").append(varname).append(";     //").append(memory_call_name)
                                      .append(" function");
                          else{ //自定义函数调用的指针声明  int *ab = func(&m);
                               CharSourceRange CSR = CharSourceRange(call_fun->getSourceRange(), true);
                               const std::string call_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
                               unsigned int argnumber = call_fun->getNumArgs();
                               string call_name = call_fun -> getDirectCallee()->getNameInfo().getAsString();
                               if(argnumber == 0)
                                  newcontent.append("decl ").append(varname).append("; call ").append(call_stmt).append("; transfer ").append(call_stmt).append(" ").append(varname).append(";");
                               else{
                                  string parameters = parameters_change(call_fun, sourceManager_, lopt);
                                  newcontent.append("decl ").append(varname).append("; call ").append(call_name).append("(").append(parameters).append(")")
                                      .append("; transfer ").append(call_name).append("(").append(parameters).append(")").append(" ").append(varname).append(";");
                               }
                          }
                      }
                      if(dre)    //带有非NULL的初始值的指针声明 Point *P3 = pm; 
                         newcontent.append("decl ").append(varname).append("; transfer ").append(dre->getDecl()->getNameAsString()).append(" ").append(varname).append(";");
                      if(uo){   //带有&符号的指针声明 
                         Expr *sub_uo = uo -> getSubExpr();
                         CharSourceRange Op_CSR = CharSourceRange(SourceRange(uo->getBeginLoc(),sub_uo->getBeginLoc().getLocWithOffset(-1)), true);
                         const std::string Op_name = Lexer::getSourceText(Op_CSR, sourceManager_, lopt).str();
                         const DeclRefExpr *uo_sub_dre = dyn_cast<DeclRefExpr>(sub_uo -> IgnoreParenCasts());
                         const ArraySubscriptExpr *uo_sub_array = dyn_cast<ArraySubscriptExpr>(sub_uo -> IgnoreParenCasts());
                         if(Op_name == "&"){   
                           if(uo_sub_dre){    //int *p = &a;
                            if(read_mut_ref_var(varname))
                                newcontent.append("decl ").append(varname).append("; ").append(varname).append(" mborrow ").append(uo_sub_dre->getDecl()->getNameAsString()).append(";");
                            else
                                newcontent.append("decl ").append(varname).append("; ").append(varname).append(" borrow ").append(uo_sub_dre->getDecl()->getNameAsString()).append(";");
                         }
                           if(uo_sub_array){     //int *gm = &k[0];
                            const clang::DeclRefExpr *array_DR = clang::dyn_cast<clang::DeclRefExpr>(uo_sub_array->getBase()->IgnoreImpCasts());
                            string array_name = array_DR -> getDecl()->getNameAsString(); 
                            if(read_mut_ref_var(varname))
                                newcontent.append("decl ").append(varname).append("; ").append(varname).append(" mborrow ").append(array_name).append(";");
                            else
                                newcontent.append("decl ").append(varname).append("; ").append(varname).append(" borrow ").append(array_name).append(";");
                         }
                       }
                        if(Op_name == "&"){ 
                           if(uo_sub_dre)   //int *p = *a;
                              newcontent.append("decl ").append(varname).append("; ").append("transfer *")
                                           .append(uo_sub_dre->getDecl()->getNameAsString()).append(" *").append(varname).append(";");
                           if(uo_sub_array){ //int *p = *a[0];
                              const clang::DeclRefExpr *array_DR = clang::dyn_cast<clang::DeclRefExpr>(uo_sub_array->getBase()->IgnoreImpCasts());
                              string array_name = array_DR -> getDecl()->getNameAsString(); 
                              const Expr *idx = uo_sub_array -> getIdx();
                              CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                              const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str();
                              newcontent.append("decl ").append(varname).append("; ").append("transfer *(")
                                           .append(array_name).append(".").append(idx_value).append(") ").append(varname).append(";");
                           }
                        }
                      }
                  }
               }
               else{    //不具有初始值的变量声明
                 if(varType->isPointerType())
                    newcontent.append("decl ").append(varname).append(";");
                 else
                    newcontent.append("decl ").append(varname).append(";");
               }
            }
            if(it != stmt->decl_end())
              newcontent.append("\n");
          }
           if(flag_var == "true"){
               Replacement repl(sourceManager_, CharSourceRange(range,true), newcontent);
               repl.apply(rewriter_);
               //测试
               outs() << vardecl_stmt + " success0";
               cout << endl;
             }
      }
      else{    //变量单行声明
         const Decl *singleDecl = stmt->getSingleDecl();
         const VarDecl *vardecl = dyn_cast<VarDecl>(singleDecl);
         if(vardecl != nullptr && sourceManager_.isWrittenInMainFile(vardecl->getLocation())){
            std::string varname = return_varname(vardecl);   //变量名
            SourceRange range = return_endloc(vardecl, sourceManager_, lopt);  //变量声明语句的覆盖范围
            CharSourceRange CSR = CharSourceRange(vardecl->getSourceRange(), true);
            const std::string vardecl_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
            const clang::Type *varType = vardecl->getType().getTypePtr();
            if(vardecl -> hasInit()){  //具有初始值的变量声明
                 const Expr *init = vardecl -> getAnyInitializer();
                 CharSourceRange expr_CSR = CharSourceRange(init->getSourceRange(), true);
                 const std::string init_stmt = Lexer::getSourceText(expr_CSR, sourceManager_, lopt).str();
                 const DeclRefExpr *dre = dyn_cast<DeclRefExpr>(init -> IgnoreParenCasts());
                 const CallExpr *call_fun = dyn_cast<CallExpr>(init -> IgnoreParenCasts());
                 const UnaryOperator *uo = dyn_cast<UnaryOperator>(init -> IgnoreParenCasts());
                 const MemberExpr *member_decl = dyn_cast<MemberExpr>(init -> IgnoreParenCasts());
                 const ArraySubscriptExpr *array_decl= dyn_cast<ArraySubscriptExpr>(init -> IgnoreParenCasts());
                 if(varType->isBuiltinType() && !find_in_for_var(sourceManager_.getSpellingLineNumber(vardecl->getBeginLoc()))){  //内置类型
                   if(dre){   //int a = b；
                         string old_owner = dre -> getDecl() -> getNameAsString();
                         newcontent = "decl " + varname + "; transfer " + old_owner + " " + varname + ";";
                         do_replacement(sourceManager_, range, newcontent); 
                         //测试
                         outs() << vardecl_stmt + "; success1";
                         cout << endl; 
                    }
                    else if(call_fun){   //int x = y(m);
                         CharSourceRange CSR = CharSourceRange(call_fun->getSourceRange(), true);
                         const std::string call_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
                         unsigned int argnumber = call_fun->getNumArgs();
                         string call_name = call_fun -> getDirectCallee()->getNameInfo().getAsString();
                         if(argnumber == 0)
                              newcontent = "decl " + varname + "; call " + call_stmt + "; transfer newResource(copy) " + varname + ";";
                         else{
                                  string parameters = parameters_change(call_fun, sourceManager_, lopt);
                                  newcontent = "decl " + varname + "; call " + call_name + "(" + parameters + ")" + "; transfer newResource(copy) " 
                                         + varname + ";";
                             }  
                         do_replacement(sourceManager_, range, newcontent); 
                         //测试
                         outs() << vardecl_stmt + "; success2";
                         cout << endl;
                    }
                    else if(uo){    
                      const Expr *init_star = uo -> getSubExpr();
                      const DeclRefExpr *uo_dre = dyn_cast<DeclRefExpr>(init_star -> IgnoreParenImpCasts());
                      const MemberExpr *uo_member = dyn_cast<MemberExpr>(init_star -> IgnoreParenImpCasts());
                      const ArraySubscriptExpr *uo_array= dyn_cast<ArraySubscriptExpr>(init_star -> IgnoreParenImpCasts());
                      if(uo_dre){   //int x = *p;
                         newcontent = "decl " + varname + "; transfer *" + uo_dre->getDecl()->getNameAsString() + " " + varname + ";";
                         do_replacement(sourceManager_, range, newcontent);
                      }
                      if(uo_member){        //int x = *(p->y);
                         int flag = 8;
                         newcontent = member_access_content(uo_member, flag, varname);
                         do_replacement(sourceManager_, range, newcontent);
                         //测试
                         outs() << vardecl_stmt + "; success40";
                         cout << endl;
                      }
                      if(uo_array){         //int x = *(k[0]);
                         int flag = 8;
                         newcontent = arraySubscript_content(uo_array, sourceManager_, lopt, flag, varname);
                         do_replacement(sourceManager_, range, newcontent);
                         //测试
                         outs() << vardecl_stmt + "; success41";
                         cout << endl;
                      }
                    }
                    else if(member_decl){      //int x = p->y;
                        int flag = 9;
                        newcontent = member_access_content(member_decl, flag, varname);
                        do_replacement(sourceManager_, range, newcontent);
                         //测试
                        outs() << vardecl_stmt + "; success41";
                        cout << endl;
                    }
                    else if(array_decl){           //int x = k[0];
                         int flag = 9;
                         newcontent = arraySubscript_content(array_decl, sourceManager_, lopt, flag, varname);
                         do_replacement(sourceManager_, range, newcontent);
                         //测试
                         outs() << vardecl_stmt + "; success42";
                         cout << endl;
                    }
                        else {   // int x = 10;   int = 100 + m; 等其他
                             newcontent = "decl " + varname + "; transfer newResource(copy) " + varname + ";";
                             do_replacement(sourceManager_, range, newcontent); 
                             //测试
                             outs() << vardecl_stmt + "; success3";
                             cout << endl;
                        }
                 }
                 const InitListExpr *initializer_list = dyn_cast<InitListExpr>(init);  //数组类型
                 if(varType->isArrayType() && initializer_list){  //int f[10]={ 0,1,2,3,4,5,6,7,8,9 };      
                    newcontent = "decl " + varname + "; transfer newResource(copy) " + varname + ";";
                    do_replacement(sourceManager_, range, newcontent); 
                    //测试
                    outs() << vardecl_stmt + "; success4";
                    cout << endl;
                 }
                 if(varType->isStructureType()){    //结构体类型
                    if(initializer_list) {    //带有初始列表值的结构体声明 Point p1= {0, 1}
                           newcontent = "decl " + varname + "; transfer newResource(copy) " + varname + ";";
                           do_replacement(sourceManager_, range, newcontent);
                           //测试
                           outs() << vardecl_stmt + "; success5";
                           cout << endl;
                    }
                    if(dre){    //带有初始值变量的结构体声明 Point p2 = p1;
                        const std::string reference_var_name = dre -> getDecl()->getNameAsString();
                        newcontent = "decl " + varname + "; transfer" + " " + reference_var_name + " "+ varname + ";";
                        do_replacement(sourceManager_, range, newcontent);
                        //测试
                        outs() << vardecl_stmt + "; success6";
                        cout << endl; 
                    }
                 }
                 if(varType->isUnionType()){    //联合体类型，一般不对联合体进行集体赋值，因此不考虑联合体的列表赋值
                    if(dre){    //带有初始值变量的联合体声明 number n2 = n1;
                        const std::string reference_var_name = dre -> getDecl()->getNameAsString();
                        newcontent = "decl " + varname + "; transfer" + " " + reference_var_name + " "+ varname + ";";
                        do_replacement(sourceManager_, range, newcontent);
                        //测试
                        outs() << vardecl_stmt + "; success7";
                        cout << endl; 
                    }
                 }
                 if(varType->isEnumeralType()){    //枚举类型
                    if(dre){
                       const ValueDecl *val_decl = dre -> getDecl();
                       const EnumConstantDecl *enum_constant = dyn_cast<EnumConstantDecl>(val_decl);
                       if(enum_constant){ //带有初始值的枚举变量声明 today = TUE; 
                           newcontent = "decl " + varname + "; transfer newResource(copy) " + varname + ";";
                           do_replacement(sourceManager_, range, newcontent);
                           //测试
                           outs() << vardecl_stmt + "; success8";
                           cout << endl;
                       }
                       else{      //带有初始值变量的枚举声明 DAY terrowday = today;
                           const std::string reference_var_name = dre -> getDecl()->getNameAsString();
                           newcontent = "decl " + varname + "; " + "transfer" + " " + reference_var_name + " "+ varname + ";";
                           do_replacement(sourceManager_, range, newcontent);
                           //测试
                           outs() << vardecl_stmt + "; success9";
                           cout << endl; 
                       }
                    }
                 }
                 if(varType->isPointerType()){   //指针类型
                    if(init_stmt == "NULL" || init_stmt == "0"){    //初始化为NULL或0的指针
                      newcontent = "decl " + varname + "; transfer #uninit " + varname + ";";
                      do_replacement(sourceManager_, range, newcontent);
                      //测试
                      outs() << vardecl_stmt + "; success10";
                      cout << endl;
                    }
                    if(call_fun){     //函数调用的指针声明
                        std::string memory_call_name = call_fun -> getDirectCallee() -> getNameAsString();
                        if(memory_call_name == "malloc" || memory_call_name == "calloc" 
                           ||memory_call_name == "realloc" ){ //带有分配函数的指针声明(malloc，calloc，realloc) int *p = malloc(sizeof(int));
                          newcontent = "decl " + varname + "; transfer newResource() " + varname + ";    //" + memory_call_name + " function";
                          do_replacement(sourceManager_, range, newcontent); 
                          //测试
                          outs() << vardecl_stmt + "; success11";
                          cout << endl;        
                        }
                        else{    //自定义函数调用的指针声明  int *ab = func(&m);  
                          CharSourceRange CSR = CharSourceRange(call_fun->getSourceRange(), true);
                          const std::string call_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
                          unsigned int argnumber = call_fun->getNumArgs();
                          string call_name = call_fun -> getDirectCallee()->getNameInfo().getAsString();
                          if(argnumber == 0)
                              newcontent = "decl " + varname + "; call " + call_stmt + "; " + "transfer newResource() " + varname + ";";
                          else{
                                  string parameters = parameters_change(call_fun, sourceManager_, lopt);
                                  newcontent = "decl " + varname + "; call " + call_name + "(" + parameters +")" + "; transfer newResource() " 
                                         + varname + ";";
                              } 
                         do_replacement(sourceManager_, range, newcontent);
                         //测试
                         outs() << vardecl_stmt + "; success20";
                         cout << endl; 
                        }
                    }
                    if(dre){      //带有非NULL的初始值的指针声明 Point *P3 = pm;      
                          const std::string reference_var_name = dre -> getDecl()->getNameAsString();
                          newcontent = "decl " + varname + "; transfer " + reference_var_name + " "+ varname + ";";
                          do_replacement(sourceManager_, range, newcontent); 
                          //测试
                          outs() << vardecl_stmt + "; success21";
                          cout << endl; 
                        }
                    if(member_decl){       //int *c = p->y;
                         int flag = 11;
                         newcontent = member_access_content(member_decl, flag, varname);
                         do_replacement(sourceManager_, range, newcontent);
                         //测试
                         outs() << vardecl_stmt + "; success44";
                         cout << endl;
                    }
                    if(array_decl){       //int *c = k[0];
                         int flag = 11;
                         newcontent = arraySubscript_content(array_decl, sourceManager_, lopt, flag, varname);
                         do_replacement(sourceManager_, range, newcontent);
                         //测试
                         outs() << vardecl_stmt + "; success45";
                         cout << endl;
                    }
                    if(uo){   //带有&或*符号的指针声明     
                         Expr *sub_uo = uo -> getSubExpr();
                         CharSourceRange Op_CSR = CharSourceRange(SourceRange(uo->getBeginLoc(),sub_uo->getBeginLoc().getLocWithOffset(-1)), true);
                         const std::string Op_name = Lexer::getSourceText(Op_CSR, sourceManager_, lopt).str();
                         const DeclRefExpr *uo_sub_dre = dyn_cast<DeclRefExpr>(sub_uo -> IgnoreParenCasts());
                         const MemberExpr *uo_sub_member = dyn_cast<MemberExpr>(sub_uo -> IgnoreParenCasts());
                         const ArraySubscriptExpr *uo_sub_array = dyn_cast<ArraySubscriptExpr>(sub_uo -> IgnoreParenCasts());
                         if(Op_name == "&"){
                          if(uo_sub_dre){  //int *p = &a;
                            if(read_mut_ref_var(varname))
                              newcontent = "decl " + varname + "; " + varname + " mborrow " + uo_sub_dre->getDecl()->getNameAsString() + ";";
                            else 
                              newcontent = "decl " + varname + "; " + varname + " borrow " + uo_sub_dre->getDecl()->getNameAsString() + ";";
                          }
                          if(uo_sub_member){         //int *p = &(p->x);
                              int flag = 10;
                              string other;
                              if(read_mut_ref_var(varname))
                                 other = "decl " + varname + "; " + varname + " mborrow ";
                              else
                                 other = "decl " + varname + "; " + varname + " borrow ";
                              newcontent = member_access_content(uo_sub_member, flag, other);
                          }
                          if(uo_sub_array){       //int *gm = &k[0];
                                int flag = 10;
                                string other;
                                if(read_mut_ref_var(varname))
                                   other = "decl " + varname + "; " + varname + " mborrow ";
                                else
                                   other = "decl " + varname + "; " + varname + " borrow ";
                                newcontent = arraySubscript_content(uo_sub_array, sourceManager_, lopt, flag, other);
                          }
                         }
                        if(Op_name == "*"){
                           if(uo_sub_dre)          //int *b = *c;
                             newcontent = "decl " + varname + "; transfer " + "*" + uo_sub_dre->getDecl()->getNameAsString() + " *" + varname + ";";
                        }
                         
                        do_replacement(sourceManager_, range, newcontent);
                         //测试
                        outs() << vardecl_stmt + "; success43";
                        cout << endl;
                  }
                 }
            }
            else{   //不具有初始值的变量声明
                 if(varType->isPointerType())
                    newcontent = "decl " + varname + ";";
                 else
                    newcontent = "decl " + varname + ";";
                 do_replacement(sourceManager_, range, newcontent); 
                 //测试
                 outs() << vardecl_stmt + "; success38";
                 cout << endl; 
            }
         }
      }
    }

    //47static全局变量声明
    const VarDecl *static_var = Result.Nodes.getNodeAs<VarDecl>("static_var"); 
    if(static_var != nullptr){
      const clang::Type *varType = static_var->getType().getTypePtr();
      SourceRange range = return_endloc(static_var, sourceManager_, lopt);  //变量声明语句的覆盖范围
      CharSourceRange CSR = CharSourceRange(static_var->getSourceRange(), true);
      string varname = return_varname(static_var);
      string newcontent;
      if(static_var -> hasInit()){
          if(!varType->isPointerType())
             newcontent = "decl " + varname + "; transfer newResource(copy) " + varname + ";";
          //else
            // newcontent = "decl " + varname + ";" + " transfer newResource() " + varname;
      }
      else{   
          if(varType->isPointerType())
             newcontent = "decl " + varname + ";";
          else
             newcontent = "decl " + varname + ";";
      }
      do_replacement(sourceManager_, range, newcontent);
      //测试
      const std::string vardecl_stmt = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
      outs() << vardecl_stmt + "; success47";
      cout << endl;
    }

    //48复杂结构类型声明(struct、enumeration、union),有typedet
    const TypedefDecl *typedef_complex_data = Result.Nodes.getNodeAs<TypedefDecl>("typedef_complex_data");
    if(typedef_complex_data != nullptr){
        const clang::Type *type = typedef_complex_data -> getTypeSourceInfo() ->getType().getTypePtr();
        if(type->isRecordType() || type->isEnumeralType()){
           //cout<<"//"<< endl;
           SourceRange decl_range = typedef_complex_data->getSourceRange();
           SourceLocation decl_begin = decl_range.getBegin();
           SourceLocation decl_end = clang::Lexer::getLocForEndOfToken(decl_range.getEnd(), 0, sourceManager_, lopt).getLocWithOffset(2);;
           clang::Rewriter::RewriteOptions RO;
           RO.IncludeInsertsAtBeginOfRange = true;
           RO.IncludeInsertsAtEndOfRange = true;
           RO.RemoveLineIfEmpty = true;
           rewriter_.RemoveText(CharSourceRange(SourceRange(decl_begin, decl_end), false), RO);  //delete
        //测试
        const std::string vardecl_stmt = Lexer::getSourceText(CharSourceRange(decl_range, true), sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success48";
        cout << endl;
      }
    }
/*---------------------------------------------------------赋值语句转换------------------------------------------------*/
    //12带copy的transfer的action。transfer newResource(copy) a;   
    const Stmt *assignment_type1 = Result.Nodes.getNodeAs<Stmt>("assignment_type1");
    const Stmt *LHS_type1 = Result.Nodes.getNodeAs<Stmt>("LHS_type1");
    if (assignment_type1 != nullptr && LHS_type1 != nullptr){

           CharSourceRange CSR = CharSourceRange(LHS_type1->getSourceRange(), true);
           const std::string onwer_name = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
           CharSourceRange CSR2 = CharSourceRange(assignment_type1->getSourceRange(), true);
           const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
           const ArraySubscriptExpr *array_element = dyn_cast<ArraySubscriptExpr>(LHS_type1);
           const UnaryOperator *uo_array_element = dyn_cast<UnaryOperator>(LHS_type1);
           const MemberExpr *struct_union_member = dyn_cast<MemberExpr>(LHS_type1);
           int i;
           if(!find_in_for_var(sourceManager_.getSpellingLineNumber(assignment_type1->getBeginLoc()))) 
           {
             //cout << "|||" << endl;
             SourceRange range = return_assign_endloc(assignment_type1, sourceManager_, lopt);
             std::string newcontent = "transfer newResource(copy) " + onwer_name + ";";
             int flag = 0;
             const clang::BinaryOperator *bo = dyn_cast<clang::BinaryOperator>(assignment_type1);
             const Expr *RHS_expr = bo -> getRHS();
             CharSourceRange null_CSR = CharSourceRange(RHS_expr->getSourceRange(), true);
             const std::string null_name = Lexer::getSourceText(null_CSR, sourceManager_, lopt).str();
             if(null_name == "NULL"){    //date = NULL; delete
                //cout << "**" << vardecl_stmt << "**" << endl;
                newcontent = "transfer #uninit " + onwer_name + ";";
              }
             //若是数组元素赋值
             else if(array_element)
             {   //q[2]=3;
                const clang::DeclRefExpr *array_DR = clang::dyn_cast<clang::DeclRefExpr>(array_element->getBase()->IgnoreParenImpCasts());
                const clang::UnaryOperator *UO= clang::dyn_cast<clang::UnaryOperator>(array_element->getBase()->IgnoreParenImpCasts());
                if(array_DR)
                {
                  const VarDecl *array_decl = clang::dyn_cast<VarDecl>(array_DR->getDecl());
                  if(find_heap_var(return_varname(array_decl)))   //char * data; data = (char *)malloc(100*sizeof(char));data[100-1] = '\0';
                     flag = 12;
                  else
                     flag = 1;
                  newcontent = arraySubscript_content(array_element, sourceManager_, lopt, flag, "");
                }
                else if(UO)
                {    //(*data)[i] = 5;
                  //cout << "//" << endl;
                  Expr *star_aub_exp = UO -> getSubExpr();
                  const clang::DeclRefExpr *star_expr_simple = clang::dyn_cast<clang::DeclRefExpr>(star_aub_exp->IgnoreParenImpCasts());
                  CharSourceRange Op_CSR = CharSourceRange(SourceRange(UO->getBeginLoc(),star_aub_exp->getBeginLoc().getLocWithOffset(-1)), true);
                  const std::string Op_name = Lexer::getSourceText(Op_CSR, sourceManager_, lopt).str();
                  string array_name = star_expr_simple->getDecl()->getNameAsString();
                  const Expr *idx = array_element -> getIdx();                            //数组索引
                  CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                  const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str();
                  if(star_expr_simple && Op_name == "*")
                  {
                    if(find_heap_var(array_name))
                      newcontent = "transfer newResource() (*" + array_name + ")." + idx_value + ";   // array element assignment";
                    
                    else
                       newcontent = "transfer newResource(copy) (*" + array_name + ")." + idx_value + ";  // array element assignment";
                  }
                }
                else{
                  cout << vardecl_stmt << " can not support! --12" << endl;
                  exit(0);
                }
             }
             //是否为成员变量赋值
             else if(struct_union_member)    
             {
              Expr *right_exp = struct_union_member->getBase();
              const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(right_exp->IgnoreParenImpCasts());
              const ArraySubscriptExpr *sub_array = clang::dyn_cast<ArraySubscriptExpr>(right_exp->IgnoreParenImpCasts());
              if(struct_union_DR){    //p4.x = 10; 或p3->x = 19
                   flag = 1;
                   newcontent = member_access_content(struct_union_member, flag, "");
              }
              else if(sub_array){      // data[i].intOne = 1;
                   const clang::DeclRefExpr *array_DR = clang::dyn_cast<clang::DeclRefExpr>(sub_array->getBase()->IgnoreImpCasts());
                   string array_name = array_DR -> getDecl()->getNameAsString();  //数组名
                   const Expr *idx = sub_array -> getIdx();                            //数组索引
                   CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                   const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str(); 
                   const clang::Type *type = array_DR -> getDecl() -> getType().getTypePtr();
                   if(type->isPointerType()){
                      const clang::PointerType *po_str = clang::dyn_cast<clang::PointerType>(type);
                      RecordDecl *ref_str = po_str->getPointeeType().getTypePtr()->getAsRecordDecl();
                      string member_name = struct_union_member->getMemberDecl()->getNameAsString(); //成员名
                      if(ref_str){
                         //cout << "//" << endl;
                         clang::RecordDecl::field_iterator jt;
                         i = 0;
                         for(jt = ref_str->field_begin(); jt != ref_str->field_end(); ++jt){
                              if(jt->getNameAsString() == member_name)  break;
                              else
                                    ++i;
                        }
                        if(find_heap_var(array_name))
                          newcontent = "transfer newResource() " + array_name + "." + idx_value + "." + to_string(i);
                        else
                          newcontent = "transfer newResource(copy)" + array_name + "." + idx_value + "." + to_string(i);
                      }
                   }
              }
              
             }
             //是否为指针变量赋值
             else if(uo_array_element)
             {
              const ArraySubscriptExpr *sub_array_element = dyn_cast<ArraySubscriptExpr>(uo_array_element->getSubExpr()->IgnoreParenImpCasts());
              const MemberExpr *sub_member_exp = dyn_cast<MemberExpr>(uo_array_element->getSubExpr()->IgnoreParenImpCasts());
              flag = 2;
              if(sub_array_element)  //若为指针数组    //*(myint[2]) = 4;
                  newcontent = arraySubscript_content(sub_array_element, sourceManager_, lopt, flag, "");
              if(sub_member_exp)     //是否为成员指针变量赋值   //*(p3->y) = 20;
                  newcontent = member_access_content(sub_member_exp, flag, "");  
             }
    /*         else
             {
               cout << vardecl_stmt << " can not support! --12" << endl;
               exit(0);
             }*/
           do_replacement(sourceManager_, range, newcontent);
           //测试
           outs() << vardecl_stmt + "; success12 ";
           cout << endl;  
          }
       }

    //13赋值语句左边为指针、结构体成员访问、数组，右值为相应类型的变量的action。transfer p1 p2;
    const Stmt *assignment_type2 = Result.Nodes.getNodeAs<Stmt>("assignment_type2");
    const Stmt *LHS_type2 = Result.Nodes.getNodeAs<Stmt>("LHS_type2");
    const VarDecl *RHS_type2 = Result.Nodes.getNodeAs<VarDecl>("RHS_type2");

       if (assignment_type2 != nullptr && LHS_type2 != nullptr && RHS_type2 != nullptr){
           //cout << "///" << endl;
           std::string old_owner_name = return_varname(RHS_type2);
           CharSourceRange CSR = CharSourceRange(LHS_type2->getSourceRange(), true);
           const std::string new_onwer_name = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
           SourceRange range = return_assign_endloc(assignment_type2, sourceManager_, lopt);
           std::string newcontent = "transfer " + old_owner_name + " " + new_onwer_name + ";";   //p4 = p1;  
           const ArraySubscriptExpr *array_element = dyn_cast<ArraySubscriptExpr>(LHS_type2); 
           const MemberExpr *struct_union_member = dyn_cast<MemberExpr>(LHS_type2);
           CharSourceRange CSR2 = CharSourceRange(assignment_type2->getSourceRange(), true);
           const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
           //是否为数组元素赋值
           if(array_element){   //q[2]=sm;
                const clang::DeclRefExpr *array_DR = clang::dyn_cast<clang::DeclRefExpr>(array_element->getBase()->IgnoreParenImpCasts());
                const clang::UnaryOperator *UO= clang::dyn_cast<clang::UnaryOperator>(array_element->getBase()->IgnoreParenImpCasts());
                if(array_DR){
                    int flag = 3;
                    newcontent = arraySubscript_content(array_element, sourceManager_, lopt, flag, old_owner_name);
                }
                else if(UO){    //(*data)[i] = i;
                  Expr *star_aub_exp = UO -> getSubExpr();
                  const clang::DeclRefExpr *star_expr_simple = clang::dyn_cast<clang::DeclRefExpr>(star_aub_exp->IgnoreParenImpCasts());
                  CharSourceRange Op_CSR = CharSourceRange(SourceRange(UO->getBeginLoc(),star_aub_exp->getBeginLoc().getLocWithOffset(-1)), true);
                  const std::string Op_name = Lexer::getSourceText(Op_CSR, sourceManager_, lopt).str();
                  string array_name = star_expr_simple->getDecl()->getNameAsString();
                  const Expr *idx = array_element -> getIdx();                            //数组索引
                  CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                  const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str();
                  if(star_expr_simple && Op_name == "*")
                    newcontent = "transfer " + old_owner_name + " (*" + array_name + ")." + idx_value + ";";
                }
                else{
                  cout << vardecl_stmt << " can not support! --13" << endl;
                  exit(0);
                }
           }
           //是否为成员变量赋值
           else if(struct_union_member)    //p4.x = fm; 或p3->x = fm
           {
              const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(struct_union_member->getBase()->IgnoreImpCasts());
              if(struct_union_DR){
                    int flag = 3;
                    newcontent = member_access_content(struct_union_member, flag, old_owner_name);
              }
             else{
               //cout << "//" << endl;
               cout << vardecl_stmt << " can not support! --13" << endl;
               exit(0);
                }
           }
           do_replacement(sourceManager_, range, newcontent);
           //测试
           outs() << vardecl_stmt + "; success13";
           cout << endl; 
       }

    //50赋值语句右值为指针解引用或是数组的action。a = *c,  transfer *c a;
    const Stmt *assignment_type4 = Result.Nodes.getNodeAs<Stmt>("assignment_type4");
    const Expr *LHS_type4 = Result.Nodes.getNodeAs<Expr>("LHS_type4");
    const UnaryOperator *pointer_refer = Result.Nodes.getNodeAs<UnaryOperator>("pointer_refer");
    const MemberExpr *member_assign = Result.Nodes.getNodeAs<MemberExpr>("member_assign");
    const ArraySubscriptExpr *array_assign = Result.Nodes.getNodeAs<ArraySubscriptExpr>("array_assign");
    if (assignment_type4 != nullptr){
        CharSourceRange CSR = CharSourceRange(LHS_type4->getSourceRange(), true);
        const std::string new_owner_name = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
        SourceRange range = return_assign_endloc(assignment_type4, sourceManager_, lopt);
        string newcontent;
        if(pointer_refer){    //*
           const Expr *star_expr = pointer_refer->getSubExpr();
           const ArraySubscriptExpr *sub_array_element = dyn_cast<ArraySubscriptExpr>(star_expr->IgnoreParenImpCasts());
           const MemberExpr *sub_member_exp = dyn_cast<MemberExpr>(star_expr->IgnoreParenImpCasts());
           const DeclRefExpr *sub_declref = dyn_cast<DeclRefExpr>(star_expr->IgnoreParenImpCasts());
           if(sub_declref)      //a = *c
              newcontent = "transfer *" + sub_declref->getDecl()->getNameAsString() + " " + new_owner_name + ";";
           if(sub_member_exp){  //a = *(p3->x)
              int flag = 7;
              newcontent = member_access_content(sub_member_exp, flag, new_owner_name);
           }
           if(sub_array_element){   //a = *myint[2]
              int flag = 7;
              newcontent = arraySubscript_content(sub_array_element, sourceManager_, lopt, flag, new_owner_name);
           }
        }
        if(member_assign){    //a = p3->x
          int flag = 6;
          newcontent = member_access_content(member_assign, flag, new_owner_name);
        }
        if(array_assign){     //a = k[1]
          int flag = 6;
          const ArraySubscriptExpr *sub_array = dyn_cast<ArraySubscriptExpr>(LHS_type4->IgnoreParenImpCasts());
          //const MemberExpr *sub_member = dyn_cast<MemberExpr>(LHS_type4->IgnoreParenImpCasts());
          if(sub_array){      //l[j] = k[i]
              const DeclRefExpr *array_DR = dyn_cast<DeclRefExpr>(sub_array -> getBase()->IgnoreImpCasts());
              string array_name = array_DR -> getDecl()->getNameAsString();  //数组名
              const Expr *idx = sub_array -> getIdx();                            //数组索引
              CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
              const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str(); 
              string owner_array_name = array_name + "." + idx_value;
              newcontent = arraySubscript_content(array_assign, sourceManager_, lopt, flag, owner_array_name);
            }
           else
              newcontent = arraySubscript_content(array_assign, sourceManager_, lopt, flag, new_owner_name);
        }
        do_replacement(sourceManager_, range, newcontent);
        //测试
        CharSourceRange CSR2 = CharSourceRange(assignment_type4->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success50";
        cout << endl; 
    }


    //14赋值语句右值为malloc等内存分配函数的action。transfer newResource() pm; 
    const Stmt *assignment_type3 = Result.Nodes.getNodeAs<Stmt>("assignment_type3");
    const Stmt *LHS_type3 = Result.Nodes.getNodeAs<Stmt>("LHS_type3");
    const FunctionDecl *memory_name = Result.Nodes.getNodeAs<FunctionDecl>("memory_name");
    if (assignment_type3 != nullptr && LHS_type3 != nullptr && memory_name !=nullptr){

           CharSourceRange CSR = CharSourceRange(LHS_type3->getSourceRange(), true);
           const std::string onwer_name = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
           SourceRange range = return_assign_endloc(assignment_type3, sourceManager_, lopt);
           string function_name = memory_name->getNameInfo().getAsString();
           std::string newcontent = "transfer newResource() " + onwer_name + ";" + "      //" + function_name + " function";
           //是否为数组元素赋值
           const ArraySubscriptExpr *array_element = dyn_cast<ArraySubscriptExpr>(LHS_type3);
           if(array_element){   //q[2]=malloc(sizeof(int));
                int flag = 4;
                newcontent = arraySubscript_content(array_element, sourceManager_, lopt, flag, function_name);
           }
           //是否为成员变量赋值
           const MemberExpr *struct_union_member = dyn_cast<MemberExpr>(LHS_type3);
           if(struct_union_member)    //p3->y =(int *)malloc(sizeof(int));    
           {
              const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(struct_union_member->getBase()->IgnoreImpCasts());
              if(struct_union_DR){
                    int flag = 4;
                    newcontent = member_access_content(struct_union_member, flag, function_name);
              }
           }
           do_replacement(sourceManager_, range, newcontent);
           //测试
           CharSourceRange CSR2 = CharSourceRange(assignment_type3->getSourceRange(), true);
           const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
           outs() << vardecl_stmt + "; success14";
           cout << endl; 
       }
    
    //15带有borrow语义的赋值: 引用的变量是除数组、结构体成员以外的变量
    const Stmt *borrow_assign = Result.Nodes.getNodeAs<Stmt>("borrow_assign");
    const VarDecl *LHS_ref_var = Result.Nodes.getNodeAs<VarDecl>("LHS_ref_var");
    const VarDecl *RHS_ref_var = Result.Nodes.getNodeAs<VarDecl>("RHS_ref_var");
    const MemberExpr *member_expr = Result.Nodes.getNodeAs<MemberExpr>("member_expr");
    const ArraySubscriptExpr *array_subscript_expr = Result.Nodes.getNodeAs<ArraySubscriptExpr>("array_subscript_expr");
    if(borrow_assign != nullptr  && RHS_ref_var != nullptr){
        std::string newcontent;
        SourceRange range = return_assign_endloc(borrow_assign, sourceManager_, lopt);
        if(LHS_ref_var != nullptr){   //处理形如b = &a; 的赋值语句
          
          string varname = return_varname(LHS_ref_var);
          if(read_mut_ref_var(varname))     //变量为可变引用
             newcontent = varname + " mborrow " + return_varname(RHS_ref_var) + ";"; 
          else
             newcontent = varname + " borrow " + return_varname(RHS_ref_var) + ";"; 
        }

        if(array_subscript_expr != nullptr){   //处理形如myint[2] = &a的赋值语句
           int flag = 5;
           string mut_flag;
           CharSourceRange arraySubscriptExpr_CSR = CharSourceRange(array_subscript_expr->getSourceRange(), true);
           const std::string varname = Lexer::getSourceText(arraySubscriptExpr_CSR, sourceManager_, lopt).str();
           if(read_mut_ref_var(varname))    //变量为可变引用
              mut_flag = " mborrow " + return_varname(RHS_ref_var) + ";";
           else
              mut_flag = " borrow " + return_varname(RHS_ref_var) + ";";
           newcontent = arraySubscript_content(array_subscript_expr, sourceManager_, lopt, flag, mut_flag);
           
        }

        if(member_expr != nullptr){ //处理形如p3->y = &a;的赋值语句
          int flag = 5;
          string mut_flag;
          CharSourceRange CSR = CharSourceRange(member_expr->getSourceRange(), true);
          const std::string varname = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
          if(read_mut_ref_var(varname))   //变量为可变引用
             mut_flag = " mborrow " + return_varname(RHS_ref_var) + ";";
          else
             mut_flag = " borrow " + return_varname(RHS_ref_var) + ";";
          newcontent = member_access_content(member_expr, flag, mut_flag);
         }
           do_replacement(sourceManager_, range, newcontent); 
          //测试
           CharSourceRange CSR2 = CharSourceRange(borrow_assign->getSourceRange(), true);
           const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
           outs() << vardecl_stmt + "; success15";
           cout << endl;       
      }

    //16带有borrow语义的赋值: 引用的变量是数组
    const Stmt *borrow_array = Result.Nodes.getNodeAs<Stmt>("borrow_array");
    const VarDecl *array_ref_var = Result.Nodes.getNodeAs<VarDecl>("array_ref_var");
    const ArraySubscriptExpr *array_exp = Result.Nodes.getNodeAs<ArraySubscriptExpr>("array_exp");
    const MemberExpr *array_member_expr = Result.Nodes.getNodeAs<MemberExpr>("array_member_expr");
    const ArraySubscriptExpr *arraysubscriptexpr = Result.Nodes.getNodeAs<ArraySubscriptExpr>("arraysubscriptexpr");
    if(borrow_array != nullptr  && array_exp != nullptr){
      std::string newcontent;
      SourceRange range = return_assign_endloc(borrow_array, sourceManager_, lopt);
      //获取赋值语句右边引用的数组
      const clang::DeclRefExpr *DR = clang::dyn_cast<clang::DeclRefExpr>(array_exp->getBase()->IgnoreImpCasts());
      string array_name = DR -> getDecl()->getNameAsString();  //引用变量数组名
      const Expr *idx = array_exp -> getIdx();                            
      CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
      const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str(); //引用变量数组索引
    
      if(array_ref_var != nullptr){  //处理形如fm = &k[0]; 的赋值语句
          string varname = return_varname(array_ref_var);
          if(read_mut_ref_var(varname))
             newcontent = varname + " mborrow " + array_name + " ;     //borrow array element";
          else
             newcontent = varname + " borrow " + array_name + " ;     //borrow array element";
      }
      if(arraysubscriptexpr != nullptr){   //处理行如myint[2] = &k[1];的赋值语句
           int flag = 5;
           string mut_flag;
           CharSourceRange arraySubscriptExpr_CSR = CharSourceRange(arraysubscriptexpr->getSourceRange(), true);
           const std::string varname = Lexer::getSourceText(arraySubscriptExpr_CSR, sourceManager_, lopt).str();
           if(read_mut_ref_var(varname))    //变量为可变引用
              mut_flag = " mborrow " + array_name + "." + idx_value + ";    //borrow array element";
           else
              mut_flag = " borrow " + array_name + "." + idx_value + ";    //borrow array element";
           newcontent = arraySubscript_content(arraysubscriptexpr, sourceManager_, lopt, flag, mut_flag);

      }
      if(array_member_expr != nullptr){  //处理行如p3->z = &k[0];的赋值语句
           int flag = 5;
           string mut_flag;
           CharSourceRange CSR = CharSourceRange(array_member_expr->getSourceRange(), true);
           const std::string varname = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
           if(read_mut_ref_var(varname))
              mut_flag = " mborrow " + array_name + "." + idx_value + ";    //borrow array element";
           else
              mut_flag = " borrow " + array_name + "." + idx_value + ";    //borrow array element";
          newcontent = member_access_content(array_member_expr, flag, mut_flag);
        
       }
      do_replacement(sourceManager_, range, newcontent); 
     //测试
      CharSourceRange CSR2 = CharSourceRange(borrow_array->getSourceRange(), true);
      const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
      outs() << vardecl_stmt + "; success16";
      cout << endl;  
    }

    //37带有borrow语义的赋值: 引用的变量是结构体成员
    const Stmt *borrow_struct = Result.Nodes.getNodeAs<Stmt>("borrow_struct");
    const VarDecl *struct_ref_var = Result.Nodes.getNodeAs<VarDecl>("struct_ref_var");
    const ArraySubscriptExpr *struct_array_expr = Result.Nodes.getNodeAs<ArraySubscriptExpr>("struct_array_expr");
    const MemberExpr *struct_member_expr = Result.Nodes.getNodeAs<MemberExpr>("struct_member_expr");
    const MemberExpr *struct_member_access = Result.Nodes.getNodeAs<MemberExpr>("struct_member_access");
    if(borrow_struct != nullptr  && struct_member_access != nullptr){
       SourceRange range = return_assign_endloc(borrow_struct, sourceManager_, lopt);
       const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(struct_member_access->getBase()->IgnoreImpCasts());
       string newcontent;
       string struct_union_name = struct_union_DR -> getDecl()->getNameAsString();  //引用的结构体/联合体变量名
       string member_name = struct_member_access->getMemberDecl()->getNameAsString(); //引用的结构体/联合体成员名
       int member_idx = member_id(struct_union_DR, member_name);   //获取引用的结构体/联合体成员在struct/union中定义的序号
       if(struct_ref_var != nullptr){        //处理形如 b = &(p3->x);
          string varname = return_varname(struct_ref_var);
          if(read_mut_ref_var(varname))
             newcontent = varname + " mborrow " + struct_union_name + "." + to_string(member_idx) + ";       //borrow member variable";
          else
             newcontent = varname + " borrow " + struct_union_name + "." + to_string(member_idx) + ";       //borrow member variable";
       }
       if(struct_array_expr != nullptr){      //处理形如 myint[2] = &(p4.x);    
          int flag = 5;
          string mut_flag;
          CharSourceRange arraySubscriptExpr_CSR = CharSourceRange(struct_array_expr->getSourceRange(), true);
          const std::string varname = Lexer::getSourceText(arraySubscriptExpr_CSR, sourceManager_, lopt).str();
          if(read_mut_ref_var(varname))
             mut_flag = " mborrow " + struct_union_name + "." + to_string(member_idx) + ";       //borrow member variable";
          else
             mut_flag = " borrow " + struct_union_name + "." + to_string(member_idx) + ";       //borrow member variable";
         newcontent = arraySubscript_content(struct_array_expr, sourceManager_, lopt, flag, mut_flag);
       }
       if(struct_member_expr != nullptr){      //处理形如 p3->y = &(p4.x);    
          int flag = 5;
          string mut_flag;
          CharSourceRange CSR = CharSourceRange(struct_member_expr->getSourceRange(), true);
          const std::string varname = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
          if(read_mut_ref_var(varname))
              mut_flag = " mborrow " + struct_union_name + "." + to_string(member_idx) + ";       //borrow member variable";
          else
              mut_flag = " borrow " + struct_union_name + "." + to_string(member_idx) + ";       //borrow member variable";
          newcontent = member_access_content(struct_member_expr, flag, mut_flag);
       }
       do_replacement(sourceManager_, range, newcontent); 
      //测试
      CharSourceRange CSR2 = CharSourceRange(borrow_struct->getSourceRange(), true);
      const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
      outs() << vardecl_stmt + "; success37";
      cout << endl;     
    }
/*---------------------------------------------------------函数声明转换------------------------------------------------*/
    //18其他函数声明,没有函数体的action
    const FunctionDecl *common_func_nobody = Result.Nodes.getNodeAs<FunctionDecl>("common_func_nobody"); 
    if (common_func_nobody != nullptr && sourceManager_.isWrittenInMainFile(common_func_nobody->getLocation())){
            //获取函数声明源码位置
            SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(common_func_nobody->getSourceRange().getEnd(), 0, sourceManager_, lopt).getLocWithOffset(1);
            SourceRange range = SourceRange(common_func_nobody->getSourceRange().getBegin(),decl_end_end);
            CharSourceRange CSR = CharSourceRange(range, true);
            const std::string text = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
            
            //获取函数名
            string fun_name = common_func_nobody->getNameInfo().getName().getAsString();
            //string newcontent = "("; 
            std::string text_no= "fn " + fun_name + "(";
            //获取函数参数
            for(unsigned int i = 0; i < common_func_nobody->getNumParams(); i++){
              //获取参数名
              std::string param_name = common_func_nobody->parameters()[i]->getNameAsString();
              //获取参数类型
              const clang::Type *param_type = common_func_nobody->parameters()[i]->getType().getTypePtr(); 
              //获取参数类型名
             // std::string param_type_name = common_func->parameters()[i]->getType().getAsString();
              if( param_type->isPointerType()){
                
                  if(i == common_func_nobody->getNumParams() - 1)
                       text_no.append(param_name).append(":#own()");
                  else 
                       text_no.append(param_name).append(":#own(), ");    
               }

              else{
                
                  if(i == common_func_nobody->getNumParams() - 1)
                      text_no.append(param_name).append(":#own(copy)");  
                  else
                      text_no.append(param_name).append(":#own(copy), ");
              }             
            }
             
             text_no.append(")");
             //获取返回值类型
            // string ty = common_func_nobody->getReturnType().getAsString();
            // cout << "//" << ty << "//" << endl;
             const clang::Type *return_type = common_func_nobody->getReturnType().getTypePtr();
             if(return_type->isPointerType())
                  text_no.append(" -> ").append("#own();");
             else if(return_type->isVoidType())   //无返回值
                  text_no.append(" -> #voidTy;");
             else
                  text_no.append(" -> ").append("#own(copy);");
             do_replacement(sourceManager_, range, text_no);
             //测试
          outs() << "function " + fun_name + "; success18";
          cout << endl; 
      }

    //19其他函数定义，有函数体的action
    const FunctionDecl *common_func_body = Result.Nodes.getNodeAs<FunctionDecl>("common_func_body"); 
    const CompoundStmt *function_body = Result.Nodes.getNodeAs<CompoundStmt>("function_body");
    if (common_func_body != nullptr && function_body != nullptr && sourceManager_.isWrittenInMainFile(common_func_body->getLocation())){
            //获取函数声明源码位置
            SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(function_body->getSourceRange().getBegin(), 0, sourceManager_, lopt).getLocWithOffset(-1);
            SourceRange range = SourceRange(common_func_body->getSourceRange().getBegin(),decl_end_end);
            CharSourceRange CSR = CharSourceRange(range, false);
            const std::string text = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
            
            //获取函数名
            string fun_name = common_func_body->getNameInfo().getName().getAsString();
            //string newcontent = "("; 
            std::string text_no= "fn " + fun_name + "(";
            //获取函数参数
            for(unsigned int i = 0; i < common_func_body->getNumParams(); i++){
              //获取参数名
              std::string param_name = common_func_body->parameters()[i]->getNameAsString();
              //获取参数类型
              const clang::Type *param_type = common_func_body->parameters()[i]->getType().getTypePtr(); 
              if( param_type->isPointerType()){
                
                  if(i == common_func_body->getNumParams() - 1)
                       text_no.append(param_name).append(":#own()");
                  else 
                       text_no.append(param_name).append(":#own(), ");    
               }

              else{
                
                  if(i == common_func_body->getNumParams() - 1)
                      text_no.append(param_name).append(":#own(copy)");  
                  else
                      text_no.append(param_name).append(":#own(copy), ");
              }             
            }
             
             text_no.append(")");
             //获取返回值类型
            //string ty = common_func_body->getReturnType().getAsString();
             //cout << "//" << ty << "//" << endl;
             const clang::Type *return_type = common_func_body->getReturnType().getTypePtr();
             if(return_type->isPointerType())
                  text_no.append(" -> ").append("#own()");
             else if(return_type->isVoidType())
                  text_no.append(" -> ").append("#voidTy");
             else
                  text_no.append(" -> ").append("#own(copy)");
             do_replacement(sourceManager_, range, text_no);

             //函数体后添加；
             SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(function_body->getEndLoc(), 1, sourceManager_, lopt);
             rewriter_.InsertTextAfterToken(add_comma, ";");
             //测试
          outs() << "function " + fun_name + "; success19";
          cout << endl; 
      }

      //52 main函数的call调用
      const FunctionDecl *main_fun = Result.Nodes.getNodeAs<FunctionDecl>("main_fun"); 
      const CompoundStmt *main_body = Result.Nodes.getNodeAs<CompoundStmt>("main_body");
      if (main_fun != nullptr && main_body != nullptr && sourceManager_.isWrittenInMainFile(main_fun->getLocation())){
         SourceLocation add_call_loc = clang::Lexer::getLocForEndOfToken(main_body->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
         std::string add_call_str;
         add_call_str.append("\n");
         add_call_str.append("decl arg1;\n").append("decl argv1;\n \n").append("transfer newResource(copy) arg1;\n")
                     .append("transfer newResource() argv1;\n \n").append("call main(arg1, argv1);");
         rewriter_.InsertTextAfterToken(add_call_loc, add_call_str);

         //测试
         outs() << "main function add success52";
        cout << endl; 
      }
/*-----------------------------------------------------函数return语句的转换-----------------------------------------------*/
     //32 return i;返回一个变量, i;
     const ReturnStmt *return_var_stmt = Result.Nodes.getNodeAs<ReturnStmt>("return_var_stmt"); 
     const VarDecl *return_var = Result.Nodes.getNodeAs<VarDecl>("return_var"); 
     const clang::UnaryOperator *return_unaryOp = Result.Nodes.getNodeAs<clang::UnaryOperator>("return_unaryOp");
     const MemberExpr *return_member = Result.Nodes.getNodeAs<MemberExpr>("return_member"); 
     const ArraySubscriptExpr *return_array_element = Result.Nodes.getNodeAs<ArraySubscriptExpr>("return_array_element");
     if(return_var_stmt != nullptr){
           const clang::Type *ty = return_var_stmt->getRetValue()->getType().getTypePtr();
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(return_var_stmt->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(return_var_stmt->getBeginLoc(), decl_end_end);
           string newcontent;
           string copy_attr = " val(newResource(copy)) ";    //返回值类型不为指针
           if(ty -> isPointerType())    //返回值类型为指针
               copy_attr = " val(newResource()) ";
           if(return_var)    //return p;    
               newcontent = return_varname(return_var) + ";" + copy_attr + "    // return value";
           if(return_unaryOp){       //return &a;
               Expr *expr = return_unaryOp->getSubExpr();
               CharSourceRange return_unaryOp_CSR = CharSourceRange(expr->getSourceRange(), true);
               const std::string return_unaryOp_name = Lexer::getSourceText(return_unaryOp_CSR, sourceManager_, lopt).str();
               newcontent = return_unaryOp_name + ";" + copy_attr + "    // return value";
           }
           if(return_member){     //return p.y;
               const clang::DeclRefExpr *sub_struct_DR = clang::dyn_cast<clang::DeclRefExpr>(return_member->getBase()->IgnoreImpCasts());
               string name = sub_struct_DR -> getDecl()->getNameAsString();  //结构体/联合体变量名
               string member_name = return_member->getMemberDecl()->getNameAsString(); //结构体/联合体成员名
               int member_idx = member_id(sub_struct_DR, member_name); 
               newcontent = name + "." + to_string(member_idx) + ";" + copy_attr + "    // return value";
           }
           if(return_array_element){     //return a[1];  
               const clang::DeclRefExpr *DR = clang::dyn_cast<clang::DeclRefExpr>(return_array_element->getBase()->IgnoreImpCasts());
               string array_name = DR -> getDecl()->getNameAsString();  //数组名
               const Expr *idx = return_array_element -> getIdx();      //数组索引                          
               CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
               const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str(); //变量数组索引
               newcontent = array_name + "." + idx_value + ";" + copy_attr + "    // return value";
          }
          do_replacement(sourceManager_, range, newcontent); 
          //测试
        CharSourceRange CSR2 = CharSourceRange(return_var_stmt->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success32";
        cout << endl;  
     }

     //33 return y(m); 返回一个函数, call y(m);
     const ReturnStmt *return_call_stmt = Result.Nodes.getNodeAs<ReturnStmt>("return_call_stmt"); 
     const CallExpr *return_call = Result.Nodes.getNodeAs<CallExpr>("return_call"); 
     if (return_call_stmt != nullptr && return_call != nullptr){
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(return_call_stmt->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(return_call_stmt->getBeginLoc(), decl_end_end);
           const clang::Type *ty = return_call_stmt->getRetValue()->getType().getTypePtr();
           string newcontent;
           string call_format;
           CharSourceRange return_call_CSR = CharSourceRange(return_call->getSourceRange(), true);
           const std::string return_call_name = Lexer::getSourceText(return_call_CSR, sourceManager_, lopt).str();
           unsigned int argnumber = return_call->getNumArgs();
           string call_name = return_call -> getDirectCallee()->getNameInfo().getAsString();
           if(ty -> isPointerType()){
               if(argnumber == 0)    //型如cmp()
                   call_format = return_call_name;
               else       //型如cmp(&a)
                   call_format = parameters_change(return_call, sourceManager_, lopt);
               newcontent = "call " + call_format + "; " + "val(newResource())" + "    // return value(function call)";
           }
           else{
                if(argnumber == 0)    //型如cmp()
                    call_format = return_call_name;
                else       //型如cmp(&a)
                    call_format = parameters_change(return_call, sourceManager_, lopt);
                newcontent = "call " + call_name + "(" + call_format + ")" + "; " + "val(newResource(copy))" + "    // return value(function call)";
           }
           do_replacement(sourceManager_, range, newcontent); 
          //测试
        CharSourceRange CSR2 = CharSourceRange(return_call_stmt->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success33";
        cout << endl;  
     }
    //34 return; 返回空。或return NULL；删除
    const ReturnStmt *return_stmt = Result.Nodes.getNodeAs<ReturnStmt>("return_stmt"); 
    if(return_stmt != nullptr){
      const Expr *ret_var = return_stmt -> getRetValue();
      CharSourceRange CSR = CharSourceRange(ret_var->getSourceRange(), true);
      const std::string returnV = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
      if(ret_var == nullptr || returnV == "NULL"){      //处理return; 和return NULL;语句
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(return_stmt->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(return_stmt->getBeginLoc(), decl_end_end);
           clang::Rewriter::RewriteOptions RO;
           RO.IncludeInsertsAtBeginOfRange = true;
           RO.IncludeInsertsAtEndOfRange = true;
           RO.RemoveLineIfEmpty = false;
           CharSourceRange CSR1 = CharSourceRange(range, true);
           rewriter_.RemoveText(CSR1, RO);
             //测试
        CharSourceRange CSR2 = CharSourceRange(return_stmt->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success34";
        cout << endl;  
      }      
    } 

    //35 return 0;返回一个具体数值。val(newResource(copy))
    const ReturnStmt *return_value_stmt = Result.Nodes.getNodeAs<ReturnStmt>("return_value_stmt");
    if(return_value_stmt != nullptr){
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(return_value_stmt->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(return_value_stmt->getBeginLoc(), decl_end_end);
           string newcontent = "val(newResource(copy))    // return value";
           do_replacement(sourceManager_, range, newcontent); 
            //测试
        CharSourceRange CSR2 = CharSourceRange(return_value_stmt->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success35";
        cout << endl; 
    }

    //36 return *p_global>=MIN && *p_global<=MAX; 返回+ 复杂表达式;
    const ReturnStmt *return_bop_stmt = Result.Nodes.getNodeAs<ReturnStmt>("return_bop_stmt");
    const clang::BinaryOperator *binary_op = Result.Nodes.getNodeAs<clang::BinaryOperator>("binary_op");
    if(return_bop_stmt != nullptr && binary_op != nullptr){
           const clang::Type *ty = return_bop_stmt->getRetValue()->getType().getTypePtr();
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(return_bop_stmt->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(return_bop_stmt->getBeginLoc(), decl_end_end);
           string newcontent;
           Expr *return_LHS = binary_op->getLHS();
           Expr *return_RHS = binary_op->getRHS();
           const clang::DeclRefExpr *DRE = dyn_cast<clang::DeclRefExpr>(return_RHS->IgnoreParenCasts());
           const clang::UnaryOperator *RHS_UO = dyn_cast<clang::UnaryOperator>(return_RHS->IgnoreParenImpCasts());
           if(!DRE && !RHS_UO){
            const string LHS_content = handler_op_stmt(return_LHS, sourceManager_, lopt);
            newcontent.append(LHS_content).append(";");
           }
           else{   
              const string LHS_content = handler_op_stmt(return_LHS, sourceManager_, lopt);
              const string RHS_content = handler_op_stmt(return_RHS, sourceManager_, lopt);
              if(LHS_content == RHS_content)
                    newcontent.append(LHS_content).append(";");
              else 
                    newcontent.append(LHS_content).append("; ").append(RHS_content).append(";");
               }
           if(ty -> isPointerType())
              newcontent.append(" val(newResource())    // return value");
           else
              newcontent.append(" val(newResource(copy))    // return value");
           do_replacement(sourceManager_, range, newcontent); 
          //测试
        CharSourceRange CSR2 = CharSourceRange(return_bop_stmt->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success36";
        cout << endl;  
    } 
/*---------------------------------------------------------函数调用的转换-----------------------------------------------*/
    //17free函数的action
    const CallExpr *free_func = Result.Nodes.getNodeAs<CallExpr>("free_func"); 
    if (free_func != nullptr){

           CharSourceRange CSR = CharSourceRange(free_func -> getArg(0)->getSourceRange(), true);
           const std::string free_var = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(free_func->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(free_func->getBeginLoc(), decl_end_end);
           std::string newcontent = "deallocate " + free_var + ";" + "      // free function";
           do_replacement(sourceManager_, range, newcontent);
           //测试
           CharSourceRange CSR2 = CharSourceRange(free_func->getSourceRange(), true);
           const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
           outs() << vardecl_stmt + "; success17";
           cout << endl; 
      }

    //30 解引用表达式为参数的printf(“%d, %d”, *ptr, m);函数
     const CallExpr *printf_func = Result.Nodes.getNodeAs<CallExpr>("printf_func"); 
     if(printf_func != nullptr){
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(printf_func->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(printf_func->getBeginLoc(), decl_end_end);
           unsigned int argnumber = printf_func->getNumArgs();
           CharSourceRange CSR2 = CharSourceRange(printf_func->getSourceRange(), true);
           const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
           std::string newcontent;
           if (argnumber > 1)
           {
             for(unsigned int i = 1; i < argnumber; i++){
                 const Expr *call_arguments = printf_func -> getArg(i);
                 CharSourceRange CSR = CharSourceRange(call_arguments->getSourceRange(), true);
                 const std::string printf_var = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
                 const DeclRefExpr *simple_type_var = clang::dyn_cast<DeclRefExpr>(call_arguments->IgnoreParenImpCasts());
                 const MemberExpr *member_expr = clang::dyn_cast<MemberExpr>(call_arguments->IgnoreParenImpCasts());
                 const ArraySubscriptExpr *array_expr = clang::dyn_cast<ArraySubscriptExpr>(call_arguments->IgnoreParenImpCasts());
                 const UnaryOperator *star_expr = clang::dyn_cast<UnaryOperator>(call_arguments->IgnoreParenImpCasts());
                 if(call_arguments->getBeginLoc().isMacroID()) continue;     //宏定义的变量，跳过，不作转换
  
                 else if(simple_type_var)  //单个变量：m
                        newcontent.append(printf_var).append(";");

                 else if(member_expr){    //访问非指针类型成员变量 p3->x
                     const clang::DeclRefExpr *struct_union_DR = clang::dyn_cast<clang::DeclRefExpr>(member_expr->getBase()->IgnoreImpCasts());
                     string struct_union_name = struct_union_DR -> getDecl()->getNameAsString();  //结构体/联合体变量名
                     string member_name = member_expr->getMemberDecl()->getNameAsString(); //结构体/联合体成员名
                     int member_idx = member_id(struct_union_DR, member_name);   //获取成员变量在struct/union中定义的序号
                     newcontent.append(struct_union_name).append(".").append(to_string(member_idx)).append(";");
                 }

                else if(array_expr){     //数组元素类型 
                    //cout << "//" << endl;
                    const clang::DeclRefExpr *DR = clang::dyn_cast<clang::DeclRefExpr>(array_expr->getBase()->IgnoreParenImpCasts());
                    const clang::UnaryOperator *UO = clang::dyn_cast<clang::UnaryOperator>(array_expr->getBase()->IgnoreParenImpCasts());
                    const Expr *idx = array_expr -> getIdx();      //数组索引                          
                    CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                    const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str(); //变量数组索引
                    if(DR){    //a[1] 
                       string array_name = DR -> getDecl()->getNameAsString();  //数组名
                       newcontent.append(array_name).append(".").append(idx_value).append(";");
                    }
                    else if(UO){   //(*p)[i]
                       Expr *star_aub_exp = UO -> getSubExpr();
                       const clang::DeclRefExpr *star_expr_simple = clang::dyn_cast<clang::DeclRefExpr>(star_aub_exp->IgnoreParenImpCasts());
                       CharSourceRange Op_CSR = CharSourceRange(SourceRange(UO->getBeginLoc(),star_aub_exp->getBeginLoc().getLocWithOffset(-1)), true);
                       const std::string Op_name = Lexer::getSourceText(Op_CSR, sourceManager_, lopt).str();
                       if(star_expr_simple && Op_name == "*"){
                         //cout <<"++" << endl;
                         newcontent.append("(*").append(star_expr_simple->getDecl()->getNameAsString()).append(").").append(idx_value).append(";");
                       }
                    }
                    else{
                      cout << vardecl_stmt << " can not support! --30" << endl;
                      exit(0);
                    }
                }

                else if(star_expr){
                     Expr *star_aub_exp = star_expr -> getSubExpr();
                     const clang::DeclRefExpr *star_expr_simple = clang::dyn_cast<clang::DeclRefExpr>(star_aub_exp->IgnoreParenImpCasts());
                     CharSourceRange Op_CSR = CharSourceRange(SourceRange(star_expr->getBeginLoc(),star_aub_exp->getBeginLoc().getLocWithOffset(-1)), true);
                     const std::string Op_name = Lexer::getSourceText(Op_CSR, sourceManager_, lopt).str();
                     const MemberExpr *sub_member_expr = clang::dyn_cast<MemberExpr>(star_aub_exp->IgnoreParenImpCasts());
                     const ArraySubscriptExpr *sub_array_expr = clang::dyn_cast<ArraySubscriptExpr>(star_aub_exp->IgnoreParenImpCasts());
                     const BinaryOperator *bin_array_expr = clang::dyn_cast<BinaryOperator>(star_aub_exp->IgnoreParenImpCasts());
                     if(star_expr_simple){           //*ptr 或 &ptr
                         if(Op_name == "*")
                             newcontent.append("*").append(star_expr_simple->getDecl()->getNameAsString()).append(";");   //*ptr
                         //if(Op_name == "&")
                           //  newcontent.append("read(").append(star_expr_simple->getDecl()->getNameAsString()).append(");");
                     }
                
                     else if(sub_member_expr){    //*( p3->x)或&(p3.x)类型
                       const clang::DeclRefExpr *sub_struct_DR = clang::dyn_cast<clang::DeclRefExpr>(sub_member_expr->getBase()->IgnoreImpCasts());
                       string sub_name = sub_struct_DR -> getDecl()->getNameAsString();  //结构体/联合体变量名
                       string sub_member_name = sub_member_expr->getMemberDecl()->getNameAsString(); //结构体/联合体成员名
                       int member_idx = member_id(sub_struct_DR, sub_member_name); 
                       if(Op_name == "*")
                            newcontent.append("*(").append(sub_name).append(".").append(to_string(member_idx)).append(");");
                       //if(Op_name == "&")
                         //   newcontent.append("read(").append(sub_name).append(".").append(to_string(member_idx)).append(");");
                     }
                     
                     else if(sub_array_expr){       
                       const clang::DeclRefExpr *DR = clang::dyn_cast<clang::DeclRefExpr>(sub_array_expr->getBase()->IgnoreImpCasts());
                       string array_name = DR -> getDecl()->getNameAsString();  //数组名
                       const Expr *idx = sub_array_expr -> getIdx();      //数组索引                          
                       CharSourceRange idx_CSR = CharSourceRange(idx->getSourceRange(), true);
                       const std::string idx_value = Lexer::getSourceText(idx_CSR, sourceManager_, lopt).str(); //变量数组索引
                       if(Op_name == "*")   //*(a[1])类型
                          newcontent.append("*(").append(array_name).append(".").append(idx_value).append(");"); 
                       //if(Op_name == "&")   //&(a[1])类型
                        //  newcontent.append("read(").append(array_name).append(".").append(idx_value).append(");");
                     }
                     else if(bin_array_expr){
                       if(Op_name == "*")   //*(*data1 + 1))类型
                          newcontent.append(printf_var).append(";"); 
                       //if(Op_name == "&")   //&(*data1 + 1)类型
                         // newcontent.append(Lexer::getSourceText(CharSourceRange(star_aub_exp->getSourceRange(), true), sourceManager_, lopt).str()).append(";");
                     }
                     else{
                        cout << "// " << vardecl_stmt << " can not support! --30//" << endl;
                        exit(0);
                     }
                }
                else{
                  cout << "// " << vardecl_stmt << " can not support! --30 //" << endl;
                  exit(0); 
                }
              if(i == argnumber - 1)
                newcontent.append("   //printf arguments");
             }
            do_replacement(sourceManager_, range, newcontent); 
           }
           if(argnumber == 1){   //删除此类的printf
           clang::Rewriter::RewriteOptions RO;
           RO.IncludeInsertsAtBeginOfRange = true;
           RO.IncludeInsertsAtEndOfRange = true;
           RO.RemoveLineIfEmpty = false;
           CharSourceRange CSR1 = CharSourceRange(range, true);
           rewriter_.RemoveText(CSR1, RO);
          }
         //测试
        outs() << vardecl_stmt + "; success30";
        cout << endl; 
     }

     //31 scanf函数
     const CallExpr *scanf_func = Result.Nodes.getNodeAs<CallExpr>("scanf_func"); 
     if(scanf_func != nullptr){
           SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(scanf_func->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
           SourceRange range = SourceRange(scanf_func->getBeginLoc(), decl_end_end);
           unsigned int argnumber = scanf_func->getNumArgs();
           std::string newcontent;
           for(unsigned int i = 1; i < argnumber; i++){
              const clang::UnaryOperator *UO = dyn_cast<clang::UnaryOperator>(scanf_func->getArg(i)->IgnoreImplicit());
              if(UO){  //处理&a类型的参数
                    Expr *expr = UO->getSubExpr();
                    CharSourceRange has_varname_CSR = CharSourceRange(expr->getSourceRange(), true);
                    const std::string has_varname = Lexer::getSourceText(has_varname_CSR, sourceManager_, lopt).str();
                    if(i == argnumber - 1) 
                       newcontent = newcontent.append("transfer newResource(copy) ").append(has_varname).append(";").append("   //scanf arguments");
                      
                    else
                      newcontent = newcontent.append("transfer newResource(copy) ").append(has_varname).append(";").append("   //scanf arguments").append("\n");   
                   }
              
              else{  //处理指针p类型的参数
                    CharSourceRange has_varname_CSR = CharSourceRange(scanf_func->getArg(i)->getSourceRange(), true); 
                    const std::string has_varname = Lexer::getSourceText(has_varname_CSR, sourceManager_, lopt).str();
                    newcontent = newcontent.append("transfer newResource() ").append(has_varname).append(";").append("   //scanf arguments");
              }
           }
           do_replacement(sourceManager_, range, newcontent); 
        //测试
        CharSourceRange CSR2 = CharSourceRange(scanf_func->getSourceRange(), true);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success31";
        cout << endl;  
     }

    //(2）22赋值语句里的函数调用:返回值不是指针的函数调用（除去头文件的函数）。call y(m); transfer newResource(copy) x;
    const Stmt *assignment_call = Result.Nodes.getNodeAs<Stmt>("assignment_call"); 
    const Stmt *assign_LHS1 = Result.Nodes.getNodeAs<Stmt>("assign_LHS1"); 
    const CallExpr *assign_RHS1 = Result.Nodes.getNodeAs<CallExpr>("assign_RHS1");
    if(assign_LHS1 != nullptr && assignment_call != nullptr && assign_RHS1 != nullptr && 
                          sourceManager_.isWrittenInMainFile(assign_RHS1->getDirectCallee()->getLocation())){
            //outs() << "has ";
            SourceRange assign_LHS1_range = SourceRange(assign_LHS1->getSourceRange().getBegin(),assign_LHS1->getSourceRange().getEnd());
            CharSourceRange assign_LHS1_CSR = CharSourceRange(assign_LHS1_range, true);
            const std::string assign_LHS1_name = Lexer::getSourceText(assign_LHS1_CSR, sourceManager_, lopt).str(); 

            SourceRange assign_RHS1_range = SourceRange(assign_RHS1->getBeginLoc(),assign_RHS1->getEndLoc());
            CharSourceRange assign_RHS1_CSR = CharSourceRange(assign_RHS1_range, true);
            const std::string assign_RHS1_name = Lexer::getSourceText(assign_RHS1_CSR, sourceManager_, lopt).str();  

            SourceRange range = return_assign_endloc(assignment_call, sourceManager_, lopt);
            unsigned int argnumber = assign_RHS1->getNumArgs();
            string call_name = assign_RHS1 -> getDirectCallee()->getNameInfo().getAsString();
            string newcontent;
            if(argnumber == 0)    //型如cmp()
               newcontent = "call " + assign_RHS1_name + "; " + " transfer newResource(copy) " + assign_LHS1_name + ";";
            else{
               
               string parameters = parameters_change(assign_RHS1, sourceManager_, lopt);
               newcontent = "call " + call_name + "(" + parameters + ")" + "; " + " transfer newResource(copy) " + assign_LHS1_name + ";";
            } 
            do_replacement(sourceManager_, range, newcontent);
            //测试
            CharSourceRange CSR2 = CharSourceRange(assignment_call->getSourceRange(), true);
            const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
            outs() << vardecl_stmt + "; success22";
            cout << endl;
       }

    //(2) 23赋值语句里的函数调用:返回值是指针的函数调用（除去头文件的函数）。call func(&n); transfer newResource() p;
    const Stmt *assignment_call_pointer = Result.Nodes.getNodeAs<Stmt>("assignment_call_pointer"); 
    const VarDecl *assign_var = Result.Nodes.getNodeAs<VarDecl>("assign_var");
    const CallExpr *assign_RHS2 = Result.Nodes.getNodeAs<CallExpr>("assign_RHS2");
    if(assignment_call_pointer != nullptr && assign_var != nullptr && assign_RHS2 != nullptr && 
                          sourceManager_.isWrittenInMainFile(assign_RHS2->getDirectCallee()->getLocation())){
            
             std::string varname = return_varname(assign_var);

             SourceRange assign_RHS2_range = SourceRange(assign_RHS2->getBeginLoc(),assign_RHS2->getEndLoc());
             CharSourceRange assign_RHS2_CSR = CharSourceRange(assign_RHS2_range, true);
             const std::string assign_RHS2_name = Lexer::getSourceText(assign_RHS2_CSR, sourceManager_, lopt).str();
             SourceRange range = return_assign_endloc(assignment_call_pointer, sourceManager_, lopt);
             unsigned int argnumber = assign_RHS2->getNumArgs();
             string call_name = assign_RHS2 -> getDirectCallee()->getNameInfo().getAsString();
             string newcontent;
             if(argnumber == 0)    //型如cmp()
                  newcontent = "call " + assign_RHS2_name + "; " + "transfer newResource() " + varname + ";";
             else{
                  string parameters = parameters_change(assign_RHS2, sourceManager_, lopt);
                  newcontent =  "call " + call_name + "(" + parameters + ")" + ";" 
                                + " transfer newResource() " + varname + ";";
             }
             do_replacement(sourceManager_, range, newcontent);
             //测试
             CharSourceRange CSR2 = CharSourceRange(assignment_call_pointer->getSourceRange(), true);
             const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
             outs() << vardecl_stmt + "; success23";
             cout << endl;
      }

    //(2) 49赋值语句右值为库函数,copy属性根据调用函数的返回值确定 i = strlen(aString); call strlen(aString); transfer newResource() p;
    const Stmt *assignment_library_call = Result.Nodes.getNodeAs<Stmt>("assignment_library_call");
    const CallExpr *library = Result.Nodes.getNodeAs<CallExpr>("library");
    if (assignment_library_call != nullptr && library != nullptr && sourceManager_.isWrittenInMainFile(library->getBeginLoc())){
      string call_name = library -> getDirectCallee()->getNameInfo().getAsString();
      if(call_name != "malloc" && call_name != "calloc" && call_name != "realloc"){
        const clang::BinaryOperator *bo = clang::dyn_cast<clang::BinaryOperator>(assignment_library_call);
        Expr *lhs_var = bo -> getLHS();
        CharSourceRange lhs_var_CSR = CharSourceRange(lhs_var->getSourceRange(), true);
        const std::string lhs_expr = Lexer::getSourceText(lhs_var_CSR, sourceManager_, lopt).str(); 
        //SourceLocation decl_end = clang::Lexer::getLocForEndOfToken(assignment_library_call->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
        const clang::Type *fun_return_type = library->getDirectCallee()->getReturnType().getTypePtr(); 
        unsigned int argnumber = library->getNumArgs();
        string newcontent;
        if(argnumber == 0){   //型如cmp()
          if(fun_return_type->isPointerType())
             newcontent = "call " + call_name + "(); " + "transfer newResource() " + lhs_expr + ";";
          else
             newcontent = "call " + call_name + "(); " + "transfer newResource(copy)" + lhs_expr + ";";
        }
       else{
        string parameters = parameters_change(library, sourceManager_, lopt);
        if(fun_return_type->isPointerType())
           newcontent = "call " + call_name + "(" + parameters + ")" + ";" + " transfer newResource() " + lhs_expr + ";";
        
        else
           newcontent = "call " + call_name + "(" + parameters + ")" + ";" + " transfer newResource(copy) " + lhs_expr + ";";  
       }
      do_replacement(sourceManager_, return_assign_endloc(assignment_library_call, sourceManager_, lopt), newcontent);
      //测试
      CharSourceRange CSR2 = CharSourceRange(assignment_library_call->getSourceRange(), true);
      const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
      outs() << vardecl_stmt + "; success49";
      cout << endl;
     }
    }

    //(3)24其他语句中的函数调用(除去头文件中定义的函数)：即自定义函数和宏扩展而来的函数
    const CallExpr *other_call = Result.Nodes.getNodeAs<CallExpr>("other_call");
    if (other_call != nullptr){

             SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(other_call->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
             SourceLocation startLoc = other_call->getBeginLoc();
             SourceRange range = SourceRange(startLoc, decl_end_end);
             SourceRange other_call_range = SourceRange(startLoc,other_call->getEndLoc());
             CharSourceRange other_call_CSR = CharSourceRange(other_call_range, true);
             const std::string call_expr = Lexer::getSourceText(other_call_CSR, sourceManager_, lopt).str(); 
             unsigned int argnumber = other_call->getNumArgs();
             string call_name = other_call -> getDirectCallee()->getNameInfo().getAsString();
             string newcontent;
             if(startLoc.isMacroID()){    //宏定义  memset(data,'A', 100)
                newcontent = "call " + call_expr;
                CharSourceRange expansion = sourceManager_.getImmediateExpansionRange(startLoc);
                Replacement repl(sourceManager_, expansion, newcontent);
                repl.apply(rewriter_);
                //测试
                outs() << call_expr + "; success24";
                cout << endl;
                //rewriter_.InsertTextBefore(sourceManager_.getExpansionLoc(startLoc), "call ");
             }
             else if(call_name != "free" && call_name != "malloc" && call_name != "scanf" && call_name != "printf" && call_name != "calloc" && call_name != "realloc"){
                 if(argnumber == 0)    //型如cmp()
                     newcontent = "call " + call_expr + ";";
                 else{              //型如cmp(a, argv[1])
                     string parameters = parameters_change(other_call, sourceManager_, lopt);
                     newcontent =  "call " + call_name + "(" + parameters + ")" + ";";
                }
             //cout << ">>" << sourceManager_.getExpansionLineNumber(other_call->getBeginLoc()) << ">>" << endl;
                do_replacement(sourceManager_, range, newcontent);
                //测试
                outs() << call_expr + "; success24";
                cout << endl;
            }
      }
      
     //(4)46使用头文件中除free\malloc\scanf\printf\calloc\realloc
    const CallExpr *include_call = Result.Nodes.getNodeAs<CallExpr>("include_call");
    if (include_call != nullptr){
        string call_name = include_call -> getDirectCallee()->getNameInfo().getAsString();
        SourceLocation decl_end_end = clang::Lexer::getLocForEndOfToken(include_call->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
        SourceRange range = SourceRange(include_call->getBeginLoc(), decl_end_end);
        CharSourceRange CSR2 = CharSourceRange(include_call->getSourceRange(), true);
        const std::string call_expr = Lexer::getSourceText(CSR2, sourceManager_, lopt).str(); 
        unsigned int argnumber = include_call->getNumArgs();
        std::string newcontent;
        if(call_name != "free" && call_name != "malloc" && call_name != "scanf" && call_name != "printf" 
                             && call_name != "calloc" && call_name != "realloc"){
          if(argnumber == 0)    //型如cmp()
                  newcontent = "call " + call_expr + ";";
          else{
              // outs()<<"----++";
               string parameters = parameters_change(include_call, sourceManager_, lopt);
               newcontent =  "call " + call_name + "(" + parameters + ")" + ";";
          }
          do_replacement(sourceManager_, range, newcontent);
          //测试
          outs() << call_expr + "; success46";
          cout << endl;
        }
    }
/*--------------------------------------------------------循环操作符的转换----------------------------------------------*/
    //25for语句    decl i; transfer newResource(copy) i; var; ！{block};
    const ForStmt *loop_stmt = Result.Nodes.getNodeAs<ForStmt>("loop_stmt");
    const CompoundStmt *for_body = Result.Nodes.getNodeAs<CompoundStmt>("for_body");
    if (loop_stmt != nullptr && for_body != nullptr && sourceManager_.isWrittenInMainFile(loop_stmt->getForLoc())){
        
        std::string for_var_name; 
        std::string inc_variable;
        SourceLocation end = clang::Lexer::getLocForEndOfToken(for_body -> getBeginLoc(), 0, sourceManager_, lopt).getLocWithOffset(-1);
        SourceRange for_range = SourceRange(loop_stmt -> getBeginLoc(), end);
        const Expr* for_condition = loop_stmt->getCond();
        const Expr* inc_expr = loop_stmt->getInc();
        string newcontent;
        const Stmt *for_init = loop_stmt -> getInit();
        const clang::BinaryOperator *for_assign = dyn_cast<clang::BinaryOperator>(for_init);
        const clang::DeclStmt *for_decl = dyn_cast<clang::DeclStmt>(for_init);
        if(for_decl){         //for(int i = 1; i < 10; i++)
             const clang::VarDecl *for_decl_var = dyn_cast<clang::VarDecl>(for_decl->getSingleDecl());
             for_var_name = return_varname(for_decl_var);
             newcontent.append("decl ").append(for_var_name).append(":#own(copy)").append("; ").append("transfer newResource(copy) ").append(for_var_name).append(";").append("\n");
             inc_variable = for_var_name; 
        }
        if(for_assign){      //for(i = 1; i < 10; i++)
             const Expr *for_var = for_assign->getLHS();
             CharSourceRange CSR = CharSourceRange(for_var->getSourceRange(), true);
             for_var_name = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
             newcontent.append("transfer newResource(copy) ").append(for_var_name).append(";").append("\n");
             inc_variable = for_var_name;
        }
        string lhs_var;
        string rhs_var;
        if(!for_condition){  //处理for(int i = 0; ; i++;)或for(i = 1; ; i++)
             newcontent.append(for_var_name).append("; !");
        }
        else{
          const clang::BinaryOperator *for_con_bo = dyn_cast<clang::BinaryOperator>(for_condition->IgnoreParenImpCasts());
          if(for_con_bo){
              Expr *for_con_LHS = for_con_bo -> getLHS();
              Expr *for_con_RHS = for_con_bo -> getRHS();
              lhs_var = handler_op_stmt(for_con_LHS, sourceManager_, lopt);
              rhs_var = handler_op_stmt(for_con_RHS, sourceManager_, lopt);
              const clang::DeclRefExpr *LHS_DRE = dyn_cast<clang::DeclRefExpr>(for_con_LHS->IgnoreParenImpCasts());
              const clang::DeclRefExpr *RHS_DRE = dyn_cast<clang::DeclRefExpr>(for_con_RHS->IgnoreParenImpCasts());
              if(LHS_DRE && RHS_DRE){  
                   if(lhs_var == for_var_name)  //处理for条件形如i<b
                      newcontent.append(for_var_name).append("; ").append(rhs_var).append("; !");
                   if(rhs_var == for_var_name)  //处理for条件形如b>i
                      newcontent.append(for_var_name).append("; ").append(lhs_var).append("; !");
              }else  //处理for条件形如i<4或4<i
                        newcontent.append(for_var_name).append("; !");   
          }
        }
        do_replacement(sourceManager_, for_range, newcontent);
        rewriter_.InsertTextAfter(clang::Lexer::getLocForEndOfToken(for_body -> getBeginLoc(), 0, sourceManager_, lopt), "   //for statement");
        //处理递增表达式i++，在for执行体最后插入transfer i newResource(copy);
        if(inc_expr){
           string insert_string;
           SourceLocation insert_point = clang::Lexer::getLocForEndOfToken(for_body -> getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(-1);          
           const clang::UnaryOperator *inc = dyn_cast<clang::UnaryOperator>(inc_expr->IgnoreParenImpCasts());
           if(inc){
             // outs() << "ok----";
              if(inc->isPostfix()){
           //后置递增 i++；
               //  outs()<<"++++";
                 insert_string.append(inc_variable).append("; transfer ").append(inc_variable).append(" newResource(copy);").append("\n");
               }
           //前置递增 ++i；
              if(inc->isPrefix()){
               //  outs()<<"///";
                 //insert_string = "transfer " + inc_variable + " newResource(copy); " + inc_variable + ";";
                 insert_string.append("transfer ").append(inc_variable).append(" newResource(copy);").append(inc_variable).append(";").append("\n");;
               }
             }
          rewriter_.InsertText(insert_point, insert_string, false, true);
         }
        //循环体后添加；
        SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(for_body->getEndLoc(), 1, sourceManager_, lopt);
        rewriter_.InsertTextAfterToken(add_comma, ";");
        //测试
        CharSourceRange CSR2 = CharSourceRange(for_range, false);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str(); 
        outs() << vardecl_stmt + "; success25";
        cout << endl;

      }

    //26while条件中有函数调用    call y(n); !{bolck};
    const WhileStmt *while_stmt_call = Result.Nodes.getNodeAs<WhileStmt>("while_stmt_call");
    const CompoundStmt *while_body = Result.Nodes.getNodeAs<CompoundStmt>("while_body");  
    const CallExpr *while_call = Result.Nodes.getNodeAs<CallExpr>("while_call");
    if (while_stmt_call != nullptr && while_body != nullptr && while_call != nullptr &&
                                sourceManager_.isWrittenInMainFile(while_stmt_call->getWhileLoc())){

        SourceLocation while_end = clang::Lexer::getLocForEndOfToken(while_body -> getBeginLoc(), 0, sourceManager_, lopt).getLocWithOffset(-1);
        SourceRange while_range = SourceRange(while_stmt_call->getBeginLoc(), while_end);

        CharSourceRange while_call_CSR = CharSourceRange(SourceRange(while_call->getBeginLoc(),while_call->getEndLoc()), true);
        const std::string while_call_name = Lexer::getSourceText(while_call_CSR, sourceManager_, lopt).str(); 
        unsigned int argnumber = while_call->getNumArgs();
        string call_name = while_call -> getDirectCallee()->getNameInfo().getAsString();
        string newcontent;
        if(argnumber == 0)    //型如cmp()
             newcontent = "call " + while_call_name + "; !";
        else{              //型如cmp(a, argv[1])
             string parameters = parameters_change(while_call, sourceManager_, lopt);
             newcontent =  "call " + call_name + "(" +  parameters + ")" + "; !";
        }
        do_replacement(sourceManager_, while_range, newcontent);
        rewriter_.InsertTextAfter(clang::Lexer::getLocForEndOfToken(while_body -> getBeginLoc(), 0, sourceManager_, lopt), "   //while statement");
        
        //循环体后添加；
        SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(while_body->getEndLoc(), 1, sourceManager_, lopt);
        rewriter_.InsertTextAfterToken(add_comma, ";");
        //测试
        CharSourceRange CSR2 = CharSourceRange(while_range, false);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str(); 
        outs() << vardecl_stmt + "; success26";
        cout << endl;
    }

    //27while条件中无函数调用  var!bolck
    const WhileStmt *while_stmt_nocall = Result.Nodes.getNodeAs<WhileStmt>("while_stmt_nocall");
    const CompoundStmt *while_body_nocall = Result.Nodes.getNodeAs<CompoundStmt>("while_body_nocall"); 
    if (while_stmt_nocall != nullptr && while_body_nocall != nullptr && sourceManager_.isWrittenInMainFile(while_stmt_nocall->getWhileLoc())){
        
        SourceLocation while_end = clang::Lexer::getLocForEndOfToken(while_body_nocall -> getBeginLoc(), 0, sourceManager_, lopt).getLocWithOffset(-1);
        SourceRange while_range = SourceRange(while_stmt_nocall->getBeginLoc(), while_end);
        const Expr *con_stmt = while_stmt_nocall->getCond();
        std::string newcontent;
        if (con_stmt){  
            CharSourceRange while_con = CharSourceRange(SourceRange(con_stmt->getBeginLoc(),con_stmt->getEndLoc()), true);
            const std::string while_con_name = Lexer::getSourceText(while_con, sourceManager_, lopt).str(); 
           // outs() << "//" + while_con_name + "//";
            if(while_con_name == "1" || while_con_name == "true")  //处理while(1)或者while(true)
              newcontent.append("! ");
            else{
             const clang::BinaryOperator *con_bo = dyn_cast<clang::BinaryOperator>(con_stmt->IgnoreParenImpCasts());
             if(con_bo){  //处理while(a==b)
                   
                Expr *return_LHS = con_bo->getLHS();
                Expr *return_RHS = con_bo->getRHS();
                const clang::DeclRefExpr *DRE = dyn_cast<clang::DeclRefExpr>(return_RHS->IgnoreParenCasts());
                const clang::UnaryOperator *RHS_UO = dyn_cast<clang::UnaryOperator>(return_RHS->IgnoreParenImpCasts());
                if( !DRE && !RHS_UO ){
                  const string LHS_content = handler_op_stmt(return_LHS, sourceManager_, lopt);
                  newcontent.append(LHS_content).append("; !");
                }
                else{
                   const string LHS_content = handler_op_stmt(return_LHS, sourceManager_, lopt);
                   const string RHS_content = handler_op_stmt(return_RHS, sourceManager_, lopt);
                   if(LHS_content == RHS_content)
                       newcontent.append(LHS_content).append("; !");
                   else
                       newcontent.append(LHS_content).append("; ").append(RHS_content).append("; !");
                }
              }

             else{   //处理while(a)
                   CharSourceRange condition_var_CSR = CharSourceRange(con_stmt->getSourceRange(), true);
                   const std::string condition_var = Lexer::getSourceText(condition_var_CSR, sourceManager_, lopt).str();
                   newcontent.append(condition_var).append("; !");
             }
           }
            do_replacement(sourceManager_, while_range, newcontent);
            rewriter_.InsertTextAfter(clang::Lexer::getLocForEndOfToken(while_body_nocall -> getBeginLoc(), 0, sourceManager_, lopt), "   //while statement");
        }
        //循环体后添加；
        SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(while_body_nocall->getEndLoc(), 1, sourceManager_, lopt);
        rewriter_.InsertTextAfterToken(add_comma, ";");
        //测试
        CharSourceRange CSR2 = CharSourceRange(while_range, false);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str(); 
        outs() << vardecl_stmt + "; success27";
        cout << endl;
    }   
/*---------------------------------------------------------分支语句的转换----------------------------------------------*/
      //28if条件里含有函数调用 first, second; @转换
      const IfStmt *if_stmt = Result.Nodes.getNodeAs<IfStmt>("if_stmt_call");
      const CompoundStmt *if_body = Result.Nodes.getNodeAs<CompoundStmt>("if_body");
      const CallExpr *if_call = Result.Nodes.getNodeAs<CallExpr>("if_call");
      if (if_stmt != nullptr && if_call != nullptr && if_body != nullptr &&
                                sourceManager_.isWrittenInMainFile(if_stmt->getIfLoc())){
        //处理首个if语句
        SourceLocation end = clang::Lexer::getLocForEndOfToken(if_body -> getBeginLoc(), 0, sourceManager_, lopt).getLocWithOffset(-1);
        SourceRange if_range = SourceRange(if_stmt -> getBeginLoc(), end);
        CharSourceRange if_call_CSR = CharSourceRange(SourceRange(if_call->getBeginLoc(),if_call->getEndLoc()), true);
        const std::string if_call_name = Lexer::getSourceText(if_call_CSR, sourceManager_, lopt).str();
        string call_fun_name = if_call -> getDirectCallee()-> getNameInfo().getAsString();
        string newcontent; 
        unsigned int argnumber = if_call->getNumArgs();
        if(argnumber == 0)    //型如cmp()
             newcontent = "call " + if_call_name + "; @";
        else{              //型如cmp(a, argv[1])
             string parameters = parameters_change(if_call, sourceManager_, lopt);
             newcontent =  "call " + call_fun_name + "(" +  parameters + ")" + "; @";
        }
        do_replacement(sourceManager_, if_range, newcontent);
        rewriter_.InsertTextAfter(clang::Lexer::getLocForEndOfToken(if_body -> getBeginLoc(), 0, sourceManager_, lopt), "   //if statement");
        //递归处理else if语句
        const Stmt *else_stmt = if_stmt -> getElse();
        if (else_stmt == nullptr){
          //分支体后添加；
          SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(if_body->getEndLoc(), 1, sourceManager_, lopt);
          rewriter_.InsertTextAfterToken(add_comma, ";");
        }
        else {
          const IfStmt *elseif_Stmt = dyn_cast<IfStmt>(else_stmt);
          while(elseif_Stmt){
       
             hander_elseif(elseif_Stmt, sourceManager_, lopt);
             else_stmt = elseif_Stmt -> getElse();
             elseif_Stmt = dyn_cast<IfStmt>(else_stmt);
         
          }
          //处理else语句
          hander_else( else_stmt, sourceManager_, lopt);
        }
        
        //测试
        CharSourceRange CSR2 = CharSourceRange(if_range, false);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success28";
        cout << endl;
       }

      //29if条件里不含有函数调用 @{},{}
      const IfStmt *if_stmt_nocall = Result.Nodes.getNodeAs<IfStmt>("if_stmt_nocall");
      const CompoundStmt *if_body_nocall = Result.Nodes.getNodeAs<CompoundStmt>("if_body_nocall");
      if (if_stmt_nocall != nullptr && if_body_nocall != nullptr && sourceManager_.isWrittenInMainFile(if_stmt_nocall->getIfLoc())){
        
        //处理首个if语句
        SourceLocation end = clang::Lexer::getLocForEndOfToken(if_body_nocall -> getBeginLoc(), 0, sourceManager_, lopt).getLocWithOffset(-1);
        SourceRange if_range = SourceRange(if_stmt_nocall -> getBeginLoc(), end);
        const Expr *con_stmt = if_stmt_nocall->getCond();
        string newcontent;
        if(con_stmt){   
            CharSourceRange if_con = CharSourceRange(SourceRange(con_stmt->getBeginLoc(),con_stmt->getEndLoc()), true);
            const std::string if_con_name = Lexer::getSourceText(if_con, sourceManager_, lopt).str(); 
            if(if_con_name == "1" || if_con_name == "true" || if_con_name == "0")  //处理if(1)或者if(true)
                   newcontent.append("@ ");
            else{   
             const clang::BinaryOperator *con_bo = dyn_cast<clang::BinaryOperator>(con_stmt->IgnoreParenImpCasts());
             if(con_bo){  //处理if(a==b)

                   Expr *return_LHS = con_bo->getLHS();
                   Expr *return_RHS = con_bo->getRHS();
                   const clang::DeclRefExpr *DRE = dyn_cast<clang::DeclRefExpr>(return_RHS->IgnoreParenCasts());
                   const clang::UnaryOperator *RHS_UO = dyn_cast<clang::UnaryOperator>(return_RHS->IgnoreParenImpCasts());
                   const clang::DeclRefExpr *LHS_DRE = dyn_cast<clang::DeclRefExpr>(return_LHS->IgnoreParenCasts());
                   const clang::UnaryOperator *LHS_UO = dyn_cast<clang::UnaryOperator>(return_LHS->IgnoreParenImpCasts());
                   if(!DRE && !RHS_UO && !LHS_DRE && !LHS_UO)   //if(5==5)
                      newcontent.append("@");
                   else if(!DRE && !RHS_UO){          //if(a == 6)
                        const string LHS_content = handler_op_stmt(return_LHS, sourceManager_, lopt);
                        newcontent.append(LHS_content).append("; @");
                   }
                   else if(!LHS_DRE && !LHS_UO){    //if(6 == a)
                        const string RHS_content = handler_op_stmt(return_RHS, sourceManager_, lopt);
                        newcontent.append(RHS_content).append("; @");
                   }
                   else{
                        const string LHS_content = handler_op_stmt(return_LHS, sourceManager_, lopt);
                        const string RHS_content = handler_op_stmt(return_RHS, sourceManager_, lopt);
                        newcontent.append(LHS_content).append("; ").append(RHS_content).append("; @");
                       }
                   }
             else{   //处理if(a)
                   CharSourceRange condition_var_CSR = CharSourceRange(con_stmt->getSourceRange(), true);
                   const std::string condition_var = Lexer::getSourceText(condition_var_CSR, sourceManager_, lopt).str();
                   newcontent.append(condition_var).append("; @");
             }
          }
        do_replacement(sourceManager_, if_range, newcontent);
        rewriter_.InsertTextAfter(clang::Lexer::getLocForEndOfToken(if_body_nocall -> getBeginLoc(), 0, sourceManager_, lopt), "   //if statement");
       } 
        //递归处理else if语句
        const Stmt *else_stmt = if_stmt_nocall -> getElse();
        if (else_stmt == nullptr){
          //分支体后添加；
          SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(if_body_nocall->getEndLoc(), 1, sourceManager_, lopt);
          rewriter_.InsertTextAfterToken(add_comma, ";");
        }
        else {

          const IfStmt *elseif_Stmt = dyn_cast<IfStmt>(else_stmt);
          while(elseif_Stmt){
       
          hander_elseif(elseif_Stmt, sourceManager_, lopt);
          else_stmt = elseif_Stmt -> getElse();
          elseif_Stmt = dyn_cast<IfStmt>(else_stmt);
         
          } 
        //处理else语句
        hander_else( else_stmt, sourceManager_, lopt);
      }
        
        //测试
        CharSourceRange CSR2 = CharSourceRange(if_range, false);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success29";
        cout << endl;

      }

      //39 switch case语句 @{},{}
      const SwitchStmt *switch_case_stmt = Result.Nodes.getNodeAs<SwitchStmt>("switch_case_stmt");
      if(switch_case_stmt && sourceManager_.isWrittenInMainFile(switch_case_stmt->getSwitchLoc())){
        //cout << "++" << endl;
        string newcontent;
        const Expr *switch_cond = switch_case_stmt->getCond();
        CharSourceRange switch_cond_CSR = CharSourceRange(switch_cond->getSourceRange(), true);
        const std::string switch_cond_var = Lexer::getSourceText(switch_cond_CSR, sourceManager_, lopt).str();
        //cout << "//" << switch_cond_var << "//" << endl; 

        const Stmt *switch_stmt = switch_case_stmt->getBody();
        //CharSourceRange switch_body_CSR = CharSourceRange(switch_stmt->getSourceRange(), true);
        //const std::string switch_body_stmt= Lexer::getSourceText(switch_body_CSR, sourceManager_, lopt).str();
        //cout << "--" << switch_body_stmt << "--" << endl;

        //删除switch(a){ }
        SourceLocation switch_start = switch_case_stmt -> getBeginLoc();
        SourceLocation body_start = switch_stmt -> getBeginLoc();
        SourceLocation body_end = switch_stmt -> getEndLoc();
        CharSourceRange switch_CSR = CharSourceRange(SourceRange(switch_start, body_start), true);
        const std::string switch_delete= Lexer::getSourceText(switch_CSR, sourceManager_, lopt).str();
        //cout << "==" << switch_delete << "==" << endl;
        clang::Rewriter::RewriteOptions RO;
        RO.IncludeInsertsAtBeginOfRange = true;
        RO.IncludeInsertsAtEndOfRange = true;
        RO.RemoveLineIfEmpty = false;
        rewriter_.RemoveText(switch_CSR, RO);
        rewriter_.RemoveText(body_end, 1, RO);

        //case 常亮：重写为 “，”，除了第一个case 常亮
        const SwitchCase *switch_case = switch_case_stmt -> getSwitchCaseList();
        SourceRange case_lable_range;
        CharSourceRange switch_case_CSR;
        while(switch_case -> getNextSwitchCase()){
          case_lable_range = SourceRange(switch_case->getBeginLoc(), switch_case->getColonLoc());
          switch_case_CSR = CharSourceRange(case_lable_range, true);
          const std::string switch_case_lable= Lexer::getSourceText(switch_case_CSR, sourceManager_, lopt).str();
         // cout << "<<" << switch_case_lable << ">>" << endl; 
          Replacement repl(sourceManager_, switch_case_CSR, ",");
          repl.apply(rewriter_);
          switch_case = switch_case -> getNextSwitchCase();
        }
      
        //第一个case 常亮：重写为 a; @
        case_lable_range = SourceRange(switch_case->getBeginLoc(), switch_case->getColonLoc());
        switch_case_CSR = CharSourceRange(case_lable_range, true);
        const clang::DeclRefExpr *condition_variable = dyn_cast<clang::DeclRefExpr>(switch_cond->IgnoreParenCasts());
        if(condition_variable)
          newcontent = switch_cond_var + "; @";
        else
          newcontent = "@";
        Replacement repl(sourceManager_, switch_case_CSR, newcontent);
        repl.apply(rewriter_);
        //注释switch case statement
        SourceLocation end = clang::Lexer::getLocForEndOfToken(switch_case->getColonLoc(), 0, sourceManager_, lopt).getLocWithOffset(2);
        rewriter_.InsertTextAfter(end, "   //switch case statement");
        
        //测试
        CharSourceRange CSR2 = CharSourceRange(SourceRange(switch_start, body_start), false);
        const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
        outs() << vardecl_stmt + "; success39";
        cout << endl;
      }

      //51 default语句,添加；表示分支
      const DefaultStmt *default_stmt = Result.Nodes.getNodeAs<DefaultStmt>("default_stmt");
      if(default_stmt && sourceManager_.isWrittenInMainFile(default_stmt->getDefaultLoc())){
          SourceLocation add_comma = clang::Lexer::getLocForEndOfToken(default_stmt->getEndLoc(), 1, sourceManager_, lopt);
          rewriter_.InsertTextAfterToken(add_comma, ";");
      }
/*----------------------------------------------------break, continue,空语句的删除----------------------------------------------*/
      //40删除break和continue语句
      const Stmt *loopskip = Result.Nodes.getNodeAs<Stmt>("loopskip");
      if(loopskip && sourceManager_.isWrittenInMainFile(loopskip->getBeginLoc())) {
          SourceLocation break_end = clang::Lexer::getLocForEndOfToken(loopskip->getEndLoc(), 0, sourceManager_, lopt).getLocWithOffset(1);
          clang::Rewriter::RewriteOptions RO;
          RO.IncludeInsertsAtBeginOfRange = true;
          RO.IncludeInsertsAtEndOfRange = true;
          RO.RemoveLineIfEmpty = false;
          rewriter_.RemoveText(CharSourceRange(SourceRange(loopskip->getBeginLoc(), break_end), true), RO);
          //测试
          CharSourceRange CSR2 = CharSourceRange(SourceRange(loopskip->getBeginLoc(), break_end), false);
          const std::string vardecl_stmt = Lexer::getSourceText(CSR2, sourceManager_, lopt).str();
          outs() << vardecl_stmt + "success40";
          cout << endl;
      } 
    }
    
};

class FunctionDeclASTConsumer : public clang::ASTConsumer
{

public:
  // override the constructor in order to pass CI
   FunctionDeclASTConsumer(Rewriter &R): VarD(R){
  
/*---------------------------------------------------变量声明转换的matcher-----------------------------------------*/
        //声明语句
        Matcher.addMatcher(declStmt(isExpansionInMainFile()).bind("declstmt"), &VarD);

        //全局变量
        Matcher.addMatcher(varDecl(isExpansionInMainFile(), unless(parmVarDecl()), hasGlobalStorage()).bind("static_var"),&VarD);

        //复杂结构类型声明(struct、enumeration、union)
       /* Matcher.addMatcher(decl(allOf(isExpansionInMainFile(),anyOf(recordDecl().bind("record"), enumDecl().bind("enum"))))
                               .bind("complex_data"), &VarD);*/
        Matcher.addMatcher(typedefDecl(isExpansionInMainFile()).bind("typedef_complex_data"), &VarD);
/*---------------------------------------------------赋值语句转换的matcher-----------------------------------------*/
        //赋值语句右值为具体数值或是初始化列表。带copy的transfer：transfer newResource(copy) a; 
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(expr(isExpansionInMainFile()).bind("LHS_type1")),
                                          hasRHS(expr(allOf(unless(ignoringParenImpCasts(unaryOperator())),
                                                       unless(ignoringParenImpCasts(declRefExpr())),
                                                       unless(ignoringParenCasts(callExpr())),
                                                       unless(ignoringParenImpCasts(memberExpr())),
                                                       unless(ignoringParenImpCasts(arraySubscriptExpr())))))) 
                                                       .bind("assignment_type1"), &VarD);

        //赋值语句右值为指针、数组、复合(struct等)。transfer p1 p2;
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(expr(isExpansionInMainFile()).bind("LHS_type2")),
                                          hasRHS(ignoringParenImpCasts(declRefExpr(to(
                                          varDecl().bind("RHS_type2"))))))
                                          .bind("assignment_type2"), &VarD);

        //赋值语句右值为指针解引用. a = *b;  transfer *b a;
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(expr(isExpansionInMainFile()).bind("LHS_type4")),
                                          hasRHS(anyOf(ignoringParenImpCasts(unaryOperator(hasOperatorName("*")).bind("pointer_refer")),
                                                       ignoringParenImpCasts(memberExpr().bind("member_assign")),
                                                       ignoringParenImpCasts(arraySubscriptExpr().bind("array_assign")))))
                                          .bind("assignment_type4"), &VarD);
        //赋值语句右值为malloc等内存分配函数。transfer newResource() pm; 
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(expr(isExpansionInMainFile()).bind("LHS_type3")),
                                          hasRHS(ignoringParenCasts(callExpr(
                                          callee(functionDecl(anyOf(hasName("malloc"),hasName("calloc"),
                                          hasName("realloc"))).bind("memory_name"))))))
                                          .bind("assignment_type3"), &VarD);

        //带有borrow语义的赋值
          //引用的变量是除数组、结构体成员以外的变量
        Matcher.addMatcher(binaryOperator(isExpansionInMainFile(),
                                          hasOperatorName("="),
                                          hasLHS(anyOf(memberExpr().bind("member_expr"),
                                                       ignoringParenImpCasts(declRefExpr(to(
                                                             varDecl().bind("LHS_ref_var")))),
                                                       arraySubscriptExpr().bind("array_subscript_expr"))),
                                          hasRHS(unaryOperator(hasOperatorName("&"),
                                                               hasUnaryOperand(ignoringParenImpCasts(
                                                                     declRefExpr(to(varDecl().bind("RHS_ref_var"))))))))
                                          .bind("borrow_assign"), &VarD);

        //带有borrow语义的赋值
          //引用的变量是数组
        Matcher.addMatcher(binaryOperator(isExpansionInMainFile(),
                                          hasOperatorName("="),
                                          hasLHS(anyOf(memberExpr().bind("array_member_expr"),
                                                       ignoringParenImpCasts(declRefExpr(to(
                                                             varDecl().bind("array_ref_var")))),
                                                       arraySubscriptExpr().bind("arraysubscriptexpr"))),
                                          hasRHS(unaryOperator(hasOperatorName("&"),
                                                               hasUnaryOperand(ignoringParenImpCasts(
                                                                      arraySubscriptExpr().bind("array_exp"))))))
                                         .bind("borrow_array"), &VarD);  

        //带有borrow语义的赋值
          //引用的变量是结构体成员
        Matcher.addMatcher(binaryOperator(isExpansionInMainFile(),
                                          hasOperatorName("="),
                                          hasLHS(anyOf(memberExpr().bind("struct_member_expr"),
                                                       ignoringParenImpCasts(declRefExpr(to(
                                                            varDecl().bind("struct_ref_var")))),
                                                       arraySubscriptExpr().bind("struct_array_expr"))),
                                          hasRHS(unaryOperator(hasOperatorName("&"),
                                                               hasUnaryOperand(ignoringParenImpCasts(
                                                                       memberExpr().bind("struct_member_access"))))))
                                          .bind("borrow_struct"), &VarD);                         
/*---------------------------------------------------函数声明转换的matcher------------------------------------------*/
    
        //其他函数声明,没有函数体
        Matcher.addMatcher(functionDecl(unless(hasBody(compoundStmt())),isExpansionInMainFile()).bind("common_func_nobody"), &VarD);

        //其他函数定义，有函数体
        Matcher.addMatcher(functionDecl(hasBody(compoundStmt().bind("function_body")),isExpansionInMainFile()).bind("common_func_body"), &VarD);

        //main函数
        Matcher.addMatcher(functionDecl(hasName("main"),hasBody(compoundStmt().bind("main_body")),isExpansionInMainFile()).bind("main_fun"), &VarD);
/*---------------------------------------------------Return语句的matcher------------------------------------------*/
        //return语句:return i; 返回一个变量, i;
        Matcher.addMatcher(returnStmt(isExpansionInMainFile(),
                                      hasReturnValue(ignoringParenImpCasts(anyOf(declRefExpr(to(varDecl().bind("return_var"))),
                                                                      unaryOperator().bind("return_unaryOp"),
                                                                      memberExpr().bind("return_member"),
                                                                      arraySubscriptExpr().bind("return_array_element")))))
                                     .bind("return_var_stmt"), &VarD);

        //return语句:return y(m); 返回一个函数, call y(m);
        Matcher.addMatcher(returnStmt(isExpansionInMainFile(),
                                      hasReturnValue(ignoringParenCasts(callExpr().bind("return_call"))))
                                     .bind("return_call_stmt"), &VarD);
        
        //return; 返回空。删除
        Matcher.addMatcher(returnStmt(isExpansionInMainFile()).bind("return_stmt"), &VarD);

        //return 0;返回一个具体数值。删除
        Matcher.addMatcher(returnStmt(isExpansionInMainFile(),
                                      hasReturnValue(ignoringParenImpCasts(anyOf(integerLiteral(),
                                                                                 stringLiteral(),
                                                                                 userDefinedLiteral(),
                                                                                 characterLiteral(),
                                                                                 floatLiteral())))).bind("return_value_stmt"), &VarD);

       //return *p_global>=MIN && *p_global<=MAX; 返回+ 复杂表达式;
        Matcher.addMatcher(returnStmt(isExpansionInMainFile(),
                                      hasReturnValue(ignoringParenImpCasts(binaryOperator(
                                                  unless(anyOf(hasOperatorName("."),
                                                               hasOperatorName("->"),
                                                               hasOperatorName("[]")))).bind("binary_op"))))
                                      .bind("return_bop_stmt"), &VarD);
/*---------------------------------------------------函数调用的matcher---------------------------------------------*/
        //free函数
        Matcher.addMatcher(callExpr(callee(functionDecl(hasName("free")))).bind("free_func"), &VarD);

        //解引用表达式为参数的printf(“%d, %d”, *ptr, m);函数
        Matcher.addMatcher(callExpr(callee(functionDecl(hasName("printf")))).bind("printf_func"), &VarD);

        //scanf函数
        Matcher.addMatcher(callExpr(callee(functionDecl(hasName("scanf")))).bind("scanf_func"), &VarD);

        //(2）赋值语句里的函数调用:返回值不是指针的函数调用（除去头文件的函数）。call y(m); transfer newResource(copy) x;
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(expr(isExpansionInMainFile()).bind("assign_LHS1")),
                                          hasRHS(ignoringParenCasts(callExpr(callee(functionDecl(isExpansionInMainFile(),
                                          returns(unless(isAnyPointer()))))).bind("assign_RHS1"))))
                                          .bind("assignment_call"), &VarD);

        //(2) 赋值语句里的函数调用:返回值是指针的函数调用（除去头文件的函数）。call func(&n); transfer func(&n) p;
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(declRefExpr(to(varDecl(isExpansionInMainFile()).bind("assign_var")))),
                                          hasRHS(ignoringParenCasts(callExpr(callee(functionDecl(isExpansionInMainFile(),
                                          returns(isAnyPointer())))).bind("assign_RHS2"))))
                                          .bind("assignment_call_pointer"), &VarD);

        //(2) 赋值语句右值为库函数,copy属性根据陈调用函数的返回值确定. i = strlen(aString);      call strlen(aString); transfer newResource() p;
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasRHS(ignoringParenCasts(callExpr(allOf(isExpansionInMainFile(),
                                                      callee(functionDecl(isExpansionInSystemHeader())))).bind("library"))))
                                          .bind("assignment_library_call"), &VarD);

        //(3)其他语句中的函数调用(除去头文件中定义的函数) call cmp().排除(1)和(2)类中的函数调用以及if、for、while、return等特殊语句条件中的函数调用。
        Matcher.addMatcher(callExpr(anyOf(allOf(callee(functionDecl(isExpansionInMainFile())),
                                                hasParent(compoundStmt())),
                                          allOf(unless(hasAncestor(ifStmt())),
                                                unless(hasAncestor(forStmt())),
                                                unless(hasAncestor(whileStmt())),
                                                unless(hasAncestor(returnStmt())),
                                                unless(hasAncestor(varDecl())),
                                                unless(hasAncestor(binaryOperator(hasOperatorName("=")))),
                                                callee(functionDecl(isExpansionInMainFile()))))).bind("other_call"), &VarD);

        //(4)使用头文件中除free\malloc\scanf\printf\calloc\realloc
        Matcher.addMatcher(callExpr(allOf(callee(functionDecl(isExpansionInSystemHeader())),
                                         isExpansionInMainFile(),
                                         unless(hasAncestor(callExpr())),
                                         unless(hasAncestor(varDecl())),
                                         unless(hasAncestor(binaryOperator()))))
                                   .bind("include_call"), &VarD);
/*---------------------------------------------------循环语句的matcher----------------------------------------------*/
        //for语句    decl i;transfer newResource(copy) i; var; ！{block}
        Matcher.addMatcher(forStmt(isExpansionInMainFile(),
                                   hasBody(compoundStmt().bind("for_body"))).bind("loop_stmt"), &VarD);

        //含有一个赋值语句的for语句  transfer newResource(copy) i; var; !{block}
        /*Matcher.addMatcher(forStmt(isExpansionInMainFile(),
                                   hasLoopInit(binaryOperator(hasOperatorName("=")).bind("for_assign")),
                                   hasBody(compoundStmt().bind("for_assign_body"))).bind("for_assign_loop"), &VarD);*/

        //while条件中有函数调用    call y(n); !bolck
        Matcher.addMatcher(whileStmt(isExpansionInMainFile(),
                                     hasBody(compoundStmt().bind("while_body")),
                                     hasCondition(hasDescendant (callExpr().bind("while_call")))).bind("while_stmt_call"), &VarD);

        //while条件中无函数调用    !bolck
        Matcher.addMatcher(whileStmt(isExpansionInMainFile(),
                                     hasBody(compoundStmt().bind("while_body_nocall")),
                                     hasCondition(unless(hasDescendant(callExpr())))).bind("while_stmt_nocall"), &VarD);
/*----------------------------------------------------分支语句的matcher----------------------------------------------*/
        //if条件里含有函数调用 call cmp(first, second); @{},{}
        Matcher.addMatcher(ifStmt(allOf(hasDescendant(compoundStmt().bind("if_body")),
                                  isExpansionInMainFile(),
                                  unless(hasAncestor(ifStmt())),
                                  hasCondition(anyOf(hasDescendant(callExpr().bind("if_call")),
                                                     callExpr().bind("if_call"),
                                                     binaryOperator(hasDescendant(callExpr().bind("if_call")))))))
                                 .bind("if_stmt_call"), &VarD);

        //if条件里不含有函数调用 @{},{}
        Matcher.addMatcher(ifStmt(allOf(hasDescendant(compoundStmt().bind("if_body_nocall")),
                                  isExpansionInMainFile(),
                                  unless(hasAncestor(ifStmt())),
                                  unless(hasCondition(anyOf(hasDescendant(callExpr()),
                                                            callExpr(),
                                                            binaryOperator(hasDescendant(callExpr())))))))
                                  .bind("if_stmt_nocall"), &VarD);
        //switch case语句 @{},{}
        Matcher.addMatcher(switchStmt(isExpansionInMainFile()).bind("switch_case_stmt"), &VarD);

        //switch case语句中的default语句
        Matcher.addMatcher(defaultStmt(isExpansionInMainFile()).bind("default_stmt"), &VarD);
/*---------------------------------------------------break，continue和空语句的matcher----------------------------------------------*/
        //break,continue和空语句
        Matcher.addMatcher(stmt(anyOf(breakStmt(isExpansionInMainFile()),
                                      continueStmt(isExpansionInMainFile()),
                                      nullStmt(isExpansionInMainFile())))
                                  .bind("loopskip"), &VarD);
                                   //  unless(hasAncestor(forStmt())),
                                    // unless(hasAncestor(whileStmt())),
                                    // unless(hasAncestor(doStmt()))).bind("breakstmt"), &VarD);
   }
   
  virtual void HandleTranslationUnit(clang::ASTContext &astContext) override
  {
   
    Matcher.matchAST(astContext);
  }

private:
  MatchFinder Matcher;
  VisitVarDecl VarD;
};

class FunctionDeclFrontendAction : public clang::ASTFrontendAction
{
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override{
        rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return make_unique<FunctionDeclASTConsumer>(rewriter_);
    }

   
    void EndSourceFileAction() override {
    SourceManager &SM = rewriter_.getSourceMgr();
    std::error_code error_code;
    const clang::FileID mainFile = SM.getMainFileID();
    std::string name = SM.getFileEntryForID(mainFile)->getName().str();
    string prefix;
    const char *ptr = strrchr(name.c_str(),'/');
    prefix = string(ptr + 1);
    //cout << "++"+ prefix << endl;
    llvm::raw_fd_ostream outFile("/home/cat409/Programs/xiaohua/4_transC2OSL_output/" + prefix + "_incomplete_osl.c", error_code, llvm::sys::fs::F_None);
    rewriter_.getEditBuffer(SM.getMainFileID()).write(outFile); // --> this will write the result to outFile
    outFile.close();
  }

private:
   Rewriter rewriter_;
};


static llvm::cl::OptionCategory MyToolCategory("my-tool options");

static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

static cl::extrahelp MoreHelp("\nMore help text...\n");

int main(int argc, const char **argv)
{
  string prefix;
  const char *ptr = strrchr(argv[1],'/');
  if (ptr == NULL) {
   prefix = argv[1];
  } else {
   prefix = string(ptr + 1);
  }
 //读取"/home/xiaohua/Documents/llvm-project/llvm/tools/clang/tools/extra/example/mut_ref_var.txt"形成mut_ref_var数组。
  ifstream mut_ref_in;
  string path1 = "/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_mut_ref_var.txt";
  mut_ref_in.open(path1, ifstream::in);
  if (!mut_ref_in.is_open()) {
      cout << "Can not find target file." << endl;
  }
  string mut_ref_buff;
  while (getline(mut_ref_in, mut_ref_buff)){
      mut_ref_var[mut_ref_var_count] = mut_ref_buff;
      mut_ref_var_count++;
  } 
  mut_ref_in.close();

  //读取"/home/xiaohua/Documents/llvm-project/llvm/tools/clang/tools/extra/example/mut_var.txt"形成mut_var数组。
  ifstream mut_var_in;
  string path3 = "/home/cat409/Programs/xiaohua/3_mut_var_output/" + prefix + "_mut_var.txt";;
  mut_var_in.open(path3, ifstream::in);
  if (!mut_var_in.is_open()) {
      cout << "Can not find target file." << endl;
  }
  string mut_var_buff;
  while (getline(mut_var_in, mut_var_buff)){
      mut_var[mut_var_count] = mut_var_buff;
      mut_var_count++;
  } 
  mut_var_in.close();

  //读取"/home/xiaohua/Documents/llvm-project/llvm/tools/clang/tools/extra/example/heap_resource.txt"形成heap_var数组。
  ifstream heap_var_in;
  string path4 = "/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_heap_resource.txt";
  heap_var_in.open(path4, ifstream::in);
  if (!heap_var_in.is_open()) {
      cout << "Can not find target file." << endl;
  }
  string heap_var_buff;
  while (getline(heap_var_in, heap_var_buff)){
      heap_var[heap_var_count] = heap_var_buff;
      heap_var_count++;
  } 
  mut_var_in.close();

  //读取"/home/xiaohua/Documents/llvm-project/llvm/tools/clang/tools/extra/example/for_decl_var.txt"形成for_variable数组。
  ifstream for_var_in;
  string path2 = "/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_for_decl_var.txt";
  for_var_in.open(path2, ifstream::in);
  if (!for_var_in.is_open()) {
      cout << "Can not find target file." << endl;
  }
  string for_buff;
  while (getline(for_var_in, for_buff)){
      for_variable[for_variable_count] = (unsigned int)stoi(for_buff);
      for_variable_count++;
  } 
  for_var_in.close();

  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  int result = Tool.run(newFrontendActionFactory<FunctionDeclFrontendAction>().get());
  return result;
}
