#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <fstream>
#include <sstream>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "clang/Driver/Options.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Tooling/RefactoringCallbacks.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Lex/Lexer.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Decl.h"
#include "llvm/IR/Instructions.h"

#define MAX 10000
using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;
using namespace std;

static int mut_ref_var_count = 0;
static int count_var = 0;
static unsigned long int mut_ref_num = 0;

string mut_ref_var[MAX];
vector<string> mut_var;
vector<string> mut_ref;   //mut_reference name
vector<int> line;       //the line number of dereference of mutable reference (*b = 7);

typedef struct line_interval{
       string mut_ref_name;
       vector<int> linenumber;
       vector<pair<int, string>> potential_mut_var;
  }line_interval;
line_interval mut_reference[MAX];

static llvm::cl::OptionCategory MyToolCategory("my-tool options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...\n");


class VisitVarDecl : public MatchFinder::MatchCallback {

private:
  Rewriter& rewriter_;
  LangOptions lopt;

public:
  explicit VisitVarDecl(Rewriter &r): rewriter_(r){}
  virtual void run(const MatchFinder::MatchResult &Result){

    ASTContext *Context = Result.Context;
    SourceManager &sourceManager_ = Context->getSourceManager();
   
    const clang::BinaryOperator *borrow_assign = Result.Nodes.getNodeAs<clang::BinaryOperator>("borrow_assign");
    const Expr *LHS_ref_var = Result.Nodes.getNodeAs<Expr>("LHS_ref_var");
    if(borrow_assign != nullptr  && LHS_ref_var != nullptr){
      int i;
      int linenumber;
      pair<int, string> line_var;
      CharSourceRange CSR = CharSourceRange(LHS_ref_var->getSourceRange(), true);
      const std::string LHS_expr= Lexer::getSourceText(CSR, sourceManager_, lopt).str();
      Expr *RHS_expr = borrow_assign -> getRHS();
      const clang::UnaryOperator *bo_stmt = clang::dyn_cast<clang::UnaryOperator>(RHS_expr->IgnoreParenImpCasts());
      Expr *ref_var = bo_stmt->getSubExpr();
      CharSourceRange mut_var_CSR = CharSourceRange(ref_var->getSourceRange(), true);
      const std::string mut_var = Lexer::getSourceText(mut_var_CSR, sourceManager_, lopt).str();
      //cout << "//" << LHS_expr << "//" << endl;
      for(i = 0; i < mut_ref_num; i++){
        if(LHS_expr == mut_reference[i].mut_ref_name){
           linenumber = sourceManager_.getSpellingLineNumber(borrow_assign->getBeginLoc());
           mut_reference[i].linenumber.push_back(linenumber);
           line_var.first = linenumber;
           line_var.second = mut_var;
           mut_reference[i].potential_mut_var.push_back(line_var);
           break;
        }
        if(i == mut_ref_num)
          break;
      }
    }
   
    const VarDecl *mut_decl = Result.Nodes.getNodeAs<VarDecl>("mut_decl");
    if(mut_decl != nullptr){
      int i;
      int linenumber;
      pair<int, string> line_var;
      CharSourceRange mut_var_CSR;
      const DeclRefExpr *pointer_ref;
      const VarDecl *pointer_ref_decl;
      const Expr *pointer_init;
      const clang::UnaryOperator *uo_pointer_init;
      string var_name = mut_decl -> getNameAsString();
      const Expr *init = mut_decl -> getInit();
      const clang::UnaryOperator *bo_init = clang::dyn_cast<clang::UnaryOperator>(init->IgnoreParenImpCasts());
      for(i = 0; i < mut_ref_num; i++){
         if( var_name == mut_reference[i].mut_ref_name){
           if(bo_init){       //int *d = &a;
             Expr *ref_var = bo_init->getSubExpr();
             mut_var_CSR = CharSourceRange(ref_var->getSourceRange(), true);
             const std::string mut_var = Lexer::getSourceText(mut_var_CSR, sourceManager_, lopt).str();
             linenumber = sourceManager_.getSpellingLineNumber(mut_decl->getBeginLoc());
             mut_reference[i].linenumber.push_back(linenumber);
             line_var.first = linenumber;
             line_var.second = mut_var;
             mut_reference[i].potential_mut_var.push_back(line_var);
             break;
           }
           else{           //int *p = &a; int *d = p;  find a;
             pointer_ref = clang::dyn_cast<DeclRefExpr>(init->IgnoreParenImpCasts()); 
             pointer_ref_decl = clang::dyn_cast<VarDecl>(pointer_ref->getDecl());
             pointer_init = pointer_ref_decl -> getInit();
             uo_pointer_init = clang::dyn_cast<clang::UnaryOperator>(pointer_init->IgnoreParenImpCasts());
             while(!uo_pointer_init){   //int *m = &a; int *p = m; int *d = p;  find a;
                 pointer_ref = clang::dyn_cast<DeclRefExpr>(pointer_init->IgnoreParenImpCasts());
                 pointer_ref_decl = clang::dyn_cast<VarDecl>(pointer_ref->getDecl());
                 pointer_init = pointer_ref_decl -> getInit();
                 uo_pointer_init = clang::dyn_cast<clang::UnaryOperator>(pointer_init->IgnoreParenImpCasts());
             }
            linenumber = sourceManager_.getSpellingLineNumber(pointer_ref_decl->getBeginLoc());
            mut_reference[i].linenumber.push_back(linenumber);
            pointer_init = pointer_ref_decl -> getInit();
            uo_pointer_init = clang::dyn_cast<clang::UnaryOperator>(pointer_init->IgnoreParenImpCasts());
            Expr *ref_var_decl = uo_pointer_init->getSubExpr();
            mut_var_CSR = CharSourceRange(ref_var_decl->getSourceRange(), true);
            const std::string mut_var_decl = Lexer::getSourceText(mut_var_CSR, sourceManager_, lopt).str();
            line_var.first = linenumber;
            line_var.second = mut_var_decl;
            mut_reference[i].potential_mut_var.push_back(line_var);
           }
         }  
      }
    }
  } 
};

class FunctionDeclASTConsumer : public clang::ASTConsumer
{

public:
  
   FunctionDeclASTConsumer(Rewriter &R): VarD(R){
  /*------------------------------------------------  设置变量标志--------------------------------------------------*/
    
        //带有&的赋值语句
        Matcher.addMatcher(binaryOperator(isExpansionInMainFile(),
                                          hasOperatorName("="),
                                          hasLHS(expr().bind("LHS_ref_var")),
                                          hasRHS(unaryOperator(hasOperatorName("&"),
                                                               hasUnaryOperand(ignoringParenImpCasts(anyOf(
                                                                     declRefExpr(to(varDecl())),
                                                                     memberExpr(),
                                                                     arraySubscriptExpr()))))))
                                          .bind("borrow_assign"), &VarD);

        //指针声明  int *p = &a;
        Matcher.addMatcher(varDecl(isExpansionInMainFile(),
                                   unless(parmVarDecl()),
                                   hasType(pointerType()),
                                   hasInitializer(expr())).bind("mut_decl"), &VarD);
        
   }
  
  virtual void HandleTranslationUnit(clang::ASTContext &astContext) override
  {
   
    Matcher.matchAST(astContext);
  }

private:
  MatchFinder Matcher;
  VisitVarDecl VarD;
};

class FunctionDeclFrontendAction : public clang::ASTFrontendAction
{
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override{
        rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return make_unique<FunctionDeclASTConsumer>(rewriter_);
    }

private:
   Rewriter rewriter_;
};

int main(int argc, const char **argv)
{
  unsigned long int i, j, k, m;
  string prefix;
  const char *ptr = strrchr(argv[1],'/');
  if (ptr == NULL) {
   prefix = argv[1];
  } else {
   prefix = string(ptr + 1);
  }

  //读取"/home/xiaohua/Documents/-project/llvm/tools/clang/tools/extra/transC2OSL/mut_ref_var.txt"
  ifstream mut_var_in;
  string path1 = "/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_mut_ref_var.txt";
  mut_var_in.open(path1, ifstream::in);
  if (!mut_var_in.is_open()) {
      cout << "Can not find target file." << endl;
  }
  string mut_var_buff;
  while (getline(mut_var_in, mut_var_buff)){
      mut_ref_var[mut_ref_var_count] = mut_var_buff;
      mut_ref_var_count++;
  } 
  mut_var_in.close();
  
  //mutable reference name
  for (i = 0; i < mut_ref_var_count; i = i + 2)
    mut_ref.push_back(mut_ref_var[i]);
  
  std::sort(mut_ref.begin(), mut_ref.end());
  mut_ref.erase(unique(mut_ref.begin(), mut_ref.end()), mut_ref.end());
  
  //the line number of dereference of mutable reference (*b = 7);
  for (i = 1; i < mut_ref_var_count; i = i + 2)
    line.push_back(stoi(mut_ref_var[i]));
  
  //Initializes the structure
  mut_ref_num = mut_ref.size();
  for(i = 0; i < mut_ref_num; i++)
      mut_reference[i].mut_ref_name = mut_ref[i];
  
  for(i = 0; i < mut_ref_var_count; i = i + 2){
    for(j = 0; j < mut_ref_num; j++){
      if(mut_ref_var[i] == mut_reference[j].mut_ref_name)
          (mut_reference[j].linenumber).push_back(stoi(mut_ref_var[i + 1]));
    }
  }
  
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  int result = Tool.run(newFrontendActionFactory<FunctionDeclFrontendAction>().get());
 
  /*for(i = 0; i < mut_ref_num; i++){
     cout << mut_reference[i].mut_ref_name << endl;
    for(j = 0; j < (mut_reference[i].linenumber).size(); j++){
     cout << (mut_reference[i].linenumber)[j] << endl;
    }
 }
 for(i = 0; i < mut_ref_num; i++){
    for(k = 0; k < (mut_reference[i].potential_mut_var).size(); k++)
     cout << (mut_reference[i].potential_mut_var)[k].first << "//" << (mut_reference[i].potential_mut_var)[k].second;
 }*/

for(i = 0; i < mut_ref_num; i++){
  std::sort(mut_reference[i].linenumber.begin(), mut_reference[i].linenumber.end()); 
}

for(i = 0; i < mut_ref_num; i++){   //extract the mutable variables
  if(mut_reference[i].linenumber.size() > 1){
    vector<int> same_line;
    auto iter = set_intersection(line.begin(),line.end(),mut_reference[i].linenumber.begin(),mut_reference[i].linenumber.end(),inserter(same_line,same_line.begin()));
      for(k = 0; k < same_line.size(); k++){
        for(j = 0; j < mut_reference[i].linenumber.size(); j++){
          if(same_line[k] == mut_reference[i].linenumber[j]){
            for(m = 0; m < mut_reference[i].potential_mut_var.size(); m++){
              if(mut_reference[i].linenumber[j - 1] == mut_reference[i].potential_mut_var[m].first){
                 mut_var.push_back(mut_reference[i].potential_mut_var[m].second);
                 break;
              }
            }
            break;
          }
        }
      } 
  }  
}
std::sort(mut_var.begin(), mut_var.end());
mut_var.erase(unique(mut_var.begin(), mut_var.end()), mut_var.end());
//输出可变变量数组到mut_var.txt文件
 FILE *mut_var_fp;
  mut_var_fp = fopen(string("/home/cat409/Programs/xiaohua/3_mut_var_output/" + prefix + "_mut_var.txt").c_str(), "wt");
  if(mut_var_fp == nullptr){
    outs() << "file opening is fail!";
    cout << endl;
    return 0;
  }
  else {
    for(string::size_type i = 0; i != mut_var.size(); i++)
      fprintf(mut_var_fp, "%s\n", mut_var[i].c_str());  
  }
  if(fclose(mut_var_fp) == 0)
  outs() << "mut_var.txt file close success!";
  cout << endl;
  return result;
}
