#include <iostream>
#include <string>
#include <cstdio>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "clang/Driver/Options.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Tooling/RefactoringCallbacks.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Lex/Lexer.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Decl.h"
#include "llvm/IR/Instructions.h"
#include <cstring>

#define MAX 10000
using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;
using namespace std;

static int count_ref = 0;
static int count_for = 0;
static int count_heap = 0;

string ref_var[MAX];
int for_variable[MAX];
string heap_resource[MAX];

static llvm::cl::OptionCategory MyToolCategory("my-tool options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...\n");

class VisitVarDecl : public MatchFinder::MatchCallback {

private:
  Rewriter& rewriter_;
  LangOptions lopt;

public:
  explicit VisitVarDecl(Rewriter &r): rewriter_(r){}
  virtual void run(const MatchFinder::MatchResult &Result){

    ASTContext *Context = Result.Context;
    SourceManager &sourceManager_ = Context->getSourceManager();

  //匹配形如*b = 7；的解引用表达式
   const Stmt *dereference_exp = Result.Nodes.getNodeAs<Stmt>("dereference_exp");
   const VarDecl *mut_vars = Result.Nodes.getNodeAs<VarDecl>("mut_var");
   if(dereference_exp != nullptr && mut_vars != nullptr){  //int *p = &m; int *d = p; *d = 20; 
        ref_var[count_ref] = mut_vars->getName().str();      
        count_ref++;
        ref_var[count_ref] =to_string(sourceManager_.getSpellingLineNumber(dereference_exp -> getBeginLoc()));
        count_ref++;
      }
   //针对结构体字段：匹配形如*(p3->y) = 10;的解引用表达式
   const Stmt *field_dereference = Result.Nodes.getNodeAs<Stmt>("field_dereference"); 
   const Stmt *mut_field = Result.Nodes.getNodeAs<Stmt>("mut_field"); 
   if(field_dereference != nullptr && mut_field != nullptr){
        CharSourceRange CSR = CharSourceRange(mut_field->getSourceRange(), true);
        const std::string varname = Lexer::getSourceText(CSR, sourceManager_, lopt).str();
        ref_var[count_ref] = varname;    
        count_ref++;
        ref_var[count_ref] =to_string(sourceManager_.getSpellingLineNumber(field_dereference -> getBeginLoc()));
        count_ref++;
     }
   //匹配通过指针给数组赋值：行如*(gm+i) = i;的赋值
   const Stmt *array_dereference = Result.Nodes.getNodeAs<Stmt>("array_dereference"); 
   const VarDecl *array_var = Result.Nodes.getNodeAs<VarDecl>("array_var");
   if(array_dereference != nullptr && array_var != nullptr){
        ref_var[count_ref] = array_var->getName().str();      
        count_ref++;
      }
    //匹配形如指针数组的赋值：*myint[2] = 4;
   const Stmt *pointer_array_assign = Result.Nodes.getNodeAs<Stmt>("pointer_array_assign");
   const ArraySubscriptExpr *arraySubscriptExpr = Result.Nodes.getNodeAs<ArraySubscriptExpr>("arraySubscriptExpr");
   if(pointer_array_assign != nullptr && arraySubscriptExpr != nullptr){
       CharSourceRange arraySubscriptExpr_CSR = CharSourceRange(arraySubscriptExpr->getSourceRange(), true);
       ref_var[count_ref] = Lexer::getSourceText(arraySubscriptExpr_CSR, sourceManager_, lopt).str(); //数组表达式
       count_ref++;
   }
   //for语句匹配(含有变量声明的for语句)
   const ForStmt *loop_stmt = Result.Nodes.getNodeAs<ForStmt>("loop_stmt");
   const CompoundStmt *for_body = Result.Nodes.getNodeAs<CompoundStmt>("for_body");
   const VarDecl *for_var = Result.Nodes.getNodeAs<VarDecl>("for_var");
   if (loop_stmt != nullptr && for_body != nullptr && for_var != nullptr && 
                         sourceManager_.isWrittenInMainFile(loop_stmt->getForLoc())){
        for_variable[count_for] = sourceManager_.getSpellingLineNumber(for_var->getBeginLoc());
        count_for++;
   }
   //for语句匹配(含有赋值语句的for语句)
   const ForStmt *for_assign_loop = Result.Nodes.getNodeAs<ForStmt>("for_assign_loop");
   const CompoundStmt *for_assign_body = Result.Nodes.getNodeAs<CompoundStmt>("for_assign_body");
   const clang::BinaryOperator *for_assign = Result.Nodes.getNodeAs<clang::BinaryOperator>("for_assign");
   if (for_assign_loop != nullptr && for_assign_body != nullptr && for_assign != nullptr && 
                         sourceManager_.isWrittenInMainFile(for_assign_loop->getForLoc())){
        for_variable[count_for] = sourceManager_.getSpellingLineNumber(for_assign->getBeginLoc());
        count_for++;
   }
   
   //赋值语句右值为malloc等内存分配函数。
   const Stmt *assignment_type3 = Result.Nodes.getNodeAs<Stmt>("assignment_type3");
   const Stmt *LHS_type3 = Result.Nodes.getNodeAs<Stmt>("LHS_type3");
   const FunctionDecl *memory_name = Result.Nodes.getNodeAs<FunctionDecl>("memory_name");
   if (assignment_type3 != nullptr && LHS_type3 != nullptr && memory_name !=nullptr){
       CharSourceRange malloc_in_assign = CharSourceRange(LHS_type3->getSourceRange(), true);
       heap_resource[count_heap] = Lexer::getSourceText(malloc_in_assign, sourceManager_, lopt).str(); 
       count_heap++;
   }

   //带有分配函数的指针声明
   const VarDecl *memory_in_vardecl = Result.Nodes.getNodeAs<VarDecl>("memory_in_vardecl");
   if(memory_in_vardecl){
      heap_resource[count_heap] = memory_in_vardecl -> getNameAsString();
      count_heap++;
   }
  } 
};

class FunctionDeclASTConsumer : public clang::ASTConsumer
{

public:
  
   FunctionDeclASTConsumer(Rewriter &R): VarD(R){
  /*------------------------------------------------  设置变量标志--------------------------------------------------*/
        //匹配所有变量
        Matcher.addMatcher(varDecl(isExpansionInMainFile()).bind("all_variables"), &VarD);
        //匹配形如*b = 7；的解引用表达式
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(unaryOperator(hasOperatorName("*"),
                                                               hasUnaryOperand(ignoringParenImpCasts(
                                                               declRefExpr(to(varDecl().bind("mut_var"))))))))
                                          .bind("dereference_exp"), &VarD);
        //针对结构体字段：匹配形如*(p3->y) = 10;的解引用表达式
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(unaryOperator(hasOperatorName("*"),
                                                               hasUnaryOperand(ignoringParenImpCasts(
                                                                  memberExpr().bind("mut_field")))))).bind("field_dereference") , &VarD);
        //匹配通过指针给数组赋值：行如*(gm+i) = i;的赋值
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(unaryOperator(hasOperatorName("*"),
                                                               hasUnaryOperand(ignoringParenImpCasts(binaryOperator(
                                                                                                           hasOperatorName("+"),
                                                                                                           hasLHS(ignoringParenImpCasts(declRefExpr(
                                                                                                              to(varDecl().bind("array_var"))))),
                                                                                                           hasRHS(ignoringParenImpCasts(declRefExpr(
                                                                                                              to(varDecl(hasType(isInteger()))))))))))))
                                          .bind("array_dereference"), &VarD);
        //匹配形如指针数组的赋值：*myint[2] = 4;
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(unaryOperator(hasOperatorName("*"),
                                          hasUnaryOperand(ignoringParenImpCasts(arraySubscriptExpr().bind("arraySubscriptExpr"))))))
                                          .bind("pointer_array_assign"), &VarD);
        //for语句匹配(含有变量声明的for语句)
        Matcher.addMatcher(forStmt(isExpansionInMainFile(),
                                   hasLoopInit(declStmt(hasSingleDecl(varDecl().bind("for_var")))),
                                   hasBody(compoundStmt().bind("for_body"))).bind("loop_stmt"), &VarD);

        //for语句匹配(含有赋值语句的for语句)
        Matcher.addMatcher(forStmt(isExpansionInMainFile(),
                                   hasLoopInit(binaryOperator(hasOperatorName("=")).bind("for_assign")),
                                   hasBody(compoundStmt().bind("for_assign_body"))).bind("for_assign_loop"), &VarD);

        //匹配所有malloc等内存分配的变量，即找到存储在堆里的变量
           //赋值语句右值为malloc等内存分配函数。transfer new_resource pm; 
        Matcher.addMatcher(binaryOperator(hasOperatorName("="),
                                          hasLHS(expr(isExpansionInMainFile()).bind("LHS_type3")),
                                          hasRHS(ignoringParenCasts(callExpr(
                                          callee(functionDecl(anyOf(hasName("malloc"),hasName("calloc"),
                                          hasName("realloc"))).bind("memory_name"))))))
                                          .bind("assignment_type3"), &VarD);
         //带有分配函数的指针声明
        Matcher.addMatcher(varDecl(isExpansionInMainFile(),
                                          hasType(pointerType()),
                                          hasInitializer(ignoringParenCasts(callExpr(
                                               callee(functionDecl(anyOf(hasName("malloc"),hasName("calloc"),
                                                                         hasName("realloc"))).bind("memory_call"))))))
                                  .bind("memory_in_vardecl"), &VarD);
   }
  
  virtual void HandleTranslationUnit(clang::ASTContext &astContext) override
  {
   
    Matcher.matchAST(astContext);
  }

private:
  MatchFinder Matcher;
  VisitVarDecl VarD;
};

class FunctionDeclFrontendAction : public clang::ASTFrontendAction
{
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override{
        rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return make_unique<FunctionDeclASTConsumer>(rewriter_);
    }

private:
   Rewriter rewriter_;
};


int main(int argc, const char **argv)
{
  
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  int result = Tool.run(newFrontendActionFactory<FunctionDeclFrontendAction>().get());
  
  string prefix;
  
  const char *ptr = strrchr(argv[1],'/');
  if (ptr == NULL) {
   prefix = argv[1];
  } else {
   prefix = string(ptr + 1);
  }
  
  
 
//输出可变引用数组到mut_ref_var.txt文件
 int i;
 FILE *mut_ref_var_fp;
 mut_ref_var_fp = fopen(string("/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_mut_ref_var.txt").c_str(), "wt");
  if(mut_ref_var_fp == nullptr){
    outs() << "file opening is fail!";
    cout << endl;
    return 0;
  }
  else {
    for(i = 0; i < count_ref; i++)
      fprintf(mut_ref_var_fp, "%s\n", ref_var[i].c_str());
    
  }
  if(fclose(mut_ref_var_fp) == 0)
  outs() << "mut_ref_var.txt file close success!";
  cout << endl; 

  //输出for语句中声明的变量到for_decl_var.txt文件
 FILE *for_decl_var_fp;
 for_decl_var_fp = fopen(string("/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_for_decl_var.txt").c_str(), "wt");
  if(for_decl_var_fp == nullptr){
    outs() << "file opening is fail!";
    cout << endl;
    return 0;
  }
  else {
    for(i = 0; i < count_for; i++)
      fprintf(for_decl_var_fp, "%d\n", for_variable[i]);
    
  }
  if(fclose(for_decl_var_fp) == 0)
  outs() << "for_decl_var.txt file close success!";
  cout << endl; 

  //输出在堆上分配资源的变量到heap_resource.txt文件
  FILE *heap_resource_fp;
 heap_resource_fp = fopen(string("/home/cat409/Programs/xiaohua/2_all_var_output/" + prefix + "_heap_resource.txt").c_str(), "wt");
  if(heap_resource_fp == nullptr){
    outs() << "file opening is fail!";
    cout << endl;
    return 0;
  }
  else {
    for(i = 0; i < count_heap; i++)
      fprintf(heap_resource_fp, "%s\n", heap_resource[i].c_str());
    
  }
  if(fclose(heap_resource_fp) == 0)
  outs() << "heap_resource.txt file close success!";
  cout << endl;
  return result;
}
