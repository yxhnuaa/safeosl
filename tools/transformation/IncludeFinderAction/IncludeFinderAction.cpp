#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include "clang/Frontend/CompilerInstance.h"
#include <clang/Frontend/FrontendActions.h>
#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/PPCallbacks.h"
#include "clang/Lex/Token.h"
#include "clang/Lex/MacroInfo.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/LangOptions.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Rewrite/Core/Rewriter.h"

using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace std;


void print_osl(Rewriter &rewriter_, CharSourceRange CSR, SourceManager &sm, LangOptions lopt){
        clang::Rewriter::RewriteOptions RO;
        RO.IncludeInsertsAtBeginOfRange = true;
        RO.IncludeInsertsAtEndOfRange = true;
        RO.RemoveLineIfEmpty = true;
        rewriter_.setSourceMgr(sm,lopt);
        rewriter_.RemoveText(CSR, RO);
        const clang::FileID mainFile = sm.getMainFileID();
        std::string name = sm.getFileEntryForID(mainFile)->getName().str();
        //outs() << "/"+ name + "/";
        string prefix;
        const char *ptr = strrchr(name.c_str(),'/');
        prefix = string(ptr + 1);
        prefix = prefix.substr(0, prefix.length() - 17);
        SourceManager &SM = rewriter_.getSourceMgr();
        std::error_code error_code;
        llvm::raw_fd_ostream outFile("/home/cat409/Programs/xiaohua/5_include_output/" + prefix + "_complete_osl.c", error_code, llvm::sys::fs::F_None);
        rewriter_.getEditBuffer(SM.getMainFileID()).write(outFile); 
        outFile.close();
}

class IncludeFinderAction : public clang::PreprocessOnlyAction
{
protected:
    virtual void ExecuteAction();
};


class CallbacksProxy : public clang::PPCallbacks
{
public:
    inline CallbacksProxy(clang::PPCallbacks &master);

public:
    //#include，#import...声明
    virtual inline void InclusionDirective(clang::SourceLocation hashLoc,
                                           const clang::Token &includeTok,
                                           clang::StringRef fileName,
                                           bool isAngled,
                                           clang::CharSourceRange filenameRange,
                                           const clang::FileEntry *file,
                                           clang::StringRef searchPath,
                                           clang::StringRef relativePath,
                                           const clang::Module *imported,
                                           clang::SrcMgr::CharacteristicKind filetype);
    //#define声明
    virtual inline void MacroDefined(const clang::Token &MacroNameTok, const MacroDirective *MD);
    //#ifdef声明

private:
    clang::PPCallbacks &master;
};

inline CallbacksProxy::CallbacksProxy(clang::PPCallbacks &master) : master(master){}

inline void CallbacksProxy::InclusionDirective(clang::SourceLocation hashLoc,
                                   const clang::Token &includeTok,
                                   clang::StringRef fileName,
                                   bool isAngled,
                                   clang::CharSourceRange filenameRange,
                                   const clang::FileEntry *file,
                                   clang::StringRef searchPath,
                                   clang::StringRef relativePath,
                                   const clang::Module *imported,
                                   clang::SrcMgr::CharacteristicKind filetype){
    master.InclusionDirective(hashLoc, includeTok, fileName, isAngled, filenameRange, file, searchPath, relativePath, imported, filetype);
}

inline void CallbacksProxy::MacroDefined(const clang::Token &MacroNameTok, const MacroDirective *MD){
    master.MacroDefined(MacroNameTok, MD);
}

class IncludeFinder : private clang::PPCallbacks
{
public:
    explicit inline IncludeFinder(const clang::CompilerInstance &compiler);

public:
    inline clang::PPCallbacks * createPreprocessorCallbacks();
    virtual inline void InclusionDirective(clang::SourceLocation hashLoc,
                                           const clang::Token &includeTok,
                                           clang::StringRef fileName,
                                           bool isAngled,
                                           clang::CharSourceRange filenameRange,
                                           const clang::FileEntry *file,
                                           clang::StringRef searchPath,
                                           clang::StringRef relativePath,
                                           const clang::Module *imported,
                                           clang::SrcMgr::CharacteristicKind filetype);

    virtual inline void MacroDefined(const clang::Token &MacroNameTok, const MacroDirective *MD);
private:
    const clang::CompilerInstance &compiler;
    std::string name;
    LangOptions lopt;
    Rewriter rewriter_;
};

inline IncludeFinder::IncludeFinder(const clang::CompilerInstance &compiler) : compiler(compiler){
    const clang::FileID mainFile = compiler.getSourceManager().getMainFileID();
    name = compiler.getSourceManager().getFileEntryForID(mainFile)->getName().str();
   // outs() << "/"+ name + "/";
}

inline clang::PPCallbacks *IncludeFinder::createPreprocessorCallbacks(){
    return new CallbacksProxy(*this);
}

//#include，#import...声明        
inline void IncludeFinder::InclusionDirective(clang::SourceLocation hashLoc,
                                  const clang::Token &includeTok,
                                  clang::StringRef fileName,
                                  bool isAngled,
                                  clang::CharSourceRange filenameRange,
                                  const clang::FileEntry *file,
                                  clang::StringRef searchPath,
                                  clang::StringRef relativePath,
                                  const clang::Module *imported, 
                                  clang::SrcMgr::CharacteristicKind filetype){ 
    
    clang::SourceManager &sm = compiler.getSourceManager();
    if (sm.isInMainFile(hashLoc)) {   
        SourceLocation include_end = clang::Lexer::getLocForEndOfToken(filenameRange.getEnd(), 0, sm, lopt).getLocWithOffset(-1);
        SourceRange range(hashLoc, include_end);
        const std::string vardecl_stmt = Lexer::getSourceText(CharSourceRange(range, true), sm, lopt).str();
        outs() << vardecl_stmt;
        cout<<endl;
  
        CharSourceRange CSR1 = CharSourceRange(range, true);
        //output
        print_osl(rewriter_, CSR1, sm, lopt);
    }
 }

//#define声明
inline void IncludeFinder::MacroDefined(const clang::Token &MacroNameTok, const MacroDirective *MD){
    clang::SourceManager &sm = compiler.getSourceManager();
    SourceLocation begin = sm.getSpellingLoc(MD->getLocation());
    if (sm.isWrittenInMainFile(begin)){
        
    }
      
}

void IncludeFinderAction::ExecuteAction(){
    
    IncludeFinder includeFinder(getCompilerInstance());
    getCompilerInstance().getPreprocessor().addPPCallbacks(std::unique_ptr<clang::PPCallbacks> (includeFinder.createPreprocessorCallbacks()));
    clang::PreprocessOnlyAction::ExecuteAction();
}


static llvm::cl::OptionCategory toolCategory("includeFinderAction options");
static llvm::cl::extrahelp commonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...\n");

int main(int argc, const char *argv[])
{

   CommonOptionsParser optionsParser(argc, argv, toolCategory);
   ClangTool tool(optionsParser.getCompilations(), optionsParser.getSourcePathList());
   return tool.run(newFrontendActionFactory<IncludeFinderAction>().get());
}
