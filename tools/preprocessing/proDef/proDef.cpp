#include<algorithm>
#include<fstream>
#include<sstream>
#include<map>
#include <iostream>
#include <string>
#include <cstdio>
#include<cstdlib>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "clang/Driver/Options.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Tooling/RefactoringCallbacks.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Lex/Lexer.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Decl.h"
#include "llvm/IR/Instructions.h"
#include <cstring>
#include<vector>
#include<map>

#define MAX 10000
using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;
using namespace std;

static int doNumber = 0;
static string SourceName;

string ref_var[MAX];
int for_variable[MAX];
string heap_resource[MAX];

static llvm::cl::OptionCategory MyToolCategory("my-tool options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...\n");

class VisitVarDecl : public MatchFinder::MatchCallback {

private:
  Rewriter& rewriter_;
  LangOptions lopt;

public:
  explicit VisitVarDecl(Rewriter &r): rewriter_(r){}

  virtual void run(const MatchFinder::MatchResult &Result){
  }
};

class FunctionDeclASTConsumer : public clang::ASTConsumer
{

public:

   FunctionDeclASTConsumer(Rewriter &R): VarD(R){

   }

  virtual void HandleTranslationUnit(clang::ASTContext &astContext) override
  {

    Matcher.matchAST(astContext);
  }

private:
  MatchFinder Matcher;
  VisitVarDecl VarD;
};

class FunctionDeclFrontendAction : public clang::ASTFrontendAction
{
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override{
        rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return std::make_unique<FunctionDeclASTConsumer>(rewriter_);
    }

private:
   Rewriter rewriter_;
};

string deleteChar(string str, char c){
    string::iterator it;
    for (it = str.begin(); it != str.end(); ++it){
        if ( *it == c){
            str.erase(it);
            it--;
        }
    }
    return str;
}

int Lastindex(string s, char c){
    int res = 0;
    for(int i = 0;i < s.length(); i++){
        if(s[i] == c && i > res){
            res = i;
        }
    }
    return res;
}

class DefProcesser{

public:
    string path;

    DefProcesser(string path){
        this->path = path;
    };

    string outBlank(string str){     //打印string最开始的空格
        string res = "";
        for(int i = 0; i < str.length(); i++){
            if(int(str[i]) == 32){   //该字符是空格
                res += " ";
            }
            else
                break;
        }
        return res;
    };

    bool isVarChar(char c){
        if(c >= 'a' && c <= 'z')
            return true;
        else if(c >= 'A' && c <= 'Z')
            return true;
        else if((c >= '0' && c <= '9') || c == '_')
            return true;
        else
            return false;
    };

    string FindInMap(string word, map<string, string> m){
        if(m.size() == 0)
            return word;
        map<string, string>::iterator iter = m.begin();
        while(iter != m.end()){
            if(word.find(iter -> first) != string::npos){
                int index = 0,
                keyLength = (iter -> first).length();
                while((index = word.find(iter -> first, index))!=string::npos){
                    if(index == 0 && index + keyLength < word.length()){
                        if(!isVarChar(word[index + keyLength])){
                            word.replace(index, keyLength, m[iter -> first]);
                        }
                    }
                    else if(index > 0 && index + keyLength == word.length()){
                        if(!isVarChar(word[index - 1])){
                            word.replace(index, keyLength, m[iter -> first]);
                        }
                    }
                    else if(!isVarChar(word[index - 1]) && !isVarChar(word[index + keyLength]))
                        word.replace(index, keyLength, m[iter -> first]);
                    index++;
                }
            }
            iter++;
        }
        return word;
    };

    bool isDefined(vector<string> vec, string s){
        if(vec.size() == 0)
            return false;
        for(int i = 0; i < vec.size(); i++){
            if(vec[i] == s)
                return true;
        }
        return false;
    };

    string getSourceName(){
        int pos1 = path.rfind("/"),
            pos2 = path.rfind(".");
        return path.substr(pos1 + 1, pos2 - pos1 - 1);
    };

    string getSourceType(){
        int pos = path.rfind(".");
        return path.substr(pos + 1, path.length() - pos);
    };

    void proDef(){
        map<string, string> defMap;   //记录型如"#define N 100"
        vector<string> defVec;    //记录型如"#define DEBUG"
        defVec.push_back("INCLUDEMAIN");
        int defCounter = 0;
        string line, word;
        string outname = "../PreproC_out/" + this->getSourceName() + "." + this->getSourceType();
        ifstream infile(this->path);
        ofstream outfile(outname);
        if(infile.is_open()){
            cout<<"******File Open Successfully!******\n";
        }
        else{
            cout<<"******File Open Failed!******\n";
            return;
        }
        bool isEnterMain = false; //是否进入主函数
        bool isIfdefValid = true; //ifdef是否满足
        bool isinIfdef = false;   //endif判断标志
        bool isinCase = false;
        while(getline(infile, line)){  //逐行读
            //cout << "该行长度:" << line.length() << "首字母:" << int(line[0]) << "---->" << line << endl;
            if(line.length() == 0 || (line.length() == 1 && int(line[0]) == 13))
                continue;
            if(line.find("int main()") != string::npos)
                isEnterMain = true;   //表示已进入main函数
            istringstream istrm(line);
            string outline = outBlank(line);
            vector<string> oneline;
            //将一行单词存入vector
            while(istrm >> word){
                oneline.push_back(word);
            }
            if(oneline[0] == "#endif"){     //该行是#endif
                defCounter--;
                if(defCounter == 0 && isinIfdef){
                    isIfdefValid = true;
                    isinIfdef = false;
                }
                outline = outline + "//" + line;
                outfile << outline << endl;
                continue;
            }
            if(oneline[0] == "#ifdef"){     //该行是#ifdef
                outline = outline + "//" + line;
                if(isinIfdef)
                    defCounter++;
                if(!isDefined(defVec, oneline[1])){  //#ifdef不满足
                    isIfdefValid = false;
                    if(!isinIfdef){
                        defCounter++;
                        isinIfdef = true;
                    }
                }
                outfile << outline << endl;
                continue;
            }
            if(oneline[0] == "#ifndef"){     //该行是#ifndef
                outline = outline + "//" + line;
                if(isinIfdef)
                    defCounter++;
                if(isDefined(defVec, oneline[1])){  //#ifndef不满足
                    isIfdefValid = false;
                    if(!isinIfdef){
                        defCounter++;
                        isinIfdef = true;
                    }
                }
                outfile << outline << endl;
                continue;
            }
            if(!isIfdefValid){
                outline = outline + "//" + line;
                outfile << outline << endl;
                continue;
            }
            if(oneline[0] == "#define" && !isEnterMain){    //该行是#define ......
                if(oneline.size() == 2){      //型如#define DEBUG
                    defVec.push_back(oneline[1]);
                }
                else if(oneline.size() == 3){  //型如#define N 100
                    defMap.insert(pair<string, string>(oneline[1], oneline[2]));
                }
                outline = outline + "//" + line;
                outfile << outline << endl;
                continue;
            }
            if(oneline[0].find("case") != string::npos || oneline[0].find("default") != string::npos){      //该行是case或者default
                isinCase = true;
                outline = outline + line + "{";
                outfile << outline << endl;
                continue;
            }
            if(oneline[0].find("break") != string::npos && isinCase){  //case里的break
                isinCase = false;
                outline = outline + line + "}";
                outfile << outline << endl;
                continue;
            }

            for(int i = 0; i < oneline.size(); i++){   //替换行中所有被宏定义的变量
                outline = outline + FindInMap(oneline[i], defMap) + " ";
            }
            outfile << outline << endl;
        }
        infile.close();
        outfile.close();
        cout<<"******proDef Done!******\n";
    };
};

int main(int argc, const char **argv)
{
    CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
    ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
    int result = Tool.run(newFrontendActionFactory<FunctionDeclFrontendAction>().get());
    string path = OptionsParser.getSourcePathList()[0];
    int p1 = Lastindex(path, '/') + 1,
           p2 = Lastindex(path, '.');
    string SourceName = path.substr(p1, p2 - p1);
    DefProcesser defpro(path);
    defpro.proDef();
}
