#include <iostream>
#include <string>
#include <cstdio>
#include<cstdlib>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "clang/Driver/Options.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Tooling/RefactoringCallbacks.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Lex/Lexer.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Decl.h"
#include "llvm/IR/Instructions.h"
#include <cstring>
#include<vector>

#define MAX 10000
using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;
using namespace std;

static int doNumber = 0;
static string SourceName;

string ref_var[MAX];
int for_variable[MAX];
string heap_resource[MAX];

static llvm::cl::OptionCategory MyToolCategory("my-tool options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...\n");

class VisitVarDecl : public MatchFinder::MatchCallback {

private:
  Rewriter& rewriter_;
  LangOptions lopt;

public:
  explicit VisitVarDecl(Rewriter &r): rewriter_(r){}

  virtual void run(const MatchFinder::MatchResult &Result){

    ASTContext *Context = Result.Context;
    SourceManager &sourceManager_ = Context->getSourceManager();
   const DoStmt *do_while_stmt = Result.Nodes.getNodeAs<DoStmt>("do_while_stmt");
   const CompoundStmt *do_body = Result.Nodes.getNodeAs<CompoundStmt>("do_body");
   if(do_while_stmt != nullptr && do_body != nullptr){
        doNumber++;
   }
  }
};

class FunctionDeclASTConsumer : public clang::ASTConsumer
{

public:

   FunctionDeclASTConsumer(Rewriter &R): VarD(R){
  /*------------------------------------------------  设置变量标志--------------------------------------------------*/
        //匹配do-while语句
        Matcher.addMatcher(doStmt(isExpansionInMainFile(),
                                                      hasBody(compoundStmt().bind("do_body")),
                                                      hasCondition(binaryOperator().bind("do_condition_operator"))
                                 ).bind("do_while_stmt"), &VarD);
   }

  virtual void HandleTranslationUnit(clang::ASTContext &astContext) override
  {

    Matcher.matchAST(astContext);
  }

private:
  MatchFinder Matcher;
  VisitVarDecl VarD;
};

class FunctionDeclFrontendAction : public clang::ASTFrontendAction
{
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override{
        rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return std::make_unique<FunctionDeclASTConsumer>(rewriter_);
    }
/*
    void EndSourceFileAction() override {
        SourceManager &SM = rewriter_.getSourceMgr();
        std::error_code error_code;
        llvm::raw_fd_ostream outFile("/home/andrew/llvm+clang11.0/out/o1.cpp", error_code, llvm::sys::fs::F_None);
        rewriter_.getEditBuffer(SM.getMainFileID()).write(outFile); // --> this will write the result to outFile
        outFile.close();
    }
*/
private:
   Rewriter rewriter_;
};

int Lastindex(string s, char c){
    int res = 0;
    for(int i = 0;i < s.length(); i++){
        if(s[i] == c && i > res){
            res = i;
        }
    }
    return res;
}

int main(int argc, const char **argv)
{
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  int result = Tool.run(newFrontendActionFactory<FunctionDeclFrontendAction>().get());
  string path = OptionsParser.getSourcePathList()[0];
    int p1 = Lastindex(path, '/') + 1,
         p2 = Lastindex(path, '.');
  SourceName = path.substr(p1, p2 - p1);
  if(doNumber == 0){
    cout << "\n******Find 0 Do-while!*******\n";
    string cpcommand = "cp ../PreproC_out/" + SourceName + ".cpp " + "../PreproC_out/" + SourceName + "_out.cpp";
    const char *cp = cpcommand.data();
    system(cp);
    return 0;
  }
  cout << "\n******Find " << doNumber << " Do-while!******\n";
  string precommand = "dowhilePre " + path,
                execommand = "dowhileExec " + path;
  const char *pre = precommand.data(),
                         *exe = execommand.data();
  system(pre);
  system(exe);
  doNumber--;
  precommand = "dowhilePre ../PreproC_out/" + SourceName + "_out.cpp";
  execommand = "dowhileExec ../PreproC_out/" + SourceName + "_out.cpp";
  const char *pre1 = precommand.data(),
                          *exe1 = execommand.data();
  while(doNumber > 0){
    system(pre1);
    system(exe1);
    doNumber--;
  }
  system("rm ../PreproC_out/otemp.cpp");
  system("rm ../PreproC_out/otemp");             //删除预处理用的临时文件
}
