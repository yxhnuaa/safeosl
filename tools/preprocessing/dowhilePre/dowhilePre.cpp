#include <iostream>
#include <string>
#include <cstdio>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "llvm/IR/Type.h"
#include "clang/Driver/Options.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/Core/Replacement.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Tooling/RefactoringCallbacks.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Lex/Lexer.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Decl.h"
#include "llvm/IR/Instructions.h"
#include <cstring>

#define MAX 10000
using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;
using namespace std;

static int count_do= 0;

string ref_var[MAX];
int for_variable[MAX];
string heap_resource[MAX];
string path;

static llvm::cl::OptionCategory MyToolCategory("my-tool options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...\n");

class VisitVarDecl : public MatchFinder::MatchCallback {

private:
  Rewriter& rewriter_;
  LangOptions lopt;

public:
  explicit VisitVarDecl(Rewriter &r): rewriter_(r){}

  virtual void run(const MatchFinder::MatchResult &Result){

    ASTContext *Context = Result.Context;
    SourceManager &sourceManager_ = Context->getSourceManager();

   //提取while条件，在do之前判断
   const DoStmt *do_while_stmt = Result.Nodes.getNodeAs<DoStmt>("do_while_stmt");
   const CompoundStmt *do_body = Result.Nodes.getNodeAs<CompoundStmt>("do_body");
   if(do_while_stmt != nullptr && do_body != nullptr && count_do == 0){
        count_do++;
        SourceLocation whileBegin = do_while_stmt->getWhileLoc().getLocWithOffset(5),
                                          whileEnd = do_while_stmt->getEndLoc(),    //while条件始末位置
                                          doBegin = do_while_stmt->getDoLoc();         //do始位置
        SourceRange whileRange = SourceRange(whileBegin, whileEnd);
        string doCondition = rewriter_.getRewrittenText(whileRange);
        string insertText = "int WHILE_VALID_FLAG = 0;\nif" + doCondition + "\nWHILE_VALID_FLAG = 1;\nreturn WHILE_VALID_FLAG;\n";
        /*
        WHILE_VALID_FLAG = 1: 满足while条件，直接改写
        WHILE_VALID_FLAG = 0: 不满足，需先运行一次do
        */
        rewriter_.InsertTextBefore(doBegin, insertText);
        return;   //只处理一个do-while
   }
  }
};

class FunctionDeclASTConsumer : public clang::ASTConsumer
{

public:

   FunctionDeclASTConsumer(Rewriter &R): VarD(R){
        //匹配do-while语句
        Matcher.addMatcher(doStmt(isExpansionInMainFile(),
                                                      hasBody(compoundStmt().bind("do_body"))
                                 ).bind("do_while_stmt"), &VarD);
   }

  virtual void HandleTranslationUnit(clang::ASTContext &astContext) override
  {

    Matcher.matchAST(astContext);
  }

private:
  MatchFinder Matcher;
  VisitVarDecl VarD;
};

class FunctionDeclFrontendAction : public clang::ASTFrontendAction
{
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(CompilerInstance &CI, StringRef file) override{
        rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
        return std::make_unique<FunctionDeclASTConsumer>(rewriter_);
    }

    void EndSourceFileAction() override {
        SourceManager &SM = rewriter_.getSourceMgr();
        std::error_code error_code;
        llvm::raw_fd_ostream outFile("/home/andrew/llvm+clang11.0/PreproC_out/otemp.cpp", error_code, llvm::sys::fs::F_None);
        rewriter_.getEditBuffer(SM.getMainFileID()).write(outFile); // --> this will write the result to outFile
        outFile.close();
    }

private:
   Rewriter rewriter_;
};

int main(int argc, const char **argv)
{
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  path = OptionsParser.getSourcePathList()[0];
  int result = Tool.run(newFrontendActionFactory<FunctionDeclFrontendAction>().get());
}
