
/*

Rust programs

let mut number = 3;
while number != 0 {
    println!("{}!", number);
    number = number - 1;
}

*/

decl number ;

transfer newResource(copy) number ;

! { read(number) ; read(number) ; } ;