open Prelude
open Constants
open Constants.K
module Def = struct
let freshFunction (sort: string) (config: k) (counter: Z.t) : k = interned_bottom
let eval'Hash'argv (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'argv and sort = 
SortList in match c with 
| _ -> try KREFLECTION.hook_argv c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Hash'argv : k Lazy.t = lazy (eval'Hash'argv () interned_bottom (-1))
let evalisBool (c: k) (config: k) (guard: int) : k = let lbl = 
LblisBool and sort = 
SortBool in match c with 
| [Bool _] -> [Bool true]
(*{| rule ``isBool(#KToken(#token("Bool","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBool, var__1857) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBool(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1858)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_andBool_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_andBool_ and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_and c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_andBool_`(_9,#token("false","Bool"))=>#token("false","Bool")`` requires isBool(_9) ensures #token("true","Bool") [UNIQUE_ID(1a7512841ede635d09556466797d23f3f3cec57fe0fb6a68ce0c3a1cccb0b68f) contentStartColumn(8) contentStartLine(305) org.kframework.attributes.Location(Location(305,8,305,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as var_9_1859) :: []),((Bool false) :: [])) when true && (true) -> ((Bool false) :: [])
(*{| rule `` `_andBool_`(B,#token("true","Bool"))=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(b598bf51d1c959b7112e06b7f85a391ee5a18108acd52bb65ea27ef0381ed0e0) contentStartColumn(8) contentStartLine(303) org.kframework.attributes.Location(Location(303,8,303,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB_1860) :: []),((Bool true) :: [])) when true && (true) -> (varB_1860 :: [])
(*{| rule `` `_andBool_`(#token("true","Bool"),B)=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(cd0a9b340fae24c9a05126d8df860cb7145fc64875711e36efaa694085559dc0) contentStartColumn(8) contentStartLine(302) org.kframework.attributes.Location(Location(302,8,302,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool true) :: []),((Bool _ as varB_1861) :: [])) when true && (true) -> (varB_1861 :: [])
(*{| rule `` `_andBool_`(#token("false","Bool"),_7)=>#token("false","Bool")`` requires isBool(_7) ensures #token("true","Bool") [UNIQUE_ID(e6ebe927ba416a1602679eb0166f22d394adf70452e0505c00f11c036a896253) contentStartColumn(8) contentStartLine(304) org.kframework.attributes.Location(Location(304,8,304,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: []),((Bool _ as var_7_1862) :: [])) when true && (true) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisId (c: k) (config: k) (guard: int) : k = let lbl = 
LblisId and sort = 
SortBool in match c with 
(*{| rule ``isId(#KToken(#token("Id","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortId, var__1863) :: [])) -> ((Bool true) :: [])
(*{| rule ``isId(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1864)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisInt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisInt and sort = 
SortBool in match c with 
| [Int _] -> [Bool true]
(*{| rule ``isInt(#KToken(#token("Int","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortInt, var__1865) :: [])) -> ((Bool true) :: [])
(*{| rule ``isInt(#cint(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isInt(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'cint,((Int _ as varK0_1866) :: []),((Int _ as varK1_1867) :: [])) :: [])) when ((true) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isInt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1868)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisLifetime (c: k) (config: k) (guard: int) : k = let lbl = 
LblisLifetime and sort = 
SortBool in match c with 
| [Int _] -> [Bool true]
(*{| rule ``isLifetime(#KToken(#token("Lifetime","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortLifetime, var__1869) :: [])) -> ((Bool true) :: [])
(*{| rule ``isLifetime(`'__OSL-SYNTAX`(K0))=>#token("true","Bool")`` requires isId(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Apos'__OSL'Hyph'SYNTAX,(varK0_1870 :: [])) :: [])) when (isTrue (evalisId((varK0_1870 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isLifetime(#cint(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isInt(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'cint,((Int _ as varK0_1871) :: []),((Int _ as varK1_1872) :: [])) :: [])) when ((true) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isLifetime(#KToken(#token("Int","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortInt, var__1873) :: [])) -> ((Bool true) :: [])
(*{| rule ``isLifetime(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1874)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisProp (c: k) (config: k) (guard: int) : k = let lbl = 
LblisProp and sort = 
SortBool in match c with 
(*{| rule ``isProp(`mut_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lblmut_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isProp(`copy_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lblcopy_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isProp(#KToken(#token("Prop","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortProp, var__1875) :: [])) -> ((Bool true) :: [])
(*{| rule ``isProp(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1876)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisProps (c: k) (config: k) (guard: int) : k = let lbl = 
LblisProps and sort = 
SortBool in match c with 
(*{| rule ``isProps(#props(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isProp(K0),isProps(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'props,(varK0_1877 :: []),(varK1_1878 :: [])) :: [])) when (((isTrue (evalisProp((varK0_1877 :: [])) config (-1)))) && ((isTrue (evalisProps((varK1_1878 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isProps(#KToken(#token("Props","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortProps, var__1879) :: [])) -> ((Bool true) :: [])
(*{| rule ``isProps(`.List{"#props"}`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Stop'List'LBraQuotHash'props'QuotRBra') :: [])) -> ((Bool true) :: [])
(*{| rule ``isProps(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1880)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisType (c: k) (config: k) (guard: int) : k = let lbl = 
LblisType and sort = 
SortBool in match c with 
(*{| rule ``isType(#KToken(#token("Type","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortType, var__1881) :: [])) -> ((Bool true) :: [])
(*{| rule ``isType(`#voidTy_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'voidTy_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isType(#ref(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isLifetime(K0),isType(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'ref,(varK0_1882 :: []),(varK1_1883 :: [])) :: [])) when (((isTrue (evalisLifetime((varK0_1882 :: [])) config (-1)))) && ((isTrue (evalisType((varK1_1883 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isType(#own(K0))=>#token("true","Bool")`` requires isProps(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'own,(varK0_1884 :: [])) :: [])) when (isTrue (evalisProps((varK0_1884 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isType(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1885)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisParameter (c: k) (config: k) (guard: int) : k = let lbl = 
LblisParameter and sort = 
SortBool in match c with 
(*{| rule ``isParameter(#KToken(#token("Parameter","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortParameter, var__1886) :: [])) -> ((Bool true) :: [])
(*{| rule ``isParameter(#parameter(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isId(K0),isType(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'parameter,(varK0_1887 :: []),(varK1_1888 :: [])) :: [])) when (((isTrue (evalisId((varK0_1887 :: [])) config (-1)))) && ((isTrue (evalisType((varK1_1888 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isParameter(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1889)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisParameters (c: k) (config: k) (guard: int) : k = let lbl = 
LblisParameters and sort = 
SortBool in match c with 
(*{| rule ``isParameters(`.List{"_,__OSL-SYNTAX"}`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra') :: [])) -> ((Bool true) :: [])
(*{| rule ``isParameters(#KToken(#token("Parameters","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortParameters, var__1890) :: [])) -> ((Bool true) :: [])
(*{| rule ``isParameters(`_,__OSL-SYNTAX`(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isParameter(K0),isParameters(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(varK0_1891 :: []),(varK1_1892 :: [])) :: [])) when (((isTrue (evalisParameter((varK0_1891 :: [])) config (-1)))) && ((isTrue (evalisParameters((varK1_1892 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isParameters(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1893)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFparamsCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFparamsCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isFparamsCellOpt(noFparamsCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoFparamsCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFparamsCellOpt(`<fparams>`(K0))=>#token("true","Bool")`` requires isParameters(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fparams'_GT_',(varK0_1894 :: [])) :: [])) when (isTrue (evalisParameters((varK0_1894 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isFparamsCellOpt(#KToken(#token("FparamsCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFparamsCell, var__1895) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFparamsCellOpt(#KToken(#token("FparamsCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFparamsCellOpt, var__1896) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFparamsCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1897)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisK (c: k) (config: k) (guard: int) : k = let lbl = 
LblisK and sort = 
SortBool in match c with 
| [_] -> [Bool true] | _ -> [Bool false]
| [List (s,_,_)] when (s = SortList) -> [Bool true]
| [String _] -> [Bool true]
| [Map (s,_,_)] when (s = SortFunDefCellMap) -> [Bool true]
| [Bool _] -> [Bool true]
| _ -> [Bool true]
| [Map (s,_,_)] when (s = SortMap) -> [Bool true]
| [Int _] -> [Bool true]
| [Float _] -> [Bool true]
| [Map (s,_,_)] when (s = SortStateCellMap) -> [Bool true]
| [Set (s,_,_)] when (s = SortSet) -> [Bool true]
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFbodyCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFbodyCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isFbodyCellOpt(#KToken(#token("FbodyCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFbodyCell, var__1898) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFbodyCellOpt(noFbodyCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoFbodyCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFbodyCellOpt(`<fbody>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fbody'_GT_',(varK0_1899)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFbodyCellOpt(#KToken(#token("FbodyCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFbodyCellOpt, var__1900) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFbodyCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1901)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalnotBool_ (c: k) (config: k) (guard: int) : k = let lbl = 
LblnotBool_ and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_not c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `notBool_`(#token("true","Bool"))=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(de18dba5cb1b6e56941a63279edb1d16da29d7a997a3e82cbc2b81b066eadf47) contentStartColumn(8) contentStartLine(299) org.kframework.attributes.Location(Location(299,8,299,29)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool true) :: [])) -> ((Bool false) :: [])
(*{| rule `` `notBool_`(#token("false","Bool"))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(dbe4d83961158619e994f90623f08be11020d80c56685ef1ec2dd2b15760c474) contentStartColumn(8) contentStartLine(300) org.kframework.attributes.Location(Location(300,8,300,29)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: [])) -> ((Bool true) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'_LT_Eqls'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT_Eqls'Int__INT and sort = 
SortBool in match c with 
| _ -> try INT.hook_le c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalMap'Coln'lookup (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblMap'Coln'lookup and sort = 
SortKItem in match c with 
| _ -> try MAP.hook_lookup c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'stderr_K'Hyph'IO (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'stderr_K'Hyph'IO and sort = 
SortInt in match c with 
(*{| rule `` `#stderr_K-IO`(.KList)=>#token("2","Int")`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(11ef087f50cc8e9fe0fe6a8b028370816f2f2a3573af358ab6ca98318723d0e5) contentStartColumn(8) contentStartLine(910) org.kframework.attributes.Location(Location(910,8,910,20)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| () -> ((Lazy.force int2) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Hash'stderr_K'Hyph'IO : k Lazy.t = lazy (eval'Hash'stderr_K'Hyph'IO () interned_bottom (-1))
let eval'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_seek c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'Hyph'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Hyph'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_sub c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'Perc'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Perc'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_tmod c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'Plus'Int_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Plus'Int_ and sort = 
SortInt in match c with 
| _ -> try INT.hook_add c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalabsInt (c: k) (config: k) (guard: int) : k = let lbl = 
LblabsInt and sort = 
SortInt in match c with 
| _ -> try INT.hook_abs c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'EqlsEqls'K_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsEqls'K_ and sort = 
SortBool in match c with 
| _ -> try KEQUAL.hook_eq c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'EqlsEqls'Int_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsEqls'Int_ and sort = 
SortBool in match c with 
| _ -> try INT.hook_eq c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_==Int_`(I1,I2)=>`_==K_`(I1,I2)`` requires `_andBool_`(isInt(I1),isInt(I2)) ensures #token("true","Bool") [UNIQUE_ID(e92e4aa4a18bee70f2627cdafb8687e2dfcc0dd6a7f8cbb8d1bd17d751c2da2a) contentStartColumn(8) contentStartLine(407) org.kframework.attributes.Location(Location(407,8,407,40)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI1_1902) :: []),((Int _ as varI2_1903) :: [])) when ((true) && (true)) && (true) -> ((eval_'EqlsEqls'K_((varI1_1902 :: []),(varI2_1903 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'EqlsSlshEqls'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsSlshEqls'Int__INT and sort = 
SortBool in match c with 
| _ -> try INT.hook_ne c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_=/=Int__INT`(I1,I2)=>`notBool_`(`_==Int_`(I1,I2))`` requires `_andBool_`(isInt(I1),isInt(I2)) ensures #token("true","Bool") [UNIQUE_ID(59ca895589d5fc7fa573380dac68ddd6602f6fa8b2761530a861aa894455f27e) contentStartColumn(8) contentStartLine(408) org.kframework.attributes.Location(Location(408,8,408,53)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI1_1904) :: []),((Int _ as varI2_1905) :: [])) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'EqlsEqls'Int_((varI1_1904 :: []),(varI2_1905 :: [])) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_modInt__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_modInt__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_emod c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_modInt__INT`(I1,I2)=>`_%Int__INT`(`_+Int_`(`_%Int__INT`(I1,absInt(I2)),absInt(I2)),absInt(I2))`` requires `_andBool_`(`_andBool_`(isInt(I1),isInt(I2)),`_=/=Int__INT`(I2,#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(fb4bf5b2f12862af75611e525d51d7e1966bcc322ee8456d2ed1cdaadf02f6f5) concrete() contentStartColumn(5) contentStartLine(391) org.kframework.attributes.Location(Location(391,5,394,23)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((Int _ as varI1_1906) :: []),((Int _ as varI2_1907) :: [])) when ((((true) && (true))) && ((isTrue (eval_'EqlsSlshEqls'Int__INT((varI2_1907 :: []),((Lazy.force int0) :: [])) config (-1))))) && (true) -> ((eval_'Perc'Int__INT(((eval_'Plus'Int_(((eval_'Perc'Int__INT((varI1_1906 :: []),((evalabsInt((varI2_1907 :: [])) config (-1)))) config (-1))),((evalabsInt((varI2_1907 :: [])) config (-1)))) config (-1))),((evalabsInt((varI2_1907 :: [])) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'_GT__GT_'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_GT__GT_'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_shr c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'_LT__LT_'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT__LT_'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_shl c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalbitRangeInt (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblbitRangeInt and sort = 
SortInt in match c with 
| _ -> try INT.hook_bitRange c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule ``bitRangeInt(I,IDX,LEN)=>`_modInt__INT`(`_>>Int__INT`(I,IDX),`_<<Int__INT`(#token("1","Int"),LEN))`` requires `_andBool_`(`_andBool_`(isInt(I),isInt(LEN)),isInt(IDX)) ensures #token("true","Bool") [UNIQUE_ID(08ec18eb216a86553c22eacaaf8b628b19097a5f4dbbbbb68a5daa77c9337449) contentStartColumn(8) contentStartLine(384) org.kframework.attributes.Location(Location(384,8,384,70)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI_1908) :: []),((Int _ as varIDX_1909) :: []),((Int _ as varLEN_1910) :: [])) when ((((true) && (true))) && (true)) && (true) -> ((eval_modInt__INT(((eval_'_GT__GT_'Int__INT((varI_1908 :: []),(varIDX_1909 :: [])) config (-1))),((eval_'_LT__LT_'Int__INT(((Lazy.force int1) :: []),(varLEN_1910 :: [])) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalsignExtendBitRangeInt (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblsignExtendBitRangeInt and sort = 
SortInt in match c with 
| _ -> try INT.hook_signExtendBitRange c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule ``signExtendBitRangeInt(I,IDX,LEN)=>`_-Int__INT`(`_modInt__INT`(`_+Int_`(bitRangeInt(I,IDX,LEN),`_<<Int__INT`(#token("1","Int"),`_-Int__INT`(LEN,#token("1","Int")))),`_<<Int__INT`(#token("1","Int"),LEN)),`_<<Int__INT`(#token("1","Int"),`_-Int__INT`(LEN,#token("1","Int"))))`` requires `_andBool_`(`_andBool_`(isInt(I),isInt(LEN)),isInt(IDX)) ensures #token("true","Bool") [UNIQUE_ID(b413c3f4500443aec69bee9b10feb030330d60bd136c698a9947546393728a00) contentStartColumn(8) contentStartLine(386) org.kframework.attributes.Location(Location(386,8,386,149)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI_1911) :: []),((Int _ as varIDX_1912) :: []),((Int _ as varLEN_1913) :: [])) when ((((true) && (true))) && (true)) && (true) -> ((eval_'Hyph'Int__INT(((eval_modInt__INT(((eval_'Plus'Int_(((evalbitRangeInt((varI_1911 :: []),(varIDX_1912 :: []),(varLEN_1913 :: [])) config (-1))),((eval_'_LT__LT_'Int__INT(((Lazy.force int1) :: []),((eval_'Hyph'Int__INT((varLEN_1913 :: []),((Lazy.force int1) :: [])) config (-1)))) config (-1)))) config (-1))),((eval_'_LT__LT_'Int__INT(((Lazy.force int1) :: []),(varLEN_1913 :: [])) config (-1)))) config (-1))),((eval_'_LT__LT_'Int__INT(((Lazy.force int1) :: []),((eval_'Hyph'Int__INT((varLEN_1913 :: []),((Lazy.force int1) :: [])) config (-1)))) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let eval_'EqlsEqls'Bool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsEqls'Bool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_eq c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_==Bool__BOOL`(K1,K2)=>`_==K_`(K1,K2)`` requires `_andBool_`(isBool(K1),isBool(K2)) ensures #token("true","Bool") [UNIQUE_ID(a17699668426366833f88851c95d611be854ecef7a676d2061669bf689f05fd1) contentStartColumn(8) contentStartLine(765) org.kframework.attributes.Location(Location(765,8,765,43)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varK1_1914) :: []),((Bool _ as varK2_1915) :: [])) when ((true) && (true)) && (true) -> ((eval_'EqlsEqls'K_((varK1_1914 :: []),(varK2_1915 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisTimerCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisTimerCell and sort = 
SortBool in match c with 
(*{| rule ``isTimerCell(`<timer>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'timer'_GT_',((Int _ as varK0_1916) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isTimerCell(#KToken(#token("TimerCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTimerCell, var__1917) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTimerCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1918)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisSet (c: k) (config: k) (guard: int) : k = let lbl = 
LblisSet and sort = 
SortBool in match c with 
| [Set (s,_,_)] when (s = SortSet) -> [Bool true]
(*{| rule ``isSet(#KToken(#token("Set","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortSet, var__1919) :: [])) -> ((Bool true) :: [])
(*{| rule ``isSet(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1920)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'getenv (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'getenv and sort = 
SortString in match c with 
| _ -> try KREFLECTION.hook_getenv c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalintersectSet (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblintersectSet and sort = 
SortSet in match c with 
| _ -> try SET.hook_intersection c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_in_keys'LPar'_'RPar'_MAP (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_in_keys'LPar'_'RPar'_MAP and sort = 
SortBool in match c with 
| _ -> try MAP.hook_in_keys c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisString (c: k) (config: k) (guard: int) : k = let lbl = 
LblisString and sort = 
SortBool in match c with 
| [String _] -> [Bool true]
(*{| rule ``isString(#KToken(#token("String","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortString, var__1921) :: [])) -> ((Bool true) :: [])
(*{| rule ``isString(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1922)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL and sort = 
SortK in match c with 
| _ -> try KEQUAL.hook_ite c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `#if_#then_#else_#fi_K-EQUAL`(C,B1,_10)=>B1`` requires `_andBool_`(isBool(C),C) ensures #token("true","Bool") [UNIQUE_ID(0883a4d4051090478d6b8a7ac8d4e0e15e7708308fa24e63137b9ac7e87383aa) contentStartColumn(8) contentStartLine(769) org.kframework.attributes.Location(Location(769,8,769,56)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((Bool _ as varC_1923) :: []),(varB1_1924),(var_10_1925)) when ((true) && ((isTrue [varC_1923]))) && (true) -> (varB1_1924)
(*{| rule `` `#if_#then_#else_#fi_K-EQUAL`(C,_11,B2)=>B2`` requires `_andBool_`(isBool(C),`notBool_`(C)) ensures #token("true","Bool") [UNIQUE_ID(d46b5ae094d17de19ef9725da497c32e21813c6e4b5a0d6a2c4b03bd55bb312c) contentStartColumn(8) contentStartLine(770) org.kframework.attributes.Location(Location(770,8,770,64)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((Bool _ as varC_1926) :: []),(var_11_1927),(varB2_1928)) when ((true) && ((not ((isTrue [varC_1926]))))) && (true) -> (varB2_1928)
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalfindString (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblfindString and sort = 
SortInt in match c with 
| _ -> try STRING.hook_find c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalsubstrString (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblsubstrString and sort = 
SortString in match c with 
| _ -> try STRING.hook_substr c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evallengthString (c: k) (config: k) (guard: int) : k = let lbl = 
LbllengthString and sort = 
SortInt in match c with 
| _ -> try STRING.hook_length c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'_GT_Eqls'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_GT_Eqls'Int__INT and sort = 
SortBool in match c with 
| _ -> try INT.hook_ge c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalminInt'LPar'_'Comm'_'RPar'_INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblminInt'LPar'_'Comm'_'RPar'_INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_min c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `minInt(_,_)_INT`(I1,I2)=>I1`` requires `_andBool_`(`_andBool_`(isInt(I1),isInt(I2)),`_<=Int__INT`(I1,I2)) ensures #token("true","Bool") [UNIQUE_ID(c3daf36ef673ae6ce68430bc5170ec91b3d397f5f3e34aee375a841739bcfc9b) contentStartColumn(8) contentStartLine(396) org.kframework.attributes.Location(Location(396,8,396,57)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((Int _ as varI1_1929) :: []),((Int _ as varI2_1930) :: [])) when ((((true) && (true))) && ((isTrue (eval_'_LT_Eqls'Int__INT((varI1_1929 :: []),(varI2_1930 :: [])) config (-1))))) && (true) -> (varI1_1929 :: [])
(*{| rule `` `minInt(_,_)_INT`(I1,I2)=>I2`` requires `_andBool_`(`_andBool_`(isInt(I1),isInt(I2)),`_>=Int__INT`(I1,I2)) ensures #token("true","Bool") [UNIQUE_ID(f93c59dd0bdb46ab467898db4ad231d94d30e5b88427054becac14f3969eff8e) contentStartColumn(8) contentStartLine(397) org.kframework.attributes.Location(Location(397,8,397,57)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((Int _ as varI1_1931) :: []),((Int _ as varI2_1932) :: [])) when ((((true) && (true))) && ((isTrue (eval_'_GT_Eqls'Int__INT((varI1_1931 :: []),(varI2_1932 :: [])) config (-1))))) && (true) -> (varI2_1932 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'EqlsEqls'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsEqls'String__STRING and sort = 
SortBool in match c with 
| _ -> try STRING.hook_eq c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_==String__STRING`(S1,S2)=>`_==K_`(S1,S2)`` requires `_andBool_`(isString(S2),isString(S1)) ensures #token("true","Bool") [UNIQUE_ID(daddd877c886e178648c9a2a7a5b307a0f59225ac211c099829b467bb5ebf98d) contentStartColumn(8) contentStartLine(537) org.kframework.attributes.Location(Location(537,8,537,49)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varS1_1933) :: []),((String _ as varS2_1934) :: [])) when ((true) && (true)) && (true) -> ((eval_'EqlsEqls'K_((varS1_1933 :: []),(varS2_1934 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'EqlsSlshEqls'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsSlshEqls'String__STRING and sort = 
SortBool in match c with 
| _ -> try STRING.hook_ne c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_=/=String__STRING`(S1,S2)=>`notBool_`(`_==String__STRING`(S1,S2))`` requires `_andBool_`(isString(S2),isString(S1)) ensures #token("true","Bool") [UNIQUE_ID(2a5c8974455ca9cde473273aeca1a4fa8072aa68d104bc94daa71ee7bf52c8a4) contentStartColumn(8) contentStartLine(536) org.kframework.attributes.Location(Location(536,8,536,65)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varS1_1935) :: []),((String _ as varS2_1936) :: [])) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'EqlsEqls'String__STRING((varS1_1935 :: []),(varS2_1936 :: [])) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec evalfindChar (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblfindChar and sort = 
SortInt in match c with 
| _ -> try STRING.hook_findChar c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule ``findChar(_17,#token("\"\"","String"),_18)=>#token("-1","Int")`` requires `_andBool_`(isInt(_18),isString(_17)) ensures #token("true","Bool") [UNIQUE_ID(3c92cdc7bbe8b210d199a92b38a24538ca780f801572b653c3cb72d6d89a2adf) contentStartColumn(8) contentStartLine(554) org.kframework.attributes.Location(Location(554,8,554,32)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as var_17_1937) :: []),((String "") :: []),((Int _ as var_18_1938) :: [])) when ((true) && (true)) && (true) -> ((Lazy.force int'Hyph'1) :: [])
(*{| rule ``findChar(S1,S2,I)=>`#if_#then_#else_#fi_K-EQUAL`(`_==Int_`(findString(S1,substrString(S2,#token("0","Int"),#token("1","Int")),I),#token("-1","Int")),findChar(S1,substrString(S2,#token("1","Int"),lengthString(S2)),I),`#if_#then_#else_#fi_K-EQUAL`(`_==Int_`(findChar(S1,substrString(S2,#token("1","Int"),lengthString(S2)),I),#token("-1","Int")),findString(S1,substrString(S2,#token("0","Int"),#token("1","Int")),I),`minInt(_,_)_INT`(findString(S1,substrString(S2,#token("0","Int"),#token("1","Int")),I),findChar(S1,substrString(S2,#token("1","Int"),lengthString(S2)),I))))`` requires `_andBool_`(`_andBool_`(`_andBool_`(isInt(I),isString(S2)),isString(S1)),`_=/=String__STRING`(S2,#token("\"\"","String"))) ensures #token("true","Bool") [UNIQUE_ID(5a89a8f3b78438530e84da3913f33203b224f3c348d32f09785edd80c9cfe137) contentStartColumn(8) contentStartLine(553) org.kframework.attributes.Location(Location(553,8,553,431)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varS1_1939) :: []),((String _ as varS2_1940) :: []),((Int _ as varI_1941) :: [])) when ((((((true) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'String__STRING((varS2_1940 :: []),((String "") :: [])) config (-1))))) && (true) -> ((if ((isTrue (eval_'EqlsEqls'Int_(((evalfindString((varS1_1939 :: []),((evalsubstrString((varS2_1940 :: []),((Lazy.force int0) :: []),((Lazy.force int1) :: [])) config (-1))),(varI_1941 :: [])) config (-1))),((Lazy.force int'Hyph'1) :: [])) config (-1)))) then (((evalfindChar((varS1_1939 :: []),((evalsubstrString((varS2_1940 :: []),((Lazy.force int1) :: []),((evallengthString((varS2_1940 :: [])) config (-1)))) config (-1))),(varI_1941 :: [])) config (-1)))) else (((if ((isTrue (eval_'EqlsEqls'Int_(((evalfindChar((varS1_1939 :: []),((evalsubstrString((varS2_1940 :: []),((Lazy.force int1) :: []),((evallengthString((varS2_1940 :: [])) config (-1)))) config (-1))),(varI_1941 :: [])) config (-1))),((Lazy.force int'Hyph'1) :: [])) config (-1)))) then (((evalfindString((varS1_1939 :: []),((evalsubstrString((varS2_1940 :: []),((Lazy.force int0) :: []),((Lazy.force int1) :: [])) config (-1))),(varI_1941 :: [])) config (-1)))) else (((evalminInt'LPar'_'Comm'_'RPar'_INT(((evalfindString((varS1_1939 :: []),((evalsubstrString((varS2_1940 :: []),((Lazy.force int0) :: []),((Lazy.force int1) :: [])) config (-1))),(varI_1941 :: [])) config (-1))),((evalfindChar((varS1_1939 :: []),((evalsubstrString((varS2_1940 :: []),((Lazy.force int1) :: []),((evallengthString((varS2_1940 :: [])) config (-1)))) config (-1))),(varI_1941 :: [])) config (-1)))) config (-1)))))))))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalisIndexCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIndexCell and sort = 
SortBool in match c with 
(*{| rule ``isIndexCell(#KToken(#token("IndexCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexCell, var__1942) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexCell(`<index>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'index'_GT_',((Int _ as varK0_1943) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isIndexCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1944)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisKCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisKCell and sort = 
SortBool in match c with 
(*{| rule ``isKCell(`<k>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'k'_GT_',(varK0_1945)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isKCell(#KToken(#token("KCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortKCell, var__1946) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1947)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisMap (c: k) (config: k) (guard: int) : k = let lbl = 
LblisMap and sort = 
SortBool in match c with 
| [Map (s,_,_)] when (s = SortMap) -> [Bool true]
(*{| rule ``isMap(#KToken(#token("Map","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortMap, var__1948) :: [])) -> ((Bool true) :: [])
(*{| rule ``isMap(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1949)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisEnvCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisEnvCell and sort = 
SortBool in match c with 
(*{| rule ``isEnvCell(`<env>`(K0))=>#token("true","Bool")`` requires isMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as varK0_1950) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isEnvCell(#KToken(#token("EnvCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortEnvCell, var__1951) :: [])) -> ((Bool true) :: [])
(*{| rule ``isEnvCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1952)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStoreCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStoreCell and sort = 
SortBool in match c with 
(*{| rule ``isStoreCell(#KToken(#token("StoreCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStoreCell, var__1953) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStoreCell(`<store>`(K0))=>#token("true","Bool")`` requires isMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as varK0_1954) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStoreCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1955)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisList (c: k) (config: k) (guard: int) : k = let lbl = 
LblisList and sort = 
SortBool in match c with 
| [List (s,_,_)] when (s = SortList) -> [Bool true]
(*{| rule ``isList(#KToken(#token("List","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortList, var__1956) :: [])) -> ((Bool true) :: [])
(*{| rule ``isList(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1957)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStackCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStackCell and sort = 
SortBool in match c with 
(*{| rule ``isStackCell(#KToken(#token("StackCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStackCell, var__1958) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStackCell(`<stack>`(K0))=>#token("true","Bool")`` requires isList(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as varK0_1959) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStackCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1960)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisWriteCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisWriteCell and sort = 
SortBool in match c with 
(*{| rule ``isWriteCell(#KToken(#token("WriteCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortWriteCell, var__1961) :: [])) -> ((Bool true) :: [])
(*{| rule ``isWriteCell(`<write>`(K0))=>#token("true","Bool")`` requires isSet(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as varK0_1962) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isWriteCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1963)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisIndexes (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIndexes and sort = 
SortBool in match c with 
(*{| rule ``isIndexes(#indexes(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isInt(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'indexes,((Int _ as varK0_1964) :: []),((Int _ as varK1_1965) :: [])) :: [])) when ((true) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isIndexes(#KToken(#token("Indexes","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexes, var__1966) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexes(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1967)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisIndexesCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIndexesCell and sort = 
SortBool in match c with 
(*{| rule ``isIndexesCell(`<indexes>`(K0))=>#token("true","Bool")`` requires isIndexes(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'indexes'_GT_',(varK0_1968 :: [])) :: [])) when (isTrue (evalisIndexes((varK0_1968 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isIndexesCell(#KToken(#token("IndexesCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexesCell, var__1969) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexesCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1970)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStateCellMap (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStateCellMap and sort = 
SortBool in match c with 
| [Map (s,_,_)] when (s = SortStateCellMap) -> [Bool true]
(*{| rule ``isStateCellMap(#KToken(#token("StateCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStateCell, var__1971) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStateCellMap(`<state>`(K0,K1,K2,K3,K4,K5,K6,K7))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isIndexCell(K0),isKCell(K1)),isEnvCell(K2)),isStoreCell(K3)),isStackCell(K4)),isWriteCell(K5)),isTimerCell(K6)),isIndexesCell(K7)) ensures #token("true","Bool") []|}*)
| ((KApply8(Lbl'_LT_'state'_GT_',(varK0_1972 :: []),(varK1_1973 :: []),(varK2_1974 :: []),(varK3_1975 :: []),(varK4_1976 :: []),(varK5_1977 :: []),(varK6_1978 :: []),(varK7_1979 :: [])) :: [])) when (((((((((((((((isTrue (evalisIndexCell((varK0_1972 :: [])) config (-1)))) && ((isTrue (evalisKCell((varK1_1973 :: [])) config (-1)))))) && ((isTrue (evalisEnvCell((varK2_1974 :: [])) config (-1)))))) && ((isTrue (evalisStoreCell((varK3_1975 :: [])) config (-1)))))) && ((isTrue (evalisStackCell((varK4_1976 :: [])) config (-1)))))) && ((isTrue (evalisWriteCell((varK5_1977 :: [])) config (-1)))))) && ((isTrue (evalisTimerCell((varK6_1978 :: [])) config (-1)))))) && ((isTrue (evalisIndexesCell((varK7_1979 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStateCellMap(#KToken(#token("StateCellMap","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStateCellMap, var__1980) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStateCellMap(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1981)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStatesCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStatesCell and sort = 
SortBool in match c with 
(*{| rule ``isStatesCell(#KToken(#token("StatesCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStatesCell, var__1982) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStatesCell(`<states>`(K0))=>#token("true","Bool")`` requires isStateCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'states'_GT_',((Map (SortStateCellMap,_,_) as varK0_1983) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStatesCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1984)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitNstateCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitNstateCell and sort = 
SortNstateCell in match c with 
(*{| rule ``initNstateCell(.KList)=>`<nstate>`(#token("1","Int"))`` requires isInt(#token("1","Int")) ensures #token("true","Bool") [UNIQUE_ID(b639d9a907880bf37d6bbd004a2156354c61b268954d5fb2699478df1d2c7b9a) initializer()]|}*)
| () when (isTrue (evalisInt(((Lazy.force int1) :: [])) config (-1))) && (true) -> (KApply1(Lbl'_LT_'nstate'_GT_',((Lazy.force int1) :: [])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitNstateCell : k Lazy.t = lazy (evalinitNstateCell () interned_bottom (-1))
let evalSet'Coln'in (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblSet'Coln'in and sort = 
SortBool in match c with 
| _ -> try SET.hook_in c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalString2Int (c: k) (config: k) (guard: int) : k = let lbl = 
LblString2Int and sort = 
SortInt in match c with 
| _ -> try STRING.hook_string2int c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitFparamsCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitFparamsCell and sort = 
SortFparamsCell in match c with 
(*{| rule ``initFparamsCell(.KList)=>`<fparams>`(`.List{"_,__OSL-SYNTAX"}`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(2e8b30285f0ab90a1929412428a257beea1df5cfa8e8d1aaf03a073896c7a7e2) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'fparams'_GT_',(const'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra' :: [])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitFparamsCell : k Lazy.t = lazy (evalinitFparamsCell () interned_bottom (-1))
let eval_'_LT_Eqls'Set__SET (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT_Eqls'Set__SET and sort = 
SortBool in match c with 
| _ -> try SET.hook_inclusion c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisIOError (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIOError and sort = 
SortBool in match c with 
(*{| rule ``isIOError(`#ECONNREFUSED_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ECONNREFUSED_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EPIPE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EPIPE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ETOOMANYREFS_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ETOOMANYREFS_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENETUNREACH_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENETUNREACH_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EBUSY_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EBUSY_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EHOSTDOWN_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EHOSTDOWN_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOTEMPTY_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOTEMPTY_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EFBIG_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EFBIG_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#E2BIG_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'E2BIG_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOLCK_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOLCK_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EROFS_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EROFS_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EISCONN_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EISCONN_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ESRCH_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ESRCH_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EINPROGRESS_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EINPROGRESS_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ERANGE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ERANGE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(#KToken(#token("IOError","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIOError, var__1985) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOBUFS_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOBUFS_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOSPC_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOSPC_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EADDRINUSE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EADDRINUSE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EAFNOSUPPORT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EAFNOSUPPORT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EDOM_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EDOM_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EIO_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EIO_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EOPNOTSUPP_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EOPNOTSUPP_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENAMETOOLONG_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENAMETOOLONG_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EPERM_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EPERM_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EPROTONOSUPPORT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EPROTONOSUPPORT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EMLINK_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EMLINK_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOENT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOENT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOMEM_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOMEM_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ECONNRESET_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ECONNRESET_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EWOULDBLOCK_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EWOULDBLOCK_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOEXEC_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOEXEC_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ECONNABORTED_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ECONNABORTED_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOSYS_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOSYS_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EHOSTUNREACH_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EHOSTUNREACH_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EEXIST_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EEXIST_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(#unknownIOError(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'unknownIOError,((Int _ as varK0_1986) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EDESTADDRREQ_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EDESTADDRREQ_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EBADF_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EBADF_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EXDEV_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EXDEV_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOTCONN_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOTCONN_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENFILE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENFILE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENODEV_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENODEV_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENXIO_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENXIO_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENETRESET_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENETRESET_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EPFNOSUPPORT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EPFNOSUPPORT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EISDIR_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EISDIR_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ETIMEDOUT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ETIMEDOUT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EINVAL_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EINVAL_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EAGAIN_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EAGAIN_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ESPIPE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ESPIPE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EMFILE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EMFILE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EFAULT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EFAULT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENETDOWN_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENETDOWN_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EOVERFLOW_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EOVERFLOW_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EACCES_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EACCES_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EOF_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EOF_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EDEADLK_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EDEADLK_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOTSOCK_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOTSOCK_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ECHILD_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ECHILD_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EPROTOTYPE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EPROTOTYPE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOPROTOOPT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOPROTOOPT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EINTR_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EINTR_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ELOOP_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ELOOP_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ESHUTDOWN_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ESHUTDOWN_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EMSGSIZE_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EMSGSIZE_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOTDIR_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOTDIR_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EALREADY_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EALREADY_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ENOTTY_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ENOTTY_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#ESOCKTNOSUPPORT_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'ESOCKTNOSUPPORT_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#EADDRNOTAVAIL_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'EADDRNOTAVAIL_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(`#noparse_K-IO`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'noparse_K'Hyph'IO) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIOError(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1987)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'parse (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'parse and sort = 
SortKItem in match c with 
| _ -> try IO.hook_parse c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalmakeList (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblmakeList and sort = 
SortList in match c with 
| _ -> try LIST.hook_make c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisSeparator (c: k) (config: k) (guard: int) : k = let lbl = 
LblisSeparator and sort = 
SortBool in match c with 
(*{| rule ``isSeparator(#KToken(#token("Separator","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortSeparator, var__1988) :: [])) -> ((Bool true) :: [])
(*{| rule ``isSeparator(#loopSep(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'loopSep,((Int _ as varK0_1989) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isSeparator(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1990)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'LSqB'_'_LT_Hyph'undef'RSqB' (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'LSqB'_'_LT_Hyph'undef'RSqB' and sort = 
SortMap in match c with 
| _ -> try MAP.hook_remove c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalinitIndexesCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitIndexesCell and sort = 
SortIndexesCell in match c with 
(*{| rule ``initIndexesCell(.KList)=>`<indexes>`(#indexes(#token("0","Int"),#token("0","Int")))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(2a981c24e3bb07c8209ecde526aa94fef1a5617bc52520d9da32abad073aac6c) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Lazy.force int0) :: []),((Lazy.force int0) :: [])) :: [])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitIndexesCell : k Lazy.t = lazy (evalinitIndexesCell () interned_bottom (-1))
let evalisFnameCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFnameCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isFnameCellOpt(#KToken(#token("FnameCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFnameCell, var__1991) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFnameCellOpt(noFnameCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoFnameCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFnameCellOpt(#KToken(#token("FnameCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFnameCellOpt, var__1992) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFnameCellOpt(`<fname>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fname'_GT_',(varK0_1993)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFnameCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1994)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFretCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFretCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isFretCellOpt(noFretCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoFretCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFretCellOpt(`<fret>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fret'_GT_',(varK0_1995)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFretCellOpt(#KToken(#token("FretCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFretCellOpt, var__1996) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFretCellOpt(#KToken(#token("FretCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFretCell, var__1997) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFretCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_1998)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFunDefCellFragment (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunDefCellFragment and sort = 
SortBool in match c with 
(*{| rule ``isFunDefCellFragment(#KToken(#token("FunDefCellFragment","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefCellFragment, var__1999) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefCellFragment(`<funDef>-fragment`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isFnameCellOpt(K0),isFparamsCellOpt(K1)),isFretCellOpt(K2)),isFbodyCellOpt(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'funDef'_GT_Hyph'fragment,(varK0_2000 :: []),(varK1_2001 :: []),(varK2_2002 :: []),(varK3_2003 :: [])) :: [])) when (((((((isTrue (evalisFnameCellOpt((varK0_2000 :: [])) config (-1)))) && ((isTrue (evalisFparamsCellOpt((varK1_2001 :: [])) config (-1)))))) && ((isTrue (evalisFretCellOpt((varK2_2002 :: [])) config (-1)))))) && ((isTrue (evalisFbodyCellOpt((varK3_2003 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isFunDefCellFragment(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2004)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitFbodyCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitFbodyCell and sort = 
SortFbodyCell in match c with 
(*{| rule ``initFbodyCell(.KList)=>`<fbody>`(.K)`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(1f06a6f24a70654cedcde13edbf88d19c1cbc023b5215cc6d32d89f743fb960d) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'fbody'_GT_',([])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitFbodyCell : k Lazy.t = lazy (evalinitFbodyCell () interned_bottom (-1))
let eval'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_unlock c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_andThenBool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_andThenBool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_andThen c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_andThenBool__BOOL`(#token("false","Bool"),_2)=>#token("false","Bool")`` requires isBool(_2) ensures #token("true","Bool") [UNIQUE_ID(34b51d1bff350ce2cd8b17b3af8effdfcf390962b57189087f3cc3b6bbe59cbc) contentStartColumn(8) contentStartLine(309) org.kframework.attributes.Location(Location(309,8,309,36)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: []),((Bool _ as var_2_2005) :: [])) when true && (true) -> ((Bool false) :: [])
(*{| rule `` `_andThenBool__BOOL`(K,#token("true","Bool"))=>K`` requires isBool(K) ensures #token("true","Bool") [UNIQUE_ID(1aad8125a2ba7fcbd3c40ac487b7dc776993e9d167ac20f0210e14213abec45f) contentStartColumn(8) contentStartLine(308) org.kframework.attributes.Location(Location(308,8,308,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varK_2006) :: []),((Bool true) :: [])) when true && (true) -> (varK_2006 :: [])
(*{| rule `` `_andThenBool__BOOL`(_6,#token("false","Bool"))=>#token("false","Bool")`` requires isBool(_6) ensures #token("true","Bool") [UNIQUE_ID(d0a6502b90e84545c10a45f7199b2a6d77f52d0841c63dcc792b60073f300e07) contentStartColumn(8) contentStartLine(310) org.kframework.attributes.Location(Location(310,8,310,36)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as var_6_2007) :: []),((Bool false) :: [])) when true && (true) -> ((Bool false) :: [])
(*{| rule `` `_andThenBool__BOOL`(#token("true","Bool"),K)=>K`` requires isBool(K) ensures #token("true","Bool") [UNIQUE_ID(b058439c1adf0de5fb7afd07d638d2cda247a4344cbdc14812da46c5db5499df) contentStartColumn(8) contentStartLine(307) org.kframework.attributes.Location(Location(307,8,307,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool true) :: []),((Bool _ as varK_2008) :: [])) when true && (true) -> (varK_2008 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalSet'Coln'difference (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblSet'Coln'difference and sort = 
SortSet in match c with 
| _ -> try SET.hook_difference c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalSetItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblSetItem and sort = 
SortSet in match c with 
| _ -> try SET.hook_element c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Stop'Set (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Stop'Set and sort = 
SortSet in match c with 
| _ -> try SET.hook_unit c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Stop'Set : k Lazy.t = lazy (eval'Stop'Set () interned_bottom (-1))
let rec eval'Hash'writeCK (c: k * k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'writeCK and sort = 
SortBool in match c with 
| (((Int _ as var_0_2009) :: []),((Int _ as var_1_2010) :: []),((Int _ as var_2_2011) :: []),(var_3_2012)) when guard < 0(*{| rule ``#writeCK(L,B,E,_0)=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(#setChoice(#writev(L,T),_0),#match(RestS,`Set:difference`(_0,`SetItem`(#writev(L,T))))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(B),isInt(T)),isInt(E)),isSet(RestS)),isInt(L)),`_andBool_`(`_<=Int__INT`(B,T),`_<=Int__INT`(T,E)))) ensures #token("true","Bool") [UNIQUE_ID(9b2ac40d3e72d229296772060ce9bc02d42a0c44390764790e8f75fe8d907226) contentStartColumn(6) contentStartLine(188) org.kframework.attributes.Location(Location(188,6,189,47)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_3_2012) with 
| [Set (_,_,collection)] -> let choice = (KSet.fold (fun e result -> if result == interned_bottom then (match e with | (KApply2(Lbl'Hash'writev,((Int _ as var_0_2013) :: []),((Int _ as var_4_2014) :: [])) :: []) as e0 -> (let e = ((evalSet'Coln'difference((var_3_2012),((evalSetItem(e0) config (-1)))) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Set (SortSet,_,_) as var_5_2015) :: []) when ((((true) && (true))) && (((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'_LT_Eqls'Int__INT((var_1_2010 :: []),(var_4_2014 :: [])) config (-1)))) && ((isTrue (eval_'_LT_Eqls'Int__INT((var_4_2014 :: []),(var_2_2011 :: [])) config (-1))))))))) && (((compare_kitem var_0_2009 var_0_2013) = 0) && true) -> ((Bool false) :: [])| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'writeCK c config 0) else choice| _ -> (eval'Hash'writeCK c config 0))
(*{| rule ``#writeCK(_53,_54,_55,_0)=>#token("true","Bool")`` requires `_andBool_`(`_==K_`(`.Set`(.KList),_0),`_andBool_`(`_andBool_`(isInt(_53),isInt(_55)),isInt(_54))) ensures #token("true","Bool") [UNIQUE_ID(044edcb2140a98a6bbddeb19f4c97232f8e1f061534f7dc247b944e34cf087e8) contentStartColumn(6) contentStartLine(198) org.kframework.attributes.Location(Location(198,6,198,34)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as var_53_2016) :: []),((Int _ as var_54_2017) :: []),((Int _ as var_55_2018) :: []),(var_0_2019)) when (((isTrue (eval_'EqlsEqls'K_(((Lazy.force const'Stop'Set)),(var_0_2019)) config (-1)))) && (((((true) && (true))) && (true)))) && (true) -> ((Bool true) :: [])
| (((Int _ as var_0_2020) :: []),((Int _ as var_1_2021) :: []),((Int _ as var_2_2022) :: []),(var_3_2023)) when guard < 2(*{| rule ``#writeCK(L,B,E,_0)=>#writeCK(L,B,E,RestS)`` requires `_andBool_`(`_andBool_`(#setChoice(#writev(L,T),_0),#match(RestS,`Set:difference`(_0,`SetItem`(#writev(L,T))))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(B),isInt(T)),isInt(E)),isSet(RestS)),isInt(L)),`notBool_`(`_andBool_`(`_<=Int__INT`(B,T),`_<=Int__INT`(T,E))))) ensures #token("true","Bool") [UNIQUE_ID(8f9fa1f03c3395007f040281eacc6f05f64fe3f108328a61a426e71a6725b3ac) contentStartColumn(6) contentStartLine(195) org.kframework.attributes.Location(Location(195,6,196,56)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_3_2023) with 
| [Set (_,_,collection)] -> let choice = (KSet.fold (fun e result -> if result == interned_bottom then (match e with | (KApply2(Lbl'Hash'writev,((Int _ as var_0_2024) :: []),((Int _ as var_5_2025) :: [])) :: []) as e1 -> (let e = ((evalSet'Coln'difference((var_3_2023),((evalSetItem(e1) config (-1)))) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Set (SortSet,_,_) as var_4_2026) :: []) when ((((true) && (true))) && (((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((not ((((isTrue (eval_'_LT_Eqls'Int__INT((var_1_2021 :: []),(var_5_2025 :: [])) config (-1)))) && ((isTrue (eval_'_LT_Eqls'Int__INT((var_5_2025 :: []),(var_2_2022 :: [])) config (-1))))))))))) && (((compare_kitem var_0_2020 var_0_2024) = 0) && true) -> ((eval'Hash'writeCK((var_0_2020 :: []),(var_1_2021 :: []),(var_2_2022 :: []),(var_4_2026 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'writeCK c config 2) else choice| _ -> (eval'Hash'writeCK c config 2))
| (((Int _ as var_0_2027) :: []),((Int _ as var_1_2028) :: []),((Int _ as var_2_2029) :: []),(var_3_2030)) when guard < 3(*{| rule ``#writeCK(L,B,E,_0)=>#writeCK(L,B,E,RestS)`` requires `_andBool_`(`_andBool_`(#setChoice(#writev(L1,T),_0),#match(RestS,`Set:difference`(_0,`SetItem`(#writev(L1,T))))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(B),isInt(T)),isInt(E)),isSet(RestS)),isInt(L)),isInt(L1)),`_=/=Int__INT`(L,L1))) ensures #token("true","Bool") [UNIQUE_ID(f218c1a0a668d947c113a883ef03cc308aed632b82234bd7c155505d8cacd994) contentStartColumn(6) contentStartLine(191) org.kframework.attributes.Location(Location(191,6,192,26)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_3_2030) with 
| [Set (_,_,collection)] -> let choice = (KSet.fold (fun e result -> if result == interned_bottom then (match e with | (KApply2(Lbl'Hash'writev,((Int _ as var_5_2031) :: []),((Int _ as var_6_2032) :: [])) :: []) as e2 -> (let e = ((evalSet'Coln'difference((var_3_2030),((evalSetItem(e2) config (-1)))) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Set (SortSet,_,_) as var_4_2033) :: []) when ((((true) && (true))) && (((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2027 :: []),(var_5_2031 :: [])) config (-1))))))) && (true) -> ((eval'Hash'writeCK((var_0_2027 :: []),(var_1_2028 :: []),(var_2_2029 :: []),(var_4_2033 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'writeCK c config 3) else choice| _ -> (eval'Hash'writeCK c config 3))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize4 c)))])
let eval'Hash'parseInModule (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'parseInModule and sort = 
SortKItem in match c with 
| _ -> try IO.hook_parseInModule c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalisBlockItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisBlockItem and sort = 
SortBool in match c with 
(*{| rule ``isBlockItem(`#blockend_BLOCK`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'blockend_BLOCK) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBlockItem(#KToken(#token("BlockItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBlockItem, var__2034) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBlockItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2035)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'system (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'system and sort = 
SortKItem in match c with 
| _ -> try IO.hook_system c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_StateCellMap_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_StateCellMap_ and sort = 
SortStateCellMap in match c with 
| _ -> try MAP.hook_concat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisTmpCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisTmpCell and sort = 
SortBool in match c with 
(*{| rule ``isTmpCell(#KToken(#token("TmpCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTmpCell, var__2036) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTmpCell(`<tmp>`(K0))=>#token("true","Bool")`` requires isList(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'tmp'_GT_',((List (SortList,_,_) as varK0_2037) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isTmpCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2038)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisValue (c: k) (config: k) (guard: int) : k = let lbl = 
LblisValue and sort = 
SortBool in match c with 
(*{| rule ``isValue(#Loc(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isValue(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'Loc,(varK0_2039 :: []),((Int _ as varK1_2040) :: [])) :: [])) when (((isTrue (evalisValue((varK0_2039 :: [])) config (-1)))) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isValue(#rs(K0))=>#token("true","Bool")`` requires isProps(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'rs,(varK0_2041 :: [])) :: [])) when (isTrue (evalisProps((varK0_2041 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isValue(#immRef(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'immRef,((Int _ as varK0_2042) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isValue(#KToken(#token("Value","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortValue, var__2043) :: [])) -> ((Bool true) :: [])
(*{| rule ``isValue(#loc(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'loc,((Int _ as varK0_2044) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isValue(#mutRef(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'mutRef,((Int _ as varK0_2045) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isValue(`#void_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'void_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isValue(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2046)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisExps (c: k) (config: k) (guard: int) : k = let lbl = 
LblisExps and sort = 
SortBool in match c with 
(*{| rule ``isExps(#KToken(#token("Exps","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortExps, var__2047) :: [])) -> ((Bool true) :: [])
(*{| rule ``isExps(`.List{"_,__OSL-SYNTAX"}`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra') :: [])) -> ((Bool true) :: [])
(*{| rule ``isExps(`_,__OSL-SYNTAX`(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isExp(K0),isExps(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(varK0_2048 :: []),(varK1_2049 :: [])) :: [])) when (((isTrue (evalisExp((varK0_2048 :: [])) config (-1)))) && ((isTrue (evalisExps((varK1_2049 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExps(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2050)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
and evalisExp (c: k) (config: k) (guard: int) : k = let lbl = 
LblisExp and sort = 
SortBool in match c with 
(*{| rule ``isExp(`_.__OSL-SYNTAX`(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isExp(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl_'Stop'__OSL'Hyph'SYNTAX,(varK0_2051 :: []),((Int _ as varK1_2052) :: [])) :: [])) when (((isTrue (evalisExp((varK0_2051 :: [])) config (-1)))) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(`#void_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'void_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isExp(#immRef(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'immRef,((Int _ as varK0_2053) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(`*__OSL-SYNTAX`(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Star'__OSL'Hyph'SYNTAX,(varK0_2054 :: [])) :: [])) when (isTrue (evalisExp((varK0_2054 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#KToken(#token("Exp","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortExp, var__2055) :: [])) -> ((Bool true) :: [])
(*{| rule ``isExp(#TransferMB(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isK(K0),isK(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'TransferMB,(varK0_2056),(varK1_2057)) :: [])) when ((true) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#Transferuninit(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isK(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'Transferuninit,(varK0_2058),(varK1_2059 :: [])) :: [])) when ((true) && ((isTrue (evalisExp((varK1_2059 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#KToken(#token("Value","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortValue, var__2060) :: [])) -> ((Bool true) :: [])
(*{| rule ``isExp(#FnCall(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isId(K0),isExps(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'FnCall,(varK0_2061 :: []),(varK1_2062 :: [])) :: [])) when (((isTrue (evalisId((varK0_2061 :: [])) config (-1)))) && ((isTrue (evalisExps((varK1_2062 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#lv(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'lv,(varK0_2063 :: [])) :: [])) when (isTrue (evalisExp((varK0_2063 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#rs(K0))=>#token("true","Bool")`` requires isProps(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'rs,(varK0_2064 :: [])) :: [])) when (isTrue (evalisProps((varK0_2064 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#mutRef(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'mutRef,((Int _ as varK0_2065) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#Transfer(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isK(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'Transfer,(varK0_2066),(varK1_2067 :: [])) :: [])) when ((true) && ((isTrue (evalisExp((varK1_2067 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#uninitialize(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'uninitialize,(varK0_2068 :: [])) :: [])) when (isTrue (evalisExp((varK0_2068 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#read(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'read,(varK0_2069 :: [])) :: [])) when (isTrue (evalisExp((varK0_2069 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(`newResource(_)_OSL-SYNTAX`(K0))=>#token("true","Bool")`` requires isProps(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX,(varK0_2070 :: [])) :: [])) when (isTrue (evalisProps((varK0_2070 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#Loc(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isValue(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'Loc,(varK0_2071 :: []),((Int _ as varK1_2072) :: [])) :: [])) when (((isTrue (evalisValue((varK0_2071 :: [])) config (-1)))) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#TransferV(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isK(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'TransferV,(varK0_2073),(varK1_2074 :: [])) :: [])) when ((true) && ((isTrue (evalisExp((varK1_2074 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#loc(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'loc,((Int _ as varK0_2075) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#TransferIB(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isK(K0),isK(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'TransferIB,(varK0_2076),(varK1_2077)) :: [])) when ((true) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#lvDref(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'lvDref,(varK0_2078 :: [])) :: [])) when (isTrue (evalisExp((varK0_2078 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isExp(#KToken(#token("Id","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortId, var__2079) :: [])) -> ((Bool true) :: [])
(*{| rule ``isExp(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2080)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisUninit (c: k) (config: k) (guard: int) : k = let lbl = 
LblisUninit and sort = 
SortBool in match c with 
(*{| rule ``isUninit(#KToken(#token("Uninit","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortUninit, var__2081) :: [])) -> ((Bool true) :: [])
(*{| rule ``isUninit(`#uninit_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isUninit(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2082)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec evalisBlocks (c: k) (config: k) (guard: int) : k = let lbl = 
LblisBlocks and sort = 
SortBool in match c with 
(*{| rule ``isBlocks(#KToken(#token("Blocks","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBlocks, var__2083) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBlocks(`_,__OSL-SYNTAX`(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isBlock(K0),isBlocks(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(varK0_2084 :: []),(varK1_2085 :: [])) :: [])) when (((isTrue (evalisBlock((varK0_2084 :: [])) config (-1)))) && ((isTrue (evalisBlocks((varK1_2085 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isBlocks(`.List{"_,__OSL-SYNTAX"}`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra') :: [])) -> ((Bool true) :: [])
(*{| rule ``isBlocks(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2086)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
and evalisStmt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStmt and sort = 
SortBool in match c with 
(*{| rule ``isStmt(#KToken(#token("Block","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBlock, var__2087) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStmt(#KToken(#token("Stmt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStmt, var__2088) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStmt(val(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lblval,(varK0_2089 :: [])) :: [])) when (isTrue (evalisExp((varK0_2089 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#borrow(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isExp(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'borrow,(varK0_2090 :: []),(varK1_2091 :: [])) :: [])) when (((isTrue (evalisExp((varK0_2090 :: [])) config (-1)))) && ((isTrue (evalisExp((varK1_2091 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#expStmt(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'expStmt,(varK0_2092 :: [])) :: [])) when (isTrue (evalisExp((varK0_2092 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(`_;_OSL-SYNTAX`(K0))=>#token("true","Bool")`` requires isFunction(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl_'SCln'_OSL'Hyph'SYNTAX,(varK0_2093 :: [])) :: [])) when (isTrue (evalisFunction((varK0_2093 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#transfer(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isExp(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'transfer,(varK0_2094 :: []),(varK1_2095 :: [])) :: [])) when (((isTrue (evalisExp((varK0_2094 :: [])) config (-1)))) && ((isTrue (evalisExp((varK1_2095 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#block(K0))=>#token("true","Bool")`` requires isStmts(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'block,(varK0_2096 :: [])) :: [])) when (isTrue (evalisStmts((varK0_2096 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#deallocate(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'deallocate,(varK0_2097 :: [])) :: [])) when (isTrue (evalisExp((varK0_2097 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(`loop_;_OSL-SYNTAX`(K0))=>#token("true","Bool")`` requires isBlock(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lblloop_'SCln'_OSL'Hyph'SYNTAX,(varK0_2098 :: [])) :: [])) when (isTrue (evalisBlock((varK0_2098 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(`destruct_;_OSL-SYNTAX`(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbldestruct_'SCln'_OSL'Hyph'SYNTAX,(varK0_2099 :: [])) :: [])) when (isTrue (evalisExp((varK0_2099 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#repeat(K0))=>#token("true","Bool")`` requires isBlock(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'repeat,(varK0_2100 :: [])) :: [])) when (isTrue (evalisBlock((varK0_2100 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#decl(K0))=>#token("true","Bool")`` requires isId(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'decl,(varK0_2101 :: [])) :: [])) when (isTrue (evalisId((varK0_2101 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(`transfer__;_OSL-SYNTAX`(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isUninit(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbltransfer__'SCln'_OSL'Hyph'SYNTAX,(varK0_2102 :: []),(varK1_2103 :: [])) :: [])) when (((isTrue (evalisUninit((varK0_2102 :: [])) config (-1)))) && ((isTrue (evalisExp((varK1_2103 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#declTy(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isId(K0),isType(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'declTy,(varK0_2104 :: []),(varK1_2105 :: [])) :: [])) when (((isTrue (evalisId((varK0_2104 :: [])) config (-1)))) && ((isTrue (evalisType((varK1_2105 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#mborrow(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isExp(K0),isExp(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'mborrow,(varK0_2106 :: []),(varK1_2107 :: [])) :: [])) when (((isTrue (evalisExp((varK0_2106 :: [])) config (-1)))) && ((isTrue (evalisExp((varK1_2107 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(#branch(K0))=>#token("true","Bool")`` requires isBlocks(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'branch,(varK0_2108 :: [])) :: [])) when (isTrue (evalisBlocks((varK0_2108 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2109)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
and evalisStmts (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStmts and sort = 
SortBool in match c with 
(*{| rule ``isStmts(`___OSL-SYNTAX`(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isStmt(K0),isStmts(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl___OSL'Hyph'SYNTAX,(varK0_2110 :: []),(varK1_2111 :: [])) :: [])) when (((isTrue (evalisStmt((varK0_2110 :: [])) config (-1)))) && ((isTrue (evalisStmts((varK1_2111 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStmts(#KToken(#token("Stmts","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStmts, var__2112) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStmts(`.List{"___OSL-SYNTAX"}`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra') :: [])) -> ((Bool true) :: [])
(*{| rule ``isStmts(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2113)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
and evalisBlock (c: k) (config: k) (guard: int) : k = let lbl = 
LblisBlock and sort = 
SortBool in match c with 
(*{| rule ``isBlock(#block(K0))=>#token("true","Bool")`` requires isStmts(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'block,(varK0_2114 :: [])) :: [])) when (isTrue (evalisStmts((varK0_2114 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isBlock(#KToken(#token("Block","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBlock, var__2115) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBlock(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2116)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
and evalisFunction (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunction and sort = 
SortBool in match c with 
(*{| rule ``isFunction(#function(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isId(K0),isParameters(K1)),isType(K2)),isBlock(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'Hash'function,(varK0_2117 :: []),(varK1_2118 :: []),(varK2_2119 :: []),(varK3_2120 :: [])) :: [])) when (((((((isTrue (evalisId((varK0_2117 :: [])) config (-1)))) && ((isTrue (evalisParameters((varK1_2118 :: [])) config (-1)))))) && ((isTrue (evalisType((varK2_2119 :: [])) config (-1)))))) && ((isTrue (evalisBlock((varK3_2120 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isFunction(#KToken(#token("Function","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunction, var__2121) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunction(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2122)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'Plus'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Plus'String__STRING and sort = 
SortString in match c with 
| _ -> try STRING.hook_concat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'_GT_'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_GT_'Int__INT and sort = 
SortBool in match c with 
| _ -> try INT.hook_gt c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec evalreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING (c: k * k * k * k) (config: k) (guard: int) : k = let lbl = 
Lblreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING and sort = 
SortString in match c with 
| _ -> try STRING.hook_replace c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `replace(_,_,_,_)_STRING`(Source,ToReplace,Replacement,Count)=>`_+String__STRING`(`_+String__STRING`(substrString(Source,#token("0","Int"),findString(Source,ToReplace,#token("0","Int"))),Replacement),`replace(_,_,_,_)_STRING`(substrString(Source,`_+Int_`(findString(Source,ToReplace,#token("0","Int")),lengthString(ToReplace)),lengthString(Source)),ToReplace,Replacement,`_-Int__INT`(Count,#token("1","Int"))))`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isString(Replacement),isString(Source)),isInt(Count)),isString(ToReplace)),`_>Int__INT`(Count,#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(00a2618b5ebbb57a911b7f2ad35458f6b3e1d5cb14ef1e49468e9833e93e48c0) contentStartColumn(8) contentStartLine(570) org.kframework.attributes.Location(Location(570,8,573,30)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varSource_2123) :: []),((String _ as varToReplace_2124) :: []),((String _ as varReplacement_2125) :: []),((Int _ as varCount_2126) :: [])) when ((((((((true) && (true))) && (true))) && (true))) && ((isTrue (eval_'_GT_'Int__INT((varCount_2126 :: []),((Lazy.force int0) :: [])) config (-1))))) && (true) -> ((eval_'Plus'String__STRING(((eval_'Plus'String__STRING(((evalsubstrString((varSource_2123 :: []),((Lazy.force int0) :: []),((evalfindString((varSource_2123 :: []),(varToReplace_2124 :: []),((Lazy.force int0) :: [])) config (-1)))) config (-1))),(varReplacement_2125 :: [])) config (-1))),((evalreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING(((evalsubstrString((varSource_2123 :: []),((eval_'Plus'Int_(((evalfindString((varSource_2123 :: []),(varToReplace_2124 :: []),((Lazy.force int0) :: [])) config (-1))),((evallengthString((varToReplace_2124 :: [])) config (-1)))) config (-1))),((evallengthString((varSource_2123 :: [])) config (-1)))) config (-1))),(varToReplace_2124 :: []),(varReplacement_2125 :: []),((eval_'Hyph'Int__INT((varCount_2126 :: []),((Lazy.force int1) :: [])) config (-1)))) config (-1)))) config (-1)))
(*{| rule `` `replace(_,_,_,_)_STRING`(Source,_14,_15,_0)=>Source`` requires `_andBool_`(`_andBool_`(`_andBool_`(isString(_14),isString(Source)),isString(_15)),`_==Int_`(_0,#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(ba5c0b944155cbe05f7bf6c179a2ed1e9baea983ec5099e8449b31926e9b3069) contentStartColumn(8) contentStartLine(574) org.kframework.attributes.Location(Location(574,8,574,49)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varSource_2127) :: []),((String _ as var_14_2128) :: []),((String _ as var_15_2129) :: []),((Int _ as var_0_2130) :: [])) when ((((((true) && (true))) && (true))) && ((isTrue (eval_'EqlsEqls'Int_((var_0_2130 :: []),((Lazy.force int0) :: [])) config (-1))))) && (true) -> (varSource_2127 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize4 c)))])
let eval_'_LT_'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT_'Int__INT and sort = 
SortBool in match c with 
| _ -> try INT.hook_lt c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec evalcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING and sort = 
SortInt in match c with 
| _ -> try STRING.hook_countAllOccurrences c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `countAllOccurrences(_,_)_STRING`(Source,ToCount)=>`_+Int_`(#token("1","Int"),`countAllOccurrences(_,_)_STRING`(substrString(Source,`_+Int_`(findString(Source,ToCount,#token("0","Int")),lengthString(ToCount)),lengthString(Source)),ToCount))`` requires `_andBool_`(`_andBool_`(isString(ToCount),isString(Source)),`_>=Int__INT`(findString(Source,ToCount,#token("0","Int")),#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(9834f2e2641cb2ecf28969acaab73619cb181f1a69c9cfef5102f907edaeb71e) contentStartColumn(8) contentStartLine(560) org.kframework.attributes.Location(Location(560,8,561,60)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varSource_2131) :: []),((String _ as varToCount_2132) :: [])) when ((((true) && (true))) && ((isTrue (eval_'_GT_Eqls'Int__INT(((evalfindString((varSource_2131 :: []),(varToCount_2132 :: []),((Lazy.force int0) :: [])) config (-1))),((Lazy.force int0) :: [])) config (-1))))) && (true) -> ((eval_'Plus'Int_(((Lazy.force int1) :: []),((evalcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING(((evalsubstrString((varSource_2131 :: []),((eval_'Plus'Int_(((evalfindString((varSource_2131 :: []),(varToCount_2132 :: []),((Lazy.force int0) :: [])) config (-1))),((evallengthString((varToCount_2132 :: [])) config (-1)))) config (-1))),((evallengthString((varSource_2131 :: [])) config (-1)))) config (-1))),(varToCount_2132 :: [])) config (-1)))) config (-1)))
(*{| rule `` `countAllOccurrences(_,_)_STRING`(Source,ToCount)=>#token("0","Int")`` requires `_andBool_`(`_andBool_`(isString(ToCount),isString(Source)),`_<Int__INT`(findString(Source,ToCount,#token("0","Int")),#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(cb338e251d0b5234f4abbf9c60008d68dc9dd3a8b1a5a410ed51f56d43a7b5af) contentStartColumn(8) contentStartLine(558) org.kframework.attributes.Location(Location(558,8,559,59)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varSource_2133) :: []),((String _ as varToCount_2134) :: [])) when ((((true) && (true))) && ((isTrue (eval_'_LT_'Int__INT(((evalfindString((varSource_2133 :: []),(varToCount_2134 :: []),((Lazy.force int0) :: [])) config (-1))),((Lazy.force int0) :: [])) config (-1))))) && (true) -> ((Lazy.force int0) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING and sort = 
SortString in match c with 
| _ -> try STRING.hook_replaceAll c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `replaceAll(_,_,_)_STRING`(Source,ToReplace,Replacement)=>`replace(_,_,_,_)_STRING`(Source,ToReplace,Replacement,`countAllOccurrences(_,_)_STRING`(Source,ToReplace))`` requires `_andBool_`(`_andBool_`(isString(Replacement),isString(Source)),isString(ToReplace)) ensures #token("true","Bool") [UNIQUE_ID(3358d86b83068ab68fc4f0ed02513db149426f011866db38ff0a5015e8fee30d) contentStartColumn(8) contentStartLine(575) org.kframework.attributes.Location(Location(575,8,575,154)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varSource_2135) :: []),((String _ as varToReplace_2136) :: []),((String _ as varReplacement_2137) :: [])) when ((((true) && (true))) && (true)) && (true) -> ((evalreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING((varSource_2135 :: []),(varToReplace_2136 :: []),(varReplacement_2137 :: []),((evalcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING((varSource_2135 :: []),(varToReplace_2136 :: [])) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalisStatesCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStatesCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isStatesCellOpt(noStatesCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoStatesCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStatesCellOpt(#KToken(#token("StatesCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStatesCell, var__2138) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStatesCellOpt(#KToken(#token("StatesCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStatesCellOpt, var__2139) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStatesCellOpt(`<states>`(K0))=>#token("true","Bool")`` requires isStateCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'states'_GT_',((Map (SortStateCellMap,_,_) as varK0_2140) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStatesCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2141)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisNstateCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisNstateCell and sort = 
SortBool in match c with 
(*{| rule ``isNstateCell(`<nstate>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'nstate'_GT_',((Int _ as varK0_2142) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isNstateCell(#KToken(#token("NstateCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortNstateCell, var__2143) :: [])) -> ((Bool true) :: [])
(*{| rule ``isNstateCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2144)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'Xor_'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Xor_'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_pow c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalinitWriteCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitWriteCell and sort = 
SortWriteCell in match c with 
(*{| rule ``initWriteCell(.KList)=>`<write>`(`.Set`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(95ac191de68f4a35ebd8bba6a72fb508df6ff3f071078d679c988beb7567ac57) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'write'_GT_',((Lazy.force const'Stop'Set))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitWriteCell : k Lazy.t = lazy (evalinitWriteCell () interned_bottom (-1))
let evalisKItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisKItem and sort = 
SortBool in match c with 
| [_] -> [Bool true] | _ -> [Bool false]
| [List (s,_,_)] when (s = SortList) -> [Bool true]
| [String _] -> [Bool true]
| [Map (s,_,_)] when (s = SortFunDefCellMap) -> [Bool true]
| [Bool _] -> [Bool true]
| [Map (s,_,_)] when (s = SortMap) -> [Bool true]
| [Int _] -> [Bool true]
| [Float _] -> [Bool true]
| [Map (s,_,_)] when (s = SortStateCellMap) -> [Bool true]
| [Set (s,_,_)] when (s = SortSet) -> [Bool true]
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_orBool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_orBool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_or c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_orBool__BOOL`(#token("false","Bool"),B)=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(ababa6e5e3735076933657815e24f99518fe532715ea97eae22ead8e30097b53) contentStartColumn(8) contentStartLine(319) org.kframework.attributes.Location(Location(319,8,319,32)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: []),((Bool _ as varB_2145) :: [])) when true && (true) -> (varB_2145 :: [])
(*{| rule `` `_orBool__BOOL`(_8,#token("true","Bool"))=>#token("true","Bool")`` requires isBool(_8) ensures #token("true","Bool") [UNIQUE_ID(497077a299480dbc06eccb33cd98338014bd125c4c601cb88a765dbcb334b14b) contentStartColumn(8) contentStartLine(318) org.kframework.attributes.Location(Location(318,8,318,34)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as var_8_2146) :: []),((Bool true) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule `` `_orBool__BOOL`(B,#token("false","Bool"))=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(630487d34ae5fc313a9b8cae2ad45b7b80671058bca3c97a7864774c5a431711) contentStartColumn(8) contentStartLine(320) org.kframework.attributes.Location(Location(320,8,320,32)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB_2147) :: []),((Bool false) :: [])) when true && (true) -> (varB_2147 :: [])
(*{| rule `` `_orBool__BOOL`(#token("true","Bool"),_3)=>#token("true","Bool")`` requires isBool(_3) ensures #token("true","Bool") [UNIQUE_ID(166d732e9fd6609a71feb6d62f8a420d291ac81be018b646ee1177935b008f01) contentStartColumn(8) contentStartLine(317) org.kframework.attributes.Location(Location(317,8,317,34)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool true) :: []),((Bool _ as var_3_2148) :: [])) when true && (true) -> ((Bool true) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'lc (c: k * k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'lc and sort = 
SortBool in match c with 
(*{| rule ``#lc(L1,L2,L3,L4)=>`_orBool__BOOL`(`_andBool_`(`_<Int__INT`(L1,L3),`_<=Int__INT`(L3,L2)),`_andBool_`(`_<Int__INT`(L1,L4),`_<=Int__INT`(L4,L2)))`` requires `_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L4)),isInt(L2)),isInt(L1)) ensures #token("true","Bool") [UNIQUE_ID(5e7282b13fde5fa27cff7a23496168848f4ac4f26921a74d6204547a997b8ab3) contentStartColumn(6) contentStartLine(257) org.kframework.attributes.Location(Location(257,6,258,55)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varL1_2149) :: []),((Int _ as varL2_2150) :: []),((Int _ as varL3_2151) :: []),((Int _ as varL4_2152) :: [])) when ((((((true) && (true))) && (true))) && (true)) && (true) -> ([Bool ((((((isTrue (eval_'_LT_'Int__INT((varL1_2149 :: []),(varL3_2151 :: [])) config (-1)))) && ((isTrue (eval_'_LT_Eqls'Int__INT((varL3_2151 :: []),(varL2_2150 :: [])) config (-1)))))) || ((((isTrue (eval_'_LT_'Int__INT((varL1_2149 :: []),(varL4_2152 :: [])) config (-1)))) && ((isTrue (eval_'_LT_Eqls'Int__INT((varL4_2152 :: []),(varL2_2150 :: [])) config (-1))))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize4 c)))])
let eval'Stop'Map (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Stop'Map and sort = 
SortMap in match c with 
| _ -> try MAP.hook_unit c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Stop'Map : k Lazy.t = lazy (eval'Stop'Map () interned_bottom (-1))
let rec eval'Hash'borrowmutck (c: k * k * k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'borrowmutck and sort = 
SortBool in match c with 
| (((Int _ as var_0_2153) :: []),(var_1_2154),((Int _ as var_2_2155) :: []),((Int _ as var_3_2156) :: []),((Int _ as var_4_2157) :: [])) when guard < 0(*{| rule ``#borrowmutck(L4,_0,L1,L2,L3)=>#borrowmutck(L4,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(M,`_[_<-undef]`(_0,L))),#match(#br(_61,_62,#immRef(L5)),`Map:lookup`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(_61)),isInt(L5)),isInt(L4)),isInt(L2)),isKItem(L)),isInt(_62)),isMap(M)),isInt(L1)),`_=/=Int__INT`(L5,L3))) ensures #token("true","Bool") [UNIQUE_ID(80dc759cc6674e08daf3f1552aa63cac091d6516bd96e3ff472329463247d01f) contentStartColumn(6) contentStartLine(290) org.kframework.attributes.Location(Location(290,6,292,27)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2154) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | (var_6_2158 :: []) as e3 -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2154),e3) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2159) :: []) -> (let e = ((evalMap'Coln'lookup((var_1_2154),e3) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2160) :: []),((Int _ as var_8_2161) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_9_2162) :: [])) :: [])) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'Int__INT((var_9_2162 :: []),(var_4_2157 :: [])) config (-1))))))) && (true) -> ((eval'Hash'borrowmutck((var_0_2153 :: []),(var_5_2159 :: []),(var_2_2155 :: []),(var_3_2156 :: []),(var_4_2157 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 0) else choice| _ -> (eval'Hash'borrowmutck c config 0))
| (((Int _ as var_0_2163) :: []),(var_1_2164),((Int _ as var_2_2165) :: []),((Int _ as var_3_2166) :: []),((Int _ as var_4_2167) :: [])) when guard < 1(*{| rule ``#borrowmutck(L5,_0,L1,L2,L3)=>#borrowmutck(L5,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(BEG,END,#immRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L5)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG)),`_andBool_`(`_=/=Int__INT`(L5,L),`_==Bool__BOOL`(#lc(L1,L2,BEG,END),#token("false","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(e6d6007ffd951ad30bed8a3d1416f5a26d6924341fcda158a1720f4d67fd2bd4) contentStartColumn(6) contentStartLine(295) org.kframework.attributes.Location(Location(295,6,297,70)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2164) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_6_2168) :: []) as e4 -> (let e = ((evalMap'Coln'lookup((var_1_2164),e4) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2169) :: []),((Int _ as var_8_2170) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_4_2171) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2164),e4) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2172) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2163 :: []),e4) config (-1)))) && ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'lc((var_2_2165 :: []),(var_3_2166 :: []),(var_7_2169 :: []),(var_8_2170 :: [])) config (-1))),((Bool false) :: [])) config (-1))))))))) && (((compare_kitem var_4_2171 var_4_2167) = 0) && true) -> ((eval'Hash'borrowmutck((var_0_2163 :: []),(var_5_2172 :: []),(var_2_2165 :: []),(var_3_2166 :: []),(var_4_2171 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 1) else choice| _ -> (eval'Hash'borrowmutck c config 1))
| (((Int _ as var_0_2173) :: []),(var_1_2174),((Int _ as var_2_2175) :: []),((Int _ as var_3_2176) :: []),((Int _ as var_4_2177) :: [])) when guard < 2(*{| rule ``#borrowmutck(L4,_0,L1,L2,L3)=>#borrowmutck(L4,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(M,`_[_<-undef]`(_0,L))),#match(#rs(_57),`Map:lookup`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isProps(_57)),isInt(L4)),isInt(L2)),isKItem(L)),isMap(M)),isInt(L1))) ensures #token("true","Bool") [UNIQUE_ID(9194dd104bb59a1517bfa7b8bfab67a43714bc7b6da6b287d750b0f28c89b97b) contentStartColumn(6) contentStartLine(287) org.kframework.attributes.Location(Location(287,6,288,45)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2174) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | (var_6_2178 :: []) as e5 -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2174),e5) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2179) :: []) -> (let e = ((evalMap'Coln'lookup((var_1_2174),e5) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply1(Lbl'Hash'rs,(var_7_2180 :: [])) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((true) && ((isTrue (evalisProps((var_7_2180 :: [])) config (-1)))))) && (true))) && (true))) && (true))) && (true))) && (true)))) && (true) -> ((eval'Hash'borrowmutck((var_0_2173 :: []),(var_5_2179 :: []),(var_2_2175 :: []),(var_3_2176 :: []),(var_4_2177 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 2) else choice| _ -> (eval'Hash'borrowmutck c config 2))
(*{| rule ``#borrowmutck(_49,_0,_50,_51,_52)=>#token("false","Bool")`` requires `_andBool_`(`_==K_`(`.Map`(.KList),_0),`_andBool_`(`_andBool_`(`_andBool_`(isInt(_52),isInt(_49)),isInt(_51)),isInt(_50))) ensures #token("true","Bool") [UNIQUE_ID(18bec766a0dce6a4d8e9c6858abdf6efa65d958a7956a0cae30f91e798ce270a) contentStartColumn(6) contentStartLine(325) org.kframework.attributes.Location(Location(325,6,325,42)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as var_49_2181) :: []),(var_0_2182),((Int _ as var_50_2183) :: []),((Int _ as var_51_2184) :: []),((Int _ as var_52_2185) :: [])) when (((isTrue (eval_'EqlsEqls'K_(((Lazy.force const'Stop'Map)),(var_0_2182)) config (-1)))) && (((((((true) && (true))) && (true))) && (true)))) && (true) -> ((Bool false) :: [])
| (((Int _ as var_0_2186) :: []),(var_1_2187),((Int _ as var_2_2188) :: []),((Int _ as var_3_2189) :: []),((Int _ as var_4_2190) :: [])) when guard < 4(*{| rule ``#borrowmutck(L4,_0,L1,L2,L3)=>#borrowmutck(L4,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(`#uninit_OSL-SYNTAX`(.KList),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L4)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1))) ensures #token("true","Bool") [UNIQUE_ID(bae923fcffef88219c8baedde80aa23a716e31f698812cbc8f18b6f68fdff8b8) contentStartColumn(6) contentStartLine(304) org.kframework.attributes.Location(Location(304,6,305,45)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2187) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_6_2191) :: []) as e6 -> (let e = ((evalMap'Coln'lookup((var_1_2187),e6) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2187),e6) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2192) :: []) when ((((((true) && (true))) && (true))) && (((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true)))) && (true) -> ((eval'Hash'borrowmutck((var_0_2186 :: []),(var_5_2192 :: []),(var_2_2188 :: []),(var_3_2189 :: []),(var_4_2190 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 4) else choice| _ -> (eval'Hash'borrowmutck c config 4))
| (((Int _ as var_0_2193) :: []),(var_1_2194),((Int _ as var_2_2195) :: []),((Int _ as var_3_2196) :: []),((Int _ as var_4_2197) :: [])) when guard < 5(*{| rule ``#borrowmutck(L,_0,L1,L2,L3)=>#borrowmutck(L,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(#match(#br(_63,_64,_65),`Map:lookup`(_0,L)),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L2)),isExp(_65)),isInt(L)),isInt(_63)),isInt(_64)),isMap(M)),isInt(L1))) ensures #token("true","Bool") [UNIQUE_ID(ee82e282f56db854fbed08bdd437f3d970479a7a6a94ed44dd77b35a2a1042a4) contentStartColumn(6) contentStartLine(322) org.kframework.attributes.Location(Location(322,6,323,47)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (let e = ((evalMap'Coln'lookup((var_1_2194),(var_0_2193 :: [])) config (-1))) in match e with 
| [Bottom] -> (eval'Hash'borrowmutck c config 5)
| (KApply3(Lbl'Hash'br,((Int _ as var_6_2198) :: []),((Int _ as var_7_2199) :: []),(var_8_2200 :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2194),(var_0_2193 :: [])) config (-1))) in match e with 
| [Bottom] -> (eval'Hash'borrowmutck c config 5)
| ((Map (SortMap,_,_) as var_5_2201) :: []) when ((((true) && (true))) && (((((((((((((((true) && (true))) && ((isTrue (evalisExp((var_8_2200 :: [])) config (-1)))))) && (true))) && (true))) && (true))) && (true))) && (true)))) && (true) -> ((eval'Hash'borrowmutck((var_0_2193 :: []),(var_5_2201 :: []),(var_2_2195 :: []),(var_3_2196 :: []),(var_4_2197 :: [])) config (-1)))| _ -> (eval'Hash'borrowmutck c config 5))| _ -> (eval'Hash'borrowmutck c config 5))
| (((Int _ as var_0_2202) :: []),(var_1_2203),((Int _ as var_2_2204) :: []),((Int _ as var_3_2205) :: []),((Int _ as var_4_2206) :: [])) when guard < 6(*{| rule ``#borrowmutck(L5,_0,L1,L2,L4)=>#borrowmutck(L5,M,L1,L2,L4)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(M,`_[_<-undef]`(_0,L))),#match(#br(_46,_47,#mutRef(L3)),`Map:lookup`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L5)),isInt(_47)),isInt(L4)),isInt(_46)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),`_=/=Int__INT`(L3,L4))) ensures #token("true","Bool") [UNIQUE_ID(19f47d0bd585d3d4af6594b91779d49004e5b3e1aa49f58d93c5f8aaf11c3d89) contentStartColumn(6) contentStartLine(307) org.kframework.attributes.Location(Location(307,6,309,27)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2203) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_6_2207) :: []) as e7 -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2203),e7) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2208) :: []) -> (let e = ((evalMap'Coln'lookup((var_1_2203),e7) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2209) :: []),((Int _ as var_8_2210) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_9_2211) :: [])) :: [])) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'Int__INT((var_9_2211 :: []),(var_4_2206 :: [])) config (-1))))))) && (true) -> ((eval'Hash'borrowmutck((var_0_2202 :: []),(var_5_2208 :: []),(var_2_2204 :: []),(var_3_2205 :: []),(var_4_2206 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 6) else choice| _ -> (eval'Hash'borrowmutck c config 6))
| (((Int _ as var_0_2212) :: []),(var_1_2213),((Int _ as var_2_2214) :: []),((Int _ as var_3_2215) :: []),((Int _ as var_4_2216) :: [])) when guard < 7(*{| rule ``#borrowmutck(L5,_0,L1,L2,L3)=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(BEG,END,#immRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L5)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG)),`_andBool_`(`_=/=Int__INT`(L5,L),`_==Bool__BOOL`(#lc(L1,L2,BEG,END),#token("true","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(d7a58a8dba957b95ce046838236156f5385d37a02692e92aaa7523dbba58175e) contentStartColumn(6) contentStartLine(299) org.kframework.attributes.Location(Location(299,6,301,69)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2213) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_5_2217) :: []) as e8 -> (let e = ((evalMap'Coln'lookup((var_1_2213),e8) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_6_2218) :: []),((Int _ as var_7_2219) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_4_2220) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2213),e8) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_8_2221) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2212 :: []),e8) config (-1)))) && ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'lc((var_2_2214 :: []),(var_3_2215 :: []),(var_6_2218 :: []),(var_7_2219 :: [])) config (-1))),((Bool true) :: [])) config (-1))))))))) && (((compare_kitem var_4_2216 var_4_2220) = 0) && true) -> ((Bool true) :: [])| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 7) else choice| _ -> (eval'Hash'borrowmutck c config 7))
| (((Int _ as var_0_2222) :: []),(var_1_2223),((Int _ as var_2_2224) :: []),((Int _ as var_3_2225) :: []),((Int _ as var_4_2226) :: [])) when guard < 8(*{| rule ``#borrowmutck(L5,_0,L1,L2,L3)=>#borrowmutck(L5,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(BEG,END,#mutRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L5)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG)),`_andBool_`(`_=/=Int__INT`(L5,L),`_==Bool__BOOL`(#lc(L1,L2,BEG,END),#token("false","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(b09a05e9c87e9d6c4e402172dd9208a7db559c453e1402f73469dddf1ac75482) contentStartColumn(6) contentStartLine(311) org.kframework.attributes.Location(Location(311,6,314,75)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2223) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_6_2227) :: []) as e9 -> (let e = ((evalMap'Coln'lookup((var_1_2223),e9) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2228) :: []),((Int _ as var_8_2229) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_4_2230) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2223),e9) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2231) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2222 :: []),e9) config (-1)))) && ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'lc((var_2_2224 :: []),(var_3_2225 :: []),(var_7_2228 :: []),(var_8_2229 :: [])) config (-1))),((Bool false) :: [])) config (-1))))))))) && (((compare_kitem var_4_2230 var_4_2226) = 0) && true) -> ((eval'Hash'borrowmutck((var_0_2222 :: []),(var_5_2231 :: []),(var_2_2224 :: []),(var_3_2225 :: []),(var_4_2230 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 8) else choice| _ -> (eval'Hash'borrowmutck c config 8))
| (((Int _ as var_0_2232) :: []),(var_1_2233),((Int _ as var_2_2234) :: []),((Int _ as var_3_2235) :: []),((Int _ as var_4_2236) :: [])) when guard < 9(*{| rule ``#borrowmutck(L5,_0,L1,L2,L3)=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(BEG,END,#mutRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L5)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG)),`_andBool_`(`_=/=Int__INT`(L5,L),`_==Bool__BOOL`(#lc(L1,L2,BEG,END),#token("true","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(aea22591121fbe70c4f6381f3b44710bfe6d1f8f586032b97a8a63deece7c43e) contentStartColumn(6) contentStartLine(316) org.kframework.attributes.Location(Location(316,6,319,74)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2233) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_5_2237) :: []) as e10 -> (let e = ((evalMap'Coln'lookup((var_1_2233),e10) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_6_2238) :: []),((Int _ as var_7_2239) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_4_2240) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2233),e10) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_8_2241) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2232 :: []),e10) config (-1)))) && ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'lc((var_2_2234 :: []),(var_3_2235 :: []),(var_6_2238 :: []),(var_7_2239 :: [])) config (-1))),((Bool true) :: [])) config (-1))))))))) && (((compare_kitem var_4_2236 var_4_2240) = 0) && true) -> ((Bool true) :: [])| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowmutck c config 9) else choice| _ -> (eval'Hash'borrowmutck c config 9))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize5 c)))])
let eval_'_LT_'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT_'String__STRING and sort = 
SortBool in match c with 
| _ -> try STRING.hook_lt c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'_GT_'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_GT_'String__STRING and sort = 
SortBool in match c with 
| _ -> try STRING.hook_gt c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_>String__STRING`(S1,S2)=>`_<String__STRING`(S2,S1)`` requires `_andBool_`(isString(S2),isString(S1)) ensures #token("true","Bool") [UNIQUE_ID(85f6517ce0e2b7074d495585267afa5b0502e09d5bf63c57fb6c6d77174aa16a) contentStartColumn(8) contentStartLine(550) org.kframework.attributes.Location(Location(550,8,550,52)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varS1_2242) :: []),((String _ as varS2_2243) :: [])) when ((true) && (true)) && (true) -> ((eval_'_LT_'String__STRING((varS2_2243 :: []),(varS1_2242 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisBorrowItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisBorrowItem and sort = 
SortBool in match c with 
(*{| rule ``isBorrowItem(#KToken(#token("BorrowItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBorrowItem, var__2244) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBorrowItem(#br(K0,K1,K2))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(isInt(K0),isInt(K1)),isExp(K2)) ensures #token("true","Bool") []|}*)
| ((KApply3(Lbl'Hash'br,((Int _ as varK0_2245) :: []),((Int _ as varK1_2246) :: []),(varK2_2247 :: [])) :: [])) when ((((true) && (true))) && ((isTrue (evalisExp((varK2_2247 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isBorrowItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2248)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'EqlsSlshEqls'K_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsSlshEqls'K_ and sort = 
SortBool in match c with 
| _ -> try KEQUAL.hook_ne c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_=/=K_`(K1,K2)=>`notBool_`(`_==K_`(K1,K2))`` requires `_andBool_`(isK(K1),isK(K2)) ensures #token("true","Bool") [UNIQUE_ID(0221882a8af3c088550dce160a5b2e48351ef2431aad518a72f3d65258a4066d) contentStartColumn(8) contentStartLine(763) org.kframework.attributes.Location(Location(763,8,763,45)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((varK1_2249),(varK2_2250)) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'EqlsEqls'K_((varK1_2249),(varK2_2250)) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'checkInit (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'checkInit and sort = 
SortBool in match c with 
(*{| rule ``#checkInit(X,E,S)=>`_=/=K_`(`Map:lookup`(S,`Map:lookup`(E,X)),`#uninit_OSL-SYNTAX`(.KList))`` requires `_andBool_`(`_andBool_`(isMap(E),isId(X)),isMap(S)) ensures #token("true","Bool") [UNIQUE_ID(6500274db9ca237493f3b35f42a9c0a8484e163a8685e2c36411309bd83fd3fc) contentStartColumn(6) contentStartLine(333) org.kframework.attributes.Location(Location(333,6,333,56)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((varX_2251 :: []),((Map (SortMap,_,_) as varE_2252) :: []),((Map (SortMap,_,_) as varS_2253) :: [])) when ((((true) && ((isTrue (evalisId((varX_2251 :: [])) config (-1)))))) && (true)) && (true) -> ((eval_'EqlsSlshEqls'K_(((evalMap'Coln'lookup((varS_2253 :: []),((evalMap'Coln'lookup((varE_2252 :: []),(varX_2251 :: [])) config (-1)))) config (-1))),(const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalisKConfigVar (c: k) (config: k) (guard: int) : k = let lbl = 
LblisKConfigVar and sort = 
SortBool in match c with 
(*{| rule ``isKConfigVar(#KToken(#token("KConfigVar","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortKConfigVar, var__2254) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKConfigVar(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2255)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFnameCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFnameCell and sort = 
SortBool in match c with 
(*{| rule ``isFnameCell(`<fname>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fname'_GT_',(varK0_2256)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFnameCell(#KToken(#token("FnameCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFnameCell, var__2257) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFnameCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2258)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFparamsCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFparamsCell and sort = 
SortBool in match c with 
(*{| rule ``isFparamsCell(#KToken(#token("FparamsCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFparamsCell, var__2259) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFparamsCell(`<fparams>`(K0))=>#token("true","Bool")`` requires isParameters(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fparams'_GT_',(varK0_2260 :: [])) :: [])) when (isTrue (evalisParameters((varK0_2260 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isFparamsCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2261)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFretCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFretCell and sort = 
SortBool in match c with 
(*{| rule ``isFretCell(`<fret>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fret'_GT_',(varK0_2262)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFretCell(#KToken(#token("FretCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFretCell, var__2263) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFretCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2264)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFbodyCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFbodyCell and sort = 
SortBool in match c with 
(*{| rule ``isFbodyCell(#KToken(#token("FbodyCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFbodyCell, var__2265) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFbodyCell(`<fbody>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fbody'_GT_',(varK0_2266)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFbodyCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2267)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFunDefCellMap (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunDefCellMap and sort = 
SortBool in match c with 
| [Map (s,_,_)] when (s = SortFunDefCellMap) -> [Bool true]
(*{| rule ``isFunDefCellMap(#KToken(#token("FunDefCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefCell, var__2268) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefCellMap(#KToken(#token("FunDefCellMap","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefCellMap, var__2269) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefCellMap(`<funDef>`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isFnameCell(K0),isFparamsCell(K1)),isFretCell(K2)),isFbodyCell(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'funDef'_GT_',(varK0_2270 :: []),(varK1_2271 :: []),(varK2_2272 :: []),(varK3_2273 :: [])) :: [])) when (((((((isTrue (evalisFnameCell((varK0_2270 :: [])) config (-1)))) && ((isTrue (evalisFparamsCell((varK1_2271 :: [])) config (-1)))))) && ((isTrue (evalisFretCell((varK2_2272 :: [])) config (-1)))))) && ((isTrue (evalisFbodyCell((varK3_2273 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isFunDefCellMap(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2274)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFunDefsCellFragment (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunDefsCellFragment and sort = 
SortBool in match c with 
(*{| rule ``isFunDefsCellFragment(`<funDefs>-fragment`(K0))=>#token("true","Bool")`` requires isFunDefCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'funDefs'_GT_Hyph'fragment,((Map (SortFunDefCellMap,_,_) as varK0_2275) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCellFragment(#KToken(#token("FunDefsCellFragment","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefsCellFragment, var__2276) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCellFragment(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2277)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisKResult (c: k) (config: k) (guard: int) : k = let lbl = 
LblisKResult and sort = 
SortBool in match c with 
(*{| rule ``isKResult(#Loc(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isValue(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'Loc,(varK0_2278 :: []),((Int _ as varK1_2279) :: [])) :: [])) when (((isTrue (evalisValue((varK0_2278 :: [])) config (-1)))) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isKResult(#immRef(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'immRef,((Int _ as varK0_2280) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isKResult(#rs(K0))=>#token("true","Bool")`` requires isProps(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'rs,(varK0_2281 :: [])) :: [])) when (isTrue (evalisProps((varK0_2281 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isKResult(`#void_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'void_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKResult(#loc(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'loc,((Int _ as varK0_2282) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isKResult(#mutRef(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'mutRef,((Int _ as varK0_2283) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isKResult(#KToken(#token("KResult","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortKResult, var__2284) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKResult(#KToken(#token("Value","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortValue, var__2285) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKResult(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2286)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFunDefsCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunDefsCell and sort = 
SortBool in match c with 
(*{| rule ``isFunDefsCell(`<funDefs>`(K0))=>#token("true","Bool")`` requires isFunDefCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'funDefs'_GT_',((Map (SortFunDefCellMap,_,_) as varK0_2287) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCell(#KToken(#token("FunDefsCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefsCell, var__2288) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2289)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisCell and sort = 
SortBool in match c with 
(*{| rule ``isCell(`<fparams>`(K0))=>#token("true","Bool")`` requires isParameters(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fparams'_GT_',(varK0_2290 :: [])) :: [])) when (isTrue (evalisParameters((varK0_2290 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("FnameCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFnameCell, var__2291) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<states>`(K0))=>#token("true","Bool")`` requires isStateCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'states'_GT_',((Map (SortStateCellMap,_,_) as varK0_2292) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("Cell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortCell, var__2293) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("FretCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFretCell, var__2294) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("TimerCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTimerCell, var__2295) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<fname>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fname'_GT_',(varK0_2296)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("EnvCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortEnvCell, var__2297) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("FbodyCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFbodyCell, var__2298) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("TCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTCell, var__2299) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("FunDefsCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefsCell, var__2300) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("IndexesCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexesCell, var__2301) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<T>`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isStatesCell(K0),isNstateCell(K1)),isTmpCell(K2)),isFunDefsCell(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'T'_GT_',(varK0_2302 :: []),(varK1_2303 :: []),(varK2_2304 :: []),(varK3_2305 :: [])) :: [])) when (((((((isTrue (evalisStatesCell((varK0_2302 :: [])) config (-1)))) && ((isTrue (evalisNstateCell((varK1_2303 :: [])) config (-1)))))) && ((isTrue (evalisTmpCell((varK2_2304 :: [])) config (-1)))))) && ((isTrue (evalisFunDefsCell((varK3_2305 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<k>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'k'_GT_',(varK0_2306)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<indexes>`(K0))=>#token("true","Bool")`` requires isIndexes(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'indexes'_GT_',(varK0_2307 :: [])) :: [])) when (isTrue (evalisIndexes((varK0_2307 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("IndexCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexCell, var__2308) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("StatesCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStatesCell, var__2309) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<write>`(K0))=>#token("true","Bool")`` requires isSet(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as varK0_2310) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<timer>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'timer'_GT_',((Int _ as varK0_2311) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("KCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortKCell, var__2312) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<fbody>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fbody'_GT_',(varK0_2313)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<env>`(K0))=>#token("true","Bool")`` requires isMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as varK0_2314) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<state>`(K0,K1,K2,K3,K4,K5,K6,K7))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isIndexCell(K0),isKCell(K1)),isEnvCell(K2)),isStoreCell(K3)),isStackCell(K4)),isWriteCell(K5)),isTimerCell(K6)),isIndexesCell(K7)) ensures #token("true","Bool") []|}*)
| ((KApply8(Lbl'_LT_'state'_GT_',(varK0_2315 :: []),(varK1_2316 :: []),(varK2_2317 :: []),(varK3_2318 :: []),(varK4_2319 :: []),(varK5_2320 :: []),(varK6_2321 :: []),(varK7_2322 :: [])) :: [])) when (((((((((((((((isTrue (evalisIndexCell((varK0_2315 :: [])) config (-1)))) && ((isTrue (evalisKCell((varK1_2316 :: [])) config (-1)))))) && ((isTrue (evalisEnvCell((varK2_2317 :: [])) config (-1)))))) && ((isTrue (evalisStoreCell((varK3_2318 :: [])) config (-1)))))) && ((isTrue (evalisStackCell((varK4_2319 :: [])) config (-1)))))) && ((isTrue (evalisWriteCell((varK5_2320 :: [])) config (-1)))))) && ((isTrue (evalisTimerCell((varK6_2321 :: [])) config (-1)))))) && ((isTrue (evalisIndexesCell((varK7_2322 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("StateCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStateCell, var__2323) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("FparamsCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFparamsCell, var__2324) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<tmp>`(K0))=>#token("true","Bool")`` requires isList(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'tmp'_GT_',((List (SortList,_,_) as varK0_2325) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<store>`(K0))=>#token("true","Bool")`` requires isMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as varK0_2326) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<funDef>`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isFnameCell(K0),isFparamsCell(K1)),isFretCell(K2)),isFbodyCell(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'funDef'_GT_',(varK0_2327 :: []),(varK1_2328 :: []),(varK2_2329 :: []),(varK3_2330 :: [])) :: [])) when (((((((isTrue (evalisFnameCell((varK0_2327 :: [])) config (-1)))) && ((isTrue (evalisFparamsCell((varK1_2328 :: [])) config (-1)))))) && ((isTrue (evalisFretCell((varK2_2329 :: [])) config (-1)))))) && ((isTrue (evalisFbodyCell((varK3_2330 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<fret>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'fret'_GT_',(varK0_2331)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("NstateCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortNstateCell, var__2332) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<index>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'index'_GT_',((Int _ as varK0_2333) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<nstate>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'nstate'_GT_',((Int _ as varK0_2334) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("TmpCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTmpCell, var__2335) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("WriteCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortWriteCell, var__2336) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(`<funDefs>`(K0))=>#token("true","Bool")`` requires isFunDefCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'funDefs'_GT_',((Map (SortFunDefCellMap,_,_) as varK0_2337) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(`<stack>`(K0))=>#token("true","Bool")`` requires isList(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as varK0_2338) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("StackCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStackCell, var__2339) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("StoreCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStoreCell, var__2340) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(#KToken(#token("FunDefCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefCell, var__2341) :: [])) -> ((Bool true) :: [])
(*{| rule ``isCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2342)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalvalues (c: k) (config: k) (guard: int) : k = let lbl = 
Lblvalues and sort = 
SortList in match c with 
| _ -> try MAP.hook_values c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalList'Coln'get (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblList'Coln'get and sort = 
SortKItem in match c with 
| _ -> try LIST.hook_get c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'lstat'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'lstat'LPar'_'RPar'_K'Hyph'IO and sort = 
SortKItem in match c with 
| _ -> try IO.hook_lstat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitFnameCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitFnameCell and sort = 
SortFnameCell in match c with 
(*{| rule ``initFnameCell(.KList)=>`<fname>`(.K)`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(c6b0cfeec25868d70bbde3a328252881044f4087d60990b2c6e38f51bc227daa) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'fname'_GT_',([])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitFnameCell : k Lazy.t = lazy (evalinitFnameCell () interned_bottom (-1))
let evalisRItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisRItem and sort = 
SortBool in match c with 
(*{| rule ``isRItem(`#removeState_CONTROL`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'removeState_CONTROL) :: [])) -> ((Bool true) :: [])
(*{| rule ``isRItem(#KToken(#token("RItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortRItem, var__2343) :: [])) -> ((Bool true) :: [])
(*{| rule ``isRItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2344)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalStateCellMapItem (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblStateCellMapItem and sort = 
SortStateCellMap in match c with 
| _ -> try MAP.hook_element c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisStoreCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStoreCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isStoreCellOpt(noStoreCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoStoreCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStoreCellOpt(#KToken(#token("StoreCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStoreCellOpt, var__2345) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStoreCellOpt(#KToken(#token("StoreCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStoreCell, var__2346) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStoreCellOpt(`<store>`(K0))=>#token("true","Bool")`` requires isMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as varK0_2347) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStoreCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2348)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Stop'List (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Stop'List and sort = 
SortList in match c with 
| _ -> try LIST.hook_unit c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Stop'List : k Lazy.t = lazy (eval'Stop'List () interned_bottom (-1))
let evalisLoopItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisLoopItem and sort = 
SortBool in match c with 
(*{| rule ``isLoopItem(#decompose(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'decompose,(varK0_2349)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isLoopItem(#KToken(#token("LoopItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortLoopItem, var__2350) :: [])) -> ((Bool true) :: [])
(*{| rule ``isLoopItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2351)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFunDefsCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunDefsCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isFunDefsCellOpt(#KToken(#token("FunDefsCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefsCellOpt, var__2352) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCellOpt(`<funDefs>`(K0))=>#token("true","Bool")`` requires isFunDefCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'funDefs'_GT_',((Map (SortFunDefCellMap,_,_) as varK0_2353) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCellOpt(#KToken(#token("FunDefsCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefsCell, var__2354) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCellOpt(noFunDefsCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoFunDefsCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefsCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2355)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisWItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisWItem and sort = 
SortBool in match c with 
(*{| rule ``isWItem(#KToken(#token("WItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortWItem, var__2356) :: [])) -> ((Bool true) :: [])
(*{| rule ``isWItem(#writev(K0,K1))=>#token("true","Bool")`` requires `_andBool_`(isInt(K0),isInt(K1)) ensures #token("true","Bool") []|}*)
| ((KApply2(Lbl'Hash'writev,((Int _ as varK0_2357) :: []),((Int _ as varK1_2358) :: [])) :: [])) when ((true) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isWItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2359)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalrandInt (c: k) (config: k) (guard: int) : k = let lbl = 
LblrandInt and sort = 
SortInt in match c with 
| _ -> try INT.hook_rand c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'configuration_K'Hyph'REFLECTION (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'configuration_K'Hyph'REFLECTION and sort = 
SortK in match c with 
| _ -> try KREFLECTION.hook_configuration c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let evalisFloat (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFloat and sort = 
SortBool in match c with 
| [Float _] -> [Bool true]
(*{| rule ``isFloat(#KToken(#token("Float","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFloat, var__2360) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFloat(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2361)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'close'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'close'LPar'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_close c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisOItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisOItem and sort = 
SortBool in match c with 
(*{| rule ``isOItem(#borrowMutCK(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isInt(K0),isInt(K1)),isInt(K2)),isInt(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'Hash'borrowMutCK,((Int _ as varK0_2362) :: []),((Int _ as varK1_2363) :: []),((Int _ as varK2_2364) :: []),((Int _ as varK3_2365) :: [])) :: [])) when ((((((true) && (true))) && (true))) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isOItem(#Read(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'Read,(varK0_2366)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isOItem(#KToken(#token("OItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortOItem, var__2367) :: [])) -> ((Bool true) :: [])
(*{| rule ``isOItem(#borrowImmCK(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isInt(K0),isInt(K1)),isInt(K2)),isInt(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'Hash'borrowImmCK,((Int _ as varK0_2368) :: []),((Int _ as varK1_2369) :: []),((Int _ as varK2_2370) :: []),((Int _ as varK3_2371) :: [])) :: [])) when ((((((true) && (true))) && (true))) && (true)) && (true) -> ((Bool true) :: [])
(*{| rule ``isOItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2372)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitFretCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitFretCell and sort = 
SortFretCell in match c with 
(*{| rule ``initFretCell(.KList)=>`<fret>`(.K)`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(5e883c3e228f14a02660196d8c2f06947340c49dec0396774b907e418538c4f6) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'fret'_GT_',([])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitFretCell : k Lazy.t = lazy (evalinitFretCell () interned_bottom (-1))
let evalkeys_list'LPar'_'RPar'_MAP (c: k) (config: k) (guard: int) : k = let lbl = 
Lblkeys_list'LPar'_'RPar'_MAP and sort = 
SortList in match c with 
| _ -> try MAP.hook_keys_list c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Stop'FunDefCellMap (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Stop'FunDefCellMap and sort = 
SortFunDefCellMap in match c with 
| _ -> try MAP.hook_unit c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Stop'FunDefCellMap : k Lazy.t = lazy (eval'Stop'FunDefCellMap () interned_bottom (-1))
let evalinitFunDefsCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitFunDefsCell and sort = 
SortFunDefsCell in match c with 
(*{| rule ``initFunDefsCell(.KList)=>`<funDefs>`(`.FunDefCellMap`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(50459ae813ffc712b1acdd29df0d798d848b9010ab6e73b1e4682d0cb1095692) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'funDefs'_GT_',((Lazy.force const'Stop'FunDefCellMap))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitFunDefsCell : k Lazy.t = lazy (evalinitFunDefsCell () interned_bottom (-1))
let evalString2Id (c: k) (config: k) (guard: int) : k = let lbl = 
LblString2Id and sort = 
SortId in match c with 
| _ -> try STRING.hook_string2token c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalInt2String (c: k) (config: k) (guard: int) : k = let lbl = 
LblInt2String and sort = 
SortString in match c with 
| _ -> try STRING.hook_int2string c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalfreshId (c: k) (config: k) (guard: int) : k = let lbl = 
LblfreshId and sort = 
SortId in match c with 
(*{| rule ``freshId(I)=>`String2Id`(`_+String__STRING`(#token("\"_\"","String"),`Int2String`(I)))`` requires isInt(I) ensures #token("true","Bool") [UNIQUE_ID(cb89ef33f1b27d7db53de8a3c240cc3b944a9a0a9559c32b1b97c7bfec87bfa2) contentStartColumn(8) contentStartLine(745) org.kframework.attributes.Location(Location(745,8,745,62)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI_2373) :: [])) when true && (true) -> ((evalString2Id(((eval_'Plus'String__STRING(((String "_") :: []),((evalInt2String((varI_2373 :: [])) config (-1)))) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalchrChar (c: k) (config: k) (guard: int) : k = let lbl = 
LblchrChar and sort = 
SortString in match c with 
| _ -> try STRING.hook_chr c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_orElseBool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_orElseBool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_orElse c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_orElseBool__BOOL`(#token("true","Bool"),_1)=>#token("true","Bool")`` requires isBool(_1) ensures #token("true","Bool") [UNIQUE_ID(6ea998be2fb9b60b7964c4b4939293b6fecb47f52a53031c6227218949cf22f0) contentStartColumn(8) contentStartLine(322) org.kframework.attributes.Location(Location(322,8,322,33)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool true) :: []),((Bool _ as var_1_2374) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule `` `_orElseBool__BOOL`(#token("false","Bool"),K)=>K`` requires isBool(K) ensures #token("true","Bool") [UNIQUE_ID(efdd958595a7fecb549f4368ba6097862b61abc903a6d33684803a3a0043bda8) contentStartColumn(8) contentStartLine(324) org.kframework.attributes.Location(Location(324,8,324,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: []),((Bool _ as varK_2375) :: [])) when true && (true) -> (varK_2375 :: [])
(*{| rule `` `_orElseBool__BOOL`(K,#token("false","Bool"))=>K`` requires isBool(K) ensures #token("true","Bool") [UNIQUE_ID(8c7774b237a73a62ffd53be8d97ac73eb2f040dcf41983ea18840919b416d291) contentStartColumn(8) contentStartLine(325) org.kframework.attributes.Location(Location(325,8,325,37)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varK_2376) :: []),((Bool false) :: [])) when true && (true) -> (varK_2376 :: [])
(*{| rule `` `_orElseBool__BOOL`(_4,#token("true","Bool"))=>#token("true","Bool")`` requires isBool(_4) ensures #token("true","Bool") [UNIQUE_ID(2882f8b0a388fe37313c6b828cb9039a9838984f56e50b5095476c319b3dfbd8) contentStartColumn(8) contentStartLine(323) org.kframework.attributes.Location(Location(323,8,323,33)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as var_4_2377) :: []),((Bool true) :: [])) when true && (true) -> ((Bool true) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'Slsh'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Slsh'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_tdiv c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_divInt__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_divInt__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_ediv c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_divInt__INT`(I1,I2)=>`_/Int__INT`(`_-Int__INT`(I1,`_modInt__INT`(I1,I2)),I2)`` requires `_andBool_`(`_andBool_`(isInt(I1),isInt(I2)),`_=/=Int__INT`(I2,#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(b7fc8e019ea9840ee8c5e9232d39d7944e010f117308f672e92891325002610e) contentStartColumn(8) contentStartLine(388) org.kframework.attributes.Location(Location(388,8,389,23)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((Int _ as varI1_2378) :: []),((Int _ as varI2_2379) :: [])) when ((((true) && (true))) && ((isTrue (eval_'EqlsSlshEqls'Int__INT((varI2_2379 :: []),((Lazy.force int0) :: [])) config (-1))))) && (true) -> ((eval_'Slsh'Int__INT(((eval_'Hyph'Int__INT((varI1_2378 :: []),((eval_modInt__INT((varI1_2378 :: []),(varI2_2379 :: [])) config (-1)))) config (-1))),(varI2_2379 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalList'Coln'range (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblList'Coln'range and sort = 
SortList in match c with 
| _ -> try LIST.hook_range c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let eval'Hash'unwrapVal (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'unwrapVal and sort = 
SortInt in match c with 
(*{| rule ``#unwrapVal(V)=>V`` requires isInt(V) ensures #token("true","Bool") [UNIQUE_ID(1f67efce365d148d645f7b65eda0e1caf990d8dcaccdc26b5cc5dba7b6ac6978) contentStartColumn(6) contentStartLine(65) org.kframework.attributes.Location(Location(65,6,65,28)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varV_2380) :: [])) when true && (true) -> (varV_2380 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec eval'Hash'wv (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'wv and sort = 
SortInt in match c with 
(*{| rule ``#wv(X,ENV)=>#unwrapVal(`Map:lookup`(ENV,X))`` requires `_andBool_`(isMap(ENV),isId(X)) ensures #token("true","Bool") [UNIQUE_ID(689e040d702ca5e1bdd7d393106d8b56b55699b60702d44e6e4b951b649ddcbc) contentStartColumn(6) contentStartLine(67) org.kframework.attributes.Location(Location(67,6,67,46)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((varX_2381 :: []),((Map (SortMap,_,_) as varENV_2382) :: [])) when ((true) && ((isTrue (evalisId((varX_2381 :: [])) config (-1))))) && (true) -> ((eval'Hash'unwrapVal(((evalMap'Coln'lookup((varENV_2382 :: []),(varX_2381 :: [])) config (-1)))) config (-1)))
(*{| rule ``#wv(`*__OSL-SYNTAX`(E),ENV)=>#wv(E,ENV)`` requires `_andBool_`(isMap(ENV),isExp(E)) ensures #token("true","Bool") [UNIQUE_ID(25a8ae4ab391e31405db9e6a7ec16d368a6a2cb8849ab68ec0c51ab4e324a7fd) contentStartColumn(6) contentStartLine(68) org.kframework.attributes.Location(Location(68,6,68,41)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply1(Lbl'Star'__OSL'Hyph'SYNTAX,(varE_2383 :: [])) :: []),((Map (SortMap,_,_) as varENV_2384) :: [])) when ((true) && ((isTrue (evalisExp((varE_2383 :: [])) config (-1))))) && (true) -> ((eval'Hash'wv((varE_2383 :: []),(varENV_2384 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalinitEnvCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitEnvCell and sort = 
SortEnvCell in match c with 
(*{| rule ``initEnvCell(.KList)=>`<env>`(`.Map`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(c7a3b857bb45cfd6e74b7a22ee3624d3c09e5603948d8474a27aea184c4c80ec) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'env'_GT_',((Lazy.force const'Stop'Map))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitEnvCell : k Lazy.t = lazy (evalinitEnvCell () interned_bottom (-1))
let eval'Hash'compareA (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'compareA and sort = 
SortBool in match c with 
(*{| rule ``#compareA(#br(_38,_39,R1),#br(_40,_41,R2))=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(_38),isInt(_41)),isExp(R2)),isExp(R1)),isInt(_40)),isInt(_39)),`_=/=K_`(R1,R2)) ensures #token("true","Bool") [UNIQUE_ID(7a4b9a8ed2bd92c62948d226aa68d13041bd19875222d2bd0d408a97c368654e) contentStartColumn(6) contentStartLine(157) org.kframework.attributes.Location(Location(157,6,158,25)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| ((KApply3(Lbl'Hash'br,((Int _ as var_38_2385) :: []),((Int _ as var_39_2386) :: []),(varR1_2387 :: [])) :: []),(KApply3(Lbl'Hash'br,((Int _ as var_40_2388) :: []),((Int _ as var_41_2389) :: []),(varR2_2390 :: [])) :: [])) when ((((((((((((true) && (true))) && ((isTrue (evalisExp((varR2_2390 :: [])) config (-1)))))) && ((isTrue (evalisExp((varR1_2387 :: [])) config (-1)))))) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'K_((varR1_2387 :: []),(varR2_2390 :: [])) config (-1))))) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(`#uninit_OSL-SYNTAX`(.KList),#br(_23,_24,_25))=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(isInt(_24),isInt(_23)),isExp(_25)) ensures #token("true","Bool") [UNIQUE_ID(9edcadf239328ea3bb1ee169ae675d865d636e8b8438e36bd37b8b35368f4c01) contentStartColumn(6) contentStartLine(145) org.kframework.attributes.Location(Location(145,6,145,45)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []),(KApply3(Lbl'Hash'br,((Int _ as var_23_2391) :: []),((Int _ as var_24_2392) :: []),(var_25_2393 :: [])) :: [])) when ((((true) && (true))) && ((isTrue (evalisExp((var_25_2393 :: [])) config (-1))))) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(#rs(P),#rs(P1))=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(isProps(P1),isProps(P)),`_=/=K_`(P,P1)) ensures #token("true","Bool") [UNIQUE_ID(85d14520920f2f5ecdc9cf1c41dd2acd463f2d8bfc370cb50fef14a296b21062) contentStartColumn(6) contentStartLine(152) org.kframework.attributes.Location(Location(152,6,153,24)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| ((KApply1(Lbl'Hash'rs,(varP_2394 :: [])) :: []),(KApply1(Lbl'Hash'rs,(varP1_2395 :: [])) :: [])) when (((((isTrue (evalisProps((varP1_2395 :: [])) config (-1)))) && ((isTrue (evalisProps((varP_2394 :: [])) config (-1)))))) && ((isTrue (eval_'EqlsSlshEqls'K_((varP_2394 :: []),(varP1_2395 :: [])) config (-1))))) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(#rs(_34),#br(_35,_36,_37))=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isProps(_34),isInt(_36)),isExp(_37)),isInt(_35)) ensures #token("true","Bool") [UNIQUE_ID(d3607b3eaa00fcd256471a77ce1494510424f61f959b31cd3217331af52eb8f3) contentStartColumn(6) contentStartLine(148) org.kframework.attributes.Location(Location(148,6,148,44)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply1(Lbl'Hash'rs,(var_34_2396 :: [])) :: []),(KApply3(Lbl'Hash'br,((Int _ as var_35_2397) :: []),((Int _ as var_36_2398) :: []),(var_37_2399 :: [])) :: [])) when (((((((isTrue (evalisProps((var_34_2396 :: [])) config (-1)))) && (true))) && ((isTrue (evalisExp((var_37_2399 :: [])) config (-1)))))) && (true)) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(#br(_30,_31,R1),#br(_32,_33,R1))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(_33),isInt(_32)),isInt(_30)),isExp(R1)),isInt(_31)) ensures #token("true","Bool") [UNIQUE_ID(3a701ade3c9370806a69025d9ddf6eb6cd444a1d6f96775ceaae3a97d5777cc1) contentStartColumn(6) contentStartLine(155) org.kframework.attributes.Location(Location(155,6,155,49)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply3(Lbl'Hash'br,((Int _ as var_30_2400) :: []),((Int _ as var_31_2401) :: []),(varR1_2402 :: [])) :: []),(KApply3(Lbl'Hash'br,((Int _ as var_32_2403) :: []),((Int _ as var_33_2404) :: []),(varR1_2405 :: [])) :: [])) when ((((((((true) && (true))) && (true))) && ((isTrue (evalisExp((varR1_2405 :: [])) config (-1)))))) && (true)) && (((compare_kitem varR1_2405 varR1_2402) = 0) && true) -> ((Bool true) :: [])
(*{| rule ``#compareA(#br(_27,_28,_29),`#uninit_OSL-SYNTAX`(.KList))=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(isInt(_28),isInt(_27)),isExp(_29)) ensures #token("true","Bool") [UNIQUE_ID(58c5d674faeafeae4c379420d285f13296f818b99791f51e21db74e0e71c43f7) contentStartColumn(6) contentStartLine(146) org.kframework.attributes.Location(Location(146,6,146,45)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply3(Lbl'Hash'br,((Int _ as var_27_2406) :: []),((Int _ as var_28_2407) :: []),(var_29_2408 :: [])) :: []),(KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: [])) when ((((true) && (true))) && ((isTrue (evalisExp((var_29_2408 :: [])) config (-1))))) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(`#uninit_OSL-SYNTAX`(.KList),#rs(_42))=>#token("false","Bool")`` requires isProps(_42) ensures #token("true","Bool") [UNIQUE_ID(3d06b5d8bbc519a63f29a65496be7d10f0a8f3866fc06c9ed988f8f18669a761) contentStartColumn(6) contentStartLine(142) org.kframework.attributes.Location(Location(142,6,142,41)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []),(KApply1(Lbl'Hash'rs,(var_42_2409 :: [])) :: [])) when (isTrue (evalisProps((var_42_2409 :: [])) config (-1))) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(#rs(P),#rs(P))=>#token("true","Bool")`` requires isProps(P) ensures #token("true","Bool") [UNIQUE_ID(04c2f3a7727b2a811f3bf2a083682f459898631a8468eb4059a689ff85f33f26) contentStartColumn(6) contentStartLine(144) org.kframework.attributes.Location(Location(144,6,144,39)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply1(Lbl'Hash'rs,(varP_2410 :: [])) :: []),(KApply1(Lbl'Hash'rs,(varP_2411 :: [])) :: [])) when (isTrue (evalisProps((varP_2411 :: [])) config (-1))) && (((compare_kitem varP_2411 varP_2410) = 0) && true) -> ((Bool true) :: [])
(*{| rule ``#compareA(#br(_19,_20,_21),#rs(_22))=>#token("false","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isExp(_21),isProps(_22)),isInt(_19)),isInt(_20)) ensures #token("true","Bool") [UNIQUE_ID(f8dc40372ae2491282796ab5e0e871e0bef473cd0506b002a88dd9f32d6398a5) contentStartColumn(6) contentStartLine(149) org.kframework.attributes.Location(Location(149,6,149,44)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply3(Lbl'Hash'br,((Int _ as var_19_2412) :: []),((Int _ as var_20_2413) :: []),(var_21_2414 :: [])) :: []),(KApply1(Lbl'Hash'rs,(var_22_2415 :: [])) :: [])) when (((((((isTrue (evalisExp((var_21_2414 :: [])) config (-1)))) && ((isTrue (evalisProps((var_22_2415 :: [])) config (-1)))))) && (true))) && (true)) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(#rs(_26),`#uninit_OSL-SYNTAX`(.KList))=>#token("false","Bool")`` requires isProps(_26) ensures #token("true","Bool") [UNIQUE_ID(73a668ad2b45a2239e802058d04904b89249de3761c1b0261a32f9cc00b30967) contentStartColumn(6) contentStartLine(143) org.kframework.attributes.Location(Location(143,6,143,41)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply1(Lbl'Hash'rs,(var_26_2416 :: [])) :: []),(KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: [])) when (isTrue (evalisProps((var_26_2416 :: [])) config (-1))) && (true) -> ((Bool false) :: [])
(*{| rule ``#compareA(`#uninit_OSL-SYNTAX`(.KList),`#uninit_OSL-SYNTAX`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(c59c2bbec07b9915003648b9a7f26b2c5052e7f533a983fb4021879547c50856) contentStartColumn(6) contentStartLine(141) org.kframework.attributes.Location(Location(141,6,141,41)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []),(KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: [])) -> ((Bool true) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec eval'Hash'compareE (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'compareE and sort = 
SortBool in match c with 
(*{| rule ``#compareE(_0,M2)=>#token("true","Bool")`` requires `_andBool_`(`_==K_`(`.Map`(.KList),_0),isMap(M2)) ensures #token("true","Bool") [UNIQUE_ID(9dd9f3c865cc3828cc2d7b0cbf7737160ea5c17bb248947dc39fb6db8c203704) contentStartColumn(6) contentStartLine(139) org.kframework.attributes.Location(Location(139,6,139,33)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((var_0_2417),((Map (SortMap,_,_) as varM2_2418) :: [])) when (((isTrue (eval_'EqlsEqls'K_(((Lazy.force const'Stop'Map)),(var_0_2417)) config (-1)))) && (true)) && (true) -> ((Bool true) :: [])
| ((var_0_2419),((Map (SortMap,_,_) as var_1_2420) :: [])) when guard < 1(*{| rule ``#compareE(_0,M2)=>`_andBool_`(#compareA(Val,`Map:lookup`(M2,Key)),#compareE(M,M2))`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(Key,_0),#match(Val,`Map:lookup`(_0,Key))),#match(M,`_[_<-undef]`(_0,Key))),`_andBool_`(`_andBool_`(`_andBool_`(isKItem(Val),isInt(Key)),isMap(M2)),isMap(M))) ensures #token("true","Bool") [UNIQUE_ID(0b7f85aca7a11fd5043c7a9e7eb723baffd66061ed7184ef7250e8e807fefadc) contentStartColumn(6) contentStartLine(137) org.kframework.attributes.Location(Location(137,6,137,95)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_0_2419) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_3_2421) :: []) as e11 -> (let e = ((evalMap'Coln'lookup((var_0_2419),e11) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (var_2_2422 :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2419),e11) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_4_2423) :: []) when ((((((true) && (true))) && (true))) && (((((((true) && (true))) && (true))) && (true)))) && (true) -> ([Bool ((((isTrue (eval'Hash'compareA((var_2_2422 :: []),((evalMap'Coln'lookup((var_1_2420 :: []),e11) config (-1)))) config (-1)))) && ((isTrue (eval'Hash'compareE((var_4_2423 :: []),(var_1_2420 :: [])) config (-1))))))])| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'compareE c config 1) else choice| _ -> (eval'Hash'compareE c config 1))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec eval'Hash'bindParams (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'bindParams and sort = 
SortStmts in match c with 
(*{| rule ``#bindParams(`.List{"_,__OSL-SYNTAX"}`(.KList),`.List{"_,__OSL-SYNTAX"}`(.KList),SS)=>SS`` requires isStmts(SS) ensures #token("true","Bool") [UNIQUE_ID(ae986fef2148c7fe353a9becb0304bdf6c2d66a16b58ecfedc2cf63d85fc6d56) contentStartColumn(6) contentStartLine(36) org.kframework.attributes.Location(Location(36,6,36,53)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/call.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra') :: []),(KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra') :: []),(varSS_2424 :: [])) when (isTrue (evalisStmts((varSS_2424 :: [])) config (-1))) && (true) -> (varSS_2424 :: [])
(*{| rule ``#bindParams(`_,__OSL-SYNTAX`(#parameter(P,T),Ps),`_,__OSL-SYNTAX`(E,Es),SS)=>`___OSL-SYNTAX`(#decl(P),`___OSL-SYNTAX`(#transfer(E,P),#bindParams(Ps,Es,SS)))`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isExps(Es),isId(P)),isStmts(SS)),isType(T)),isParameters(Ps)),isExp(E)) ensures #token("true","Bool") [UNIQUE_ID(ed992b737d20ca251454e61fbfa179a7b9e14abd54784dd4a525dd015731de7f) contentStartColumn(6) contentStartLine(33) org.kframework.attributes.Location(Location(33,6,34,57)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/call.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(KApply2(Lbl'Hash'parameter,(varP_2425 :: []),(varT_2426 :: [])) :: []),(varPs_2427 :: [])) :: []),(KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(varE_2428 :: []),(varEs_2429 :: [])) :: []),(varSS_2430 :: [])) when (((((((((((isTrue (evalisExps((varEs_2429 :: [])) config (-1)))) && ((isTrue (evalisId((varP_2425 :: [])) config (-1)))))) && ((isTrue (evalisStmts((varSS_2430 :: [])) config (-1)))))) && ((isTrue (evalisType((varT_2426 :: [])) config (-1)))))) && ((isTrue (evalisParameters((varPs_2427 :: [])) config (-1)))))) && ((isTrue (evalisExp((varE_2428 :: [])) config (-1))))) && (true) -> (KApply2(Lbl___OSL'Hyph'SYNTAX,(KApply1(Lbl'Hash'decl,(varP_2425 :: [])) :: []),(KApply2(Lbl___OSL'Hyph'SYNTAX,(KApply2(Lbl'Hash'transfer,(varE_2428 :: []),(varP_2425 :: [])) :: []),((eval'Hash'bindParams((varPs_2427 :: []),(varEs_2429 :: []),(varSS_2430 :: [])) config (-1)))) :: [])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalupdateMap (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblupdateMap and sort = 
SortMap in match c with 
| _ -> try MAP.hook_updateAll c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisNstateCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisNstateCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isNstateCellOpt(#KToken(#token("NstateCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortNstateCell, var__2431) :: [])) -> ((Bool true) :: [])
(*{| rule ``isNstateCellOpt(`<nstate>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'nstate'_GT_',((Int _ as varK0_2432) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isNstateCellOpt(#KToken(#token("NstateCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortNstateCellOpt, var__2433) :: [])) -> ((Bool true) :: [])
(*{| rule ``isNstateCellOpt(noNstateCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoNstateCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isNstateCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2434)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_lock c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalfillList (c: k * k * k * k) (config: k) (guard: int) : k = let lbl = 
LblfillList and sort = 
SortList in match c with 
| _ -> try LIST.hook_fill c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize4 c)))])
let evalFunDefCellMapItem (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblFunDefCellMapItem and sort = 
SortFunDefCellMap in match c with 
| _ -> try MAP.hook_element c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Stop'StateCellMap (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Stop'StateCellMap and sort = 
SortStateCellMap in match c with 
| _ -> try MAP.hook_unit c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Stop'StateCellMap : k Lazy.t = lazy (eval'Stop'StateCellMap () interned_bottom (-1))
let evalisTimerCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisTimerCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isTimerCellOpt(`<timer>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'timer'_GT_',((Int _ as varK0_2435) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isTimerCellOpt(#KToken(#token("TimerCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTimerCellOpt, var__2436) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTimerCellOpt(noTimerCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoTimerCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTimerCellOpt(#KToken(#token("TimerCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTimerCell, var__2437) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTimerCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2438)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_List_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_List_ and sort = 
SortList in match c with 
| _ -> try LIST.hook_concat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec eval'Hash'inProps (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'inProps and sort = 
SortBool in match c with 
(*{| rule ``#inProps(P,#props(P,Ps))=>#token("true","Bool")`` requires isProp(P) ensures #token("true","Bool") [UNIQUE_ID(7d904d4a945a2770f200ff58eabde835d1381a8ab41222692300773a08f7e3ea) contentStartColumn(6) contentStartLine(85) org.kframework.attributes.Location(Location(85,6,85,40)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((varP_2439 :: []),(KApply2(Lbl'Hash'props,(varP_2440 :: []),(varPs_2441)) :: [])) when (isTrue (evalisProp((varP_2440 :: [])) config (-1))) && (((compare_kitem varP_2440 varP_2439) = 0) && true) -> ((Bool true) :: [])
(*{| rule ``#inProps(P,`.List{"#props"}`(.KList))=>#token("false","Bool")`` requires isProp(P) ensures #token("true","Bool") [UNIQUE_ID(2e5ba0768fe718792a322b49ee771c014ad4d2431ba988f102ab33ea4444302b) contentStartColumn(6) contentStartLine(88) org.kframework.attributes.Location(Location(88,6,88,34)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((varP_2442 :: []),(KApply0(Lbl'Stop'List'LBraQuotHash'props'QuotRBra') :: [])) when (isTrue (evalisProp((varP_2442 :: [])) config (-1))) && (true) -> ((Bool false) :: [])
(*{| rule ``#inProps(P1,#props(P,Ps))=>#inProps(P1,Ps)`` requires `_andBool_`(`_andBool_`(isProp(P1),isProps(Ps)),`_=/=K_`(P1,P)) ensures #token("true","Bool") [UNIQUE_ID(a5ac6df9d77b3382be77e4d7b14e5335ae7033d7d12a81a47d35ed487d0bae41) contentStartColumn(6) contentStartLine(86) org.kframework.attributes.Location(Location(86,6,87,24)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| ((varP1_2443 :: []),(KApply2(Lbl'Hash'props,(varP_2444),(varPs_2445 :: [])) :: [])) when (((((isTrue (evalisProp((varP1_2443 :: [])) config (-1)))) && ((isTrue (evalisProps((varPs_2445 :: [])) config (-1)))))) && ((isTrue (eval_'EqlsSlshEqls'K_((varP1_2443 :: []),(varP_2444)) config (-1))))) && (true) -> ((eval'Hash'inProps((varP1_2443 :: []),(varPs_2445 :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortInt in match c with 
| _ -> try IO.hook_open c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'PipeHyph_GT_'_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'PipeHyph_GT_'_ and sort = 
SortMap in match c with 
| _ -> try MAP.hook_element c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_xorBool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_xorBool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_xor c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_xorBool__BOOL`(B1,B2)=>`notBool_`(`_==Bool__BOOL`(B1,B2))`` requires `_andBool_`(isBool(B2),isBool(B1)) ensures #token("true","Bool") [UNIQUE_ID(1c34683a46438798498c925345e696fc017c0665504d0877c44e540d820b1ec7) contentStartColumn(8) contentStartLine(315) org.kframework.attributes.Location(Location(315,8,315,57)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB1_2446) :: []),((Bool _ as varB2_2447) :: [])) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'EqlsEqls'Bool__BOOL((varB1_2446 :: []),(varB2_2447 :: [])) config (-1))))))])
(*{| rule `` `_xorBool__BOOL`(B,#token("false","Bool"))=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(2eae84ae7b21312fecde18860149de73365b819e65aada5f952fc5b9101ae014) contentStartColumn(8) contentStartLine(313) org.kframework.attributes.Location(Location(313,8,313,38)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB_2448) :: []),((Bool false) :: [])) when true && (true) -> (varB_2448 :: [])
(*{| rule `` `_xorBool__BOOL`(#token("false","Bool"),B)=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(0b888132027262073a3306d90f4a55b0a4ea27984cd77fccd8bf29cd48efb665) contentStartColumn(8) contentStartLine(312) org.kframework.attributes.Location(Location(312,8,312,38)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: []),((Bool _ as varB_2449) :: [])) when true && (true) -> (varB_2449 :: [])
(*{| rule `` `_xorBool__BOOL`(B,B)=>#token("false","Bool")`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(70684b52edf7d3194cd01f6e5714b364631e8d96267df666bb689e5380e13cc4) contentStartColumn(8) contentStartLine(314) org.kframework.attributes.Location(Location(314,8,314,38)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB_2450) :: []),((Bool _ as varB_2451) :: [])) when true && (((compare_kitem varB_2450 varB_2451) = 0) && true) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'Hyph'Map__MAP (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Hyph'Map__MAP and sort = 
SortMap in match c with 
| _ -> try MAP.hook_difference c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalsizeMap (c: k) (config: k) (guard: int) : k = let lbl = 
LblsizeMap and sort = 
SortInt in match c with 
| _ -> try MAP.hook_size c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'compareS (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'compareS and sort = 
SortBool in match c with 
(*{| rule ``#compareS(M1,M2)=>`_andBool_`(`_==Int_`(sizeMap(M1),sizeMap(M2)),#compareE(M1,M2))`` requires `_andBool_`(isMap(M1),isMap(M2)) ensures #token("true","Bool") [UNIQUE_ID(a9adf499ecfccfae9ca9c684a30f35871415c610c650a1dbf6265d0127e5a644) contentStartColumn(6) contentStartLine(135) org.kframework.attributes.Location(Location(135,6,135,85)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Map (SortMap,_,_) as varM1_2452) :: []),((Map (SortMap,_,_) as varM2_2453) :: [])) when ((true) && (true)) && (true) -> ([Bool ((((isTrue (eval_'EqlsEqls'Int_(((evalsizeMap((varM1_2452 :: [])) config (-1))),((evalsizeMap((varM2_2453 :: [])) config (-1)))) config (-1)))) && ((isTrue (eval'Hash'compareE((varM1_2452 :: []),(varM2_2453 :: [])) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisIndexesCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIndexesCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isIndexesCellOpt(#KToken(#token("IndexesCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexesCellOpt, var__2454) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexesCellOpt(`<indexes>`(K0))=>#token("true","Bool")`` requires isIndexes(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'indexes'_GT_',(varK0_2455 :: [])) :: [])) when (isTrue (evalisIndexes((varK0_2455 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isIndexesCellOpt(#KToken(#token("IndexesCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexesCell, var__2456) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexesCellOpt(noIndexesCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoIndexesCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexesCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2457)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitStoreCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitStoreCell and sort = 
SortStoreCell in match c with 
(*{| rule ``initStoreCell(.KList)=>`<store>`(`.Map`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(ad2d78eb8f5b4d8eacd89e8cad5b23cc3232e1c0f2b77f28f32d8a601431c266) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'store'_GT_',((Lazy.force const'Stop'Map))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitStoreCell : k Lazy.t = lazy (evalinitStoreCell () interned_bottom (-1))
let eval'Hash'sort (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'sort and sort = 
SortString in match c with 
| _ -> try KREFLECTION.hook_sort c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisIndexCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIndexCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isIndexCellOpt(#KToken(#token("IndexCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexCell, var__2458) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexCellOpt(noIndexCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoIndexCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexCellOpt(#KToken(#token("IndexCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexCellOpt, var__2459) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexCellOpt(`<index>`(K0))=>#token("true","Bool")`` requires isInt(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'index'_GT_',((Int _ as varK0_2460) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isIndexCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2461)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'open'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'open'LPar'_'RPar'_K'Hyph'IO and sort = 
SortInt in match c with 
(*{| rule `` `#open(_)_K-IO`(S)=>`#open(_,_)_K-IO`(S,#token("\"r+\"","String"))`` requires isString(S) ensures #token("true","Bool") [UNIQUE_ID(ed7ee74167faa7f8e3a152e9e495d112a67407097a0bb306ac17e2ce4bebf100) contentStartColumn(8) contentStartLine(902) org.kframework.attributes.Location(Location(902,8,902,48)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varS_2462) :: [])) when true && (true) -> ((eval'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO((varS_2462 :: []),((String "r+") :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING and sort = 
SortString in match c with 
| _ -> try STRING.hook_replaceFirst c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `replaceFirst(_,_,_)_STRING`(Source,ToReplace,_16)=>Source`` requires `_andBool_`(`_andBool_`(`_andBool_`(isString(Source),isString(ToReplace)),isString(_16)),`_<Int__INT`(findString(Source,ToReplace,#token("0","Int")),#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(4320c56cb2a4f4c70e2500eb8f33dab497a5bb09aa847e4e0c0ae496ab671040) contentStartColumn(8) contentStartLine(566) org.kframework.attributes.Location(Location(566,8,567,57)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varSource_2463) :: []),((String _ as varToReplace_2464) :: []),((String _ as var_16_2465) :: [])) when ((((((true) && (true))) && (true))) && ((isTrue (eval_'_LT_'Int__INT(((evalfindString((varSource_2463 :: []),(varToReplace_2464 :: []),((Lazy.force int0) :: [])) config (-1))),((Lazy.force int0) :: [])) config (-1))))) && (true) -> (varSource_2463 :: [])
(*{| rule `` `replaceFirst(_,_,_)_STRING`(Source,ToReplace,Replacement)=>`_+String__STRING`(`_+String__STRING`(substrString(Source,#token("0","Int"),findString(Source,ToReplace,#token("0","Int"))),Replacement),substrString(Source,`_+Int_`(findString(Source,ToReplace,#token("0","Int")),lengthString(ToReplace)),lengthString(Source)))`` requires `_andBool_`(`_andBool_`(`_andBool_`(isString(Replacement),isString(Source)),isString(ToReplace)),`_>=Int__INT`(findString(Source,ToReplace,#token("0","Int")),#token("0","Int"))) ensures #token("true","Bool") [UNIQUE_ID(4941a93fc06d65bd530ddbfcaabb498d40e247effd559cda2d85e65d73692b70) contentStartColumn(8) contentStartLine(563) org.kframework.attributes.Location(Location(563,8,565,66)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varSource_2466) :: []),((String _ as varToReplace_2467) :: []),((String _ as varReplacement_2468) :: [])) when ((((((true) && (true))) && (true))) && ((isTrue (eval_'_GT_Eqls'Int__INT(((evalfindString((varSource_2466 :: []),(varToReplace_2467 :: []),((Lazy.force int0) :: [])) config (-1))),((Lazy.force int0) :: [])) config (-1))))) && (true) -> ((eval_'Plus'String__STRING(((eval_'Plus'String__STRING(((evalsubstrString((varSource_2466 :: []),((Lazy.force int0) :: []),((evalfindString((varSource_2466 :: []),(varToReplace_2467 :: []),((Lazy.force int0) :: [])) config (-1)))) config (-1))),(varReplacement_2468 :: [])) config (-1))),((evalsubstrString((varSource_2466 :: []),((eval_'Plus'Int_(((evalfindString((varSource_2466 :: []),(varToReplace_2467 :: []),((Lazy.force int0) :: [])) config (-1))),((evallengthString((varToReplace_2467 :: [])) config (-1)))) config (-1))),((evallengthString((varSource_2466 :: [])) config (-1)))) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let eval'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_putc c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisTmpCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisTmpCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isTmpCellOpt(`<tmp>`(K0))=>#token("true","Bool")`` requires isList(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'tmp'_GT_',((List (SortList,_,_) as varK0_2469) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isTmpCellOpt(#KToken(#token("TmpCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTmpCell, var__2470) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTmpCellOpt(#KToken(#token("TmpCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTmpCellOpt, var__2471) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTmpCellOpt(noTmpCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoTmpCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTmpCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2472)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'logToFile (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'logToFile and sort = 
SortK in match c with 
| _ -> try IO.hook_log c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortString in match c with 
| _ -> try IO.hook_read c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisKCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisKCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isKCellOpt(#KToken(#token("KCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortKCellOpt, var__2473) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKCellOpt(#KToken(#token("KCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortKCell, var__2474) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKCellOpt(`<k>`(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'k'_GT_',(varK0_2475)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isKCellOpt(noKCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoKCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isKCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2476)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisEnvCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisEnvCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isEnvCellOpt(noEnvCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoEnvCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isEnvCellOpt(#KToken(#token("EnvCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortEnvCell, var__2477) :: [])) -> ((Bool true) :: [])
(*{| rule ``isEnvCellOpt(`<env>`(K0))=>#token("true","Bool")`` requires isMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as varK0_2478) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isEnvCellOpt(#KToken(#token("EnvCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortEnvCellOpt, var__2479) :: [])) -> ((Bool true) :: [])
(*{| rule ``isEnvCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2480)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStackCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStackCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isStackCellOpt(noStackCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoStackCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStackCellOpt(#KToken(#token("StackCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStackCell, var__2481) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStackCellOpt(`<stack>`(K0))=>#token("true","Bool")`` requires isList(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as varK0_2482) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStackCellOpt(#KToken(#token("StackCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStackCellOpt, var__2483) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStackCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2484)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisWriteCellOpt (c: k) (config: k) (guard: int) : k = let lbl = 
LblisWriteCellOpt and sort = 
SortBool in match c with 
(*{| rule ``isWriteCellOpt(`<write>`(K0))=>#token("true","Bool")`` requires isSet(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as varK0_2485) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isWriteCellOpt(#KToken(#token("WriteCellOpt","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortWriteCellOpt, var__2486) :: [])) -> ((Bool true) :: [])
(*{| rule ``isWriteCellOpt(#KToken(#token("WriteCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortWriteCell, var__2487) :: [])) -> ((Bool true) :: [])
(*{| rule ``isWriteCellOpt(noWriteCell(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(LblnoWriteCell) :: [])) -> ((Bool true) :: [])
(*{| rule ``isWriteCellOpt(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2488)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStateCellFragment (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStateCellFragment and sort = 
SortBool in match c with 
(*{| rule ``isStateCellFragment(#KToken(#token("StateCellFragment","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStateCellFragment, var__2489) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStateCellFragment(`<state>-fragment`(K0,K1,K2,K3,K4,K5,K6,K7))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isIndexCellOpt(K0),isKCellOpt(K1)),isEnvCellOpt(K2)),isStoreCellOpt(K3)),isStackCellOpt(K4)),isWriteCellOpt(K5)),isTimerCellOpt(K6)),isIndexesCellOpt(K7)) ensures #token("true","Bool") []|}*)
| ((KApply8(Lbl'_LT_'state'_GT_Hyph'fragment,(varK0_2490 :: []),(varK1_2491 :: []),(varK2_2492 :: []),(varK3_2493 :: []),(varK4_2494 :: []),(varK5_2495 :: []),(varK6_2496 :: []),(varK7_2497 :: [])) :: [])) when (((((((((((((((isTrue (evalisIndexCellOpt((varK0_2490 :: [])) config (-1)))) && ((isTrue (evalisKCellOpt((varK1_2491 :: [])) config (-1)))))) && ((isTrue (evalisEnvCellOpt((varK2_2492 :: [])) config (-1)))))) && ((isTrue (evalisStoreCellOpt((varK3_2493 :: [])) config (-1)))))) && ((isTrue (evalisStackCellOpt((varK4_2494 :: [])) config (-1)))))) && ((isTrue (evalisWriteCellOpt((varK5_2495 :: [])) config (-1)))))) && ((isTrue (evalisTimerCellOpt((varK6_2496 :: [])) config (-1)))))) && ((isTrue (evalisIndexesCellOpt((varK7_2497 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStateCellFragment(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2498)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalId2String (c: k) (config: k) (guard: int) : k = let lbl = 
LblId2String and sort = 
SortString in match c with 
| _ -> try STRING.hook_token2string c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisBranchTmp (c: k) (config: k) (guard: int) : k = let lbl = 
LblisBranchTmp and sort = 
SortBool in match c with 
(*{| rule ``isBranchTmp(#KToken(#token("BranchTmp","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortBranchTmp, var__2499) :: [])) -> ((Bool true) :: [])
(*{| rule ``isBranchTmp(#secondBranch(K0))=>#token("true","Bool")`` requires isBlocks(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'secondBranch,(varK0_2500 :: [])) :: [])) when (isTrue (evalisBlocks((varK0_2500 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isBranchTmp(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2501)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_FunDefCellMap_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_FunDefCellMap_ and sort = 
SortFunDefCellMap in match c with 
| _ -> try MAP.hook_concat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalfreshInt (c: k) (config: k) (guard: int) : k = let lbl = 
LblfreshInt and sort = 
SortInt in match c with 
(*{| rule ``freshInt(I)=>I`` requires isInt(I) ensures #token("true","Bool") [UNIQUE_ID(d7221da1fa6b633137e79279efd265a289ea7e8496612615f43546422d446bc4) contentStartColumn(8) contentStartLine(412) org.kframework.attributes.Location(Location(412,8,412,28)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI_2502) :: [])) when true && (true) -> (varI_2502 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let freshFunction (sort: string) (config: k) (counter: Z.t) : k = match sort with 
| "Id" -> (evalfreshId ([Int counter]) config (-1))
| "Int" -> (evalfreshInt ([Int counter]) config (-1))
| _ -> invalid_arg ("Cannot find fresh function for sort " ^ sort)let eval'Hash'fresh (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'fresh and sort = 
SortKItem in match c with 
| _ -> try KREFLECTION.hook_fresh c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalMap'Coln'choice (c: k) (config: k) (guard: int) : k = let lbl = 
LblMap'Coln'choice and sort = 
SortKItem in match c with 
| _ -> try MAP.hook_choice c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_impliesBool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_impliesBool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_implies c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_impliesBool__BOOL`(B,#token("false","Bool"))=>`notBool_`(B)`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(0129e06009c7500aefab482e1bc8e348010c6bd01acd15725172192c05a70831) contentStartColumn(8) contentStartLine(330) org.kframework.attributes.Location(Location(330,8,330,45)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB_2503) :: []),((Bool false) :: [])) when true && (true) -> ([Bool ((not ((isTrue [varB_2503]))))])
(*{| rule `` `_impliesBool__BOOL`(#token("false","Bool"),_5)=>#token("true","Bool")`` requires isBool(_5) ensures #token("true","Bool") [UNIQUE_ID(3497d026ef9468fdac7e6b5de6841fad0118b17ca073ba25af4c5d8573a167b2) contentStartColumn(8) contentStartLine(328) org.kframework.attributes.Location(Location(328,8,328,40)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool false) :: []),((Bool _ as var_5_2504) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule `` `_impliesBool__BOOL`(#token("true","Bool"),B)=>B`` requires isBool(B) ensures #token("true","Bool") [UNIQUE_ID(278435b95efb1a1ce04f6d701b12e6438c3cf3aee8019b41ea93b05755d379d7) contentStartColumn(8) contentStartLine(327) org.kframework.attributes.Location(Location(327,8,327,36)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool true) :: []),((Bool _ as varB_2505) :: [])) when true && (true) -> (varB_2505 :: [])
(*{| rule `` `_impliesBool__BOOL`(_0,#token("true","Bool"))=>#token("true","Bool")`` requires isBool(_0) ensures #token("true","Bool") [UNIQUE_ID(7fdeea13463fa86d05e4d5bfb9e9c627b90ab57930f712fef1bfc08543a87d74) contentStartColumn(8) contentStartLine(329) org.kframework.attributes.Location(Location(329,8,329,39)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as var_0_2506) :: []),((Bool true) :: [])) when true && (true) -> ((Bool true) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'getc'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'getc'LPar'_'RPar'_K'Hyph'IO and sort = 
SortInt in match c with 
| _ -> try IO.hook_getc c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_Set_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_Set_ and sort = 
SortSet in match c with 
| _ -> try SET.hook_concat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalinitTmpCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitTmpCell and sort = 
SortTmpCell in match c with 
(*{| rule ``initTmpCell(.KList)=>`<tmp>`(`.List`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(b87fe2bc8b821fb29f4fa5896da7b2b6af1a56197c53730e423c4d62b2347a97) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'tmp'_GT_',((Lazy.force const'Stop'List))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitTmpCell : k Lazy.t = lazy (evalinitTmpCell () interned_bottom (-1))
let eval_'Star'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Star'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_mul c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalmaxInt'LPar'_'Comm'_'RPar'_INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblmaxInt'LPar'_'Comm'_'RPar'_INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_max c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'unwrapInt (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'unwrapInt and sort = 
SortInt in match c with 
(*{| rule ``#unwrapInt(I)=>I`` requires isInt(I) ensures #token("true","Bool") [UNIQUE_ID(faac54f9e408a78e41fdcf910c924005fa839c43b44ba28eec55f7d42f4ee06b) contentStartColumn(6) contentStartLine(359) org.kframework.attributes.Location(Location(359,6,359,28)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI_2507) :: [])) when true && (true) -> (varI_2507 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Tild'Int__INT (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Tild'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_not c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalListItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblListItem and sort = 
SortList in match c with 
| _ -> try LIST.hook_element c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec eval'Hash'list2Set (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'list2Set and sort = 
SortSet in match c with 
(*{| rule ``#list2Set(`_List_`(`ListItem`(E),L))=>`_Set_`(`SetItem`(E),#list2Set(L))`` requires `_andBool_`(isKItem(E),isList(L)) ensures #token("true","Bool") [UNIQUE_ID(393ad3cdede7d78787bc70867c479331dfd163148c86e813eb128007718df8d7) contentStartColumn(6) contentStartLine(94) org.kframework.attributes.Location(Location(94,6,94,62)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((List (SortList, Lbl_List_, (varE_2508 :: []) :: varL_2509)) :: [])) when ((true) && (true)) && (true) -> ((eval_Set_(((evalSetItem((varE_2508 :: [])) config (-1))),((eval'Hash'list2Set(((List (SortList, Lbl_List_, varL_2509)) :: [])) config (-1)))) config (-1)))
(*{| rule ``#list2Set(`.List`(.KList))=>`.Set`(.KList)`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(1e92f79185840dcaeae8877f0712e7bc1bc9b794efe58dbe049fafc4fba90475) contentStartColumn(6) contentStartLine(93) org.kframework.attributes.Location(Location(93,6,93,30)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((List (SortList, Lbl_List_, [])) :: [])) -> ((Lazy.force const'Stop'Set))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'_LT_Eqls'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT_Eqls'String__STRING and sort = 
SortBool in match c with 
| _ -> try STRING.hook_le c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_<=String__STRING`(S1,S2)=>`notBool_`(`_<String__STRING`(S2,S1))`` requires `_andBool_`(isString(S2),isString(S1)) ensures #token("true","Bool") [UNIQUE_ID(2c1110a97c0d904a19992e4eac695692515cb703f5d689aaa6945290a696d662) contentStartColumn(8) contentStartLine(549) org.kframework.attributes.Location(Location(549,8,549,63)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varS1_2510) :: []),((String _ as varS2_2511) :: [])) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'_LT_'String__STRING((varS2_2511 :: []),(varS1_2510 :: [])) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalordChar (c: k) (config: k) (guard: int) : k = let lbl = 
LblordChar and sort = 
SortInt in match c with 
| _ -> try STRING.hook_ord c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalrfindString (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblrfindString and sort = 
SortInt in match c with 
| _ -> try STRING.hook_rfind c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let rec evalrfindChar (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblrfindChar and sort = 
SortInt in match c with 
| _ -> try STRING.hook_rfindChar c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule ``rfindChar(_12,#token("\"\"","String"),_13)=>#token("-1","Int")`` requires `_andBool_`(isInt(_13),isString(_12)) ensures #token("true","Bool") [UNIQUE_ID(04cb290eea9d7c93a48f761cab20217a689222a306098df1120917b013944330) contentStartColumn(8) contentStartLine(556) org.kframework.attributes.Location(Location(556,8,556,33)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as var_12_2512) :: []),((String "") :: []),((Int _ as var_13_2513) :: [])) when ((true) && (true)) && (true) -> ((Lazy.force int'Hyph'1) :: [])
(*{| rule ``rfindChar(S1,S2,I)=>`maxInt(_,_)_INT`(rfindString(S1,substrString(S2,#token("0","Int"),#token("1","Int")),I),rfindChar(S1,substrString(S2,#token("1","Int"),lengthString(S2)),I))`` requires `_andBool_`(`_andBool_`(`_andBool_`(isInt(I),isString(S2)),isString(S1)),`_=/=String__STRING`(S2,#token("\"\"","String"))) ensures #token("true","Bool") [UNIQUE_ID(dea96adb63c0d77626a51f08fb7a6daf76e3a7b8d0205f5285542c1160f2ddeb) contentStartColumn(8) contentStartLine(555) org.kframework.attributes.Location(Location(555,8,555,182)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (((String _ as varS1_2514) :: []),((String _ as varS2_2515) :: []),((Int _ as varI_2516) :: [])) when ((((((true) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'String__STRING((varS2_2515 :: []),((String "") :: [])) config (-1))))) && (true) -> ((evalmaxInt'LPar'_'Comm'_'RPar'_INT(((evalrfindString((varS1_2514 :: []),((evalsubstrString((varS2_2515 :: []),((Lazy.force int0) :: []),((Lazy.force int1) :: [])) config (-1))),(varI_2516 :: [])) config (-1))),((evalrfindChar((varS1_2514 :: []),((evalsubstrString((varS2_2515 :: []),((Lazy.force int1) :: []),((evallengthString((varS2_2515 :: [])) config (-1)))) config (-1))),(varI_2516 :: [])) config (-1)))) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalisIndexItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisIndexItem and sort = 
SortBool in match c with 
(*{| rule ``isIndexItem(`#increaseTimer_OSL`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'increaseTimer_OSL) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexItem(`#increaseIndex_OSL`(.KList))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KApply0(Lbl'Hash'increaseIndex_OSL) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexItem(#KToken(#token("IndexItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortIndexItem, var__2517) :: [])) -> ((Bool true) :: [])
(*{| rule ``isIndexItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2518)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_Map_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_Map_ and sort = 
SortMap in match c with 
| _ -> try MAP.hook_concat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalproject'Coln'Stmts (c: k) (config: k) (guard: int) : k = let lbl = 
Lblproject'Coln'Stmts and sort = 
SortStmts in match c with 
(*{| rule `` `project:Stmts`(K)=>K`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(645f9d37511efdeada64e636036fcb47e0a28ed34a19d3d37b6fc608f8ebb1b4) projection()]|}*)
| ((varK_2519 :: [])) -> (varK_2519 :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evaldirectionalityChar (c: k) (config: k) (guard: int) : k = let lbl = 
LbldirectionalityChar and sort = 
SortString in match c with 
| _ -> try STRING.hook_directionality c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalFloat2String (c: k) (config: k) (guard: int) : k = let lbl = 
LblFloat2String and sort = 
SortString in match c with 
| _ -> try STRING.hook_float2string c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'opendir'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'opendir'LPar'_'RPar'_K'Hyph'IO and sort = 
SortKItem in match c with 
| _ -> try IO.hook_opendir c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitKCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblinitKCell and sort = 
SortKCell in match c with 
(*{| rule ``initKCell(Init)=>`<k>`(`project:Stmts`(`Map:lookup`(Init,#token("$PGM","KConfigVar"))))`` requires isStmts(`project:Stmts`(`Map:lookup`(Init,#token("$PGM","KConfigVar")))) ensures #token("true","Bool") [UNIQUE_ID(7044714313dcd361938c82b5df16fd8d6dc625dfbf3580544488482feea62cb1) initializer()]|}*)
| (((Map (SortMap,_,_) as varInit_2520) :: [])) when (isTrue (evalisStmts(((evalproject'Coln'Stmts(((evalMap'Coln'lookup((varInit_2520 :: []),(KToken (SortKConfigVar, "$PGM") :: [])) config (-1)))) config (-1)))) config (-1))) && (true) -> (KApply1(Lbl'_LT_'k'_GT_',((evalproject'Coln'Stmts(((evalMap'Coln'lookup((varInit_2520 :: []),(KToken (SortKConfigVar, "$PGM") :: [])) config (-1)))) config (-1)))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisDItem (c: k) (config: k) (guard: int) : k = let lbl = 
LblisDItem and sort = 
SortBool in match c with 
(*{| rule ``isDItem(#KToken(#token("DItem","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortDItem, var__2521) :: [])) -> ((Bool true) :: [])
(*{| rule ``isDItem(#Deallocate(K0))=>#token("true","Bool")`` requires isExp(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'Deallocate,(varK0_2522 :: [])) :: [])) when (isTrue (evalisExp((varK0_2522 :: [])) config (-1))) && (true) -> ((Bool true) :: [])
(*{| rule ``isDItem(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2523)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalsizeList (c: k) (config: k) (guard: int) : k = let lbl = 
LblsizeList and sort = 
SortInt in match c with 
| _ -> try LIST.hook_size c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStateCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStateCell and sort = 
SortBool in match c with 
(*{| rule ``isStateCell(`<state>`(K0,K1,K2,K3,K4,K5,K6,K7))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isIndexCell(K0),isKCell(K1)),isEnvCell(K2)),isStoreCell(K3)),isStackCell(K4)),isWriteCell(K5)),isTimerCell(K6)),isIndexesCell(K7)) ensures #token("true","Bool") []|}*)
| ((KApply8(Lbl'_LT_'state'_GT_',(varK0_2524 :: []),(varK1_2525 :: []),(varK2_2526 :: []),(varK3_2527 :: []),(varK4_2528 :: []),(varK5_2529 :: []),(varK6_2530 :: []),(varK7_2531 :: [])) :: [])) when (((((((((((((((isTrue (evalisIndexCell((varK0_2524 :: [])) config (-1)))) && ((isTrue (evalisKCell((varK1_2525 :: [])) config (-1)))))) && ((isTrue (evalisEnvCell((varK2_2526 :: [])) config (-1)))))) && ((isTrue (evalisStoreCell((varK3_2527 :: [])) config (-1)))))) && ((isTrue (evalisStackCell((varK4_2528 :: [])) config (-1)))))) && ((isTrue (evalisWriteCell((varK5_2529 :: [])) config (-1)))))) && ((isTrue (evalisTimerCell((varK6_2530 :: [])) config (-1)))))) && ((isTrue (evalisIndexesCell((varK7_2531 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isStateCell(#KToken(#token("StateCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStateCell, var__2532) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStateCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2533)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'EqlsSlshEqls'Bool__BOOL (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'EqlsSlshEqls'Bool__BOOL and sort = 
SortBool in match c with 
| _ -> try BOOL.hook_ne c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_=/=Bool__BOOL`(B1,B2)=>`notBool_`(`_==Bool__BOOL`(B1,B2))`` requires `_andBool_`(isBool(B2),isBool(B1)) ensures #token("true","Bool") [UNIQUE_ID(229c060a3ad56352e70dadb0089a132434aa77f4aaf4ef46e44d820031a08b83) contentStartColumn(8) contentStartLine(332) org.kframework.attributes.Location(Location(332,8,332,57)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Bool _ as varB1_2534) :: []),((Bool _ as varB2_2535) :: [])) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'EqlsEqls'Bool__BOOL((varB1_2534 :: []),(varB2_2535 :: [])) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalremoveAll (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblremoveAll and sort = 
SortMap in match c with 
| _ -> try MAP.hook_removeAll c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisTCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisTCell and sort = 
SortBool in match c with 
(*{| rule ``isTCell(#KToken(#token("TCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTCell, var__2536) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTCell(`<T>`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isStatesCell(K0),isNstateCell(K1)),isTmpCell(K2)),isFunDefsCell(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'T'_GT_',(varK0_2537 :: []),(varK1_2538 :: []),(varK2_2539 :: []),(varK3_2540 :: [])) :: [])) when (((((((isTrue (evalisStatesCell((varK0_2537 :: [])) config (-1)))) && ((isTrue (evalisNstateCell((varK1_2538 :: [])) config (-1)))))) && ((isTrue (evalisTmpCell((varK2_2539 :: [])) config (-1)))))) && ((isTrue (evalisFunDefsCell((varK3_2540 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isTCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2541)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisFunDefCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblisFunDefCell and sort = 
SortBool in match c with 
(*{| rule ``isFunDefCell(#KToken(#token("FunDefCell","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortFunDefCell, var__2542) :: [])) -> ((Bool true) :: [])
(*{| rule ``isFunDefCell(`<funDef>`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isFnameCell(K0),isFparamsCell(K1)),isFretCell(K2)),isFbodyCell(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'funDef'_GT_',(varK0_2543 :: []),(varK1_2544 :: []),(varK2_2545 :: []),(varK3_2546 :: [])) :: [])) when (((((((isTrue (evalisFnameCell((varK0_2543 :: [])) config (-1)))) && ((isTrue (evalisFparamsCell((varK1_2544 :: [])) config (-1)))))) && ((isTrue (evalisFretCell((varK2_2545 :: [])) config (-1)))))) && ((isTrue (evalisFbodyCell((varK3_2546 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isFunDefCell(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2547)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitStackCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitStackCell and sort = 
SortStackCell in match c with 
(*{| rule ``initStackCell(.KList)=>`<stack>`(`.List`(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(541ed12e6094c5492d5540f8b92537953ad450f6c7d953c5852a2739fe99b548) initializer()]|}*)
| () -> (KApply1(Lbl'_LT_'stack'_GT_',((Lazy.force const'Stop'List))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitStackCell : k Lazy.t = lazy (evalinitStackCell () interned_bottom (-1))
let evalinitIndexCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitIndexCell and sort = 
SortIndexCell in match c with 
(*{| rule ``initIndexCell(.KList)=>`<index>`(#token("0","Int"))`` requires isInt(#token("0","Int")) ensures #token("true","Bool") [UNIQUE_ID(6a6d941c66802a68315e5326b0f4c92bdef9b16e3ce5d48cd95e8d2f47530487) initializer()]|}*)
| () when (isTrue (evalisInt(((Lazy.force int0) :: [])) config (-1))) && (true) -> (KApply1(Lbl'_LT_'index'_GT_',((Lazy.force int0) :: [])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitIndexCell : k Lazy.t = lazy (evalinitIndexCell () interned_bottom (-1))
let evalinitTimerCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitTimerCell and sort = 
SortTimerCell in match c with 
(*{| rule ``initTimerCell(.KList)=>`<timer>`(#token("0","Int"))`` requires isInt(#token("0","Int")) ensures #token("true","Bool") [UNIQUE_ID(d7d038a6b57b879430ad7e60ee865e268861d91aae4076961a1db0cfc2c09e69) initializer()]|}*)
| () when (isTrue (evalisInt(((Lazy.force int0) :: [])) config (-1))) && (true) -> (KApply1(Lbl'_LT_'timer'_GT_',((Lazy.force int0) :: [])) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitTimerCell : k Lazy.t = lazy (evalinitTimerCell () interned_bottom (-1))
let evalinitStateCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblinitStateCell and sort = 
SortStateCell in match c with 
(*{| rule ``initStateCell(Init)=>`StateCellMapItem`(initIndexCell(.KList),`<state>`(initIndexCell(.KList),initKCell(Init),initEnvCell(.KList),initStoreCell(.KList),initStackCell(.KList),initWriteCell(.KList),initTimerCell(.KList),initIndexesCell(.KList)))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(d3979b11522d27c726949992ffa0877a26467b8dde8a1489a468a075e5061164) initializer()]|}*)
| (((Map (SortMap,_,_) as varInit_2548) :: [])) -> ((evalStateCellMapItem(((Lazy.force constinitIndexCell)),(KApply8(Lbl'_LT_'state'_GT_',((Lazy.force constinitIndexCell)),((evalinitKCell((varInit_2548 :: [])) config (-1))),((Lazy.force constinitEnvCell)),((Lazy.force constinitStoreCell)),((Lazy.force constinitStackCell)),((Lazy.force constinitWriteCell)),((Lazy.force constinitTimerCell)),((Lazy.force constinitIndexesCell))) :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitStatesCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblinitStatesCell and sort = 
SortStatesCell in match c with 
(*{| rule ``initStatesCell(Init)=>`<states>`(initStateCell(Init))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(960460b008f7b3e7af2be1408204327dc27be2c43ef5195e35225cdb633393fb) initializer()]|}*)
| (((Map (SortMap,_,_) as varInit_2549) :: [])) -> (KApply1(Lbl'_LT_'states'_GT_',((evalinitStateCell((varInit_2549 :: [])) config (-1)))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalinitTCell (c: k) (config: k) (guard: int) : k = let lbl = 
LblinitTCell and sort = 
SortTCell in match c with 
(*{| rule ``initTCell(Init)=>`<T>`(initStatesCell(Init),initNstateCell(.KList),initTmpCell(.KList),initFunDefsCell(.KList))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(bc4a2ec2511a678fed78f752ce4f4bfcf9fa756884ae5b89b28cc3804b4b0868) initializer()]|}*)
| (((Map (SortMap,_,_) as varInit_2550) :: [])) -> (KApply4(Lbl'_LT_'T'_GT_',((evalinitStatesCell((varInit_2550 :: [])) config (-1))),((Lazy.force constinitNstateCell)),((Lazy.force constinitTmpCell)),((Lazy.force constinitFunDefsCell))) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalFloatFormat (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblFloatFormat and sort = 
SortString in match c with 
| _ -> try STRING.hook_floatFormat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'Xor_Perc'Int___INT (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Xor_Perc'Int___INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_powmod c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let eval_'Pipe'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'Pipe'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_or c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_dividesInt__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_dividesInt__INT and sort = 
SortBool in match c with 
(*{| rule `` `_dividesInt__INT`(I1,I2)=>`_==Int_`(`_%Int__INT`(I2,I1),#token("0","Int"))`` requires `_andBool_`(isInt(I1),isInt(I2)) ensures #token("true","Bool") [UNIQUE_ID(81f77f3fbad78d0c735f87bfe9eff8339b46a8ead5ca820f8d69aafef7cb551d) contentStartColumn(8) contentStartLine(409) org.kframework.attributes.Location(Location(409,8,409,58)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as varI1_2551) :: []),((Int _ as varI2_2552) :: [])) when ((true) && (true)) && (true) -> ((eval_'EqlsEqls'Int_(((eval_'Perc'Int__INT((varI2_2552 :: []),(varI1_2551 :: [])) config (-1))),((Lazy.force int0) :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval'Hash'stat'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'stat'LPar'_'RPar'_K'Hyph'IO and sort = 
SortKItem in match c with 
| _ -> try IO.hook_stat c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalupdateList (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblupdateList and sort = 
SortList in match c with 
| _ -> try LIST.hook_updateAll c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalSet'Coln'choice (c: k) (config: k) (guard: int) : k = let lbl = 
LblSet'Coln'choice and sort = 
SortKItem in match c with 
| _ -> try SET.hook_choice c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalcategoryChar (c: k) (config: k) (guard: int) : k = let lbl = 
LblcategoryChar and sort = 
SortString in match c with 
| _ -> try STRING.hook_category c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_write c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_xorInt__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_xorInt__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_xor c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalMap'Coln'lookupOrDefault (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblMap'Coln'lookupOrDefault and sort = 
SortKItem in match c with 
| _ -> try MAP.hook_lookupOrDefault c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalString2Float (c: k) (config: k) (guard: int) : k = let lbl = 
LblString2Float and sort = 
SortFloat in match c with 
| _ -> try STRING.hook_string2float c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'stdout_K'Hyph'IO (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'stdout_K'Hyph'IO and sort = 
SortInt in match c with 
(*{| rule `` `#stdout_K-IO`(.KList)=>#token("1","Int")`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(fd57288699ac44a10b4d3e3c65ee3ad16f98646109e64ed7a5ac726279e39ae9) contentStartColumn(8) contentStartLine(909) org.kframework.attributes.Location(Location(909,8,909,20)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| () -> ((Lazy.force int1) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Hash'stdout_K'Hyph'IO : k Lazy.t = lazy (eval'Hash'stdout_K'Hyph'IO () interned_bottom (-1))
let eval_'And'Int__INT (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'And'Int__INT and sort = 
SortInt in match c with 
| _ -> try INT.hook_and c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let rec eval'Hash'borrowimmck (c: k * k * k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'borrowimmck and sort = 
SortBool in match c with 
| (((Int _ as var_0_2553) :: []),(var_1_2554),((Int _ as var_2_2555) :: []),((Int _ as var_3_2556) :: []),((Int _ as var_4_2557) :: [])) when guard < 0(*{| rule ``#borrowimmck(L5,_0,L1,L2,L3)=>#borrowimmck(L5,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(BEG,END,#mutRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L5)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG)),`_andBool_`(`_=/=Int__INT`(L5,L),`_==Bool__BOOL`(#lc(L1,L2,BEG,END),#token("false","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(b6be5c1aa3d43af761dc2085fe7ff0118e3e7f29fb7daed24652bb99b55b7213) contentStartColumn(6) contentStartLine(268) org.kframework.attributes.Location(Location(268,6,270,75)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2554) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_6_2558) :: []) as e12 -> (let e = ((evalMap'Coln'lookup((var_1_2554),e12) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2559) :: []),((Int _ as var_8_2560) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_4_2561) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2554),e12) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2562) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2553 :: []),e12) config (-1)))) && ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'lc((var_2_2555 :: []),(var_3_2556 :: []),(var_7_2559 :: []),(var_8_2560 :: [])) config (-1))),((Bool false) :: [])) config (-1))))))))) && (((compare_kitem var_4_2557 var_4_2561) = 0) && true) -> ((eval'Hash'borrowimmck((var_0_2553 :: []),(var_5_2562 :: []),(var_2_2555 :: []),(var_3_2556 :: []),(var_4_2557 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowimmck c config 0) else choice| _ -> (eval'Hash'borrowimmck c config 0))
| (((Int _ as var_0_2563) :: []),(var_1_2564),((Int _ as var_2_2565) :: []),((Int _ as var_3_2566) :: []),((Int _ as var_4_2567) :: [])) when guard < 1(*{| rule ``#borrowimmck(L5,_0,L1,L2,L3)=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(BEG,END,#mutRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L5)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG)),`_andBool_`(`_=/=Int__INT`(L5,L),`_==Bool__BOOL`(#lc(L1,L2,BEG,END),#token("true","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(49da526995df61bb74e0691b3ea1d6d3c1b6bf44e8fad24091d369d76e71d9e6) contentStartColumn(6) contentStartLine(272) org.kframework.attributes.Location(Location(272,6,274,74)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2564) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_5_2568) :: []) as e13 -> (let e = ((evalMap'Coln'lookup((var_1_2564),e13) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_6_2569) :: []),((Int _ as var_7_2570) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_4_2571) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2564),e13) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_8_2572) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'Int__INT((var_0_2563 :: []),e13) config (-1)))) && ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'lc((var_2_2565 :: []),(var_3_2566 :: []),(var_6_2569 :: []),(var_7_2570 :: [])) config (-1))),((Bool true) :: [])) config (-1))))))))) && (((compare_kitem var_4_2571 var_4_2567) = 0) && true) -> ((Bool true) :: [])| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowimmck c config 1) else choice| _ -> (eval'Hash'borrowimmck c config 1))
| (((Int _ as var_0_2573) :: []),(var_1_2574),((Int _ as var_2_2575) :: []),((Int _ as var_3_2576) :: []),((Int _ as var_4_2577) :: [])) when guard < 2(*{| rule ``#borrowimmck(L4,_0,L1,L2,L3)=>#borrowimmck(L4,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#rs(_48),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L4)),isProps(_48)),isInt(L2)),isKItem(L)),isMap(M)),isInt(L1))) ensures #token("true","Bool") [UNIQUE_ID(6936defb17ffe6992c6041ee49d9d8832270ab3d8b0bbef643228d60f96637c5) contentStartColumn(6) contentStartLine(260) org.kframework.attributes.Location(Location(260,6,260,105)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2574) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | (var_6_2578 :: []) as e14 -> (let e = ((evalMap'Coln'lookup((var_1_2574),e14) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply1(Lbl'Hash'rs,(var_7_2579 :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2574),e14) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2580) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((true) && (true))) && ((isTrue (evalisProps((var_7_2579 :: [])) config (-1)))))) && (true))) && (true))) && (true))) && (true)))) && (true) -> ((eval'Hash'borrowimmck((var_0_2573 :: []),(var_5_2580 :: []),(var_2_2575 :: []),(var_3_2576 :: []),(var_4_2577 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowimmck c config 2) else choice| _ -> (eval'Hash'borrowimmck c config 2))
| (((Int _ as var_0_2581) :: []),(var_1_2582),((Int _ as var_2_2583) :: []),((Int _ as var_3_2584) :: []),((Int _ as var_4_2585) :: [])) when guard < 3(*{| rule ``#borrowimmck(L5,_0,L1,L2,L4)=>#borrowimmck(L5,M,L1,L2,L4)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(_77,_78,#mutRef(L3)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L5)),isInt(_77)),isInt(L4)),isInt(L2)),isInt(L)),isInt(_78)),isMap(M)),isInt(L1)),`_=/=Int__INT`(L3,L4))) ensures #token("true","Bool") [UNIQUE_ID(e1aa457f77ca1c3bc272fdbedf4ecfa302c2df806f621c1a2f9d6f007368ed65) contentStartColumn(6) contentStartLine(264) org.kframework.attributes.Location(Location(264,6,266,27)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2582) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | ((Int _ as var_6_2586) :: []) as e15 -> (let e = ((evalMap'Coln'lookup((var_1_2582),e15) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2587) :: []),((Int _ as var_8_2588) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_9_2589) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2582),e15) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2590) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((isTrue (eval_'EqlsSlshEqls'Int__INT((var_9_2589 :: []),(var_4_2585 :: [])) config (-1))))))) && (true) -> ((eval'Hash'borrowimmck((var_0_2581 :: []),(var_5_2590 :: []),(var_2_2583 :: []),(var_3_2584 :: []),(var_4_2585 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowimmck c config 3) else choice| _ -> (eval'Hash'borrowimmck c config 3))
| (((Int _ as var_0_2591) :: []),(var_1_2592),((Int _ as var_2_2593) :: []),((Int _ as var_3_2594) :: []),((Int _ as var_4_2595) :: [])) when guard < 4(*{| rule ``#borrowimmck(L4,_0,L1,L2,L3)=>#borrowimmck(L4,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(`#uninit_OSL-SYNTAX`(.KList),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(L4)),isInt(L2)),isKItem(L)),isMap(M)),isInt(L1))) ensures #token("true","Bool") [UNIQUE_ID(7ec37237f0e0511339090486f74cebe8f084132167f559fd74c4d329f9648d5e) contentStartColumn(6) contentStartLine(262) org.kframework.attributes.Location(Location(262,6,262,106)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2592) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | (var_6_2596 :: []) as e16 -> (let e = ((evalMap'Coln'lookup((var_1_2592),e16) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2592),e16) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2597) :: []) when ((((((true) && (true))) && (true))) && (((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true)))) && (true) -> ((eval'Hash'borrowimmck((var_0_2591 :: []),(var_5_2597 :: []),(var_2_2593 :: []),(var_3_2594 :: []),(var_4_2595 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowimmck c config 4) else choice| _ -> (eval'Hash'borrowimmck c config 4))
| (((Int _ as var_0_2598) :: []),(var_1_2599),((Int _ as var_2_2600) :: []),((Int _ as var_3_2601) :: []),((Int _ as var_4_2602) :: [])) when guard < 5(*{| rule ``#borrowimmck(L4,_0,L1,L2,L3)=>#borrowimmck(L4,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(L,_0),#match(#br(_84,_85,#immRef(_86)),`Map:lookup`(_0,L))),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(_84),isInt(L3)),isInt(L4)),isInt(L2)),isKItem(L)),isInt(_86)),isMap(M)),isInt(L1)),isInt(_85))) ensures #token("true","Bool") [UNIQUE_ID(6e3ca156e043cd3007573db611d1bb66b7784bfd5651afbf3f517b6a2952f583) contentStartColumn(6) contentStartLine(261) org.kframework.attributes.Location(Location(261,6,261,118)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2599) with 
| [Map (_,_,collection)] -> let choice = (KMap.fold (fun e v result -> if result == interned_bottom then (match e with | (var_6_2603 :: []) as e17 -> (let e = ((evalMap'Coln'lookup((var_1_2599),e17) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| (KApply3(Lbl'Hash'br,((Int _ as var_7_2604) :: []),((Int _ as var_8_2605) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_9_2606) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2599),e17) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Map (SortMap,_,_) as var_5_2607) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true)))) && (true) -> ((eval'Hash'borrowimmck((var_0_2598 :: []),(var_5_2607 :: []),(var_2_2600 :: []),(var_3_2601 :: []),(var_4_2602 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'borrowimmck c config 5) else choice| _ -> (eval'Hash'borrowimmck c config 5))
| (((Int _ as var_0_2608) :: []),(var_1_2609),((Int _ as var_2_2610) :: []),((Int _ as var_3_2611) :: []),((Int _ as var_4_2612) :: [])) when guard < 6(*{| rule ``#borrowimmck(L,_0,L1,L2,L3)=>#borrowimmck(L,M,L1,L2,L3)`` requires `_andBool_`(`_andBool_`(#match(#br(BEG,END,#mutRef(L3)),`Map:lookup`(_0,L)),#match(M,`_[_<-undef]`(_0,L))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(L3),isInt(END)),isInt(L2)),isInt(L)),isMap(M)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(340b886943fda223d1852f954bf2bc7dbbc47d52ea37940627dee517a4f7b6f9) contentStartColumn(6) contentStartLine(277) org.kframework.attributes.Location(Location(277,6,278,47)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (let e = ((evalMap'Coln'lookup((var_1_2609),(var_0_2608 :: [])) config (-1))) in match e with 
| [Bottom] -> (eval'Hash'borrowimmck c config 6)
| (KApply3(Lbl'Hash'br,((Int _ as var_6_2613) :: []),((Int _ as var_7_2614) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_4_2615) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_1_2609),(var_0_2608 :: [])) config (-1))) in match e with 
| [Bottom] -> (eval'Hash'borrowimmck c config 6)
| ((Map (SortMap,_,_) as var_5_2616) :: []) when ((((true) && (true))) && (((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true)))) && (((compare_kitem var_4_2612 var_4_2615) = 0) && true) -> ((eval'Hash'borrowimmck((var_0_2608 :: []),(var_5_2616 :: []),(var_2_2610 :: []),(var_3_2611 :: []),(var_4_2612 :: [])) config (-1)))| _ -> (eval'Hash'borrowimmck c config 6))| _ -> (eval'Hash'borrowimmck c config 6))
(*{| rule ``#borrowimmck(_73,_0,_74,_75,_76)=>#token("false","Bool")`` requires `_andBool_`(`_==K_`(`.Map`(.KList),_0),`_andBool_`(`_andBool_`(`_andBool_`(isInt(_76),isInt(_73)),isInt(_74)),isInt(_75))) ensures #token("true","Bool") [UNIQUE_ID(689863ec3de3c975c20bb65695f35a516d54726932341ac3a66457ab8622e940) contentStartColumn(6) contentStartLine(282) org.kframework.attributes.Location(Location(282,6,282,42)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((Int _ as var_73_2617) :: []),(var_0_2618),((Int _ as var_74_2619) :: []),((Int _ as var_75_2620) :: []),((Int _ as var_76_2621) :: [])) when (((isTrue (eval_'EqlsEqls'K_(((Lazy.force const'Stop'Map)),(var_0_2618)) config (-1)))) && (((((((true) && (true))) && (true))) && (true)))) && (true) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize5 c)))])
let evallog2Int (c: k) (config: k) (guard: int) : k = let lbl = 
Lbllog2Int and sort = 
SortInt in match c with 
| _ -> try INT.hook_log2 c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'stdin_K'Hyph'IO (c: unit) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'stdin_K'Hyph'IO and sort = 
SortInt in match c with 
(*{| rule `` `#stdin_K-IO`(.KList)=>#token("0","Int")`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(595a161d3d44d3c588fcd417f7279ef70547c573e159ef5bfc70692b22149da9) contentStartColumn(8) contentStartLine(908) org.kframework.attributes.Location(Location(908,8,908,19)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| () -> ((Lazy.force int0) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let const'Hash'stdin_K'Hyph'IO : k Lazy.t = lazy (eval'Hash'stdin_K'Hyph'IO () interned_bottom (-1))
let evalBase2String (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblBase2String and sort = 
SortString in match c with 
| _ -> try STRING.hook_base2string c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisStream (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStream and sort = 
SortBool in match c with 
(*{| rule ``isStream(#buffer(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'buffer,(varK0_2622)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStream(#KToken(#token("Stream","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStream, var__2623) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStream(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2624)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'_GT_Eqls'String__STRING (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_GT_Eqls'String__STRING and sort = 
SortBool in match c with 
| _ -> try STRING.hook_ge c lbl sort config freshFunction
with Not_implemented -> match c with 
(*{| rule `` `_>=String__STRING`(S1,S2)=>`notBool_`(`_<String__STRING`(S1,S2))`` requires `_andBool_`(isString(S2),isString(S1)) ensures #token("true","Bool") [UNIQUE_ID(2e24bd4be2ac7aa21b58ac7fe4e1e4e70e5e69437dff6c98e3ff986df28f5b21) contentStartColumn(8) contentStartLine(551) org.kframework.attributes.Location(Location(551,8,551,63)) org.kframework.attributes.Source(Source(/root/k/include/builtin/domains.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (((String _ as varS1_2625) :: []),((String _ as varS2_2626) :: [])) when ((true) && (true)) && (true) -> ([Bool ((not ((isTrue (eval_'_LT_'String__STRING((varS1_2625 :: []),(varS2_2626 :: [])) config (-1))))))])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let eval_'_LT_Eqls'Map__MAP (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'_LT_Eqls'Map__MAP and sort = 
SortBool in match c with 
| _ -> try MAP.hook_inclusion c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalnewUUID_STRING (c: unit) (config: k) (guard: int) : k = let lbl = 
LblnewUUID_STRING and sort = 
SortString in match c with 
| _ -> try STRING.hook_uuid c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let evalsize (c: k) (config: k) (guard: int) : k = let lbl = 
Lblsize and sort = 
SortInt in match c with 
| _ -> try SET.hook_size c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let rec eval'Hash'existRef (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'existRef and sort = 
SortBool in match c with 
(*{| rule ``#existRef(_66,_0,_67)=>#token("false","Bool")`` requires `_andBool_`(`_==K_`(`.Set`(.KList),_0),isInt(_67)) ensures #token("true","Bool") [UNIQUE_ID(420a28c27c388305f945c50981a7a21aaaa52554a34acb17ffe9d3dc8f64e585) contentStartColumn(6) contentStartLine(105) org.kframework.attributes.Location(Location(105,6,105,36)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| ((var_66_2627),(var_0_2628),((Int _ as var_67_2629) :: [])) when (((isTrue (eval_'EqlsEqls'K_(((Lazy.force const'Stop'Set)),(var_0_2628)) config (-1)))) && (true)) && (true) -> ((Bool false) :: [])
| ((var_0_2630),(var_1_2631),((Int _ as var_2_2632) :: [])) when guard < 1(*{| rule ``#existRef(R,_0,C)=>`_andBool_`(`_>=Int__INT`(C1,C),#token("true","Bool"))`` requires `_andBool_`(`_andBool_`(#setChoice(#br(_59,C1,R),_0),#match(S,`Set:difference`(_0,`SetItem`(#br(_59,C1,R))))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(C1),isSet(S)),isInt(C)),isInt(_59)),isK(R))) ensures #token("true","Bool") [UNIQUE_ID(8a57f9b33ba228a1fddfbdd4ae668ff2c25c5030707dbac93bfc701e20185aa9) contentStartColumn(6) contentStartLine(101) org.kframework.attributes.Location(Location(101,6,101,88)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2631) with 
| [Set (_,_,collection)] -> let choice = (KSet.fold (fun e result -> if result == interned_bottom then (match e with | (KApply3(Lbl'Hash'br,((Int _ as var_4_2633) :: []),((Int _ as var_3_2634) :: []),(var_0_2635)) :: []) as e18 -> (let e = ((evalSet'Coln'difference((var_1_2631),((evalSetItem(e18) config (-1)))) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Set (SortSet,_,_) as var_5_2636) :: []) when ((((true) && (true))) && (((((((((true) && (true))) && (true))) && (true))) && (true)))) && (((compare var_0_2635 var_0_2630) = 0) && true) -> ([Bool ((((isTrue (eval_'_GT_Eqls'Int__INT((var_3_2634 :: []),(var_2_2632 :: [])) config (-1)))) && (true)))])| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'existRef c config 1) else choice| _ -> (eval'Hash'existRef c config 1))
| ((var_0_2637),(var_1_2638),((Int _ as var_2_2639) :: [])) when guard < 2(*{| rule ``#existRef(R,_0,C)=>#existRef(R,S,C)`` requires `_andBool_`(`_andBool_`(#setChoice(#br(_83,C1,R1),_0),#match(S,`Set:difference`(_0,`SetItem`(#br(_83,C1,R1))))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(C1),isSet(S)),isInt(C)),isInt(_83)),isK(R)),isExp(R1)),`_=/=K_`(R,R1))) ensures #token("true","Bool") [UNIQUE_ID(addbb2beba63359e81b53cbbdfa5c4c2a14eea9eafea4b1a949533f8d466cd1e) contentStartColumn(6) contentStartLine(102) org.kframework.attributes.Location(Location(102,6,103,24)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
 -> (match (var_1_2638) with 
| [Set (_,_,collection)] -> let choice = (KSet.fold (fun e result -> if result == interned_bottom then (match e with | (KApply3(Lbl'Hash'br,((Int _ as var_4_2640) :: []),((Int _ as var_5_2641) :: []),(var_6_2642 :: [])) :: []) as e19 -> (let e = ((evalSet'Coln'difference((var_1_2638),((evalSetItem(e19) config (-1)))) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Set (SortSet,_,_) as var_3_2643) :: []) when ((((true) && (true))) && (((((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((isTrue (evalisExp((var_6_2642 :: [])) config (-1)))))) && ((isTrue (eval_'EqlsSlshEqls'K_((var_0_2637),(var_6_2642 :: [])) config (-1))))))) && (true) -> ((eval'Hash'existRef((var_0_2637),(var_3_2643 :: []),(var_2_2639 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'existRef c config 2) else choice| _ -> (eval'Hash'existRef c config 2))
| ((var_0_2644),(var_1_2645),((Int _ as var_2_2646) :: [])) when guard < 3(*{| rule ``#existRef(R,_0,C)=>#existRef(R,S,C)`` requires `_andBool_`(`_andBool_`(#match(S,`Set:difference`(_0,`SetItem`(`#uninit_OSL-SYNTAX`(.KList)))),`Set:in`(`#uninit_OSL-SYNTAX`(.KList),_0)),`_andBool_`(`_andBool_`(isSet(S),isInt(C)),isK(R))) ensures #token("true","Bool") [UNIQUE_ID(8383a9109cbaee1fc14b643f6a2303c2e256c5a940a61663ee0472e3c7b807ba) contentStartColumn(6) contentStartLine(100) org.kframework.attributes.Location(Location(100,6,100,73)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (let e = ((evalSet'Coln'difference((var_1_2645),((evalSetItem((const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1)))) config (-1))) in match e with 
| [Bottom] -> (eval'Hash'existRef c config 3)
| ((Set (SortSet,_,_) as var_3_2647) :: []) when ((((true) && ((isTrue (evalSet'Coln'in((const'Hash'uninit_OSL'Hyph'SYNTAX :: []),(var_1_2645)) config (-1)))))) && (((((true) && (true))) && (true)))) && (true) -> ((eval'Hash'existRef((var_0_2644),(var_3_2647 :: []),(var_2_2646 :: [])) config (-1)))| _ -> (eval'Hash'existRef c config 3))
| ((var_0_2648),(var_1_2649),((Int _ as var_2_2650) :: [])) when guard < 4(*{| rule ``#existRef(R,_0,C)=>#existRef(R,S,C)`` requires `_andBool_`(`_andBool_`(#setChoice(#rs(_88),_0),#match(S,`Set:difference`(_0,`SetItem`(#rs(_88))))),`_andBool_`(`_andBool_`(`_andBool_`(isSet(S),isInt(C)),isK(R)),isProps(_88))) ensures #token("true","Bool") [UNIQUE_ID(75c58a5d5155b0b9c984b83a6ae24d35a88d50ea63c90aef18046950ee95826b) contentStartColumn(6) contentStartLine(99) org.kframework.attributes.Location(Location(99,6,99,72)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_1_2649) with 
| [Set (_,_,collection)] -> let choice = (KSet.fold (fun e result -> if result == interned_bottom then (match e with | (KApply1(Lbl'Hash'rs,(var_4_2651 :: [])) :: []) as e20 -> (let e = ((evalSet'Coln'difference((var_1_2649),((evalSetItem(e20) config (-1)))) config (-1))) in match e with 
| [Bottom] -> interned_bottom
| ((Set (SortSet,_,_) as var_3_2652) :: []) when ((((true) && (true))) && (((((((true) && (true))) && (true))) && ((isTrue (evalisProps((var_4_2651 :: [])) config (-1))))))) && (true) -> ((eval'Hash'existRef((var_0_2648),(var_3_2652 :: []),(var_2_2650 :: [])) config (-1)))| _ -> interned_bottom)| _ -> interned_bottom) else result) collection interned_bottom) in if choice == interned_bottom then (eval'Hash'existRef c config 4) else choice| _ -> (eval'Hash'existRef c config 4))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let eval_inList_ (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_inList_ and sort = 
SortBool in match c with 
| _ -> try LIST.hook_in c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalinitFunDefCell (c: unit) (config: k) (guard: int) : k = let lbl = 
LblinitFunDefCell and sort = 
SortFunDefCell in match c with 
(*{| rule ``initFunDefCell(.KList)=>`FunDefCellMapItem`(initFnameCell(.KList),`<funDef>`(initFnameCell(.KList),initFparamsCell(.KList),initFretCell(.KList),initFbodyCell(.KList)))`` requires #token("true","Bool") ensures #token("true","Bool") [UNIQUE_ID(e79cbce5a7d29b74d264d0c95d1bbcdb54e319692b74308e05b18663ab08d38e) initializer()]|}*)
| () -> ((evalFunDefCellMapItem(((Lazy.force constinitFnameCell)),(KApply4(Lbl'_LT_'funDef'_GT_',((Lazy.force constinitFnameCell)),((Lazy.force constinitFparamsCell)),((Lazy.force constinitFretCell)),((Lazy.force constinitFbodyCell))) :: [])) config (-1)))
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize0 c)))])
let constinitFunDefCell : k Lazy.t = lazy (evalinitFunDefCell () interned_bottom (-1))
let evalsrandInt (c: k) (config: k) (guard: int) : k = let lbl = 
LblsrandInt and sort = 
SortK in match c with 
| _ -> try INT.hook_srand c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalList'Coln'set (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
LblList'Coln'set and sort = 
SortList in match c with 
| _ -> try LIST.hook_update c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let evalString2Base (c: k * k) (config: k) (guard: int) : k = let lbl = 
LblString2Base and sort = 
SortInt in match c with 
| _ -> try STRING.hook_string2base c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisTCellFragment (c: k) (config: k) (guard: int) : k = let lbl = 
LblisTCellFragment and sort = 
SortBool in match c with 
(*{| rule ``isTCellFragment(#KToken(#token("TCellFragment","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortTCellFragment, var__2653) :: [])) -> ((Bool true) :: [])
(*{| rule ``isTCellFragment(`<T>-fragment`(K0,K1,K2,K3))=>#token("true","Bool")`` requires `_andBool_`(`_andBool_`(`_andBool_`(isStatesCellOpt(K0),isNstateCellOpt(K1)),isTmpCellOpt(K2)),isFunDefsCellOpt(K3)) ensures #token("true","Bool") []|}*)
| ((KApply4(Lbl'_LT_'T'_GT_Hyph'fragment,(varK0_2654 :: []),(varK1_2655 :: []),(varK2_2656 :: []),(varK3_2657 :: [])) :: [])) when (((((((isTrue (evalisStatesCellOpt((varK0_2654 :: [])) config (-1)))) && ((isTrue (evalisNstateCellOpt((varK1_2655 :: [])) config (-1)))))) && ((isTrue (evalisTmpCellOpt((varK2_2656 :: [])) config (-1)))))) && ((isTrue (evalisFunDefsCellOpt((varK3_2657 :: [])) config (-1))))) && (true) -> ((Bool true) :: [])
(*{| rule ``isTCellFragment(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2658)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalisStatesCellFragment (c: k) (config: k) (guard: int) : k = let lbl = 
LblisStatesCellFragment and sort = 
SortBool in match c with 
(*{| rule ``isStatesCellFragment(`<states>-fragment`(K0))=>#token("true","Bool")`` requires isStateCellMap(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'_LT_'states'_GT_Hyph'fragment,((Map (SortStateCellMap,_,_) as varK0_2659) :: [])) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isStatesCellFragment(#KToken(#token("StatesCellFragment","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortStatesCellFragment, var__2660) :: [])) -> ((Bool true) :: [])
(*{| rule ``isStatesCellFragment(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2661)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP (c: k * k * k) (config: k) (guard: int) : k = let lbl = 
Lbl_'LSqB'_'_LT_Hyph'_'RSqB'_MAP and sort = 
SortMap in match c with 
| _ -> try MAP.hook_update c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize3 c)))])
let eval'Hash'tell'LPar'_'RPar'_K'Hyph'IO (c: k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'tell'LPar'_'RPar'_K'Hyph'IO and sort = 
SortInt in match c with 
| _ -> try IO.hook_tell c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalkeys (c: k) (config: k) (guard: int) : k = let lbl = 
Lblkeys and sort = 
SortSet in match c with 
| _ -> try MAP.hook_keys c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let evalgetKLabel (c: k) (config: k) (guard: int) : k = let lbl = 
LblgetKLabel and sort = 
SortKItem in match c with 
| _ -> try KREFLECTION.hook_getKLabel c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO (c: k * k) (config: k) (guard: int) : k = let lbl = 
Lbl'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO and sort = 
SortK in match c with 
| _ -> try IO.hook_seekEnd c lbl sort config freshFunction
with Not_implemented -> match c with 
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize2 c)))])
let evalisLOCK (c: k) (config: k) (guard: int) : k = let lbl = 
LblisLOCK and sort = 
SortBool in match c with 
(*{| rule ``isLOCK(#compare(K0))=>#token("true","Bool")`` requires isK(K0) ensures #token("true","Bool") []|}*)
| ((KApply1(Lbl'Hash'compare,(varK0_2662)) :: [])) when true && (true) -> ((Bool true) :: [])
(*{| rule ``isLOCK(#KToken(#token("LOCK","KString"),_))=>#token("true","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") []|}*)
| ((KToken (SortLOCK, var__2663) :: [])) -> ((Bool true) :: [])
(*{| rule ``isLOCK(K)=>#token("false","Bool")`` requires #token("true","Bool") ensures #token("true","Bool") [owise()]|}*)
| ((varK_2664)) -> ((Bool false) :: [])
| _ -> raise (Stuck [denormalize (KApply(lbl, (denormalize1 c)))])
let eval (c: normal_kitem) (config: k) : k = match c with KApply(lbl, kl) -> (match lbl with 
|Lbl'Hash'argv -> eval'Hash'argv (normalize0 kl) config (-1)
|LblisFparamsCellOpt -> evalisFparamsCellOpt (normalize1 kl) config (-1)
|LblisFbodyCellOpt -> evalisFbodyCellOpt (normalize1 kl) config (-1)
|LblnotBool_ -> evalnotBool_ (normalize1 kl) config (-1)
|Lbl_'_LT_Eqls'Int__INT -> eval_'_LT_Eqls'Int__INT (normalize2 kl) config (-1)
|LblMap'Coln'lookup -> evalMap'Coln'lookup (normalize2 kl) config (-1)
|Lbl'Hash'stderr_K'Hyph'IO -> eval'Hash'stderr_K'Hyph'IO (normalize0 kl) config (-1)
|Lbl'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|LblsignExtendBitRangeInt -> evalsignExtendBitRangeInt (normalize3 kl) config (-1)
|Lbl_'EqlsEqls'Bool__BOOL -> eval_'EqlsEqls'Bool__BOOL (normalize2 kl) config (-1)
|LblisTimerCell -> evalisTimerCell (normalize1 kl) config (-1)
|LblisSet -> evalisSet (normalize1 kl) config (-1)
|Lbl'Hash'getenv -> eval'Hash'getenv (normalize1 kl) config (-1)
|LblintersectSet -> evalintersectSet (normalize2 kl) config (-1)
|Lbl_in_keys'LPar'_'RPar'_MAP -> eval_in_keys'LPar'_'RPar'_MAP (normalize2 kl) config (-1)
|LblfindChar -> evalfindChar (normalize3 kl) config (-1)
|LblisStatesCell -> evalisStatesCell (normalize1 kl) config (-1)
|LblinitNstateCell -> evalinitNstateCell (normalize0 kl) config (-1)
|LblSet'Coln'in -> evalSet'Coln'in (normalize2 kl) config (-1)
|LblisK -> evalisK (normalize1 kl) config (-1)
|LblString2Int -> evalString2Int (normalize1 kl) config (-1)
|LblinitFparamsCell -> evalinitFparamsCell (normalize0 kl) config (-1)
|Lbl_'_LT_Eqls'Set__SET -> eval_'_LT_Eqls'Set__SET (normalize2 kl) config (-1)
|LblisIOError -> evalisIOError (normalize1 kl) config (-1)
|Lbl'Hash'parse -> eval'Hash'parse (normalize2 kl) config (-1)
|LblmakeList -> evalmakeList (normalize2 kl) config (-1)
|LblisSeparator -> evalisSeparator (normalize1 kl) config (-1)
|Lbl_'LSqB'_'_LT_Hyph'undef'RSqB' -> eval_'LSqB'_'_LT_Hyph'undef'RSqB' (normalize2 kl) config (-1)
|LblinitIndexesCell -> evalinitIndexesCell (normalize0 kl) config (-1)
|LblisFunDefCellFragment -> evalisFunDefCellFragment (normalize1 kl) config (-1)
|LblinitFbodyCell -> evalinitFbodyCell (normalize0 kl) config (-1)
|Lbl'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|Lbl_'EqlsEqls'Int_ -> eval_'EqlsEqls'Int_ (normalize2 kl) config (-1)
|Lbl_andThenBool__BOOL -> eval_andThenBool__BOOL (normalize2 kl) config (-1)
|LblisFnameCellOpt -> evalisFnameCellOpt (normalize1 kl) config (-1)
|Lbl'Hash'writeCK -> eval'Hash'writeCK (normalize4 kl) config (-1)
|Lbl'Hash'parseInModule -> eval'Hash'parseInModule (normalize3 kl) config (-1)
|LblisBlockItem -> evalisBlockItem (normalize1 kl) config (-1)
|Lbl'Hash'system -> eval'Hash'system (normalize1 kl) config (-1)
|Lbl_StateCellMap_ -> eval_StateCellMap_ (normalize2 kl) config (-1)
|LblisString -> evalisString (normalize1 kl) config (-1)
|LblisProp -> evalisProp (normalize1 kl) config (-1)
|Lbl_'Perc'Int__INT -> eval_'Perc'Int__INT (normalize2 kl) config (-1)
|Lbl_'_GT__GT_'Int__INT -> eval_'_GT__GT_'Int__INT (normalize2 kl) config (-1)
|LblisTmpCell -> evalisTmpCell (normalize1 kl) config (-1)
|LblisList -> evalisList (normalize1 kl) config (-1)
|LblisFunction -> evalisFunction (normalize1 kl) config (-1)
|LblreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING -> evalreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING (normalize3 kl) config (-1)
|LblisStatesCellOpt -> evalisStatesCellOpt (normalize1 kl) config (-1)
|LblisNstateCell -> evalisNstateCell (normalize1 kl) config (-1)
|LblfindString -> evalfindString (normalize3 kl) config (-1)
|Lbl_'Xor_'Int__INT -> eval_'Xor_'Int__INT (normalize2 kl) config (-1)
|LblinitWriteCell -> evalinitWriteCell (normalize0 kl) config (-1)
|Lbl'Hash'borrowmutck -> eval'Hash'borrowmutck (normalize5 kl) config (-1)
|LblabsInt -> evalabsInt (normalize1 kl) config (-1)
|LblisEnvCell -> evalisEnvCell (normalize1 kl) config (-1)
|Lbl_'_GT_'String__STRING -> eval_'_GT_'String__STRING (normalize2 kl) config (-1)
|LblisBorrowItem -> evalisBorrowItem (normalize1 kl) config (-1)
|Lbl_'EqlsEqls'String__STRING -> eval_'EqlsEqls'String__STRING (normalize2 kl) config (-1)
|Lbl'Hash'checkInit -> eval'Hash'checkInit (normalize3 kl) config (-1)
|LblisKConfigVar -> evalisKConfigVar (normalize1 kl) config (-1)
|LblisFunDefsCellFragment -> evalisFunDefsCellFragment (normalize1 kl) config (-1)
|LblisProps -> evalisProps (normalize1 kl) config (-1)
|LblisKResult -> evalisKResult (normalize1 kl) config (-1)
|Lbl'Hash'lc -> eval'Hash'lc (normalize4 kl) config (-1)
|LblisCell -> evalisCell (normalize1 kl) config (-1)
|Lblvalues -> evalvalues (normalize1 kl) config (-1)
|LblList'Coln'get -> evalList'Coln'get (normalize2 kl) config (-1)
|LblisStackCell -> evalisStackCell (normalize1 kl) config (-1)
|Lbl'Hash'lstat'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'lstat'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|LblisIndexesCell -> evalisIndexesCell (normalize1 kl) config (-1)
|LblinitFnameCell -> evalinitFnameCell (normalize0 kl) config (-1)
|LblSetItem -> evalSetItem (normalize1 kl) config (-1)
|LblisRItem -> evalisRItem (normalize1 kl) config (-1)
|LblStateCellMapItem -> evalStateCellMapItem (normalize2 kl) config (-1)
|LblisStoreCellOpt -> evalisStoreCellOpt (normalize1 kl) config (-1)
|Lbl_'_LT_'Int__INT -> eval_'_LT_'Int__INT (normalize2 kl) config (-1)
|Lbl'Stop'List -> eval'Stop'List (normalize0 kl) config (-1)
|LblisLoopItem -> evalisLoopItem (normalize1 kl) config (-1)
|LblisFunDefsCellOpt -> evalisFunDefsCellOpt (normalize1 kl) config (-1)
|LblisWItem -> evalisWItem (normalize1 kl) config (-1)
|LblrandInt -> evalrandInt (normalize1 kl) config (-1)
|Lbl'Hash'configuration_K'Hyph'REFLECTION -> eval'Hash'configuration_K'Hyph'REFLECTION (normalize0 kl) config (-1)
|LblisUninit -> evalisUninit (normalize1 kl) config (-1)
|LblisFloat -> evalisFloat (normalize1 kl) config (-1)
|Lbl'Hash'close'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'close'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|LblisOItem -> evalisOItem (normalize1 kl) config (-1)
|LblinitFretCell -> evalinitFretCell (normalize0 kl) config (-1)
|Lblkeys_list'LPar'_'RPar'_MAP -> evalkeys_list'LPar'_'RPar'_MAP (normalize1 kl) config (-1)
|LblinitFunDefsCell -> evalinitFunDefsCell (normalize0 kl) config (-1)
|LblfreshId -> evalfreshId (normalize1 kl) config (-1)
|LblchrChar -> evalchrChar (normalize1 kl) config (-1)
|Lbl_orElseBool__BOOL -> eval_orElseBool__BOOL (normalize2 kl) config (-1)
|Lbl_divInt__INT -> eval_divInt__INT (normalize2 kl) config (-1)
|LblisIndexCell -> evalisIndexCell (normalize1 kl) config (-1)
|LblList'Coln'range -> evalList'Coln'range (normalize3 kl) config (-1)
|Lbl'Hash'wv -> eval'Hash'wv (normalize2 kl) config (-1)
|Lbl_'Plus'Int_ -> eval_'Plus'Int_ (normalize2 kl) config (-1)
|LblisKCell -> evalisKCell (normalize1 kl) config (-1)
|Lbl_orBool__BOOL -> eval_orBool__BOOL (normalize2 kl) config (-1)
|Lbl_'_GT_Eqls'Int__INT -> eval_'_GT_Eqls'Int__INT (normalize2 kl) config (-1)
|LblinitEnvCell -> evalinitEnvCell (normalize0 kl) config (-1)
|Lbl'Hash'compareE -> eval'Hash'compareE (normalize2 kl) config (-1)
|Lbl'Hash'bindParams -> eval'Hash'bindParams (normalize3 kl) config (-1)
|LblupdateMap -> evalupdateMap (normalize2 kl) config (-1)
|LblInt2String -> evalInt2String (normalize1 kl) config (-1)
|LblisNstateCellOpt -> evalisNstateCellOpt (normalize1 kl) config (-1)
|Lbl_'EqlsSlshEqls'K_ -> eval_'EqlsSlshEqls'K_ (normalize2 kl) config (-1)
|Lbl'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|LblcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING -> evalcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING (normalize2 kl) config (-1)
|Lbl_'_GT_'Int__INT -> eval_'_GT_'Int__INT (normalize2 kl) config (-1)
|LblfillList -> evalfillList (normalize4 kl) config (-1)
|LblFunDefCellMapItem -> evalFunDefCellMapItem (normalize2 kl) config (-1)
|LblisBlocks -> evalisBlocks (normalize1 kl) config (-1)
|LblbitRangeInt -> evalbitRangeInt (normalize3 kl) config (-1)
|Lbl_'_LT_'String__STRING -> eval_'_LT_'String__STRING (normalize2 kl) config (-1)
|Lbl'Stop'StateCellMap -> eval'Stop'StateCellMap (normalize0 kl) config (-1)
|LblisTimerCellOpt -> evalisTimerCellOpt (normalize1 kl) config (-1)
|Lbl_List_ -> eval_List_ (normalize2 kl) config (-1)
|Lbl'Hash'inProps -> eval'Hash'inProps (normalize2 kl) config (-1)
|Lbl'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|Lbl_'PipeHyph_GT_'_ -> eval_'PipeHyph_GT_'_ (normalize2 kl) config (-1)
|Lbl_xorBool__BOOL -> eval_xorBool__BOOL (normalize2 kl) config (-1)
|Lbl_'Hyph'Map__MAP -> eval_'Hyph'Map__MAP (normalize2 kl) config (-1)
|Lbl'Hash'compareS -> eval'Hash'compareS (normalize2 kl) config (-1)
|LblisIndexesCellOpt -> evalisIndexesCellOpt (normalize1 kl) config (-1)
|LblinitStoreCell -> evalinitStoreCell (normalize0 kl) config (-1)
|Lbl_'EqlsEqls'K_ -> eval_'EqlsEqls'K_ (normalize2 kl) config (-1)
|Lbl'Hash'sort -> eval'Hash'sort (normalize1 kl) config (-1)
|LblisIndexCellOpt -> evalisIndexCellOpt (normalize1 kl) config (-1)
|Lbl'Hash'open'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'open'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|LblreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING -> evalreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING (normalize3 kl) config (-1)
|Lbl'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|LblisTmpCellOpt -> evalisTmpCellOpt (normalize1 kl) config (-1)
|Lbl'Stop'Map -> eval'Stop'Map (normalize0 kl) config (-1)
|Lbl'Hash'logToFile -> eval'Hash'logToFile (normalize2 kl) config (-1)
|Lbl_'EqlsSlshEqls'String__STRING -> eval_'EqlsSlshEqls'String__STRING (normalize2 kl) config (-1)
|Lbl'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|LblisStateCellFragment -> evalisStateCellFragment (normalize1 kl) config (-1)
|LblisType -> evalisType (normalize1 kl) config (-1)
|LblId2String -> evalId2String (normalize1 kl) config (-1)
|LblisInt -> evalisInt (normalize1 kl) config (-1)
|LblisBranchTmp -> evalisBranchTmp (normalize1 kl) config (-1)
|Lbl_FunDefCellMap_ -> eval_FunDefCellMap_ (normalize2 kl) config (-1)
|Lbl'Hash'fresh -> eval'Hash'fresh (normalize1 kl) config (-1)
|LblMap'Coln'choice -> evalMap'Coln'choice (normalize1 kl) config (-1)
|LblisFunDefsCell -> evalisFunDefsCell (normalize1 kl) config (-1)
|LblisExps -> evalisExps (normalize1 kl) config (-1)
|Lbl_impliesBool__BOOL -> eval_impliesBool__BOOL (normalize2 kl) config (-1)
|Lbl'Hash'getc'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'getc'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|Lbl_Set_ -> eval_Set_ (normalize2 kl) config (-1)
|LblinitTmpCell -> evalinitTmpCell (normalize0 kl) config (-1)
|Lbl_'Star'Int__INT -> eval_'Star'Int__INT (normalize2 kl) config (-1)
|LblmaxInt'LPar'_'Comm'_'RPar'_INT -> evalmaxInt'LPar'_'Comm'_'RPar'_INT (normalize2 kl) config (-1)
|Lbl'Hash'unwrapInt -> eval'Hash'unwrapInt (normalize1 kl) config (-1)
|LblisBool -> evalisBool (normalize1 kl) config (-1)
|Lbl'Tild'Int__INT -> eval'Tild'Int__INT (normalize1 kl) config (-1)
|LblisStackCellOpt -> evalisStackCellOpt (normalize1 kl) config (-1)
|Lbl'Hash'list2Set -> eval'Hash'list2Set (normalize1 kl) config (-1)
|Lbl_'_LT_Eqls'String__STRING -> eval_'_LT_Eqls'String__STRING (normalize2 kl) config (-1)
|LblordChar -> evalordChar (normalize1 kl) config (-1)
|LblisStmt -> evalisStmt (normalize1 kl) config (-1)
|LblrfindChar -> evalrfindChar (normalize3 kl) config (-1)
|Lbl_modInt__INT -> eval_modInt__INT (normalize2 kl) config (-1)
|LblisIndexItem -> evalisIndexItem (normalize1 kl) config (-1)
|Lbl_Map_ -> eval_Map_ (normalize2 kl) config (-1)
|Lblproject'Coln'Stmts -> evalproject'Coln'Stmts (normalize1 kl) config (-1)
|Lbl_'Hyph'Int__INT -> eval_'Hyph'Int__INT (normalize2 kl) config (-1)
|Lbl'Hash'compareA -> eval'Hash'compareA (normalize2 kl) config (-1)
|LbldirectionalityChar -> evaldirectionalityChar (normalize1 kl) config (-1)
|LblFloat2String -> evalFloat2String (normalize1 kl) config (-1)
|Lbl'Hash'opendir'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'opendir'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|LblinitKCell -> evalinitKCell (normalize1 kl) config (-1)
|LblisValue -> evalisValue (normalize1 kl) config (-1)
|LblisDItem -> evalisDItem (normalize1 kl) config (-1)
|LblsizeList -> evalsizeList (normalize1 kl) config (-1)
|Lbl'Stop'Set -> eval'Stop'Set (normalize0 kl) config (-1)
|LblString2Id -> evalString2Id (normalize1 kl) config (-1)
|LblisStateCell -> evalisStateCell (normalize1 kl) config (-1)
|Lbl_'EqlsSlshEqls'Bool__BOOL -> eval_'EqlsSlshEqls'Bool__BOOL (normalize2 kl) config (-1)
|LblremoveAll -> evalremoveAll (normalize2 kl) config (-1)
|LblisTCell -> evalisTCell (normalize1 kl) config (-1)
|LblisFunDefCell -> evalisFunDefCell (normalize1 kl) config (-1)
|Lbl_andBool_ -> eval_andBool_ (normalize2 kl) config (-1)
|LblinitStackCell -> evalinitStackCell (normalize0 kl) config (-1)
|LbllengthString -> evallengthString (normalize1 kl) config (-1)
|LblisFnameCell -> evalisFnameCell (normalize1 kl) config (-1)
|LblinitTCell -> evalinitTCell (normalize1 kl) config (-1)
|LblFloatFormat -> evalFloatFormat (normalize2 kl) config (-1)
|Lbl_'Plus'String__STRING -> eval_'Plus'String__STRING (normalize2 kl) config (-1)
|Lbl_'Xor_Perc'Int___INT -> eval_'Xor_Perc'Int___INT (normalize3 kl) config (-1)
|Lbl_'Pipe'Int__INT -> eval_'Pipe'Int__INT (normalize2 kl) config (-1)
|Lbl_dividesInt__INT -> eval_dividesInt__INT (normalize2 kl) config (-1)
|LblinitIndexCell -> evalinitIndexCell (normalize0 kl) config (-1)
|LblisStateCellMap -> evalisStateCellMap (normalize1 kl) config (-1)
|LblisFunDefCellMap -> evalisFunDefCellMap (normalize1 kl) config (-1)
|LblrfindString -> evalrfindString (normalize3 kl) config (-1)
|Lbl'Hash'stat'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'stat'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|LblupdateList -> evalupdateList (normalize3 kl) config (-1)
|LblSet'Coln'choice -> evalSet'Coln'choice (normalize1 kl) config (-1)
|LblcategoryChar -> evalcategoryChar (normalize1 kl) config (-1)
|LblSet'Coln'difference -> evalSet'Coln'difference (normalize2 kl) config (-1)
|LblfreshInt -> evalfreshInt (normalize1 kl) config (-1)
|LblisKCellOpt -> evalisKCellOpt (normalize1 kl) config (-1)
|Lbl'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|LblisBlock -> evalisBlock (normalize1 kl) config (-1)
|LblisParameters -> evalisParameters (normalize1 kl) config (-1)
|Lbl_xorInt__INT -> eval_xorInt__INT (normalize2 kl) config (-1)
|LblMap'Coln'lookupOrDefault -> evalMap'Coln'lookupOrDefault (normalize3 kl) config (-1)
|LblString2Float -> evalString2Float (normalize1 kl) config (-1)
|Lbl'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL -> eval'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL (normalize3 kl) config (-1)
|LblinitStateCell -> evalinitStateCell (normalize1 kl) config (-1)
|Lbl'Hash'stdout_K'Hyph'IO -> eval'Hash'stdout_K'Hyph'IO (normalize0 kl) config (-1)
|Lbl_'And'Int__INT -> eval_'And'Int__INT (normalize2 kl) config (-1)
|Lbl'Hash'borrowimmck -> eval'Hash'borrowimmck (normalize5 kl) config (-1)
|Lbl_'EqlsSlshEqls'Int__INT -> eval_'EqlsSlshEqls'Int__INT (normalize2 kl) config (-1)
|Lbl_'_LT__LT_'Int__INT -> eval_'_LT__LT_'Int__INT (normalize2 kl) config (-1)
|Lbllog2Int -> evallog2Int (normalize1 kl) config (-1)
|Lbl'Hash'stdin_K'Hyph'IO -> eval'Hash'stdin_K'Hyph'IO (normalize0 kl) config (-1)
|LblBase2String -> evalBase2String (normalize2 kl) config (-1)
|LblListItem -> evalListItem (normalize1 kl) config (-1)
|LblisWriteCell -> evalisWriteCell (normalize1 kl) config (-1)
|LblisStream -> evalisStream (normalize1 kl) config (-1)
|Lbl_'_GT_Eqls'String__STRING -> eval_'_GT_Eqls'String__STRING (normalize2 kl) config (-1)
|Lbl'Stop'FunDefCellMap -> eval'Stop'FunDefCellMap (normalize0 kl) config (-1)
|Lbl_'_LT_Eqls'Map__MAP -> eval_'_LT_Eqls'Map__MAP (normalize2 kl) config (-1)
|LblnewUUID_STRING -> evalnewUUID_STRING (normalize0 kl) config (-1)
|LblsizeMap -> evalsizeMap (normalize1 kl) config (-1)
|LblisId -> evalisId (normalize1 kl) config (-1)
|LblsubstrString -> evalsubstrString (normalize3 kl) config (-1)
|Lblsize -> evalsize (normalize1 kl) config (-1)
|LblisIndexes -> evalisIndexes (normalize1 kl) config (-1)
|LblinitTimerCell -> evalinitTimerCell (normalize0 kl) config (-1)
|Lbl'Hash'unwrapVal -> eval'Hash'unwrapVal (normalize1 kl) config (-1)
|LblisFretCellOpt -> evalisFretCellOpt (normalize1 kl) config (-1)
|Lbl'Hash'existRef -> eval'Hash'existRef (normalize3 kl) config (-1)
|LblinitStatesCell -> evalinitStatesCell (normalize1 kl) config (-1)
|LblisWriteCellOpt -> evalisWriteCellOpt (normalize1 kl) config (-1)
|Lbl_inList_ -> eval_inList_ (normalize2 kl) config (-1)
|LblinitFunDefCell -> evalinitFunDefCell (normalize0 kl) config (-1)
|LblminInt'LPar'_'Comm'_'RPar'_INT -> evalminInt'LPar'_'Comm'_'RPar'_INT (normalize2 kl) config (-1)
|LblisMap -> evalisMap (normalize1 kl) config (-1)
|LblsrandInt -> evalsrandInt (normalize1 kl) config (-1)
|LblisKItem -> evalisKItem (normalize1 kl) config (-1)
|LblisStoreCell -> evalisStoreCell (normalize1 kl) config (-1)
|LblisStmts -> evalisStmts (normalize1 kl) config (-1)
|LblList'Coln'set -> evalList'Coln'set (normalize3 kl) config (-1)
|LblString2Base -> evalString2Base (normalize2 kl) config (-1)
|LblisTCellFragment -> evalisTCellFragment (normalize1 kl) config (-1)
|LblisStatesCellFragment -> evalisStatesCellFragment (normalize1 kl) config (-1)
|Lblreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING -> evalreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING (normalize4 kl) config (-1)
|LblisFbodyCell -> evalisFbodyCell (normalize1 kl) config (-1)
|Lbl_'Slsh'Int__INT -> eval_'Slsh'Int__INT (normalize2 kl) config (-1)
|Lbl_'LSqB'_'_LT_Hyph'_'RSqB'_MAP -> eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP (normalize3 kl) config (-1)
|Lbl'Hash'tell'LPar'_'RPar'_K'Hyph'IO -> eval'Hash'tell'LPar'_'RPar'_K'Hyph'IO (normalize1 kl) config (-1)
|Lblkeys -> evalkeys (normalize1 kl) config (-1)
|LblisLifetime -> evalisLifetime (normalize1 kl) config (-1)
|LblisParameter -> evalisParameter (normalize1 kl) config (-1)
|LblisFparamsCell -> evalisFparamsCell (normalize1 kl) config (-1)
|LblgetKLabel -> evalgetKLabel (normalize1 kl) config (-1)
|LblisEnvCellOpt -> evalisEnvCellOpt (normalize1 kl) config (-1)
|LblisExp -> evalisExp (normalize1 kl) config (-1)
|Lbl'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO -> eval'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO (normalize2 kl) config (-1)
|LblisLOCK -> evalisLOCK (normalize1 kl) config (-1)
|LblisFretCell -> evalisFretCell (normalize1 kl) config (-1)
| _ -> [denormalize c])
| _ -> [denormalize c]
let rec get_next_op_from_exp(c: kitem) : (k -> k * (step_function)) = step
and step (c:k) : k * step_function =
 try let config = c in match c with 
| _ -> lookups_step c c (-1)
with Sys.Break -> raise (Stuck c)
and lookups_step (c: k) (config: k) (guard: int) : k * step_function = match c with 
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_0_2665)) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: []) when guard < 74 -> (match (var_0_2665) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> let rec stepElt = fun guard -> if result == interned_bottom then (match e with (*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#expStmt(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#expStmt0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(a8f1fc103e86f774e188b8c29afb4d411bcdfe291538cd0fa9b073f1b4156162) cool() klabel(#expStmt) org.kframework.attributes.Location(Location(50,12,50,73)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl-syntax.k)) productionID(970519188) strict()]|}*)
| (var_4_2669) as e22 when guard < 0 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e22) config (-1))) in match e with 
| [Bottom] -> (stepElt 0)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2670),(KApply1(Lbl'_LT_'k'_GT_',(var_5_2671 :: KApply0(Lbl'Hash'freezer'Hash'expStmt0_) :: var_6_2672)) :: []),(var_7_2673),(var_8_2674),(var_9_2675),(var_10_2676),(var_11_2677),(var_12_2678)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e22) config (-1))) in match e with 
| [Bottom] -> (stepElt 0)
| ((Map (SortStateCellMap,_,_) as var_13_2679) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_2671 :: [])) config (-1)))) && (((true) && ((isTrue (evalisKResult((var_5_2671 :: [])) config (-1))))))))) && (((compare var_4_2670 var_4_2669) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e22,(KApply8(Lbl'_LT_'state'_GT_',e22,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'expStmt,(var_5_2671 :: [])) :: var_6_2672)) :: []),(var_7_2673),(var_8_2674),(var_9_2675),(var_10_2676),(var_11_2677),(var_12_2678)) :: [])) config (-1))),(var_13_2679 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 0))| _ -> (stepElt 0))
(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,_5,`<indexes>`(#indexes(`_+Int_`(C,#token("1","Int")),_56)))),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(`#increaseIndex_OSL`(.KList)~>DotVar3),_1,_2,_3,_4,_5,`<indexes>`(#indexes(C,_56))),`Map:lookup`(_9,_0))),#match(DotVar1,`_[_<-undef]`(_9,_0))),`_andBool_`(isInt(_56),isInt(C))) ensures #token("true","Bool") [UNIQUE_ID(5d5b5ca93c74d7556f2f90904f3761f73bed9c75b0b1edcfbe087511bfb4ecc4) contentStartColumn(6) contentStartLine(75) org.kframework.attributes.Location(Location(75,6,76,63)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2680) as e23 when guard < 1 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e23) config (-1))) in match e with 
| [Bottom] -> (stepElt 1)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2681),(KApply1(Lbl'_LT_'k'_GT_',(KApply0(Lbl'Hash'increaseIndex_OSL) :: var_5_2682)) :: []),(var_6_2683),(var_7_2684),(var_8_2685),(var_9_2686),(var_10_2687),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_11_2688) :: []),((Int _ as var_12_2689) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e23) config (-1))) in match e with 
| [Bottom] -> (stepElt 1)
| ((Map (SortStateCellMap,_,_) as var_13_2690) :: []) when ((((((true) && (true))) && (true))) && (((true) && (true)))) && (((compare var_4_2681 var_4_2680) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e23,(KApply8(Lbl'_LT_'state'_GT_',e23,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2682)) :: []),(var_6_2683),(var_7_2684),(var_8_2685),(var_9_2686),(var_10_2687),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((eval_'Plus'Int_((var_11_2688 :: []),((Lazy.force int1) :: [])) config (-1))),(var_12_2689 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_13_2690 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 1))| _ -> (stepElt 1))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#TransferMB(HOLE,K1)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#TransferMB1_`(K1)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isK(HOLE),isK(K1)),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(f569cc11096e1a41a772e200c458db0be31c73282d8ad8671170d92595f0afce) cool() klabel(#TransferMB) org.kframework.attributes.Location(Location(139,12,139,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1209581847) strict(1)]|}*)
| (var_4_2691) as e24 when guard < 2 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e24) config (-1))) in match e with 
| [Bottom] -> (stepElt 2)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2692),(KApply1(Lbl'_LT_'k'_GT_',(var_5_2693 :: KApply1(Lbl'Hash'freezer'Hash'TransferMB1_,(var_6_2694)) :: var_7_2695)) :: []),(var_8_2696),(var_9_2697),(var_10_2698),(var_11_2699),(var_12_2700),(var_13_2701)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e24) config (-1))) in match e with 
| [Bottom] -> (stepElt 2)
| ((Map (SortStateCellMap,_,_) as var_14_2702) :: []) when ((((((true) && (true))) && (true))) && (((((true) && (true))) && (((true) && ((isTrue (evalisKResult((var_5_2693 :: [])) config (-1))))))))) && (((compare var_4_2692 var_4_2691) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e24,(KApply8(Lbl'_LT_'state'_GT_',e24,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferMB,(var_5_2693 :: []),(var_6_2694)) :: var_7_2695)) :: []),(var_8_2696),(var_9_2697),(var_10_2698),(var_11_2699),(var_12_2700),(var_13_2701)) :: [])) config (-1))),(var_14_2702 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 2))| _ -> (stepElt 2))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),`<env>`(ENV),_1,`<stack>`(`_List_`(`.List`(.KList),DotVar4)),_2,_3,_4)),DotVar1)``),_5,_6,_7) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(`#blockend_BLOCK`(.KList)~>DotVar3),`<env>`(_44),_1,`<stack>`(`_List_`(`ListItem`(ENV),DotVar4)),_2,_3,_4),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),`_andBool_`(isMap(ENV),isMap(_44))) ensures #token("true","Bool") [UNIQUE_ID(92ee3b050c6ad4fbc3d7435b457530c7094e752b25462deb01e9f42cc9603edb) contentStartColumn(6) contentStartLine(24) org.kframework.attributes.Location(Location(24,6,26,55)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/block.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2703) as e25 when guard < 3 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e25) config (-1))) in match e with 
| [Bottom] -> (stepElt 3)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2704),(KApply1(Lbl'_LT_'k'_GT_',(KApply0(Lbl'Hash'blockend_BLOCK) :: var_5_2705)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_13_2706) :: [])) :: []),(var_7_2707),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList, Lbl_List_, ((Map (SortMap,_,_) as var_6_2708) :: []) :: var_8_2709)) :: [])) :: []),(var_9_2710),(var_10_2711),(var_11_2712)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e25) config (-1))) in match e with 
| [Bottom] -> (stepElt 3)
| ((Map (SortStateCellMap,_,_) as var_12_2713) :: []) when ((((((true) && (true))) && (true))) && (((true) && (true)))) && (((compare var_4_2704 var_4_2703) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e25,(KApply8(Lbl'_LT_'state'_GT_',e25,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2705)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_2708 :: [])) :: []),(var_7_2707),(KApply1(Lbl'_LT_'stack'_GT_',((eval_List_(((Lazy.force const'Stop'List)),((List (SortList, Lbl_List_, var_8_2709)) :: [])) config (-1)))) :: []),(var_9_2710),(var_10_2711),(var_11_2712)) :: [])) config (-1))),(var_12_2713 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 3))| _ -> (stepElt 3))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#Read0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Read(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isK(HOLE),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(043dcdc69e66d003dd62a59472edd4f2d0b41eb5bb133676e0bcbc0637d450fa) heat() klabel(#Read) org.kframework.attributes.Location(Location(176,12,176,40)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(2042399335) strict()]|}*)
| (var_4_2714) as e26 when guard < 4 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e26) config (-1))) in match e with 
| [Bottom] -> (stepElt 4)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2715),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(var_5_2716)) :: var_6_2717)) :: []),(var_7_2718),(var_8_2719),(var_9_2720),(var_10_2721),(var_11_2722),(var_12_2723)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e26) config (-1))) in match e with 
| [Bottom] -> (stepElt 4)
| ((Map (SortStateCellMap,_,_) as var_13_2724) :: []) when ((((((true) && (true))) && (true))) && (((true) && (((true) && ((not ((isTrue (evalisKResult((var_5_2716)) config (-1))))))))))) && (((compare var_4_2715 var_4_2714) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e26,(KApply8(Lbl'_LT_'state'_GT_',e26,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2716 @ const'Hash'freezer'Hash'Read0_ :: var_6_2717)) :: []),(var_7_2718),(var_8_2719),(var_9_2720),(var_10_2721),(var_11_2722),(var_12_2723)) :: [])) config (-1))),(var_13_2724 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 4))| _ -> (stepElt 4))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezerval0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(val(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(7fb72add0bfa65f44b945087907a6bc503589f93d093dacff150b549766b011f) heat() klabel(val) org.kframework.attributes.Location(Location(51,12,51,56)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl-syntax.k)) productionID(359558578) strict()]|}*)
| (var_4_2725) as e27 when guard < 5 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e27) config (-1))) in match e with 
| [Bottom] -> (stepElt 5)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2726),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lblval,(var_5_2727 :: [])) :: var_6_2728)) :: []),(var_7_2729),(var_8_2730),(var_9_2731),(var_10_2732),(var_11_2733),(var_12_2734)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e27) config (-1))) in match e with 
| [Bottom] -> (stepElt 5)
| ((Map (SortStateCellMap,_,_) as var_13_2735) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_2727 :: [])) config (-1)))) && (((true) && ((not ((isTrue (evalisKResult((var_5_2727 :: [])) config (-1))))))))))) && (((compare var_4_2726 var_4_2725) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e27,(KApply8(Lbl'_LT_'state'_GT_',e27,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2727 :: const'Hash'freezerval0_ :: var_6_2728)) :: []),(var_7_2729),(var_8_2730),(var_9_2731),(var_10_2732),(var_11_2733),(var_12_2734)) :: [])) config (-1))),(var_13_2735 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 5))| _ -> (stepElt 5))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Deallocate(#lv(E))~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#deallocate(E)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isExp(E)) ensures #token("true","Bool") [UNIQUE_ID(32224f8bc83373e52f56131e94ba2cfd7ce7e6fce1c13aaeab45f3e4e5fec5bf) contentStartColumn(6) contentStartLine(368) org.kframework.attributes.Location(Location(368,6,368,47)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2736) as e28 when guard < 6 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e28) config (-1))) in match e with 
| [Bottom] -> (stepElt 6)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2737),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'deallocate,(var_5_2738 :: [])) :: var_6_2739)) :: []),(var_7_2740),(var_8_2741),(var_9_2742),(var_10_2743),(var_11_2744),(var_12_2745)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e28) config (-1))) in match e with 
| [Bottom] -> (stepElt 6)
| ((Map (SortStateCellMap,_,_) as var_13_2746) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisExp((var_5_2738 :: [])) config (-1))))) && (((compare var_4_2737 var_4_2736) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e28,(KApply8(Lbl'_LT_'state'_GT_',e28,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Deallocate,(KApply1(Lbl'Hash'lv,(var_5_2738 :: [])) :: [])) :: var_6_2739)) :: []),(var_7_2740),(var_8_2741),(var_9_2742),(var_10_2743),(var_11_2744),(var_12_2745)) :: [])) config (-1))),(var_13_2746 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 6))| _ -> (stepElt 6))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Read(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#Read0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isK(HOLE),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(36df34e3632848a8cb874a29eb37c134af30f69f7d949ca219829fbf286ab2ae) cool() klabel(#Read) org.kframework.attributes.Location(Location(176,12,176,40)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(2042399335) strict()]|}*)
| (var_4_2747) as e29 when guard < 7 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e29) config (-1))) in match e with 
| [Bottom] -> (stepElt 7)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2748),(KApply1(Lbl'_LT_'k'_GT_',(var_5_2749 :: KApply0(Lbl'Hash'freezer'Hash'Read0_) :: var_6_2750)) :: []),(var_7_2751),(var_8_2752),(var_9_2753),(var_10_2754),(var_11_2755),(var_12_2756)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e29) config (-1))) in match e with 
| [Bottom] -> (stepElt 7)
| ((Map (SortStateCellMap,_,_) as var_13_2757) :: []) when ((((((true) && (true))) && (true))) && (((true) && (((true) && ((isTrue (evalisKResult((var_5_2749 :: [])) config (-1))))))))) && (((compare var_4_2748 var_4_2747) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e29,(KApply8(Lbl'_LT_'state'_GT_',e29,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(var_5_2749 :: [])) :: var_6_2750)) :: []),(var_7_2751),(var_8_2752),(var_9_2753),(var_10_2754),(var_11_2755),(var_12_2756)) :: [])) config (-1))),(var_13_2757 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 7))| _ -> (stepElt 7))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(`#removeState_CONTROL`(.KList)),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),_1,_2,_3)),`StateCellMapItem`(`<index>`(N),`<state>`(`<index>`(N),`<k>`(Pro),`<env>`(ENV1),`<store>`(STORE1),`<stack>`(STACK1),_4,_5,_6))),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#loopSep(N)~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),_1,_2,_3),`Map:lookup`(_10,_0))),#match(`<state>`(`<index>`(N),`<k>`(Pro),`<env>`(ENV1),`<store>`(STORE1),`<stack>`(STACK1),_4,_5,_6),`Map:lookup`(_10,`<index>`(N)))),#match(DotVar1,`_[_<-undef]`(`_[_<-undef]`(_10,_0),`<index>`(N)))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isMap(ENV1),isMap(ENV)),isInt(N)),isK(Rest)),isMap(STORE)),isK(Pro)),isList(STACK)),isList(STACK1)),isMap(STORE1)),`_andBool_`(`_==K_`(Pro,#compare(Rest)),#compareS(STORE,STORE1)))) ensures #token("true","Bool") [UNIQUE_ID(dcbde4804b469daa0fe01b15d7c401ae3f491a6aa8f44e3621e29754c61c0361) contentStartColumn(6) contentStartLine(88) org.kframework.attributes.Location(Location(88,6,103,72)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_2758) as e30 when guard < 8 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e30) config (-1))) in match e with 
| [Bottom] -> (stepElt 8)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2759),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loopSep,((Int _ as var_11_2760) :: [])) :: var_20_2761)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_5_2762) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_6_2763) :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_7_2764) :: [])) :: []),(var_8_2765),(var_9_2766),(var_10_2767)) :: []) -> (let e = ((evalMap'Coln'lookup((var_0_2665),(KApply1(Lbl'_LT_'index'_GT_',(var_11_2760 :: [])) :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 8)
| (KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',((Int _ as var_11_2768) :: [])) :: []),(KApply1(Lbl'_LT_'k'_GT_',(var_12_2769)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_13_2770) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_14_2771) :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_15_2772) :: [])) :: []),(var_16_2773),(var_17_2774),(var_18_2775)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'(((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e30) config (-1))),(KApply1(Lbl'_LT_'index'_GT_',(var_11_2760 :: [])) :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 8)
| ((Map (SortStateCellMap,_,_) as var_19_2776) :: []) when ((((((((true) && (true))) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsEqls'K_((var_12_2769),(KApply1(Lbl'Hash'compare,(var_20_2761)) :: [])) config (-1)))) && ((isTrue (eval'Hash'compareS((var_6_2763 :: []),(var_14_2771 :: [])) config (-1))))))))) && (((compare var_4_2759 var_4_2758) = 0) && ((compare_kitem var_11_2760 var_11_2768) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((eval_StateCellMap_(((evalStateCellMapItem(e30,(KApply8(Lbl'_LT_'state'_GT_',e30,(KApply1(Lbl'_LT_'k'_GT_',(const'Hash'removeState_CONTROL :: [])) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_5_2762 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_6_2763 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_7_2764 :: [])) :: []),(var_8_2765),(var_9_2766),(var_10_2767)) :: [])) config (-1))),((evalStateCellMapItem((KApply1(Lbl'_LT_'index'_GT_',(var_11_2760 :: [])) :: []),(KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',(var_11_2760 :: [])) :: []),(KApply1(Lbl'_LT_'k'_GT_',(var_12_2769)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_13_2770 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_14_2771 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_15_2772 :: [])) :: []),(var_16_2773),(var_17_2774),(var_18_2775)) :: [])) config (-1)))) config (-1))),(var_19_2776 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 8))| _ -> (stepElt 8))| _ -> (stepElt 8))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#Deallocate0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Deallocate(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(527ef7144735dca2f7b4866cf493ea2a875b223f5a36a7a42bee2968a3b0781b) heat() klabel(#Deallocate) org.kframework.attributes.Location(Location(365,12,365,48)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(690844188) strict()]|}*)
| (var_4_2777) as e31 when guard < 9 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e31) config (-1))) in match e with 
| [Bottom] -> (stepElt 9)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2778),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Deallocate,(var_5_2779 :: [])) :: var_6_2780)) :: []),(var_7_2781),(var_8_2782),(var_9_2783),(var_10_2784),(var_11_2785),(var_12_2786)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e31) config (-1))) in match e with 
| [Bottom] -> (stepElt 9)
| ((Map (SortStateCellMap,_,_) as var_13_2787) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_2779 :: [])) config (-1)))) && (((true) && ((not ((isTrue (evalisKResult((var_5_2779 :: [])) config (-1))))))))))) && (((compare var_4_2777 var_4_2778) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e31,(KApply8(Lbl'_LT_'state'_GT_',e31,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2779 :: const'Hash'freezer'Hash'Deallocate0_ :: var_6_2780)) :: []),(var_7_2781),(var_8_2782),(var_9_2783),(var_10_2784),(var_11_2785),(var_12_2786)) :: [])) config (-1))),(var_13_2787 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 9))| _ -> (stepElt 9))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transfer(#rs(PS),#loc(L))~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(F,#rs(PS)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#TransferV(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,F))),#match(#rs(PS),`Map:lookup`(_9,F))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isProps(PS),isInt(F)),isInt(L)),#inProps(`copy_OSL-SYNTAX`(.KList),PS))) ensures #token("true","Bool") [UNIQUE_ID(ca9ef1992cf8a8bc5dae872816a2b37cd4afdd6c59f9065e5c73c73e42ae40f8) contentStartColumn(6) contentStartLine(133) org.kframework.attributes.Location(Location(133,6,135,33)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_2788) as e32 when guard < 10 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e32) config (-1))) in match e with 
| [Bottom] -> (stepElt 10)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2789),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferV,(KApply1(Lbl'Hash'loc,((Int _ as var_9_2790) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_6_2791) :: [])) :: [])) :: var_7_2792)) :: []),(var_8_2793),(KApply1(Lbl'_LT_'store'_GT_',(var_16_2794)) :: []),(var_11_2795),(var_12_2796),(var_13_2797),(var_14_2798)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_16_2794),(var_9_2790 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 10)
| ((Map (SortMap,_,_) as var_10_2799) :: []) -> (let e = ((evalMap'Coln'lookup((var_16_2794),(var_9_2790 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 10)
| (KApply1(Lbl'Hash'rs,(var_5_2800 :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e32) config (-1))) in match e with 
| [Bottom] -> (stepElt 10)
| ((Map (SortStateCellMap,_,_) as var_15_2801) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((((((isTrue (evalisProps((var_5_2800 :: [])) config (-1)))) && (true))) && (true))) && ((isTrue (eval'Hash'inProps((constcopy_OSL'Hyph'SYNTAX :: []),(var_5_2800 :: [])) config (-1))))))) && (((compare var_4_2788 var_4_2789) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e32,(KApply8(Lbl'_LT_'state'_GT_',e32,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(KApply1(Lbl'Hash'rs,(var_5_2800 :: [])) :: []),(KApply1(Lbl'Hash'loc,(var_6_2791 :: [])) :: [])) :: var_7_2792)) :: []),(var_8_2793),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_9_2790 :: []),(KApply1(Lbl'Hash'rs,(var_5_2800 :: [])) :: [])) config (-1))),(var_10_2799 :: [])) config (-1)))) :: []),(var_11_2795),(var_12_2796),(var_13_2797),(var_14_2798)) :: [])) config (-1))),(var_15_2801 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 10))| _ -> (stepElt 10))| _ -> (stepElt 10))| _ -> (stepElt 10))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transferuninit(`#uninit_OSL-SYNTAX`(.KList),#lv(TX))~>`#increaseIndex_OSL`(.KList)~>`#increaseTimer_OSL`(.KList)~>DotVar3),`<env>`(ENV),_1,_2,`<write>`(`_Set_`(`SetItem`(#writev(#wv(TX,ENV),TR)),DotVar4)),`<timer>`(TR),_3)),DotVar1)``),_4,_5,_6) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(`transfer__;_OSL-SYNTAX`(`#uninit_OSL-SYNTAX`(.KList),TX)~>DotVar3),`<env>`(ENV),_1,_2,`<write>`(_7),`<timer>`(TR),_3),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),#match(DotVar4,_7)),`_andBool_`(`_andBool_`(isMap(ENV),isExp(TX)),isInt(TR))) ensures #token("true","Bool") [UNIQUE_ID(a336f116919c1d481e47ada9fc303ec7f20f3006f8bac15059396482f7d26c32) contentStartColumn(6) contentStartLine(50) org.kframework.attributes.Location(Location(50,6,54,68)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2802) as e33 when guard < 11 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e33) config (-1))) in match e with 
| [Bottom] -> (stepElt 11)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2803),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbltransfer__'SCln'_OSL'Hyph'SYNTAX,(KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []),(var_5_2804 :: [])) :: var_6_2805)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_7_2806) :: [])) :: []),(var_8_2807),(var_9_2808),(KApply1(Lbl'_LT_'write'_GT_',(var_14_2809)) :: []),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_10_2810) :: [])) :: []),(var_12_2811)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e33) config (-1))) in match e with 
| [Bottom] -> (stepElt 11)
| ((Map (SortStateCellMap,_,_) as var_13_2812) :: []) -> (let e = (var_14_2809) in match e with 
| [Bottom] -> (stepElt 11)
| ((Set (SortSet,_,_) as var_11_2813) :: []) when ((((((((true) && (true))) && (true))) && (true))) && (((((true) && ((isTrue (evalisExp((var_5_2804 :: [])) config (-1)))))) && (true)))) && (((compare var_4_2803 var_4_2802) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e33,(KApply8(Lbl'_LT_'state'_GT_',e33,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transferuninit,(const'Hash'uninit_OSL'Hyph'SYNTAX :: []),(KApply1(Lbl'Hash'lv,(var_5_2804 :: [])) :: [])) :: const'Hash'increaseIndex_OSL :: const'Hash'increaseTimer_OSL :: var_6_2805)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_7_2806 :: [])) :: []),(var_8_2807),(var_9_2808),(KApply1(Lbl'_LT_'write'_GT_',((eval_Set_(((evalSetItem((KApply2(Lbl'Hash'writev,((eval'Hash'wv((var_5_2804 :: []),(var_7_2806 :: [])) config (-1))),(var_10_2810 :: [])) :: [])) config (-1))),(var_11_2813 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_10_2810 :: [])) :: []),(var_12_2811)) :: [])) config (-1))),(var_13_2812 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 11))| _ -> (stepElt 11))| _ -> (stepElt 11))
(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(`#blockend_BLOCK`(.KList)~>DotVar3),`<env>`(`_Map_`(`_|->_`(X,L),DotVar4)),`<store>`(`_Map_`(`.Map`(.KList),DotVar5)),`<stack>`(`_List_`(`.List`(.KList),DotVar6)),_1,_2,_3)),DotVar1)``),_4,_5,_6) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(`#blockend_BLOCK`(.KList)~>DotVar3),`<env>`(_7),`<store>`(_8),`<stack>`(`_List_`(`ListItem`(X),DotVar6)),_1,_2,_3),`Map:lookup`(_9,_0))),#match(L,`Map:lookup`(_7,X))),#match(DotVar4,`_[_<-undef]`(_7,X))),#match(DotVar1,`_[_<-undef]`(_9,_0))),#match(DotVar5,`_[_<-undef]`(_8,L))),#match(_43,`Map:lookup`(_8,L))),`_andBool_`(`_andBool_`(isId(X),isInt(L)),isKItem(_43))) ensures #token("true","Bool") [UNIQUE_ID(c916ca0be35c2a1887a20de2601199b95e51cdcee7c90454bc061d6411f84245) contentStartColumn(6) contentStartLine(19) org.kframework.attributes.Location(Location(19,6,22,52)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/block.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2814) as e34 when guard < 12 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e34) config (-1))) in match e with 
| [Bottom] -> (stepElt 12)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2815),(KApply1(Lbl'_LT_'k'_GT_',(KApply0(Lbl'Hash'blockend_BLOCK) :: var_5_2816)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_15_2817)) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_16_2818)) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList, Lbl_List_, (var_6_2819 :: []) :: var_10_2820)) :: [])) :: []),(var_11_2821),(var_12_2822),(var_13_2823)) :: []) -> (let e = ((evalMap'Coln'lookup((var_15_2817),(var_6_2819 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 12)
| ((Int _ as var_7_2824) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_15_2817),(var_6_2819 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 12)
| ((Map (SortMap,_,_) as var_8_2825) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e34) config (-1))) in match e with 
| [Bottom] -> (stepElt 12)
| ((Map (SortStateCellMap,_,_) as var_14_2826) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_16_2818),(var_7_2824 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 12)
| ((Map (SortMap,_,_) as var_9_2827) :: []) -> (let e = ((evalMap'Coln'lookup((var_16_2818),(var_7_2824 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 12)
| (var_17_2828 :: []) when ((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((((isTrue (evalisId((var_6_2819 :: [])) config (-1)))) && (true))) && (true)))) && (((compare var_4_2814 var_4_2815) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e34,(KApply8(Lbl'_LT_'state'_GT_',e34,(KApply1(Lbl'_LT_'k'_GT_',(const'Hash'blockend_BLOCK :: var_5_2816)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_6_2819 :: []),(var_7_2824 :: [])) config (-1))),(var_8_2825 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((Lazy.force const'Stop'Map)),(var_9_2827 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((eval_List_(((Lazy.force const'Stop'List)),((List (SortList, Lbl_List_, var_10_2820)) :: [])) config (-1)))) :: []),(var_11_2821),(var_12_2822),(var_13_2823)) :: [])) config (-1))),(var_14_2826 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 12))| _ -> (stepElt 12))| _ -> (stepElt 12))| _ -> (stepElt 12))| _ -> (stepElt 12))| _ -> (stepElt 12))
(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#br(C,C,#immRef(F))),DotVar4)),_2,_3,_4,`<indexes>`(#indexes(C,_71)))),DotVar1)``),_5,_6,_7) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(#TransferIB(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(_8),_2,_3,_4,`<indexes>`(#indexes(C,_71))),`Map:lookup`(_9,_0))),#match(DotVar4,`_[_<-undef]`(_8,L))),#match(_70,`Map:lookup`(_8,L))),#match(DotVar1,`_[_<-undef]`(_9,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(C),isInt(_71)),isInt(F)),isInt(L)),isKItem(_70))) ensures #token("true","Bool") [UNIQUE_ID(b1ffcff9e488c340a35a6982565b2fffa44e6dbf3ccd6704fe0c26fb25f90a3b) contentStartColumn(6) contentStartLine(150) org.kframework.attributes.Location(Location(150,6,152,49)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2829) as e35 when guard < 13 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e35) config (-1))) in match e with 
| [Bottom] -> (stepElt 13)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2830),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferIB,(KApply1(Lbl'Hash'loc,((Int _ as var_9_2831) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_7_2832) :: [])) :: [])) :: var_5_2833)) :: []),(var_6_2834),(KApply1(Lbl'_LT_'store'_GT_',(var_16_2835)) :: []),(var_11_2836),(var_12_2837),(var_13_2838),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_8_2839) :: []),((Int _ as var_14_2840) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_16_2835),(var_7_2832 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 13)
| ((Map (SortMap,_,_) as var_10_2841) :: []) -> (let e = ((evalMap'Coln'lookup((var_16_2835),(var_7_2832 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 13)
| (var_17_2842 :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e35) config (-1))) in match e with 
| [Bottom] -> (stepElt 13)
| ((Map (SortStateCellMap,_,_) as var_15_2843) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((((true) && (true))) && (true))) && (true))) && (true)))) && (((compare var_4_2829 var_4_2830) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e35,(KApply8(Lbl'_LT_'state'_GT_',e35,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2833)) :: []),(var_6_2834),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_2832 :: []),(KApply3(Lbl'Hash'br,(var_8_2839 :: []),(var_8_2839 :: []),(KApply1(Lbl'Hash'immRef,(var_9_2831 :: [])) :: [])) :: [])) config (-1))),(var_10_2841 :: [])) config (-1)))) :: []),(var_11_2836),(var_12_2837),(var_13_2838),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_8_2839 :: []),(var_14_2840 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_15_2843 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 13))| _ -> (stepElt 13))| _ -> (stepElt 13))| _ -> (stepElt 13))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#TransferV(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(STORE),_2,_3,_4,`<indexes>`(#indexes(C,_69)))),DotVar1)``),_5,_6,_7) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(#Transfer(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(STORE),_2,_3,_4,`<indexes>`(#indexes(C,_69))),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(C),isInt(_69)),isInt(F)),isMap(STORE)),isInt(L)),`_andBool_`(`notBool_`(#existRef(#immRef(F),#list2Set(values(STORE)),C)),`notBool_`(#existRef(#mutRef(F),#list2Set(values(STORE)),C))))) ensures #token("true","Bool") [UNIQUE_ID(bea2b5111edbe602db9e0577ab63a7db98575b5239631462c9ba94f097c8f2ea) contentStartColumn(6) contentStartLine(115) org.kframework.attributes.Location(Location(115,6,119,67)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_2844) as e36 when guard < 14 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e36) config (-1))) in match e with 
| [Bottom] -> (stepElt 14)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2845),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(KApply1(Lbl'Hash'loc,((Int _ as var_5_2846) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_6_2847) :: [])) :: [])) :: var_7_2848)) :: []),(var_8_2849),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_9_2850) :: [])) :: []),(var_10_2851),(var_11_2852),(var_12_2853),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_13_2854) :: []),((Int _ as var_14_2855) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e36) config (-1))) in match e with 
| [Bottom] -> (stepElt 14)
| ((Map (SortStateCellMap,_,_) as var_15_2856) :: []) when ((((((true) && (true))) && (true))) && (((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((not ((isTrue (eval'Hash'existRef((KApply1(Lbl'Hash'immRef,(var_5_2846 :: [])) :: []),((eval'Hash'list2Set(((evalvalues((var_9_2850 :: [])) config (-1)))) config (-1))),(var_13_2854 :: [])) config (-1)))))) && ((not ((isTrue (eval'Hash'existRef((KApply1(Lbl'Hash'mutRef,(var_5_2846 :: [])) :: []),((eval'Hash'list2Set(((evalvalues((var_9_2850 :: [])) config (-1)))) config (-1))),(var_13_2854 :: [])) config (-1))))))))))) && (((compare var_4_2844 var_4_2845) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e36,(KApply8(Lbl'_LT_'state'_GT_',e36,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferV,(KApply1(Lbl'Hash'loc,(var_5_2846 :: [])) :: []),(KApply1(Lbl'Hash'loc,(var_6_2847 :: [])) :: [])) :: var_7_2848)) :: []),(var_8_2849),(KApply1(Lbl'_LT_'store'_GT_',(var_9_2850 :: [])) :: []),(var_10_2851),(var_11_2852),(var_12_2853),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_13_2854 :: []),(var_14_2855 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_15_2856 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 14))| _ -> (stepElt 14))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transferuninit(K0,HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#Transferuninit0_`(K0)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isExp(HOLE),isK(K0)),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(f24f15bf129541fe19096e1c2f8cf608f3c3db61efae7d1fe75d79e06f123796) cool() klabel(#Transferuninit) org.kframework.attributes.Location(Location(37,12,37,46)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(975956144) strict(2)]|}*)
| (var_4_2857) as e37 when guard < 15 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e37) config (-1))) in match e with 
| [Bottom] -> (stepElt 15)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2858),(KApply1(Lbl'_LT_'k'_GT_',(var_6_2859 :: KApply1(Lbl'Hash'freezer'Hash'Transferuninit0_,(var_5_2860)) :: var_7_2861)) :: []),(var_8_2862),(var_9_2863),(var_10_2864),(var_11_2865),(var_12_2866),(var_13_2867)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e37) config (-1))) in match e with 
| [Bottom] -> (stepElt 15)
| ((Map (SortStateCellMap,_,_) as var_14_2868) :: []) when ((((((true) && (true))) && (true))) && ((((((isTrue (evalisExp((var_6_2859 :: [])) config (-1)))) && (true))) && (((true) && ((isTrue (evalisKResult((var_6_2859 :: [])) config (-1))))))))) && (((compare var_4_2858 var_4_2857) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e37,(KApply8(Lbl'_LT_'state'_GT_',e37,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transferuninit,(var_5_2860),(var_6_2859 :: [])) :: var_7_2861)) :: []),(var_8_2862),(var_9_2863),(var_10_2864),(var_11_2865),(var_12_2866),(var_13_2867)) :: [])) config (-1))),(var_14_2868 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 15))| _ -> (stepElt 15))
(*{| rule `<T>`(`<states>`(``_6=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#loc(L1)~>DotVar3),_1,`<store>`(`_[_<-_]_MAP`(Rho,L,#br(BEG,TIMER,#immRef(L1)))),_2,`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(C,_45)))),DotVar1)``),_3,_4,_5) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_6),#match(`<state>`(_0,`<k>`(#borrowImmCK(L,BEG,END,L1)~>DotVar3),_1,`<store>`(Rho),_2,`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(C,_45))),`Map:lookup`(_6,_0))),#match(DotVar1,`_[_<-undef]`(_6,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(TIMER),isInt(END)),isMap(Rho)),isInt(C)),isSet(WRITE)),isInt(L)),isInt(_45)),isInt(L1)),isInt(BEG)),`_andBool_`(`_==Bool__BOOL`(#borrowimmck(L,Rho,BEG,TIMER,L1),#token("false","Bool")),#writeCK(L1,BEG,TIMER,WRITE)))) ensures #token("true","Bool") [UNIQUE_ID(774bc0f3dbb74bc132de7b2b715d70c72f463e973ee8010c8c5419db24152445) contentStartColumn(6) contentStartLine(202) org.kframework.attributes.Location(Location(202,6,208,45)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_2869) as e38 when guard < 16 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e38) config (-1))) in match e with 
| [Bottom] -> (stepElt 16)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2870),(KApply1(Lbl'_LT_'k'_GT_',(KApply4(Lbl'Hash'borrowImmCK,((Int _ as var_9_2871) :: []),((Int _ as var_10_2872) :: []),((Int _ as var_17_2873) :: []),((Int _ as var_5_2874) :: [])) :: var_6_2875)) :: []),(var_7_2876),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_8_2877) :: [])) :: []),(var_12_2878),(KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as var_13_2879) :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_11_2880) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_14_2881) :: []),((Int _ as var_15_2882) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e38) config (-1))) in match e with 
| [Bottom] -> (stepElt 16)
| ((Map (SortStateCellMap,_,_) as var_16_2883) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'borrowimmck((var_9_2871 :: []),(var_8_2877 :: []),(var_10_2872 :: []),(var_11_2880 :: []),(var_5_2874 :: [])) config (-1))),((Bool false) :: [])) config (-1)))) && ((isTrue (eval'Hash'writeCK((var_5_2874 :: []),(var_10_2872 :: []),(var_11_2880 :: []),(var_13_2879 :: [])) config (-1))))))))) && (((compare var_4_2869 var_4_2870) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e38,(KApply8(Lbl'_LT_'state'_GT_',e38,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loc,(var_5_2874 :: [])) :: var_6_2875)) :: []),(var_7_2876),(KApply1(Lbl'_LT_'store'_GT_',((eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP((var_8_2877 :: []),(var_9_2871 :: []),(KApply3(Lbl'Hash'br,(var_10_2872 :: []),(var_11_2880 :: []),(KApply1(Lbl'Hash'immRef,(var_5_2874 :: [])) :: [])) :: [])) config (-1)))) :: []),(var_12_2878),(KApply1(Lbl'_LT_'write'_GT_',(var_13_2879 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_11_2880 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_14_2881 :: []),(var_15_2882 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_16_2883 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 16))| _ -> (stepElt 16))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#loc(I)~>DotVar3),`<env>`(`_Map_`(`_|->_`(X,I),DotVar4)),_1,_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lv(X)~>DotVar3),`<env>`(_9),_1,_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(I,`Map:lookup`(_9,X))),#match(DotVar4,`_[_<-undef]`(_9,X))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isInt(I),isId(X))) ensures #token("true","Bool") [UNIQUE_ID(770d08e5054a5867192fe90d20604816a388232fea4041ee60ea87de44ee4c70) contentStartColumn(6) contentStartLine(41) org.kframework.attributes.Location(Location(41,6,42,39)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2884) as e39 when guard < 17 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e39) config (-1))) in match e with 
| [Bottom] -> (stepElt 17)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2885),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lv,(var_7_2886 :: [])) :: var_6_2887)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_15_2888)) :: []),(var_9_2889),(var_10_2890),(var_11_2891),(var_12_2892),(var_13_2893)) :: []) -> (let e = ((evalMap'Coln'lookup((var_15_2888),(var_7_2886 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 17)
| ((Int _ as var_5_2894) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_15_2888),(var_7_2886 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 17)
| ((Map (SortMap,_,_) as var_8_2895) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e39) config (-1))) in match e with 
| [Bottom] -> (stepElt 17)
| ((Map (SortStateCellMap,_,_) as var_14_2896) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((true) && ((isTrue (evalisId((var_7_2886 :: [])) config (-1))))))) && (((compare var_4_2884 var_4_2885) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e39,(KApply8(Lbl'_LT_'state'_GT_',e39,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loc,(var_5_2894 :: [])) :: var_6_2887)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_2886 :: []),(var_5_2894 :: [])) config (-1))),(var_8_2895 :: [])) config (-1)))) :: []),(var_9_2889),(var_10_2890),(var_11_2891),(var_12_2892),(var_13_2893)) :: [])) config (-1))),(var_14_2896 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 17))| _ -> (stepElt 17))| _ -> (stepElt 17))| _ -> (stepElt 17))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(`.List{"___OSL-SYNTAX"}`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))) ensures #token("true","Bool") [UNIQUE_ID(344d6052a56fe9b1f8c15cf6dd99963a026afce1d35a3e29a0e6734bd65b8261) contentStartColumn(6) contentStartLine(20) org.kframework.attributes.Location(Location(20,6,20,17)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2897) as e40 when guard < 18 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e40) config (-1))) in match e with 
| [Bottom] -> (stepElt 18)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2898),(KApply1(Lbl'_LT_'k'_GT_',(KApply0(Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra') :: var_5_2899)) :: []),(var_6_2900),(var_7_2901),(var_8_2902),(var_9_2903),(var_10_2904),(var_11_2905)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e40) config (-1))) in match e with 
| [Bottom] -> (stepElt 18)
| ((Map (SortStateCellMap,_,_) as var_12_2906) :: []) when ((((true) && (true))) && (true)) && (((compare var_4_2898 var_4_2897) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e40,(KApply8(Lbl'_LT_'state'_GT_',e40,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2899)) :: []),(var_6_2900),(var_7_2901),(var_8_2902),(var_9_2903),(var_10_2904),(var_11_2905)) :: [])) config (-1))),(var_12_2906 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 18))| _ -> (stepElt 18))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#rs(R)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Transfer(#rs(R),#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(_60,`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isProps(R),isKItem(_60)),isInt(L))) ensures #token("true","Bool") [UNIQUE_ID(27831a875e75b349000198dc7944082770d313452edf63daf53cb87277560e9a) contentStartColumn(6) contentStartLine(122) org.kframework.attributes.Location(Location(122,6,123,50)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2907) as e41 when guard < 19 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e41) config (-1))) in match e with 
| [Bottom] -> (stepElt 19)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2908),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(KApply1(Lbl'Hash'rs,(var_8_2909 :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_7_2910) :: [])) :: [])) :: var_5_2911)) :: []),(var_6_2912),(KApply1(Lbl'_LT_'store'_GT_',(var_15_2913)) :: []),(var_10_2914),(var_11_2915),(var_12_2916),(var_13_2917)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_15_2913),(var_7_2910 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 19)
| ((Map (SortMap,_,_) as var_9_2918) :: []) -> (let e = ((evalMap'Coln'lookup((var_15_2913),(var_7_2910 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 19)
| (var_16_2919 :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e41) config (-1))) in match e with 
| [Bottom] -> (stepElt 19)
| ((Map (SortStateCellMap,_,_) as var_14_2920) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((((isTrue (evalisProps((var_8_2909 :: [])) config (-1)))) && (true))) && (true)))) && (((compare var_4_2907 var_4_2908) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e41,(KApply8(Lbl'_LT_'state'_GT_',e41,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2911)) :: []),(var_6_2912),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_2910 :: []),(KApply1(Lbl'Hash'rs,(var_8_2909 :: [])) :: [])) config (-1))),(var_9_2918 :: [])) config (-1)))) :: []),(var_10_2914),(var_11_2915),(var_12_2916),(var_13_2917)) :: [])) config (-1))),(var_14_2920 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 19))| _ -> (stepElt 19))| _ -> (stepElt 19))| _ -> (stepElt 19))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(S~>Ss~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(`___OSL-SYNTAX`(S,Ss)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isStmts(Ss),isStmt(S))) ensures #token("true","Bool") [UNIQUE_ID(feb3d964f0a1134778ff7c506bf467abb6f39f207719f250723e72e5f978d637) contentStartColumn(6) contentStartLine(19) org.kframework.attributes.Location(Location(19,6,19,32)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2921) as e42 when guard < 20 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e42) config (-1))) in match e with 
| [Bottom] -> (stepElt 20)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2922),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl___OSL'Hyph'SYNTAX,(var_5_2923 :: []),(var_6_2924 :: [])) :: var_7_2925)) :: []),(var_8_2926),(var_9_2927),(var_10_2928),(var_11_2929),(var_12_2930),(var_13_2931)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e42) config (-1))) in match e with 
| [Bottom] -> (stepElt 20)
| ((Map (SortStateCellMap,_,_) as var_14_2932) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisStmts((var_6_2924 :: [])) config (-1)))) && ((isTrue (evalisStmt((var_5_2923 :: [])) config (-1))))))) && (((compare var_4_2921 var_4_2922) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e42,(KApply8(Lbl'_LT_'state'_GT_',e42,(KApply1(Lbl'_LT_'k'_GT_',(var_5_2923 :: var_6_2924 :: var_7_2925)) :: []),(var_8_2926),(var_9_2927),(var_10_2928),(var_11_2929),(var_12_2930),(var_13_2931)) :: [])) config (-1))),(var_14_2932 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 20))| _ -> (stepElt 20))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#lvDref(#lv(E))~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lv(`*__OSL-SYNTAX`(E))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isExp(E)) ensures #token("true","Bool") [UNIQUE_ID(69b49fc56e16138a99dabb96df197685168dbc7df94f38a8ee31a7b14c694367) contentStartColumn(6) contentStartLine(240) org.kframework.attributes.Location(Location(240,6,240,39)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2933) as e43 when guard < 21 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e43) config (-1))) in match e with 
| [Bottom] -> (stepElt 21)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2934),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lv,(KApply1(Lbl'Star'__OSL'Hyph'SYNTAX,(var_5_2935 :: [])) :: [])) :: var_6_2936)) :: []),(var_7_2937),(var_8_2938),(var_9_2939),(var_10_2940),(var_11_2941),(var_12_2942)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e43) config (-1))) in match e with 
| [Bottom] -> (stepElt 21)
| ((Map (SortStateCellMap,_,_) as var_13_2943) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisExp((var_5_2935 :: [])) config (-1))))) && (((compare var_4_2933 var_4_2934) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e43,(KApply8(Lbl'_LT_'state'_GT_',e43,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lvDref,(KApply1(Lbl'Hash'lv,(var_5_2935 :: [])) :: [])) :: var_6_2936)) :: []),(var_7_2937),(var_8_2938),(var_9_2939),(var_10_2940),(var_11_2941),(var_12_2942)) :: [])) config (-1))),(var_13_2943 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 21))| _ -> (stepElt 21))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Read(#read(#loc(L)))~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Read(#loc(L))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isInt(L)) ensures #token("true","Bool") [UNIQUE_ID(e3648e1177613979775f9d7040c930da4608f7bda33c5413428bc0c78ff9e116) contentStartColumn(6) contentStartLine(178) org.kframework.attributes.Location(Location(178,6,178,48)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2944) as e44 when guard < 22 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e44) config (-1))) in match e with 
| [Bottom] -> (stepElt 22)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2945),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(KApply1(Lbl'Hash'loc,((Int _ as var_5_2946) :: [])) :: [])) :: var_6_2947)) :: []),(var_7_2948),(var_8_2949),(var_9_2950),(var_10_2951),(var_11_2952),(var_12_2953)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e44) config (-1))) in match e with 
| [Bottom] -> (stepElt 22)
| ((Map (SortStateCellMap,_,_) as var_13_2954) :: []) when ((((((true) && (true))) && (true))) && (true)) && (((compare var_4_2944 var_4_2945) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e44,(KApply8(Lbl'_LT_'state'_GT_',e44,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,(var_5_2946 :: [])) :: [])) :: [])) :: var_6_2947)) :: []),(var_7_2948),(var_8_2949),(var_9_2950),(var_10_2951),(var_11_2952),(var_12_2953)) :: [])) config (-1))),(var_13_2954 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 22))| _ -> (stepElt 22))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#loc(L)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lv(#loc(L))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isInt(L)) ensures #token("true","Bool") [UNIQUE_ID(801964a9b766aa631b560eb7ece73aeed07d28c7704592c1df7a28d4dbb5076c) contentStartColumn(6) contentStartLine(234) org.kframework.attributes.Location(Location(234,6,234,33)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2955) as e45 when guard < 23 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e45) config (-1))) in match e with 
| [Bottom] -> (stepElt 23)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2956),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lv,(KApply1(Lbl'Hash'loc,((Int _ as var_5_2957) :: [])) :: [])) :: var_6_2958)) :: []),(var_7_2959),(var_8_2960),(var_9_2961),(var_10_2962),(var_11_2963),(var_12_2964)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e45) config (-1))) in match e with 
| [Bottom] -> (stepElt 23)
| ((Map (SortStateCellMap,_,_) as var_13_2965) :: []) when ((((((true) && (true))) && (true))) && (true)) && (((compare var_4_2955 var_4_2956) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e45,(KApply8(Lbl'_LT_'state'_GT_',e45,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loc,(var_5_2957 :: [])) :: var_6_2958)) :: []),(var_7_2959),(var_8_2960),(var_9_2961),(var_10_2962),(var_11_2963),(var_12_2964)) :: [])) config (-1))),(var_13_2965 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 23))| _ -> (stepElt 23))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(`#blockend_BLOCK`(.KList)~>V~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(V~>`#blockend_BLOCK`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isValue(V)) ensures #token("true","Bool") [UNIQUE_ID(3d940d191cb6924c5734bfc64aa7aa5b6d6e9613fb5bdbac0353c9eb7b943903) contentStartColumn(6) contentStartLine(17) org.kframework.attributes.Location(Location(17,6,17,44)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/block.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_2966) as e46 when guard < 24 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e46) config (-1))) in match e with 
| [Bottom] -> (stepElt 24)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2967),(KApply1(Lbl'_LT_'k'_GT_',(var_5_2968 :: KApply0(Lbl'Hash'blockend_BLOCK) :: var_6_2969)) :: []),(var_7_2970),(var_8_2971),(var_9_2972),(var_10_2973),(var_11_2974),(var_12_2975)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e46) config (-1))) in match e with 
| [Bottom] -> (stepElt 24)
| ((Map (SortStateCellMap,_,_) as var_13_2976) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisValue((var_5_2968 :: [])) config (-1))))) && (((compare var_4_2966 var_4_2967) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e46,(KApply8(Lbl'_LT_'state'_GT_',e46,(KApply1(Lbl'_LT_'k'_GT_',(const'Hash'blockend_BLOCK :: var_5_2968 :: var_6_2969)) :: []),(var_7_2970),(var_8_2971),(var_9_2972),(var_10_2973),(var_11_2974),(var_12_2975)) :: [])) config (-1))),(var_13_2976 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 24))| _ -> (stepElt 24))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`.StateCellMap`(.KList),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(`#removeState_CONTROL`(.KList)),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))) ensures #token("true","Bool") [UNIQUE_ID(7290f055c528648030b41b64f07663f7ce9a92f59c0b758de2382df185599d88) contentStartColumn(8) contentStartLine(108) org.kframework.attributes.Location(Location(108,8,111,25)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_5_2977) as e47 when guard < 25 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e47) config (-1))) in match e with 
| [Bottom] -> (stepElt 25)
| (KApply8(Lbl'_LT_'state'_GT_',(var_5_2978),(KApply1(Lbl'_LT_'k'_GT_',(KApply0(Lbl'Hash'removeState_CONTROL) :: [])) :: []),(var_6_2979),(var_7_2980),(var_8_2981),(var_9_2982),(var_10_2983),(var_11_2984)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e47) config (-1))) in match e with 
| [Bottom] -> (stepElt 25)
| ((Map (SortStateCellMap,_,_) as var_4_2985) :: []) when ((((true) && (true))) && (true)) && (((compare var_5_2977 var_5_2978) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((Lazy.force const'Stop'StateCellMap)),(var_4_2985 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 25))| _ -> (stepElt 25))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transfer(K0,HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#Transfer0_`(K0)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isExp(HOLE),isK(K0)),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(5087f3e877cbda5fd012dd16d9c954df171973cce06908301c120350ad87b879) cool() klabel(#Transfer) org.kframework.attributes.Location(Location(35,12,35,38)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1021886708) strict()]|}*)
| (var_4_2986) as e48 when guard < 26 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e48) config (-1))) in match e with 
| [Bottom] -> (stepElt 26)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2987),(KApply1(Lbl'_LT_'k'_GT_',(var_6_2988 :: KApply1(Lbl'Hash'freezer'Hash'Transfer0_,(var_5_2989)) :: var_7_2990)) :: []),(var_8_2991),(var_9_2992),(var_10_2993),(var_11_2994),(var_12_2995),(var_13_2996)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e48) config (-1))) in match e with 
| [Bottom] -> (stepElt 26)
| ((Map (SortStateCellMap,_,_) as var_14_2997) :: []) when ((((((true) && (true))) && (true))) && ((((((isTrue (evalisExp((var_6_2988 :: [])) config (-1)))) && (true))) && (((true) && ((isTrue (evalisKResult((var_6_2988 :: [])) config (-1))))))))) && (((compare var_4_2986 var_4_2987) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e48,(KApply8(Lbl'_LT_'state'_GT_',e48,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(var_5_2989),(var_6_2988 :: [])) :: var_7_2990)) :: []),(var_8_2991),(var_9_2992),(var_10_2993),(var_11_2994),(var_12_2995),(var_13_2996)) :: [])) config (-1))),(var_14_2997 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 26))| _ -> (stepElt 26))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#TransferMB1_`(K1)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#TransferMB(HOLE,K1)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isK(HOLE),isK(K1)),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(28256b9403a9b7b751161d2439842bc07543dc4e88502da5c040c6165a2eca5c) heat() klabel(#TransferMB) org.kframework.attributes.Location(Location(139,12,139,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1209581847) strict(1)]|}*)
| (var_4_2998) as e49 when guard < 27 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e49) config (-1))) in match e with 
| [Bottom] -> (stepElt 27)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_2999),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferMB,(var_5_3000),(var_6_3001)) :: var_7_3002)) :: []),(var_8_3003),(var_9_3004),(var_10_3005),(var_11_3006),(var_12_3007),(var_13_3008)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e49) config (-1))) in match e with 
| [Bottom] -> (stepElt 27)
| ((Map (SortStateCellMap,_,_) as var_14_3009) :: []) when ((((((true) && (true))) && (true))) && (((((true) && (true))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3000)) config (-1))))))))))) && (((compare var_4_2998 var_4_2999) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e49,(KApply8(Lbl'_LT_'state'_GT_',e49,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3000 @ KApply1(Lbl'Hash'freezer'Hash'TransferMB1_,(var_6_3001)) :: var_7_3002)) :: []),(var_8_3003),(var_9_3004),(var_10_3005),(var_11_3006),(var_12_3007),(var_13_3008)) :: [])) config (-1))),(var_14_3009 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 27))| _ -> (stepElt 27))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#expStmt(V)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isValue(V)) ensures #token("true","Bool") [UNIQUE_ID(7b3387907644d7b8a3b04d1a3baac0e4e52335b7edce2085fa4f2f7d8c464819) contentStartColumn(6) contentStartLine(25) org.kframework.attributes.Location(Location(25,6,25,20)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3010) as e50 when guard < 28 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e50) config (-1))) in match e with 
| [Bottom] -> (stepElt 28)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3011),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'expStmt,(var_13_3012 :: [])) :: var_5_3013)) :: []),(var_6_3014),(var_7_3015),(var_8_3016),(var_9_3017),(var_10_3018),(var_11_3019)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e50) config (-1))) in match e with 
| [Bottom] -> (stepElt 28)
| ((Map (SortStateCellMap,_,_) as var_12_3020) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisValue((var_13_3012 :: [])) config (-1))))) && (((compare var_4_3011 var_4_3010) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e50,(KApply8(Lbl'_LT_'state'_GT_',e50,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3013)) :: []),(var_6_3014),(var_7_3015),(var_8_3016),(var_9_3017),(var_10_3018),(var_11_3019)) :: [])) config (-1))),(var_12_3020 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 28))| _ -> (stepElt 28))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(Rest~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#decompose(#repeat(B)~>Rest)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isBlock(B),isK(Rest))) ensures #token("true","Bool") [UNIQUE_ID(fdcfe4ae52a11bbb445b00bbb5651161c55df0a255605b1db7a40f814cf9cc81) contentStartColumn(6) contentStartLine(86) org.kframework.attributes.Location(Location(86,6,86,47)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3021) as e51 when guard < 29 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e51) config (-1))) in match e with 
| [Bottom] -> (stepElt 29)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3022),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'decompose,(KApply1(Lbl'Hash'repeat,(var_14_3023 :: [])) :: var_5_3024)) :: var_6_3025)) :: []),(var_7_3026),(var_8_3027),(var_9_3028),(var_10_3029),(var_11_3030),(var_12_3031)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e51) config (-1))) in match e with 
| [Bottom] -> (stepElt 29)
| ((Map (SortStateCellMap,_,_) as var_13_3032) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisBlock((var_14_3023 :: [])) config (-1)))) && (true)))) && (((compare var_4_3022 var_4_3021) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e51,(KApply8(Lbl'_LT_'state'_GT_',e51,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3024 @ var_6_3025)) :: []),(var_7_3026),(var_8_3027),(var_9_3028),(var_10_3029),(var_11_3030),(var_12_3031)) :: [])) config (-1))),(var_13_3032 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 29))| _ -> (stepElt 29))
(*{| rule `<T>`(`<states>`(``_6=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#loc(L1)~>DotVar3),_1,`<store>`(`_[_<-_]_MAP`(Rho,L,#br(BEG,TIMER,#mutRef(L1)))),_2,`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(C,_81)))),DotVar1)``),_3,_4,_5) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_6),#match(`<state>`(_0,`<k>`(#borrowMutCK(L,BEG,END,L1)~>DotVar3),_1,`<store>`(Rho),_2,`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(C,_81))),`Map:lookup`(_6,_0))),#match(DotVar1,`_[_<-undef]`(_6,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(TIMER),isInt(END)),isInt(_81)),isMap(Rho)),isInt(C)),isSet(WRITE)),isInt(L)),isInt(L1)),isInt(BEG)),`_andBool_`(`_==Bool__BOOL`(#borrowmutck(L,Rho,BEG,TIMER,L1),#token("false","Bool")),#writeCK(L1,BEG,TIMER,WRITE)))) ensures #token("true","Bool") [UNIQUE_ID(dedd2c83e769be94bb1d7d46ad7e068d9a3f6739f22327ea7a2ab408cb991455) contentStartColumn(6) contentStartLine(210) org.kframework.attributes.Location(Location(210,6,216,45)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_3033) as e52 when guard < 30 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e52) config (-1))) in match e with 
| [Bottom] -> (stepElt 30)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3034),(KApply1(Lbl'_LT_'k'_GT_',(KApply4(Lbl'Hash'borrowMutCK,((Int _ as var_9_3035) :: []),((Int _ as var_10_3036) :: []),((Int _ as var_17_3037) :: []),((Int _ as var_5_3038) :: [])) :: var_6_3039)) :: []),(var_7_3040),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_8_3041) :: [])) :: []),(var_12_3042),(KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as var_13_3043) :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_11_3044) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_14_3045) :: []),((Int _ as var_15_3046) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e52) config (-1))) in match e with 
| [Bottom] -> (stepElt 30)
| ((Map (SortStateCellMap,_,_) as var_16_3047) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'borrowmutck((var_9_3035 :: []),(var_8_3041 :: []),(var_10_3036 :: []),(var_11_3044 :: []),(var_5_3038 :: [])) config (-1))),((Bool false) :: [])) config (-1)))) && ((isTrue (eval'Hash'writeCK((var_5_3038 :: []),(var_10_3036 :: []),(var_11_3044 :: []),(var_13_3043 :: [])) config (-1))))))))) && (((compare var_4_3033 var_4_3034) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e52,(KApply8(Lbl'_LT_'state'_GT_',e52,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loc,(var_5_3038 :: [])) :: var_6_3039)) :: []),(var_7_3040),(KApply1(Lbl'_LT_'store'_GT_',((eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP((var_8_3041 :: []),(var_9_3035 :: []),(KApply3(Lbl'Hash'br,(var_10_3036 :: []),(var_11_3044 :: []),(KApply1(Lbl'Hash'mutRef,(var_5_3038 :: [])) :: [])) :: [])) config (-1)))) :: []),(var_12_3042),(KApply1(Lbl'_LT_'write'_GT_',(var_13_3043 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_11_3044 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_14_3045 :: []),(var_15_3046 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_16_3047 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 30))| _ -> (stepElt 30))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(val(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezerval0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(faac9720d8b496dd10b7c6563651e087c02f26e364b70825934fb2f8201b0701) cool() klabel(val) org.kframework.attributes.Location(Location(51,12,51,56)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl-syntax.k)) productionID(359558578) strict()]|}*)
| (var_4_3048) as e53 when guard < 31 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e53) config (-1))) in match e with 
| [Bottom] -> (stepElt 31)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3049),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3050 :: KApply0(Lbl'Hash'freezerval0_) :: var_6_3051)) :: []),(var_7_3052),(var_8_3053),(var_9_3054),(var_10_3055),(var_11_3056),(var_12_3057)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e53) config (-1))) in match e with 
| [Bottom] -> (stepElt 31)
| ((Map (SortStateCellMap,_,_) as var_13_3058) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_3050 :: [])) config (-1)))) && (((true) && ((isTrue (evalisKResult((var_5_3050 :: [])) config (-1))))))))) && (((compare var_4_3048 var_4_3049) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e53,(KApply8(Lbl'_LT_'state'_GT_',e53,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lblval,(var_5_3050 :: [])) :: var_6_3051)) :: []),(var_7_3052),(var_8_3053),(var_9_3054),(var_10_3055),(var_11_3056),(var_12_3057)) :: [])) config (-1))),(var_13_3058 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 31))| _ -> (stepElt 31))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#lvDref(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#lvDref0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(9964aafc9c528c3e0736c598f8da73ffb796d10a23226ee1594017700a99f9cc) cool() klabel(#lvDref) org.kframework.attributes.Location(Location(238,12,238,40)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1037329131) strict()]|}*)
| (var_4_3059) as e54 when guard < 32 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e54) config (-1))) in match e with 
| [Bottom] -> (stepElt 32)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3060),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3061 :: KApply0(Lbl'Hash'freezer'Hash'lvDref0_) :: var_6_3062)) :: []),(var_7_3063),(var_8_3064),(var_9_3065),(var_10_3066),(var_11_3067),(var_12_3068)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e54) config (-1))) in match e with 
| [Bottom] -> (stepElt 32)
| ((Map (SortStateCellMap,_,_) as var_13_3069) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_3061 :: [])) config (-1)))) && (((true) && ((isTrue (evalisKResult((var_5_3061 :: [])) config (-1))))))))) && (((compare var_4_3059 var_4_3060) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e54,(KApply8(Lbl'_LT_'state'_GT_',e54,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lvDref,(var_5_3061 :: [])) :: var_6_3062)) :: []),(var_7_3063),(var_8_3064),(var_9_3065),(var_10_3066),(var_11_3067),(var_12_3068)) :: [])) config (-1))),(var_13_3069 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 32))| _ -> (stepElt 32))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Read(#rs(R))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isProps(R)) ensures #token("true","Bool") [UNIQUE_ID(4c56fadb5a221976c8f22ef3a541c2971b9546defeb9809accd35a1733aee476) contentStartColumn(6) contentStartLine(180) org.kframework.attributes.Location(Location(180,6,180,30)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3070) as e55 when guard < 33 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e55) config (-1))) in match e with 
| [Bottom] -> (stepElt 33)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3071),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(KApply1(Lbl'Hash'rs,(var_13_3072 :: [])) :: [])) :: var_5_3073)) :: []),(var_6_3074),(var_7_3075),(var_8_3076),(var_9_3077),(var_10_3078),(var_11_3079)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e55) config (-1))) in match e with 
| [Bottom] -> (stepElt 33)
| ((Map (SortStateCellMap,_,_) as var_12_3080) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisProps((var_13_3072 :: [])) config (-1))))) && (((compare var_4_3070 var_4_3071) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e55,(KApply8(Lbl'_LT_'state'_GT_',e55,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3073)) :: []),(var_6_3074),(var_7_3075),(var_8_3076),(var_9_3077),(var_10_3078),(var_11_3079)) :: [])) config (-1))),(var_12_3080 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 33))| _ -> (stepElt 33))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#expStmt0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#expStmt(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(91717f9dd98fe2b826052b0ae5ff88485b75ba033f39efeaa44fe549e30403b2) heat() klabel(#expStmt) org.kframework.attributes.Location(Location(50,12,50,73)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl-syntax.k)) productionID(970519188) strict()]|}*)
| (var_4_3081) as e56 when guard < 34 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e56) config (-1))) in match e with 
| [Bottom] -> (stepElt 34)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3082),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'expStmt,(var_5_3083 :: [])) :: var_6_3084)) :: []),(var_7_3085),(var_8_3086),(var_9_3087),(var_10_3088),(var_11_3089),(var_12_3090)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e56) config (-1))) in match e with 
| [Bottom] -> (stepElt 34)
| ((Map (SortStateCellMap,_,_) as var_13_3091) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_3083 :: [])) config (-1)))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3083 :: [])) config (-1))))))))))) && (((compare var_4_3081 var_4_3082) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e56,(KApply8(Lbl'_LT_'state'_GT_',e56,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3083 :: const'Hash'freezer'Hash'expStmt0_ :: var_6_3084)) :: []),(var_7_3085),(var_8_3086),(var_9_3087),(var_10_3088),(var_11_3089),(var_12_3090)) :: [])) config (-1))),(var_13_3091 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 34))| _ -> (stepElt 34))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#borrowMutCK(L,BEG,END,L1)~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#br(BEG,END,#immRef(L1))),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lvDref(#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(#br(BEG,END,#immRef(L1)),`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isInt(END),isInt(L)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(f65b9ea5434d0d3ebf669f4551b3053b693b819ef73df1eeaabf9f3b43b67222) contentStartColumn(6) contentStartLine(246) org.kframework.attributes.Location(Location(246,6,247,75)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3092) as e57 when guard < 35 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e57) config (-1))) in match e with 
| [Bottom] -> (stepElt 35)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3093),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lvDref,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3094) :: [])) :: [])) :: var_9_3095)) :: []),(var_10_3096),(KApply1(Lbl'_LT_'store'_GT_',(var_17_3097)) :: []),(var_12_3098),(var_13_3099),(var_14_3100),(var_15_3101)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_17_3097),(var_5_3094 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 35)
| ((Map (SortMap,_,_) as var_11_3102) :: []) -> (let e = ((evalMap'Coln'lookup((var_17_3097),(var_5_3094 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 35)
| (KApply3(Lbl'Hash'br,((Int _ as var_6_3103) :: []),((Int _ as var_7_3104) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_8_3105) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e57) config (-1))) in match e with 
| [Bottom] -> (stepElt 35)
| ((Map (SortStateCellMap,_,_) as var_16_3106) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((true) && (true))) && (true))) && (true)))) && (((compare var_4_3092 var_4_3093) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e57,(KApply8(Lbl'_LT_'state'_GT_',e57,(KApply1(Lbl'_LT_'k'_GT_',(KApply4(Lbl'Hash'borrowMutCK,(var_5_3094 :: []),(var_6_3103 :: []),(var_7_3104 :: []),(var_8_3105 :: [])) :: var_9_3095)) :: []),(var_10_3096),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_5_3094 :: []),(KApply3(Lbl'Hash'br,(var_6_3103 :: []),(var_7_3104 :: []),(KApply1(Lbl'Hash'immRef,(var_8_3105 :: [])) :: [])) :: [])) config (-1))),(var_11_3102 :: [])) config (-1)))) :: []),(var_12_3098),(var_13_3099),(var_14_3100),(var_15_3101)) :: [])) config (-1))),(var_16_3106 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 35))| _ -> (stepElt 35))| _ -> (stepElt 35))| _ -> (stepElt 35))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Deallocate(#rs(Ps))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isProps(Ps)) ensures #token("true","Bool") [UNIQUE_ID(73e3a9056a98d857f08237e3636ab8a9b6f85ab45d2a8d4cbb638ab8305dd3cd) contentStartColumn(6) contentStartLine(373) org.kframework.attributes.Location(Location(373,6,373,37)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3107) as e58 when guard < 36 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e58) config (-1))) in match e with 
| [Bottom] -> (stepElt 36)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3108),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Deallocate,(KApply1(Lbl'Hash'rs,(var_13_3109 :: [])) :: [])) :: var_5_3110)) :: []),(var_6_3111),(var_7_3112),(var_8_3113),(var_9_3114),(var_10_3115),(var_11_3116)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e58) config (-1))) in match e with 
| [Bottom] -> (stepElt 36)
| ((Map (SortStateCellMap,_,_) as var_12_3117) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisProps((var_13_3109 :: [])) config (-1))))) && (((compare var_4_3107 var_4_3108) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e58,(KApply8(Lbl'_LT_'state'_GT_',e58,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3110)) :: []),(var_6_3111),(var_7_3112),(var_8_3113),(var_9_3114),(var_10_3115),(var_11_3116)) :: [])) config (-1))),(var_12_3117 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 36))| _ -> (stepElt 36))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(V~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(val(V)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isValue(V)) ensures #token("true","Bool") [UNIQUE_ID(d684bddc4a31306f52c63abb8f13971928a219a90e50c4fd84c025316017911f) contentStartColumn(6) contentStartLine(362) org.kframework.attributes.Location(Location(362,6,362,23)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3118) as e59 when guard < 37 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e59) config (-1))) in match e with 
| [Bottom] -> (stepElt 37)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3119),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lblval,(var_5_3120 :: [])) :: var_6_3121)) :: []),(var_7_3122),(var_8_3123),(var_9_3124),(var_10_3125),(var_11_3126),(var_12_3127)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e59) config (-1))) in match e with 
| [Bottom] -> (stepElt 37)
| ((Map (SortStateCellMap,_,_) as var_13_3128) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisValue((var_5_3120 :: [])) config (-1))))) && (((compare var_4_3118 var_4_3119) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e59,(KApply8(Lbl'_LT_'state'_GT_',e59,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3120 :: var_6_3121)) :: []),(var_7_3122),(var_8_3123),(var_9_3124),(var_10_3125),(var_11_3126),(var_12_3127)) :: [])) config (-1))),(var_13_3128 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 37))| _ -> (stepElt 37))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,`#uninit_OSL-SYNTAX`(.KList)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Deallocate(#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(V,`Map:lookup`(_9,L))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isValue(V),isInt(L))) ensures #token("true","Bool") [UNIQUE_ID(e2f7b8fc29d0c341a3af03c3f2544a3179084ea4c552ca2b2bb46119f6e8c91d) contentStartColumn(6) contentStartLine(370) org.kframework.attributes.Location(Location(370,6,371,65)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3129) as e60 when guard < 38 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e60) config (-1))) in match e with 
| [Bottom] -> (stepElt 38)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3130),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Deallocate,(KApply1(Lbl'Hash'loc,((Int _ as var_7_3131) :: [])) :: [])) :: var_5_3132)) :: []),(var_6_3133),(KApply1(Lbl'_LT_'store'_GT_',(var_14_3134)) :: []),(var_9_3135),(var_10_3136),(var_11_3137),(var_12_3138)) :: []) -> (let e = ((evalMap'Coln'lookup((var_14_3134),(var_7_3131 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 38)
| (var_15_3139 :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_14_3134),(var_7_3131 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 38)
| ((Map (SortMap,_,_) as var_8_3140) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e60) config (-1))) in match e with 
| [Bottom] -> (stepElt 38)
| ((Map (SortStateCellMap,_,_) as var_13_3141) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (evalisValue((var_15_3139 :: [])) config (-1)))) && (true)))) && (((compare var_4_3129 var_4_3130) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e60,(KApply8(Lbl'_LT_'state'_GT_',e60,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3132)) :: []),(var_6_3133),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_3131 :: []),(const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1))),(var_8_3140 :: [])) config (-1)))) :: []),(var_9_3135),(var_10_3136),(var_11_3137),(var_12_3138)) :: [])) config (-1))),(var_13_3141 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 38))| _ -> (stepElt 38))| _ -> (stepElt 38))| _ -> (stepElt 38))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#rs(N)~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#rs(N)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#read(#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(#rs(N),`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isProps(N),isInt(L))) ensures #token("true","Bool") [UNIQUE_ID(5e21a8ea429b59135b47500758a7ac7583f06a44b797db42a1eb6c2c7be23af6) contentStartColumn(6) contentStartLine(169) org.kframework.attributes.Location(Location(169,6,170,49)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3142) as e61 when guard < 39 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e61) config (-1))) in match e with 
| [Bottom] -> (stepElt 39)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3143),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,((Int _ as var_8_3144) :: [])) :: [])) :: var_6_3145)) :: []),(var_7_3146),(KApply1(Lbl'_LT_'store'_GT_',(var_15_3147)) :: []),(var_10_3148),(var_11_3149),(var_12_3150),(var_13_3151)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_15_3147),(var_8_3144 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 39)
| ((Map (SortMap,_,_) as var_9_3152) :: []) -> (let e = ((evalMap'Coln'lookup((var_15_3147),(var_8_3144 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 39)
| (KApply1(Lbl'Hash'rs,(var_5_3153 :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e61) config (-1))) in match e with 
| [Bottom] -> (stepElt 39)
| ((Map (SortStateCellMap,_,_) as var_14_3154) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (evalisProps((var_5_3153 :: [])) config (-1)))) && (true)))) && (((compare var_4_3143 var_4_3142) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e61,(KApply8(Lbl'_LT_'state'_GT_',e61,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'rs,(var_5_3153 :: [])) :: var_6_3145)) :: []),(var_7_3146),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_8_3144 :: []),(KApply1(Lbl'Hash'rs,(var_5_3153 :: [])) :: [])) config (-1))),(var_9_3152 :: [])) config (-1)))) :: []),(var_10_3148),(var_11_3149),(var_12_3150),(var_13_3151)) :: [])) config (-1))),(var_14_3154 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 39))| _ -> (stepElt 39))| _ -> (stepElt 39))| _ -> (stepElt 39))
(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,`<timer>`(`_+Int_`(TIMER,#token("1","Int"))),_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(`#increaseTimer_OSL`(.KList)~>DotVar3),_1,_2,_3,_4,`<timer>`(TIMER),_5),`Map:lookup`(_9,_0))),#match(DotVar1,`_[_<-undef]`(_9,_0))),isInt(TIMER)) ensures #token("true","Bool") [UNIQUE_ID(929c1a50e8eb0fb145cbfb12bfb5bb190beb76124484dad4ee28c2160552203c) contentStartColumn(6) contentStartLine(78) org.kframework.attributes.Location(Location(78,6,79,48)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3155) as e62 when guard < 40 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e62) config (-1))) in match e with 
| [Bottom] -> (stepElt 40)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3156),(KApply1(Lbl'_LT_'k'_GT_',(KApply0(Lbl'Hash'increaseTimer_OSL) :: var_5_3157)) :: []),(var_6_3158),(var_7_3159),(var_8_3160),(var_9_3161),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_10_3162) :: [])) :: []),(var_11_3163)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e62) config (-1))) in match e with 
| [Bottom] -> (stepElt 40)
| ((Map (SortStateCellMap,_,_) as var_12_3164) :: []) when ((((((true) && (true))) && (true))) && (true)) && (((compare var_4_3155 var_4_3156) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e62,(KApply8(Lbl'_LT_'state'_GT_',e62,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3157)) :: []),(var_6_3158),(var_7_3159),(var_8_3160),(var_9_3161),(KApply1(Lbl'_LT_'timer'_GT_',((eval_'Plus'Int_((var_10_3162 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(var_11_3163)) :: [])) config (-1))),(var_12_3164 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 40))| _ -> (stepElt 40))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#Transfer0_`(K0)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Transfer(K0,HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isExp(HOLE),isK(K0)),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(b26dd0a38c100416025cd949eb36be020f9c9527ef30016506520f476a76fa6d) heat() klabel(#Transfer) org.kframework.attributes.Location(Location(35,12,35,38)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1021886708) strict()]|}*)
| (var_4_3165) as e63 when guard < 41 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e63) config (-1))) in match e with 
| [Bottom] -> (stepElt 41)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3166),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(var_6_3167),(var_5_3168 :: [])) :: var_7_3169)) :: []),(var_8_3170),(var_9_3171),(var_10_3172),(var_11_3173),(var_12_3174),(var_13_3175)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e63) config (-1))) in match e with 
| [Bottom] -> (stepElt 41)
| ((Map (SortStateCellMap,_,_) as var_14_3176) :: []) when ((((((true) && (true))) && (true))) && ((((((isTrue (evalisExp((var_5_3168 :: [])) config (-1)))) && (true))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3168 :: [])) config (-1))))))))))) && (((compare var_4_3165 var_4_3166) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e63,(KApply8(Lbl'_LT_'state'_GT_',e63,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3168 :: KApply1(Lbl'Hash'freezer'Hash'Transfer0_,(var_6_3167)) :: var_7_3169)) :: []),(var_8_3170),(var_9_3171),(var_10_3172),(var_11_3173),(var_12_3174),(var_13_3175)) :: [])) config (-1))),(var_14_3176 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 41))| _ -> (stepElt 41))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#lvDref0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lvDref(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(50da0121afe4a4129fdb9a9bd6942c132fad806055754ee56529c5b837fa9a91) heat() klabel(#lvDref) org.kframework.attributes.Location(Location(238,12,238,40)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1037329131) strict()]|}*)
| (var_4_3177) as e64 when guard < 42 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e64) config (-1))) in match e with 
| [Bottom] -> (stepElt 42)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3178),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lvDref,(var_5_3179 :: [])) :: var_6_3180)) :: []),(var_7_3181),(var_8_3182),(var_9_3183),(var_10_3184),(var_11_3185),(var_12_3186)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e64) config (-1))) in match e with 
| [Bottom] -> (stepElt 42)
| ((Map (SortStateCellMap,_,_) as var_13_3187) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_3179 :: [])) config (-1)))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3179 :: [])) config (-1))))))))))) && (((compare var_4_3177 var_4_3178) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e64,(KApply8(Lbl'_LT_'state'_GT_',e64,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3179 :: const'Hash'freezer'Hash'lvDref0_ :: var_6_3180)) :: []),(var_7_3181),(var_8_3182),(var_9_3183),(var_10_3184),(var_11_3185),(var_12_3186)) :: [])) config (-1))),(var_13_3187 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 42))| _ -> (stepElt 42))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Read(#borrowImmCK(L,BEG,END,L1))~>#loc(L1)~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#br(BEG,END,#immRef(L1))),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#read(#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(#br(BEG,END,#immRef(L1)),`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isInt(END),isInt(L)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(6bff73fa6d1a0996603a6a8054fa4217d822c02b3098454fbeb34753b43baf71) contentStartColumn(6) contentStartLine(172) org.kframework.attributes.Location(Location(172,6,173,75)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3188) as e65 when guard < 43 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e65) config (-1))) in match e with 
| [Bottom] -> (stepElt 43)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3189),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3190) :: [])) :: [])) :: var_9_3191)) :: []),(var_10_3192),(KApply1(Lbl'_LT_'store'_GT_',(var_17_3193)) :: []),(var_12_3194),(var_13_3195),(var_14_3196),(var_15_3197)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_17_3193),(var_5_3190 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 43)
| ((Map (SortMap,_,_) as var_11_3198) :: []) -> (let e = ((evalMap'Coln'lookup((var_17_3193),(var_5_3190 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 43)
| (KApply3(Lbl'Hash'br,((Int _ as var_6_3199) :: []),((Int _ as var_7_3200) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_8_3201) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e65) config (-1))) in match e with 
| [Bottom] -> (stepElt 43)
| ((Map (SortStateCellMap,_,_) as var_16_3202) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((true) && (true))) && (true))) && (true)))) && (((compare var_4_3188 var_4_3189) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e65,(KApply8(Lbl'_LT_'state'_GT_',e65,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(KApply4(Lbl'Hash'borrowImmCK,(var_5_3190 :: []),(var_6_3199 :: []),(var_7_3200 :: []),(var_8_3201 :: [])) :: [])) :: KApply1(Lbl'Hash'loc,(var_8_3201 :: [])) :: var_9_3191)) :: []),(var_10_3192),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_5_3190 :: []),(KApply3(Lbl'Hash'br,(var_6_3199 :: []),(var_7_3200 :: []),(KApply1(Lbl'Hash'immRef,(var_8_3201 :: [])) :: [])) :: [])) config (-1))),(var_11_3198 :: [])) config (-1)))) :: []),(var_12_3194),(var_13_3195),(var_14_3196),(var_15_3197)) :: [])) config (-1))),(var_16_3202 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 43))| _ -> (stepElt 43))| _ -> (stepElt 43))| _ -> (stepElt 43))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`_StateCellMap_`(`StateCellMapItem`(`<index>`(N),`<state>`(`<index>`(N),`<k>`(Pro),`<env>`(ENV1),`<store>`(STORE1),`<stack>`(STACK1),_0,_1,_2)),`StateCellMapItem`(_3,`<state>`(_3,`<k>`(#loopSep(`_+Int_`(N,#token("1","Int")))~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),_4,_5,_6))),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_3,_10),#match(`<state>`(_3,`<k>`(#loopSep(N)~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),_4,_5,_6),`Map:lookup`(_10,_3))),#match(`<state>`(`<index>`(N),`<k>`(Pro),`<env>`(ENV1),`<store>`(STORE1),`<stack>`(STACK1),_0,_1,_2),`Map:lookup`(_10,`<index>`(N)))),#match(DotVar1,`_[_<-undef]`(`_[_<-undef]`(_10,`<index>`(N)),_3))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isMap(ENV1),isMap(ENV)),isInt(N)),isK(Rest)),isMap(STORE)),isK(Pro)),isList(STACK)),isList(STACK1)),isMap(STORE1)),`_orBool__BOOL`(`_=/=K_`(Pro,#compare(Rest)),`_==Bool__BOOL`(#compareS(STORE,STORE1),#token("false","Bool"))))) ensures #token("true","Bool") [UNIQUE_ID(91e6670445b90dd28ce931f28dfec4cb08b8e32e017a077e59d7bbcdd94774f7) contentStartColumn(6) contentStartLine(114) org.kframework.attributes.Location(Location(114,6,126,88)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_12_3203) as e66 when guard < 44 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e66) config (-1))) in match e with 
| [Bottom] -> (stepElt 44)
| (KApply8(Lbl'_LT_'state'_GT_',(var_12_3204),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loopSep,((Int _ as var_4_3205) :: [])) :: var_13_3206)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_14_3207) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_15_3208) :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_16_3209) :: [])) :: []),(var_17_3210),(var_18_3211),(var_19_3212)) :: []) -> (let e = ((evalMap'Coln'lookup((var_0_2665),(KApply1(Lbl'_LT_'index'_GT_',(var_4_3205 :: [])) :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 44)
| (KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',((Int _ as var_4_3213) :: [])) :: []),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3214)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_6_3215) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_7_3216) :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_8_3217) :: [])) :: []),(var_9_3218),(var_10_3219),(var_11_3220)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'(((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),(KApply1(Lbl'_LT_'index'_GT_',(var_4_3213 :: [])) :: [])) config (-1))),e66) config (-1))) in match e with 
| [Bottom] -> (stepElt 44)
| ((Map (SortStateCellMap,_,_) as var_20_3221) :: []) when ((((((((true) && (true))) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (eval_'EqlsSlshEqls'K_((var_5_3214),(KApply1(Lbl'Hash'compare,(var_13_3206)) :: [])) config (-1)))) || ((isTrue (eval_'EqlsEqls'Bool__BOOL(((eval'Hash'compareS((var_15_3208 :: []),(var_7_3216 :: [])) config (-1))),((Bool false) :: [])) config (-1))))))))) && (((compare_kitem var_4_3213 var_4_3205) = 0) && ((compare var_12_3204 var_12_3203) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((eval_StateCellMap_(((evalStateCellMapItem((KApply1(Lbl'_LT_'index'_GT_',(var_4_3213 :: [])) :: []),(KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',(var_4_3213 :: [])) :: []),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3214)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3215 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_7_3216 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_8_3217 :: [])) :: []),(var_9_3218),(var_10_3219),(var_11_3220)) :: [])) config (-1))),((evalStateCellMapItem(e66,(KApply8(Lbl'_LT_'state'_GT_',e66,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loopSep,((eval_'Plus'Int_((var_4_3213 :: []),((Lazy.force int1) :: [])) config (-1)))) :: var_13_3206)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_14_3207 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_15_3208 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_16_3209 :: [])) :: []),(var_17_3210),(var_18_3211),(var_19_3212)) :: [])) config (-1)))) config (-1))),(var_20_3221 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 44))| _ -> (stepElt 44))| _ -> (stepElt 44))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(`newResource(_)_OSL-SYNTAX`(Ps)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lv(`newResource(_)_OSL-SYNTAX`(Ps))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isProps(Ps)) ensures #token("true","Bool") [UNIQUE_ID(49c921bc79f7df504d996bd00a12ba077ca601a2602294f622166c698d8675b0) contentStartColumn(6) contentStartLine(39) org.kframework.attributes.Location(Location(39,6,39,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3222) as e67 when guard < 45 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e67) config (-1))) in match e with 
| [Bottom] -> (stepElt 45)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3223),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lv,(KApply1(LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX,(var_5_3224 :: [])) :: [])) :: var_6_3225)) :: []),(var_7_3226),(var_8_3227),(var_9_3228),(var_10_3229),(var_11_3230),(var_12_3231)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e67) config (-1))) in match e with 
| [Bottom] -> (stepElt 45)
| ((Map (SortStateCellMap,_,_) as var_13_3232) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisProps((var_5_3224 :: [])) config (-1))))) && (((compare var_4_3222 var_4_3223) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e67,(KApply8(Lbl'_LT_'state'_GT_',e67,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX,(var_5_3224 :: [])) :: var_6_3225)) :: []),(var_7_3226),(var_8_3227),(var_9_3228),(var_10_3229),(var_11_3230),(var_12_3231)) :: [])) config (-1))),(var_13_3232 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 45))| _ -> (stepElt 45))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transfer(HOLE,K1)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#Transfer1_`(K1)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isK(HOLE),isExp(K1)),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(31a91b01ef5b0d20c60373d38a77e91b20de80f3c0a0a78af5a106bacabe2548) cool() klabel(#Transfer) org.kframework.attributes.Location(Location(35,12,35,38)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1021886708) strict()]|}*)
| (var_4_3233) as e68 when guard < 46 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e68) config (-1))) in match e with 
| [Bottom] -> (stepElt 46)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3234),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3235 :: KApply1(Lbl'Hash'freezer'Hash'Transfer1_,(var_6_3236 :: [])) :: var_7_3237)) :: []),(var_8_3238),(var_9_3239),(var_10_3240),(var_11_3241),(var_12_3242),(var_13_3243)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e68) config (-1))) in match e with 
| [Bottom] -> (stepElt 46)
| ((Map (SortStateCellMap,_,_) as var_14_3244) :: []) when ((((((true) && (true))) && (true))) && (((((true) && ((isTrue (evalisExp((var_6_3236 :: [])) config (-1)))))) && (((true) && ((isTrue (evalisKResult((var_5_3235 :: [])) config (-1))))))))) && (((compare var_4_3233 var_4_3234) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e68,(KApply8(Lbl'_LT_'state'_GT_',e68,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(var_5_3235 :: []),(var_6_3236 :: [])) :: var_7_3237)) :: []),(var_8_3238),(var_9_3239),(var_10_3240),(var_11_3241),(var_12_3242),(var_13_3243)) :: [])) config (-1))),(var_14_3244 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 46))| _ -> (stepElt 46))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#read(#loc(L))~>DotVar3),`<env>`(`_Map_`(`_|->_`(X,L),DotVar4)),_1,_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#read(X)~>DotVar3),`<env>`(_9),_1,_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(L,`Map:lookup`(_9,X))),#match(DotVar4,`_[_<-undef]`(_9,X))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isId(X),isInt(L))) ensures #token("true","Bool") [UNIQUE_ID(3fa14ead58d654e5a721d8bd333705e9b3c206cc0e7309d94ad66782b365b810) contentStartColumn(6) contentStartLine(166) org.kframework.attributes.Location(Location(166,6,167,39)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3245) as e69 when guard < 47 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e69) config (-1))) in match e with 
| [Bottom] -> (stepElt 47)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3246),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(var_7_3247 :: [])) :: var_6_3248)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_15_3249)) :: []),(var_9_3250),(var_10_3251),(var_11_3252),(var_12_3253),(var_13_3254)) :: []) -> (let e = ((evalMap'Coln'lookup((var_15_3249),(var_7_3247 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 47)
| ((Int _ as var_5_3255) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_15_3249),(var_7_3247 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 47)
| ((Map (SortMap,_,_) as var_8_3256) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e69) config (-1))) in match e with 
| [Bottom] -> (stepElt 47)
| ((Map (SortStateCellMap,_,_) as var_14_3257) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (evalisId((var_7_3247 :: [])) config (-1)))) && (true)))) && (((compare var_4_3246 var_4_3245) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e69,(KApply8(Lbl'_LT_'state'_GT_',e69,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,(var_5_3255 :: [])) :: [])) :: var_6_3248)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_3247 :: []),(var_5_3255 :: [])) config (-1))),(var_8_3256 :: [])) config (-1)))) :: []),(var_9_3250),(var_10_3251),(var_11_3252),(var_12_3253),(var_13_3254)) :: [])) config (-1))),(var_14_3257 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 47))| _ -> (stepElt 47))| _ -> (stepElt 47))| _ -> (stepElt 47))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(`#void_OSL-SYNTAX`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(V~>`#void_OSL-SYNTAX`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isValue(V)) ensures #token("true","Bool") [UNIQUE_ID(a317b06f426194f02e09bf4d67705fbcee304747025008ceae9e1e83e6b04fd4) contentStartColumn(6) contentStartLine(30) org.kframework.attributes.Location(Location(30,6,30,31)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/call.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3258) as e70 when guard < 48 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e70) config (-1))) in match e with 
| [Bottom] -> (stepElt 48)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3259),(KApply1(Lbl'_LT_'k'_GT_',(var_13_3260 :: KApply0(Lbl'Hash'void_OSL'Hyph'SYNTAX) :: var_5_3261)) :: []),(var_6_3262),(var_7_3263),(var_8_3264),(var_9_3265),(var_10_3266),(var_11_3267)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e70) config (-1))) in match e with 
| [Bottom] -> (stepElt 48)
| ((Map (SortStateCellMap,_,_) as var_12_3268) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisValue((var_13_3260 :: [])) config (-1))))) && (((compare var_4_3258 var_4_3259) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e70,(KApply8(Lbl'_LT_'state'_GT_',e70,(KApply1(Lbl'_LT_'k'_GT_',(const'Hash'void_OSL'Hyph'SYNTAX :: var_5_3261)) :: []),(var_6_3262),(var_7_3263),(var_8_3264),(var_9_3265),(var_10_3266),(var_11_3267)) :: [])) config (-1))),(var_12_3268 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 48))| _ -> (stepElt 48))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(V~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(V~>`.List{"___OSL-SYNTAX"}`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isValue(V)) ensures #token("true","Bool") [UNIQUE_ID(ba0cd2a310839620f3de02ba0066bfe27b5ba1d277bca3eecdc7e10f96cd03da) contentStartColumn(6) contentStartLine(23) org.kframework.attributes.Location(Location(23,6,23,28)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3269) as e71 when guard < 49 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e71) config (-1))) in match e with 
| [Bottom] -> (stepElt 49)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3270),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3271 :: KApply0(Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra') :: var_6_3272)) :: []),(var_7_3273),(var_8_3274),(var_9_3275),(var_10_3276),(var_11_3277),(var_12_3278)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e71) config (-1))) in match e with 
| [Bottom] -> (stepElt 49)
| ((Map (SortStateCellMap,_,_) as var_13_3279) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisValue((var_5_3271 :: [])) config (-1))))) && (((compare var_4_3269 var_4_3270) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e71,(KApply8(Lbl'_LT_'state'_GT_',e71,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3271 :: var_6_3272)) :: []),(var_7_3273),(var_8_3274),(var_9_3275),(var_10_3276),(var_11_3277),(var_12_3278)) :: [])) config (-1))),(var_13_3279 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 49))| _ -> (stepElt 49))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,`#uninit_OSL-SYNTAX`(.KList)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Transferuninit(`#uninit_OSL-SYNTAX`(.KList),#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(_79,`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isKItem(_79),isInt(L))) ensures #token("true","Bool") [UNIQUE_ID(e1a3613f961632c0aaeb2da5d233e2d450bf4569257c10bcaffe80452458cfa3) contentStartColumn(6) contentStartLine(125) org.kframework.attributes.Location(Location(125,6,126,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3280) as e72 when guard < 50 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e72) config (-1))) in match e with 
| [Bottom] -> (stepElt 50)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3281),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transferuninit,(KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_7_3282) :: [])) :: [])) :: var_5_3283)) :: []),(var_6_3284),(KApply1(Lbl'_LT_'store'_GT_',(var_14_3285)) :: []),(var_9_3286),(var_10_3287),(var_11_3288),(var_12_3289)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_14_3285),(var_7_3282 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 50)
| ((Map (SortMap,_,_) as var_8_3290) :: []) -> (let e = ((evalMap'Coln'lookup((var_14_3285),(var_7_3282 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 50)
| (var_15_3291 :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e72) config (-1))) in match e with 
| [Bottom] -> (stepElt 50)
| ((Map (SortStateCellMap,_,_) as var_13_3292) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((true) && (true)))) && (((compare var_4_3280 var_4_3281) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e72,(KApply8(Lbl'_LT_'state'_GT_',e72,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3283)) :: []),(var_6_3284),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_3282 :: []),(const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1))),(var_8_3290 :: [])) config (-1)))) :: []),(var_9_3286),(var_10_3287),(var_11_3288),(var_12_3289)) :: [])) config (-1))),(var_13_3292 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 50))| _ -> (stepElt 50))| _ -> (stepElt 50))| _ -> (stepElt 50))
(*{| rule `<T>`(`<states>`(``_6=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),`<env>`(ENV),`<store>`(`_[_<-_]_MAP`(ST,#unwrapInt(`Map:lookup`(ENV,X)),#br(C,C,#immRef(#unwrapInt(`Map:lookup`(ENV,Y)))))),_1,_2,`<timer>`(`_+Int_`(TIMER,#token("1","Int"))),`<indexes>`(#indexes(`_+Int_`(C,#token("1","Int")),_72)))),DotVar1)``),_3,_4,_5) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_6),#match(`<state>`(_0,`<k>`(#borrow(X,Y)~>DotVar3),`<env>`(ENV),`<store>`(ST),_1,_2,`<timer>`(TIMER),`<indexes>`(#indexes(C,_72))),`Map:lookup`(_6,_0))),#match(DotVar1,`_[_<-undef]`(_6,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(TIMER),isId(Y)),isInt(C)),isMap(ENV)),isId(X)),isInt(_72)),isMap(ST)),#checkInit(Y,ENV,ST))) ensures #token("true","Bool") [UNIQUE_ID(dea1e408212d8cf948839738cd864661ced24bb02371d787c3dd149073cf085a) contentStartColumn(6) contentStartLine(337) org.kframework.attributes.Location(Location(337,6,344,35)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_3293) as e73 when guard < 51 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e73) config (-1))) in match e with 
| [Bottom] -> (stepElt 51)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3294),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'borrow,(var_8_3295 :: []),(var_10_3296 :: [])) :: var_5_3297)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_6_3298) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_7_3299) :: [])) :: []),(var_11_3300),(var_12_3301),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_13_3302) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_9_3303) :: []),((Int _ as var_14_3304) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e73) config (-1))) in match e with 
| [Bottom] -> (stepElt 51)
| ((Map (SortStateCellMap,_,_) as var_15_3305) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((true) && ((isTrue (evalisId((var_10_3296 :: [])) config (-1)))))) && (true))) && (true))) && ((isTrue (evalisId((var_8_3295 :: [])) config (-1)))))) && (true))) && (true))) && ((isTrue (eval'Hash'checkInit((var_10_3296 :: []),(var_6_3298 :: []),(var_7_3299 :: [])) config (-1))))))) && (((compare var_4_3293 var_4_3294) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e73,(KApply8(Lbl'_LT_'state'_GT_',e73,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3297)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3298 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP((var_7_3299 :: []),((eval'Hash'unwrapInt(((evalMap'Coln'lookup((var_6_3298 :: []),(var_8_3295 :: [])) config (-1)))) config (-1))),(KApply3(Lbl'Hash'br,(var_9_3303 :: []),(var_9_3303 :: []),(KApply1(Lbl'Hash'immRef,((eval'Hash'unwrapInt(((evalMap'Coln'lookup((var_6_3298 :: []),(var_10_3296 :: [])) config (-1)))) config (-1)))) :: [])) :: [])) config (-1)))) :: []),(var_11_3300),(var_12_3301),(KApply1(Lbl'_LT_'timer'_GT_',((eval_'Plus'Int_((var_13_3302 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((eval_'Plus'Int_((var_9_3303 :: []),((Lazy.force int1) :: [])) config (-1))),(var_14_3304 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_15_3305 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 51))| _ -> (stepElt 51))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#TransferMB(#read(#loc(F)),#loc(L))~>#uninitialize(#loc(F))~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(F,#br(BEG,END,#mutRef(L1))),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#TransferV(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(#br(BEG,END,#mutRef(L1)),`Map:lookup`(_9,F))),#match(DotVar4,`_[_<-undef]`(_9,F))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(END),isInt(F)),isInt(L)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(60f8feb45e6ffc3325af23dcc421e7d8769da811013a7ff665c1222bc8a6564c) contentStartColumn(6) contentStartLine(145) org.kframework.attributes.Location(Location(145,6,148,61)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3306) as e74 when guard < 52 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e74) config (-1))) in match e with 
| [Bottom] -> (stepElt 52)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3307),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferV,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3308) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_6_3309) :: [])) :: [])) :: var_7_3310)) :: []),(var_8_3311),(KApply1(Lbl'_LT_'store'_GT_',(var_18_3312)) :: []),(var_13_3313),(var_14_3314),(var_15_3315),(var_16_3316)) :: []) -> (let e = ((evalMap'Coln'lookup((var_18_3312),(var_5_3308 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 52)
| (KApply3(Lbl'Hash'br,((Int _ as var_9_3317) :: []),((Int _ as var_10_3318) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_11_3319) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_18_3312),(var_5_3308 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 52)
| ((Map (SortMap,_,_) as var_12_3320) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e74) config (-1))) in match e with 
| [Bottom] -> (stepElt 52)
| ((Map (SortStateCellMap,_,_) as var_17_3321) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((((true) && (true))) && (true))) && (true))) && (true)))) && (((compare var_4_3306 var_4_3307) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e74,(KApply8(Lbl'_LT_'state'_GT_',e74,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferMB,(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,(var_5_3308 :: [])) :: [])) :: []),(KApply1(Lbl'Hash'loc,(var_6_3309 :: [])) :: [])) :: KApply1(Lbl'Hash'uninitialize,(KApply1(Lbl'Hash'loc,(var_5_3308 :: [])) :: [])) :: var_7_3310)) :: []),(var_8_3311),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_5_3308 :: []),(KApply3(Lbl'Hash'br,(var_9_3317 :: []),(var_10_3318 :: []),(KApply1(Lbl'Hash'mutRef,(var_11_3319 :: [])) :: [])) :: [])) config (-1))),(var_12_3320 :: [])) config (-1)))) :: []),(var_13_3313),(var_14_3314),(var_15_3315),(var_16_3316)) :: [])) config (-1))),(var_17_3321 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 52))| _ -> (stepElt 52))| _ -> (stepElt 52))| _ -> (stepElt 52))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#TransferV(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(STORE),_2,_3,_4,`<indexes>`(#indexes(C,_82)))),DotVar1)``),_5,_6,_7) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(#Transfer(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(STORE),_2,_3,_4,`<indexes>`(#indexes(C,_82))),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(C),isInt(F)),isMap(STORE)),isInt(L)),isInt(_82)),`_andBool_`(`notBool_`(#existRef(#immRef(F),#list2Set(values(STORE)),C)),`notBool_`(#existRef(#mutRef(F),#list2Set(values(STORE)),C))))) ensures #token("true","Bool") [UNIQUE_ID(bea2b5111edbe602db9e0577ab63a7db98575b5239631462c9ba94f097c8f2ea) contentStartColumn(6) contentStartLine(108) org.kframework.attributes.Location(Location(108,6,112,67)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_3322) as e75 when guard < 53 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e75) config (-1))) in match e with 
| [Bottom] -> (stepElt 53)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3323),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3324) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_6_3325) :: [])) :: [])) :: var_7_3326)) :: []),(var_8_3327),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_9_3328) :: [])) :: []),(var_10_3329),(var_11_3330),(var_12_3331),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_13_3332) :: []),((Int _ as var_14_3333) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e75) config (-1))) in match e with 
| [Bottom] -> (stepElt 53)
| ((Map (SortStateCellMap,_,_) as var_15_3334) :: []) when ((((((true) && (true))) && (true))) && (((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((not ((isTrue (eval'Hash'existRef((KApply1(Lbl'Hash'immRef,(var_5_3324 :: [])) :: []),((eval'Hash'list2Set(((evalvalues((var_9_3328 :: [])) config (-1)))) config (-1))),(var_13_3332 :: [])) config (-1)))))) && ((not ((isTrue (eval'Hash'existRef((KApply1(Lbl'Hash'mutRef,(var_5_3324 :: [])) :: []),((eval'Hash'list2Set(((evalvalues((var_9_3328 :: [])) config (-1)))) config (-1))),(var_13_3332 :: [])) config (-1))))))))))) && (((compare var_4_3323 var_4_3322) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e75,(KApply8(Lbl'_LT_'state'_GT_',e75,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferV,(KApply1(Lbl'Hash'loc,(var_5_3324 :: [])) :: []),(KApply1(Lbl'Hash'loc,(var_6_3325 :: [])) :: [])) :: var_7_3326)) :: []),(var_8_3327),(KApply1(Lbl'_LT_'store'_GT_',(var_9_3328 :: [])) :: []),(var_10_3329),(var_11_3330),(var_12_3331),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_13_3332 :: []),(var_14_3333 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_15_3334 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 53))| _ -> (stepElt 53))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#borrowImmCK(L,BEG,END,L1)~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#br(BEG,END,#immRef(L1))),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lvDref(#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(#br(BEG,END,#immRef(L1)),`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isInt(END),isInt(L)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(311563087fa06964cc0a7e15ad73beec4125de80901afe8ca01282811b731f9c) contentStartColumn(6) contentStartLine(242) org.kframework.attributes.Location(Location(242,6,243,75)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3335) as e76 when guard < 54 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e76) config (-1))) in match e with 
| [Bottom] -> (stepElt 54)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3336),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lvDref,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3337) :: [])) :: [])) :: var_9_3338)) :: []),(var_10_3339),(KApply1(Lbl'_LT_'store'_GT_',(var_17_3340)) :: []),(var_12_3341),(var_13_3342),(var_14_3343),(var_15_3344)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_17_3340),(var_5_3337 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 54)
| ((Map (SortMap,_,_) as var_11_3345) :: []) -> (let e = ((evalMap'Coln'lookup((var_17_3340),(var_5_3337 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 54)
| (KApply3(Lbl'Hash'br,((Int _ as var_6_3346) :: []),((Int _ as var_7_3347) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_8_3348) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e76) config (-1))) in match e with 
| [Bottom] -> (stepElt 54)
| ((Map (SortStateCellMap,_,_) as var_16_3349) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((true) && (true))) && (true))) && (true)))) && (((compare var_4_3335 var_4_3336) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e76,(KApply8(Lbl'_LT_'state'_GT_',e76,(KApply1(Lbl'_LT_'k'_GT_',(KApply4(Lbl'Hash'borrowImmCK,(var_5_3337 :: []),(var_6_3346 :: []),(var_7_3347 :: []),(var_8_3348 :: [])) :: var_9_3338)) :: []),(var_10_3339),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_5_3337 :: []),(KApply3(Lbl'Hash'br,(var_6_3346 :: []),(var_7_3347 :: []),(KApply1(Lbl'Hash'immRef,(var_8_3348 :: [])) :: [])) :: [])) config (-1))),(var_11_3345 :: [])) config (-1)))) :: []),(var_12_3341),(var_13_3342),(var_14_3343),(var_15_3344)) :: [])) config (-1))),(var_16_3349 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 54))| _ -> (stepElt 54))| _ -> (stepElt 54))| _ -> (stepElt 54))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#loc(L)~>DotVar3),`<env>`(`_Map_`(`_|->_`(X,L),DotVar4)),_1,_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#lv(X)~>DotVar3),`<env>`(_9),_1,_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(L,`Map:lookup`(_9,X))),#match(DotVar4,`_[_<-undef]`(_9,X))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(X),isInt(L))) ensures #token("true","Bool") [UNIQUE_ID(1bf6ca92cd4edf6de4fae941359229599d4c96b05161d8ac101c81cd6d5ab905) contentStartColumn(6) contentStartLine(231) org.kframework.attributes.Location(Location(231,6,232,38)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3350) as e77 when guard < 55 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e77) config (-1))) in match e with 
| [Bottom] -> (stepElt 55)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3351),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'lv,(var_7_3352 :: [])) :: var_6_3353)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_15_3354)) :: []),(var_9_3355),(var_10_3356),(var_11_3357),(var_12_3358),(var_13_3359)) :: []) -> (let e = ((evalMap'Coln'lookup((var_15_3354),(var_7_3352 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 55)
| ((Int _ as var_5_3360) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_15_3354),(var_7_3352 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 55)
| ((Map (SortMap,_,_) as var_8_3361) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e77) config (-1))) in match e with 
| [Bottom] -> (stepElt 55)
| ((Map (SortStateCellMap,_,_) as var_14_3362) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((isTrue (evalisExp((var_7_3352 :: [])) config (-1)))) && (true)))) && (((compare var_4_3350 var_4_3351) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e77,(KApply8(Lbl'_LT_'state'_GT_',e77,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loc,(var_5_3360 :: [])) :: var_6_3353)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_3352 :: []),(var_5_3360 :: [])) config (-1))),(var_8_3361 :: [])) config (-1)))) :: []),(var_9_3355),(var_10_3356),(var_11_3357),(var_12_3358),(var_13_3359)) :: [])) config (-1))),(var_14_3362 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 55))| _ -> (stepElt 55))| _ -> (stepElt 55))| _ -> (stepElt 55))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,`<store>`(`_[_<-_]_MAP`(Rho,L,#br(C,C,#mutRef(F)))),_2,_3,_4,`<indexes>`(#indexes(C,_80)))),DotVar1)``),_5,_6,_7) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(#TransferMB(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(Rho),_2,_3,_4,`<indexes>`(#indexes(C,_80))),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isMap(Rho),isInt(C)),isInt(F)),isInt(L)),isInt(_80))) ensures #token("true","Bool") [UNIQUE_ID(8b2879958e627c0caf2de54d9f6c1f2b92d56140edfd7bc532b473ea975606c4) contentStartColumn(6) contentStartLine(155) org.kframework.attributes.Location(Location(155,6,157,49)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3363) as e78 when guard < 56 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e78) config (-1))) in match e with 
| [Bottom] -> (stepElt 56)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3364),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferMB,(KApply1(Lbl'Hash'loc,((Int _ as var_10_3365) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_8_3366) :: [])) :: [])) :: var_5_3367)) :: []),(var_6_3368),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_7_3369) :: [])) :: []),(var_11_3370),(var_12_3371),(var_13_3372),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_9_3373) :: []),((Int _ as var_14_3374) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e78) config (-1))) in match e with 
| [Bottom] -> (stepElt 56)
| ((Map (SortStateCellMap,_,_) as var_15_3375) :: []) when ((((((true) && (true))) && (true))) && (((((((((true) && (true))) && (true))) && (true))) && (true)))) && (((compare var_4_3363 var_4_3364) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e78,(KApply8(Lbl'_LT_'state'_GT_',e78,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3367)) :: []),(var_6_3368),(KApply1(Lbl'_LT_'store'_GT_',((eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP((var_7_3369 :: []),(var_8_3366 :: []),(KApply3(Lbl'Hash'br,(var_9_3373 :: []),(var_9_3373 :: []),(KApply1(Lbl'Hash'mutRef,(var_10_3365 :: [])) :: [])) :: [])) config (-1)))) :: []),(var_11_3370),(var_12_3371),(var_13_3372),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_9_3373 :: []),(var_14_3374 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_15_3375 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 56))| _ -> (stepElt 56))
(*{| rule `<T>`(`<states>`(``_6=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),`<env>`(`_[_<-_]_MAP`(Rho,X,N)),`<store>`(`_Map_`(`_|->_`(N,`#uninit_OSL-SYNTAX`(.KList)),DotVar4)),`<stack>`(`_List_`(`ListItem`(X),DotVar5)),_1,`<timer>`(`_+Int_`(TI,#token("1","Int"))),`<indexes>`(#indexes(`_+Int_`(N,#token("1","Int")),_68)))),DotVar1)``),_2,_3,_4) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_6),#match(`<state>`(_0,`<k>`(#decl(X)~>DotVar3),`<env>`(Rho),`<store>`(_5),`<stack>`(DotVar5),_1,`<timer>`(TI),`<indexes>`(#indexes(N,_68))),`Map:lookup`(_6,_0))),#match(DotVar1,`_[_<-undef]`(_6,_0))),#match(DotVar4,_5)),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(TI),isMap(Rho)),isInt(N)),isInt(_68)),isId(X))) ensures #token("true","Bool") [UNIQUE_ID(781a132fd4e961f96969c620d3e3eaec1adfcb62cbc20d38535d4caabb8704c0) contentStartColumn(6) contentStartLine(27) org.kframework.attributes.Location(Location(27,6,32,59)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3376) as e79 when guard < 57 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e79) config (-1))) in match e with 
| [Bottom] -> (stepElt 57)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3377),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'decl,(var_7_3378 :: [])) :: var_5_3379)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_6_3380) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_15_3381)) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_10_3382) :: [])) :: []),(var_11_3383),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_12_3384) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_8_3385) :: []),((Int _ as var_13_3386) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e79) config (-1))) in match e with 
| [Bottom] -> (stepElt 57)
| ((Map (SortStateCellMap,_,_) as var_14_3387) :: []) -> (let e = (var_15_3381) in match e with 
| [Bottom] -> (stepElt 57)
| ((Map (SortMap,_,_) as var_9_3388) :: []) when ((((((((true) && (true))) && (true))) && (true))) && (((((((((true) && (true))) && (true))) && (true))) && ((isTrue (evalisId((var_7_3378 :: [])) config (-1))))))) && (((compare var_4_3376 var_4_3377) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e79,(KApply8(Lbl'_LT_'state'_GT_',e79,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3379)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP((var_6_3380 :: []),(var_7_3378 :: []),(var_8_3385 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_8_3385 :: []),(const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1))),(var_9_3388 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((eval_List_(((evalListItem((var_7_3378 :: [])) config (-1))),(var_10_3382 :: [])) config (-1)))) :: []),(var_11_3383),(KApply1(Lbl'_LT_'timer'_GT_',((eval_'Plus'Int_((var_12_3384 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((eval_'Plus'Int_((var_8_3385 :: []),((Lazy.force int1) :: [])) config (-1))),(var_13_3386 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_14_3387 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 57))| _ -> (stepElt 57))| _ -> (stepElt 57))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transfer(#lv(EF),#lv(TX))~>`#increaseIndex_OSL`(.KList)~>`#increaseTimer_OSL`(.KList)~>DotVar3),`<env>`(ENV),_1,_2,`<write>`(`_Set_`(`SetItem`(#writev(#wv(TX,ENV),TR)),DotVar4)),`<timer>`(TR),_3)),DotVar1)``),_4,_5,_6) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(#transfer(EF,TX)~>DotVar3),`<env>`(ENV),_1,_2,`<write>`(_7),`<timer>`(TR),_3),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),#match(DotVar4,_7)),`_andBool_`(`_andBool_`(`_andBool_`(isMap(ENV),isExp(EF)),isExp(TX)),isInt(TR))) ensures #token("true","Bool") [UNIQUE_ID(5cb419709f4baf0ffd8e167b9c996701fcb35dac201e21ee29cbc2705792b2fa) contentStartColumn(6) contentStartLine(44) org.kframework.attributes.Location(Location(44,6,48,68)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3389) as e80 when guard < 58 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e80) config (-1))) in match e with 
| [Bottom] -> (stepElt 58)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3390),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'transfer,(var_5_3391 :: []),(var_6_3392 :: [])) :: var_7_3393)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_8_3394) :: [])) :: []),(var_9_3395),(var_10_3396),(KApply1(Lbl'_LT_'write'_GT_',(var_15_3397)) :: []),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_11_3398) :: [])) :: []),(var_13_3399)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e80) config (-1))) in match e with 
| [Bottom] -> (stepElt 58)
| ((Map (SortStateCellMap,_,_) as var_14_3400) :: []) -> (let e = (var_15_3397) in match e with 
| [Bottom] -> (stepElt 58)
| ((Set (SortSet,_,_) as var_12_3401) :: []) when ((((((((true) && (true))) && (true))) && (true))) && (((((((true) && ((isTrue (evalisExp((var_5_3391 :: [])) config (-1)))))) && ((isTrue (evalisExp((var_6_3392 :: [])) config (-1)))))) && (true)))) && (((compare var_4_3389 var_4_3390) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e80,(KApply8(Lbl'_LT_'state'_GT_',e80,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(KApply1(Lbl'Hash'lv,(var_5_3391 :: [])) :: []),(KApply1(Lbl'Hash'lv,(var_6_3392 :: [])) :: [])) :: const'Hash'increaseIndex_OSL :: const'Hash'increaseTimer_OSL :: var_7_3393)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_8_3394 :: [])) :: []),(var_9_3395),(var_10_3396),(KApply1(Lbl'_LT_'write'_GT_',((eval_Set_(((evalSetItem((KApply2(Lbl'Hash'writev,((eval'Hash'wv((var_6_3392 :: []),(var_8_3394 :: [])) config (-1))),(var_11_3398 :: [])) :: [])) config (-1))),(var_12_3401 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_11_3398 :: [])) :: []),(var_13_3399)) :: [])) config (-1))),(var_14_3400 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 58))| _ -> (stepElt 58))| _ -> (stepElt 58))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#rs(Ps)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(`newResource(_)_OSL-SYNTAX`(Ps)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isProps(Ps)) ensures #token("true","Bool") [UNIQUE_ID(8e50f140a5c4a28497b310a39fef8f99f631d432e37429b7c8407be34e4605ec) contentStartColumn(6) contentStartLine(17) org.kframework.attributes.Location(Location(17,6,17,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3402) as e81 when guard < 59 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e81) config (-1))) in match e with 
| [Bottom] -> (stepElt 59)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3403),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX,(var_5_3404 :: [])) :: var_6_3405)) :: []),(var_7_3406),(var_8_3407),(var_9_3408),(var_10_3409),(var_11_3410),(var_12_3411)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e81) config (-1))) in match e with 
| [Bottom] -> (stepElt 59)
| ((Map (SortStateCellMap,_,_) as var_13_3412) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisProps((var_5_3404 :: [])) config (-1))))) && (((compare var_4_3402 var_4_3403) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e81,(KApply8(Lbl'_LT_'state'_GT_',e81,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'rs,(var_5_3404 :: [])) :: var_6_3405)) :: []),(var_7_3406),(var_8_3407),(var_9_3408),(var_10_3409),(var_11_3410),(var_12_3411)) :: [])) config (-1))),(var_13_3412 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 59))| _ -> (stepElt 59))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#TransferIB(HOLE,K1)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#TransferIB1_`(K1)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isK(HOLE),isK(K1)),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(2c536c23c3867d73948f213fefcca335737876b051501ad6bb772ceda903eeb1) cool() klabel(#TransferIB) org.kframework.attributes.Location(Location(138,12,138,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1533644280) strict(1)]|}*)
| (var_4_3413) as e82 when guard < 60 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e82) config (-1))) in match e with 
| [Bottom] -> (stepElt 60)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3414),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3415 :: KApply1(Lbl'Hash'freezer'Hash'TransferIB1_,(var_6_3416)) :: var_7_3417)) :: []),(var_8_3418),(var_9_3419),(var_10_3420),(var_11_3421),(var_12_3422),(var_13_3423)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e82) config (-1))) in match e with 
| [Bottom] -> (stepElt 60)
| ((Map (SortStateCellMap,_,_) as var_14_3424) :: []) when ((((((true) && (true))) && (true))) && (((((true) && (true))) && (((true) && ((isTrue (evalisKResult((var_5_3415 :: [])) config (-1))))))))) && (((compare var_4_3413 var_4_3414) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e82,(KApply8(Lbl'_LT_'state'_GT_',e82,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferIB,(var_5_3415 :: []),(var_6_3416)) :: var_7_3417)) :: []),(var_8_3418),(var_9_3419),(var_10_3420),(var_11_3421),(var_12_3422),(var_13_3423)) :: [])) config (-1))),(var_14_3424 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 60))| _ -> (stepElt 60))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Transfer(#rs(PS),#loc(L))~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(F,`#uninit_OSL-SYNTAX`(.KList)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#TransferV(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,F))),#match(#rs(PS),`Map:lookup`(_9,F))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isProps(PS),isInt(F)),isInt(L)),`notBool_`(#inProps(`copy_OSL-SYNTAX`(.KList),PS)))) ensures #token("true","Bool") [UNIQUE_ID(4af0fe18591122bf96de73ecbd7d1324ca73f8bf4442ff8d6032de6ab340fcad) contentStartColumn(6) contentStartLine(128) org.kframework.attributes.Location(Location(128,6,130,41)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_3425) as e83 when guard < 61 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e83) config (-1))) in match e with 
| [Bottom] -> (stepElt 61)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3426),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferV,(KApply1(Lbl'Hash'loc,((Int _ as var_9_3427) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_6_3428) :: [])) :: [])) :: var_7_3429)) :: []),(var_8_3430),(KApply1(Lbl'_LT_'store'_GT_',(var_16_3431)) :: []),(var_11_3432),(var_12_3433),(var_13_3434),(var_14_3435)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_16_3431),(var_9_3427 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 61)
| ((Map (SortMap,_,_) as var_10_3436) :: []) -> (let e = ((evalMap'Coln'lookup((var_16_3431),(var_9_3427 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 61)
| (KApply1(Lbl'Hash'rs,(var_5_3437 :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e83) config (-1))) in match e with 
| [Bottom] -> (stepElt 61)
| ((Map (SortStateCellMap,_,_) as var_15_3438) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((((((isTrue (evalisProps((var_5_3437 :: [])) config (-1)))) && (true))) && (true))) && ((not ((isTrue (eval'Hash'inProps((constcopy_OSL'Hyph'SYNTAX :: []),(var_5_3437 :: [])) config (-1))))))))) && (((compare var_4_3426 var_4_3425) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e83,(KApply8(Lbl'_LT_'state'_GT_',e83,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(KApply1(Lbl'Hash'rs,(var_5_3437 :: [])) :: []),(KApply1(Lbl'Hash'loc,(var_6_3428 :: [])) :: [])) :: var_7_3429)) :: []),(var_8_3430),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_9_3427 :: []),(const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1))),(var_10_3436 :: [])) config (-1)))) :: []),(var_11_3432),(var_12_3433),(var_13_3434),(var_14_3435)) :: [])) config (-1))),(var_15_3438 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 61))| _ -> (stepElt 61))| _ -> (stepElt 61))| _ -> (stepElt 61))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#loopSep(`_+Int_`(N,#token("1","Int")))~>Rest),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#loopSep(N)~>Rest),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isInt(N),isK(Rest))) ensures #token("true","Bool") [UNIQUE_ID(95155f8e1d3e57b6352e0a6ff1e1ae07491e461d5e5a801b399f665f55b3f61d) contentStartColumn(6) contentStartLine(80) org.kframework.attributes.Location(Location(80,6,80,71)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3439) as e84 when guard < 62 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e84) config (-1))) in match e with 
| [Bottom] -> (stepElt 62)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3440),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loopSep,((Int _ as var_5_3441) :: [])) :: var_6_3442)) :: []),(var_7_3443),(var_8_3444),(var_9_3445),(var_10_3446),(var_11_3447),(var_12_3448)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e84) config (-1))) in match e with 
| [Bottom] -> (stepElt 62)
| ((Map (SortStateCellMap,_,_) as var_13_3449) :: []) when ((((((true) && (true))) && (true))) && (((true) && (true)))) && (((compare var_4_3440 var_4_3439) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e84,(KApply8(Lbl'_LT_'state'_GT_',e84,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loopSep,((eval_'Plus'Int_((var_5_3441 :: []),((Lazy.force int1) :: [])) config (-1)))) :: var_6_3442)) :: []),(var_7_3443),(var_8_3444),(var_9_3445),(var_10_3446),(var_11_3447),(var_12_3448)) :: [])) config (-1))),(var_13_3449 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 62))| _ -> (stepElt 62))
(*{| rule `<T>`(`<states>`(``_8=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(Ss~>`#blockend_BLOCK`(.KList)~>DotVar3),`<env>`(ENV),_1,`<stack>`(`_List_`(`ListItem`(ENV),DotVar4)),_2,_3,_4)),DotVar1)``),_5,_6,_7) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_8),#match(`<state>`(_0,`<k>`(#block(Ss)~>DotVar3),`<env>`(ENV),_1,`<stack>`(DotVar4),_2,_3,_4),`Map:lookup`(_8,_0))),#match(DotVar1,`_[_<-undef]`(_8,_0))),`_andBool_`(isMap(ENV),isStmts(Ss))) ensures #token("true","Bool") [UNIQUE_ID(808ffa833cb26a5dfa6a0b2e9d7282c710af0d1d2442297dfd1f7697e127b9ba) contentStartColumn(6) contentStartLine(13) org.kframework.attributes.Location(Location(13,6,15,49)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/block.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3450) as e85 when guard < 63 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e85) config (-1))) in match e with 
| [Bottom] -> (stepElt 63)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3451),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'block,(var_5_3452 :: [])) :: var_6_3453)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_7_3454) :: [])) :: []),(var_8_3455),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_9_3456) :: [])) :: []),(var_10_3457),(var_11_3458),(var_12_3459)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e85) config (-1))) in match e with 
| [Bottom] -> (stepElt 63)
| ((Map (SortStateCellMap,_,_) as var_13_3460) :: []) when ((((((true) && (true))) && (true))) && (((true) && ((isTrue (evalisStmts((var_5_3452 :: [])) config (-1))))))) && (((compare var_4_3451 var_4_3450) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e85,(KApply8(Lbl'_LT_'state'_GT_',e85,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3452 :: const'Hash'blockend_BLOCK :: var_6_3453)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_7_3454 :: [])) :: []),(var_8_3455),(KApply1(Lbl'_LT_'stack'_GT_',((eval_List_(((evalListItem((var_7_3454 :: [])) config (-1))),(var_9_3456 :: [])) config (-1)))) :: []),(var_10_3457),(var_11_3458),(var_12_3459)) :: [])) config (-1))),(var_13_3460 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 63))| _ -> (stepElt 63))
(*{| rule `<T>`(`<states>`(``_6=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),`<env>`(ENV),`<store>`(`_[_<-_]_MAP`(ST,#unwrapInt(`Map:lookup`(ENV,X)),#br(C,C,#mutRef(#unwrapInt(`Map:lookup`(ENV,Y)))))),_1,_2,`<timer>`(`_+Int_`(TIMER,#token("1","Int"))),`<indexes>`(#indexes(`_+Int_`(C,#token("1","Int")),_87)))),DotVar1)``),_3,_4,_5) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_6),#match(`<state>`(_0,`<k>`(#mborrow(X,Y)~>DotVar3),`<env>`(ENV),`<store>`(ST),_1,_2,`<timer>`(TIMER),`<indexes>`(#indexes(C,_87))),`Map:lookup`(_6,_0))),#match(DotVar1,`_[_<-undef]`(_6,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(TIMER),isId(Y)),isInt(C)),isMap(ENV)),isId(X)),isInt(_87)),isMap(ST)),#checkInit(Y,ENV,ST))) ensures #token("true","Bool") [UNIQUE_ID(fd337401940d9f255df0ad0b4f66d92e009040616cd4a9d8368af9b2bec5cd39) contentStartColumn(6) contentStartLine(348) org.kframework.attributes.Location(Location(348,6,353,35)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_3461) as e86 when guard < 64 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e86) config (-1))) in match e with 
| [Bottom] -> (stepElt 64)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3462),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'mborrow,(var_8_3463 :: []),(var_10_3464 :: [])) :: var_5_3465)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_6_3466) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_7_3467) :: [])) :: []),(var_11_3468),(var_12_3469),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_13_3470) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_9_3471) :: []),((Int _ as var_14_3472) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e86) config (-1))) in match e with 
| [Bottom] -> (stepElt 64)
| ((Map (SortStateCellMap,_,_) as var_15_3473) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((true) && ((isTrue (evalisId((var_10_3464 :: [])) config (-1)))))) && (true))) && (true))) && ((isTrue (evalisId((var_8_3463 :: [])) config (-1)))))) && (true))) && (true))) && ((isTrue (eval'Hash'checkInit((var_10_3464 :: []),(var_6_3466 :: []),(var_7_3467 :: [])) config (-1))))))) && (((compare var_4_3462 var_4_3461) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e86,(KApply8(Lbl'_LT_'state'_GT_',e86,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3465)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3466 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((eval_'LSqB'_'_LT_Hyph'_'RSqB'_MAP((var_7_3467 :: []),((eval'Hash'unwrapInt(((evalMap'Coln'lookup((var_6_3466 :: []),(var_8_3463 :: [])) config (-1)))) config (-1))),(KApply3(Lbl'Hash'br,(var_9_3471 :: []),(var_9_3471 :: []),(KApply1(Lbl'Hash'mutRef,((eval'Hash'unwrapInt(((evalMap'Coln'lookup((var_6_3466 :: []),(var_10_3464 :: [])) config (-1)))) config (-1)))) :: [])) :: [])) config (-1)))) :: []),(var_11_3468),(var_12_3469),(KApply1(Lbl'_LT_'timer'_GT_',((eval_'Plus'Int_((var_13_3470 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((eval_'Plus'Int_((var_9_3471 :: []),((Lazy.force int1) :: [])) config (-1))),(var_14_3472 :: [])) :: [])) :: [])) :: [])) config (-1))),(var_15_3473 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 64))| _ -> (stepElt 64))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#secondBranch(Bs)~>B~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#branch(B,Bs)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isBlock(B),isBlocks(Bs))) ensures #token("true","Bool") [UNIQUE_ID(487c1b298452ec40f59a0bd3b24f2952bb47f604ec5a03c55ceb202fa0679d40) contentStartColumn(6) contentStartLine(10) org.kframework.attributes.Location(Location(10,6,10,59)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3474) as e87 when guard < 65 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e87) config (-1))) in match e with 
| [Bottom] -> (stepElt 65)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3475),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'branch,(var_6_3476 :: []),(var_5_3477 :: [])) :: var_7_3478)) :: []),(var_8_3479),(var_9_3480),(var_10_3481),(var_11_3482),(var_12_3483),(var_13_3484)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e87) config (-1))) in match e with 
| [Bottom] -> (stepElt 65)
| ((Map (SortStateCellMap,_,_) as var_14_3485) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisBlock((var_6_3476 :: [])) config (-1)))) && ((isTrue (evalisBlocks((var_5_3477 :: [])) config (-1))))))) && (((compare var_4_3474 var_4_3475) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e87,(KApply8(Lbl'_LT_'state'_GT_',e87,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'secondBranch,(var_5_3477 :: [])) :: var_6_3476 :: var_7_3478)) :: []),(var_8_3479),(var_9_3480),(var_10_3481),(var_11_3482),(var_12_3483),(var_13_3484)) :: [])) config (-1))),(var_14_3485 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 65))| _ -> (stepElt 65))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#TransferIB(#read(#loc(F)),#loc(L))~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(F,#br(BEG,END,#immRef(L1))),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#TransferV(#loc(F),#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,F))),#match(#br(BEG,END,#immRef(L1)),`Map:lookup`(_9,F))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(END),isInt(F)),isInt(L)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(94336861b840d096a8efbc4d1f5998623d9d19f056d669ff7d2ba7d39e8ff3c7) contentStartColumn(6) contentStartLine(142) org.kframework.attributes.Location(Location(142,6,143,61)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3486) as e88 when guard < 66 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e88) config (-1))) in match e with 
| [Bottom] -> (stepElt 66)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3487),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferV,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3488) :: [])) :: []),(KApply1(Lbl'Hash'loc,((Int _ as var_6_3489) :: [])) :: [])) :: var_7_3490)) :: []),(var_8_3491),(KApply1(Lbl'_LT_'store'_GT_',(var_18_3492)) :: []),(var_13_3493),(var_14_3494),(var_15_3495),(var_16_3496)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_18_3492),(var_5_3488 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 66)
| ((Map (SortMap,_,_) as var_12_3497) :: []) -> (let e = ((evalMap'Coln'lookup((var_18_3492),(var_5_3488 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 66)
| (KApply3(Lbl'Hash'br,((Int _ as var_9_3498) :: []),((Int _ as var_10_3499) :: []),(KApply1(Lbl'Hash'immRef,((Int _ as var_11_3500) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e88) config (-1))) in match e with 
| [Bottom] -> (stepElt 66)
| ((Map (SortStateCellMap,_,_) as var_17_3501) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((((true) && (true))) && (true))) && (true))) && (true)))) && (((compare var_4_3486 var_4_3487) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e88,(KApply8(Lbl'_LT_'state'_GT_',e88,(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferIB,(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,(var_5_3488 :: [])) :: [])) :: []),(KApply1(Lbl'Hash'loc,(var_6_3489 :: [])) :: [])) :: var_7_3490)) :: []),(var_8_3491),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_5_3488 :: []),(KApply3(Lbl'Hash'br,(var_9_3498 :: []),(var_10_3499 :: []),(KApply1(Lbl'Hash'immRef,(var_11_3500 :: [])) :: [])) :: [])) config (-1))),(var_12_3497 :: [])) config (-1)))) :: []),(var_13_3493),(var_14_3494),(var_15_3495),(var_16_3496)) :: [])) config (-1))),(var_17_3501 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 66))| _ -> (stepElt 66))| _ -> (stepElt 66))| _ -> (stepElt 66))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#TransferIB1_`(K1)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#TransferIB(HOLE,K1)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isK(HOLE),isK(K1)),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(7e755c2fab4f677cb25527783d30641efb49575f5152fc292709f8973f8511d1) heat() klabel(#TransferIB) org.kframework.attributes.Location(Location(138,12,138,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1533644280) strict(1)]|}*)
| (var_4_3502) as e89 when guard < 67 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e89) config (-1))) in match e with 
| [Bottom] -> (stepElt 67)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3503),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'TransferIB,(var_5_3504),(var_6_3505)) :: var_7_3506)) :: []),(var_8_3507),(var_9_3508),(var_10_3509),(var_11_3510),(var_12_3511),(var_13_3512)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e89) config (-1))) in match e with 
| [Bottom] -> (stepElt 67)
| ((Map (SortStateCellMap,_,_) as var_14_3513) :: []) when ((((((true) && (true))) && (true))) && (((((true) && (true))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3504)) config (-1))))))))))) && (((compare var_4_3503 var_4_3502) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e89,(KApply8(Lbl'_LT_'state'_GT_',e89,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3504 @ KApply1(Lbl'Hash'freezer'Hash'TransferIB1_,(var_6_3505)) :: var_7_3506)) :: []),(var_8_3507),(var_9_3508),(var_10_3509),(var_11_3510),(var_12_3511),(var_13_3512)) :: [])) config (-1))),(var_14_3513 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 67))| _ -> (stepElt 67))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,`<store>`(`_Map_`(`_|->_`(F,`#uninit_OSL-SYNTAX`(.KList)),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#uninitialize(#loc(F))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(_58,`Map:lookup`(_9,F))),#match(DotVar4,`_[_<-undef]`(_9,F))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isInt(F),isKItem(_58))) ensures #token("true","Bool") [UNIQUE_ID(b8f3536fd37c0603e5c9944f77297ce03c8a3b732c5c575eeb9dd102208bf8da) contentStartColumn(6) contentStartLine(159) org.kframework.attributes.Location(Location(159,6,160,51)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3514) as e90 when guard < 68 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e90) config (-1))) in match e with 
| [Bottom] -> (stepElt 68)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3515),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'uninitialize,(KApply1(Lbl'Hash'loc,((Int _ as var_7_3516) :: [])) :: [])) :: var_5_3517)) :: []),(var_6_3518),(KApply1(Lbl'_LT_'store'_GT_',(var_14_3519)) :: []),(var_9_3520),(var_10_3521),(var_11_3522),(var_12_3523)) :: []) -> (let e = ((evalMap'Coln'lookup((var_14_3519),(var_7_3516 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 68)
| (var_15_3524 :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_14_3519),(var_7_3516 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 68)
| ((Map (SortMap,_,_) as var_8_3525) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e90) config (-1))) in match e with 
| [Bottom] -> (stepElt 68)
| ((Map (SortStateCellMap,_,_) as var_13_3526) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((true) && (true)))) && (((compare var_4_3514 var_4_3515) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e90,(KApply8(Lbl'_LT_'state'_GT_',e90,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3517)) :: []),(var_6_3518),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_7_3516 :: []),(const'Hash'uninit_OSL'Hyph'SYNTAX :: [])) config (-1))),(var_8_3525 :: [])) config (-1)))) :: []),(var_9_3520),(var_10_3521),(var_11_3522),(var_12_3523)) :: [])) config (-1))),(var_13_3526 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 68))| _ -> (stepElt 68))| _ -> (stepElt 68))| _ -> (stepElt 68))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Read(#borrowMutCK(L,BEG,END,L1))~>#loc(L1)~>DotVar3),_1,`<store>`(`_Map_`(`_|->_`(L,#br(BEG,END,#mutRef(L1))),DotVar4)),_2,_3,_4,_5)),DotVar1)``),_6,_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#read(#loc(L))~>DotVar3),_1,`<store>`(_9),_2,_3,_4,_5),`Map:lookup`(_10,_0))),#match(DotVar4,`_[_<-undef]`(_9,L))),#match(#br(BEG,END,#mutRef(L1)),`Map:lookup`(_9,L))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isInt(END),isInt(L)),isInt(L1)),isInt(BEG))) ensures #token("true","Bool") [UNIQUE_ID(7f772b7b274214985fe222aae8f0727d4fd0da770e94917f96deb5aa84ae59d1) contentStartColumn(6) contentStartLine(182) org.kframework.attributes.Location(Location(182,6,183,75)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3527) as e91 when guard < 69 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e91) config (-1))) in match e with 
| [Bottom] -> (stepElt 69)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3528),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(KApply1(Lbl'Hash'loc,((Int _ as var_5_3529) :: [])) :: [])) :: var_9_3530)) :: []),(var_10_3531),(KApply1(Lbl'_LT_'store'_GT_',(var_17_3532)) :: []),(var_12_3533),(var_13_3534),(var_14_3535),(var_15_3536)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_17_3532),(var_5_3529 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 69)
| ((Map (SortMap,_,_) as var_11_3537) :: []) -> (let e = ((evalMap'Coln'lookup((var_17_3532),(var_5_3529 :: [])) config (-1))) in match e with 
| [Bottom] -> (stepElt 69)
| (KApply3(Lbl'Hash'br,((Int _ as var_6_3538) :: []),((Int _ as var_7_3539) :: []),(KApply1(Lbl'Hash'mutRef,((Int _ as var_8_3540) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e91) config (-1))) in match e with 
| [Bottom] -> (stepElt 69)
| ((Map (SortStateCellMap,_,_) as var_16_3541) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && (((((((true) && (true))) && (true))) && (true)))) && (((compare var_4_3527 var_4_3528) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e91,(KApply8(Lbl'_LT_'state'_GT_',e91,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Read,(KApply4(Lbl'Hash'borrowMutCK,(var_5_3529 :: []),(var_6_3538 :: []),(var_7_3539 :: []),(var_8_3540 :: [])) :: [])) :: KApply1(Lbl'Hash'loc,(var_8_3540 :: [])) :: var_9_3530)) :: []),(var_10_3531),(KApply1(Lbl'_LT_'store'_GT_',((eval_Map_(((eval_'PipeHyph_GT_'_((var_5_3529 :: []),(KApply3(Lbl'Hash'br,(var_6_3538 :: []),(var_7_3539 :: []),(KApply1(Lbl'Hash'mutRef,(var_8_3540 :: [])) :: [])) :: [])) config (-1))),(var_11_3537 :: [])) config (-1)))) :: []),(var_12_3533),(var_13_3534),(var_14_3535),(var_15_3536)) :: [])) config (-1))),(var_16_3541 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 69))| _ -> (stepElt 69))| _ -> (stepElt 69))| _ -> (stepElt 69))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#Transferuninit0_`(K0)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Transferuninit(K0,HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isExp(HOLE),isK(K0)),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(aaa9546198ef2350904d1504fad2003244643d3a8b2ae5c8e13ac25a234266bd) heat() klabel(#Transferuninit) org.kframework.attributes.Location(Location(37,12,37,46)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(975956144) strict(2)]|}*)
| (var_4_3542) as e92 when guard < 70 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e92) config (-1))) in match e with 
| [Bottom] -> (stepElt 70)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3543),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transferuninit,(var_6_3544),(var_5_3545 :: [])) :: var_7_3546)) :: []),(var_8_3547),(var_9_3548),(var_10_3549),(var_11_3550),(var_12_3551),(var_13_3552)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e92) config (-1))) in match e with 
| [Bottom] -> (stepElt 70)
| ((Map (SortStateCellMap,_,_) as var_14_3553) :: []) when ((((((true) && (true))) && (true))) && ((((((isTrue (evalisExp((var_5_3545 :: [])) config (-1)))) && (true))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3545 :: [])) config (-1))))))))))) && (((compare var_4_3543 var_4_3542) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e92,(KApply8(Lbl'_LT_'state'_GT_',e92,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3545 :: KApply1(Lbl'Hash'freezer'Hash'Transferuninit0_,(var_6_3544)) :: var_7_3546)) :: []),(var_8_3547),(var_9_3548),(var_10_3549),(var_11_3550),(var_12_3551),(var_13_3552)) :: [])) config (-1))),(var_14_3553 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 70))| _ -> (stepElt 70))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(HOLE~>`#freezer#Transfer1_`(K1)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(#Transfer(HOLE,K1)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(`_andBool_`(isK(HOLE),isExp(K1)),`_andBool_`(#token("true","Bool"),`notBool_`(isKResult(HOLE))))) ensures #token("true","Bool") [UNIQUE_ID(3d34ba2b277708b0f07ef0ab89efd5fed4c3d45d2157a5343610a215b31279bf) heat() klabel(#Transfer) org.kframework.attributes.Location(Location(35,12,35,38)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(1021886708) strict()]|}*)
| (var_4_3554) as e93 when guard < 71 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e93) config (-1))) in match e with 
| [Bottom] -> (stepElt 71)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3555),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'Transfer,(var_5_3556),(var_6_3557 :: [])) :: var_7_3558)) :: []),(var_8_3559),(var_9_3560),(var_10_3561),(var_11_3562),(var_12_3563),(var_13_3564)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e93) config (-1))) in match e with 
| [Bottom] -> (stepElt 71)
| ((Map (SortStateCellMap,_,_) as var_14_3565) :: []) when ((((((true) && (true))) && (true))) && (((((true) && ((isTrue (evalisExp((var_6_3557 :: [])) config (-1)))))) && (((true) && ((not ((isTrue (evalisKResult((var_5_3556)) config (-1))))))))))) && (((compare var_4_3554 var_4_3555) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e93,(KApply8(Lbl'_LT_'state'_GT_',e93,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3556 @ KApply1(Lbl'Hash'freezer'Hash'Transfer1_,(var_6_3557 :: [])) :: var_7_3558)) :: []),(var_8_3559),(var_9_3560),(var_10_3561),(var_11_3562),(var_12_3563),(var_13_3564)) :: [])) config (-1))),(var_14_3565 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 71))| _ -> (stepElt 71))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#Deallocate(HOLE)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(HOLE~>`#freezer#Deallocate0_`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),`_andBool_`(isExp(HOLE),`_andBool_`(#token("true","Bool"),isKResult(HOLE)))) ensures #token("true","Bool") [UNIQUE_ID(deb5cf048b96ccb63084a9440ca74ab2b8709173096ad9f9481f0e7c61499d5d) cool() klabel(#Deallocate) org.kframework.attributes.Location(Location(365,12,365,48)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) productionID(690844188) strict()]|}*)
| (var_4_3566) as e94 when guard < 72 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e94) config (-1))) in match e with 
| [Bottom] -> (stepElt 72)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3567),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3568 :: KApply0(Lbl'Hash'freezer'Hash'Deallocate0_) :: var_6_3569)) :: []),(var_7_3570),(var_8_3571),(var_9_3572),(var_10_3573),(var_11_3574),(var_12_3575)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e94) config (-1))) in match e with 
| [Bottom] -> (stepElt 72)
| ((Map (SortStateCellMap,_,_) as var_13_3576) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisExp((var_5_3568 :: [])) config (-1)))) && (((true) && ((isTrue (evalisKResult((var_5_3568 :: [])) config (-1))))))))) && (((compare var_4_3566 var_4_3567) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e94,(KApply8(Lbl'_LT_'state'_GT_',e94,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'Deallocate,(var_5_3568 :: [])) :: var_6_3569)) :: []),(var_7_3570),(var_8_3571),(var_9_3572),(var_10_3573),(var_11_3574),(var_12_3575)) :: [])) config (-1))),(var_13_3576 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 72))| _ -> (stepElt 72))
(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#read(X)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(`*__OSL-SYNTAX`(X)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isExp(X)) ensures #token("true","Bool") [UNIQUE_ID(53db71facbe8af0a217900232136c5c6d73d93ad7ba7a15d3b003b5ba4f06e12) contentStartColumn(6) contentStartLine(164) org.kframework.attributes.Location(Location(164,6,164,23)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_4_3577) as e95 when guard < 73 -> (let e = ((evalMap'Coln'lookup((var_0_2665),e95) config (-1))) in match e with 
| [Bottom] -> (stepElt 73)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3578),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Star'__OSL'Hyph'SYNTAX,(var_5_3579 :: [])) :: var_6_3580)) :: []),(var_7_3581),(var_8_3582),(var_9_3583),(var_10_3584),(var_11_3585),(var_12_3586)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_2665),e95) config (-1))) in match e with 
| [Bottom] -> (stepElt 73)
| ((Map (SortStateCellMap,_,_) as var_13_3587) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisExp((var_5_3579 :: [])) config (-1))))) && (((compare var_4_3577 var_4_3578) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e95,(KApply8(Lbl'_LT_'state'_GT_',e95,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(var_5_3579 :: [])) :: var_6_3580)) :: []),(var_7_3581),(var_8_3582),(var_9_3583),(var_10_3584),(var_11_3585),(var_12_3586)) :: [])) config (-1))),(var_13_3587 :: [])) config (-1)))) :: []),(var_1_2666),(var_2_2667),(var_3_2668)) :: [])), (StepFunc step))| _ -> (stepElt 73))| _ -> (stepElt 73))
| _ -> (interned_bottom, (StepFunc step))) else (result, f) in stepElt guard) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 74) else (choice, f)| _ -> (lookups_step c config 74))
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_0_3588)) :: []),(KApply1(Lbl'_LT_'nstate'_GT_',((Int _ as var_1_3589) :: [])) :: []),(var_2_3590),(var_3_3591)) :: []) when guard < 77 -> (match (var_0_3588) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> let rec stepElt = fun guard -> if result == interned_bottom then (match e with (*{| rule `<T>`(`<states>`(``_2=>`_StateCellMap_`(`_StateCellMap_`(`StateCellMapItem`(`<index>`(NS),`<state>`(`<index>`(NS),`<k>`(B~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C)))),`StateCellMapItem`(DotVar2,`<state>`(DotVar2,`<k>`(#secondBranch(Bs)~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C))))),DotVar1)``),`<nstate>`(``NS=>`_+Int_`(NS,#token("1","Int"))``),_0,_1) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(DotVar2,_2),#match(`<state>`(DotVar2,`<k>`(#secondBranch(`_,__OSL-SYNTAX`(B,Bs))~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C))),`Map:lookup`(_2,DotVar2))),#match(DotVar1,`_[_<-undef]`(_2,DotVar2))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(A),isInt(TIMER)),isBlock(B)),isInt(NS)),isInt(C)),isMap(ENV)),isK(Rest)),isMap(STORE)),isSet(WRITE)),isList(STACK)),isBlocks(Bs))) ensures #token("true","Bool") [UNIQUE_ID(e95ab020aa5cec3108d66467300b5ec04bc245ef9b3afe25ff6891290a6889e1) contentStartColumn(6) contentStartLine(19) org.kframework.attributes.Location(Location(19,6,37,12)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_13_3592) as e97 when guard < 75 -> (let e = ((evalMap'Coln'lookup((var_0_3588),e97) config (-1))) in match e with 
| [Bottom] -> (stepElt 75)
| (KApply8(Lbl'_LT_'state'_GT_',(var_13_3593),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'secondBranch,(KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(var_4_3594 :: []),(var_14_3595 :: [])) :: [])) :: var_5_3596)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_6_3597) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_7_3598) :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_8_3599) :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as var_9_3600) :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_10_3601) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_11_3602) :: []),((Int _ as var_12_3603) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3588),e97) config (-1))) in match e with 
| [Bottom] -> (stepElt 75)
| ((Map (SortStateCellMap,_,_) as var_15_3604) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((((true) && (true))) && ((isTrue (evalisBlock((var_4_3594 :: [])) config (-1)))))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && ((isTrue (evalisBlocks((var_14_3595 :: [])) config (-1))))))) && (((compare var_13_3592 var_13_3593) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((eval_StateCellMap_(((evalStateCellMapItem((KApply1(Lbl'_LT_'index'_GT_',(var_1_3589 :: [])) :: []),(KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',(var_1_3589 :: [])) :: []),(KApply1(Lbl'_LT_'k'_GT_',(var_4_3594 :: var_5_3596)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3597 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_7_3598 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_8_3599 :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',(var_9_3600 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_10_3601 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_11_3602 :: []),(var_12_3603 :: [])) :: [])) :: [])) :: [])) config (-1))),((evalStateCellMapItem(e97,(KApply8(Lbl'_LT_'state'_GT_',e97,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'secondBranch,(var_14_3595 :: [])) :: var_5_3596)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3597 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_7_3598 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_8_3599 :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',(var_9_3600 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_10_3601 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_11_3602 :: []),(var_12_3603 :: [])) :: [])) :: [])) :: [])) config (-1)))) config (-1))),(var_15_3604 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'nstate'_GT_',((eval_'Plus'Int_((var_1_3589 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(var_2_3590),(var_3_3591)) :: [])), (StepFunc step))| _ -> (stepElt 75))| _ -> (stepElt 75))
(*{| rule `<T>`(`<states>`(``_2=>`_StateCellMap_`(`_StateCellMap_`(`_StateCellMap_`(`StateCellMapItem`(`<index>`(NS),`<state>`(`<index>`(NS),`<k>`(#compare(#repeat(B)~>Rest)),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C)))),`StateCellMapItem`(`<index>`(`_+Int_`(NS,#token("1","Int"))),`<state>`(`<index>`(`_+Int_`(NS,#token("1","Int"))),`<k>`(Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C))))),`StateCellMapItem`(DotVar2,`<state>`(DotVar2,`<k>`(B~>#loopSep(#token("0","Int"))~>#repeat(B)~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C))))),DotVar1)``),`<nstate>`(``NS=>`_+Int_`(NS,#token("2","Int"))``),_0,_1) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(DotVar2,_2),#match(`<state>`(DotVar2,`<k>`(#repeat(B)~>Rest),`<env>`(ENV),`<store>`(STORE),`<stack>`(STACK),`<write>`(WRITE),`<timer>`(TIMER),`<indexes>`(#indexes(A,C))),`Map:lookup`(_2,DotVar2))),#match(DotVar1,`_[_<-undef]`(_2,DotVar2))),`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(isInt(TIMER),isInt(A)),isBlock(B)),isInt(NS)),isInt(C)),isMap(ENV)),isK(Rest)),isMap(STORE)),isSet(WRITE)),isList(STACK))) ensures #token("true","Bool") [UNIQUE_ID(aaa3ae54fa6646a92b8281a5db54ec9dca33db71581b8af302c00d5bed9edfde) contentStartColumn(6) contentStartLine(43) org.kframework.attributes.Location(Location(43,6,72,13)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
| (var_13_3605) as e98 when guard < 76 -> (let e = ((evalMap'Coln'lookup((var_0_3588),e98) config (-1))) in match e with 
| [Bottom] -> (stepElt 76)
| (KApply8(Lbl'_LT_'state'_GT_',(var_13_3606),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'repeat,(var_4_3607 :: [])) :: var_5_3608)) :: []),(KApply1(Lbl'_LT_'env'_GT_',((Map (SortMap,_,_) as var_6_3609) :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',((Map (SortMap,_,_) as var_7_3610) :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',((List (SortList,_,_) as var_8_3611) :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',((Set (SortSet,_,_) as var_9_3612) :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',((Int _ as var_10_3613) :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,((Int _ as var_11_3614) :: []),((Int _ as var_12_3615) :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3588),e98) config (-1))) in match e with 
| [Bottom] -> (stepElt 76)
| ((Map (SortStateCellMap,_,_) as var_14_3616) :: []) when ((((((true) && (true))) && (true))) && (((((((((((((((((((true) && (true))) && ((isTrue (evalisBlock((var_4_3607 :: [])) config (-1)))))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true))) && (true)))) && (((compare var_13_3606 var_13_3605) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((eval_StateCellMap_(((eval_StateCellMap_(((evalStateCellMapItem((KApply1(Lbl'_LT_'index'_GT_',(var_1_3589 :: [])) :: []),(KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',(var_1_3589 :: [])) :: []),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'compare,(KApply1(Lbl'Hash'repeat,(var_4_3607 :: [])) :: var_5_3608)) :: [])) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3609 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_7_3610 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_8_3611 :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',(var_9_3612 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_10_3613 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_11_3614 :: []),(var_12_3615 :: [])) :: [])) :: [])) :: [])) config (-1))),((evalStateCellMapItem((KApply1(Lbl'_LT_'index'_GT_',((eval_'Plus'Int_((var_1_3589 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(KApply8(Lbl'_LT_'state'_GT_',(KApply1(Lbl'_LT_'index'_GT_',((eval_'Plus'Int_((var_1_3589 :: []),((Lazy.force int1) :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'k'_GT_',(var_5_3608)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3609 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_7_3610 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_8_3611 :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',(var_9_3612 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_10_3613 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_11_3614 :: []),(var_12_3615 :: [])) :: [])) :: [])) :: [])) config (-1)))) config (-1))),((evalStateCellMapItem(e98,(KApply8(Lbl'_LT_'state'_GT_',e98,(KApply1(Lbl'_LT_'k'_GT_',(var_4_3607 :: KApply1(Lbl'Hash'loopSep,((Lazy.force int0) :: [])) :: KApply1(Lbl'Hash'repeat,(var_4_3607 :: [])) :: var_5_3608)) :: []),(KApply1(Lbl'_LT_'env'_GT_',(var_6_3609 :: [])) :: []),(KApply1(Lbl'_LT_'store'_GT_',(var_7_3610 :: [])) :: []),(KApply1(Lbl'_LT_'stack'_GT_',(var_8_3611 :: [])) :: []),(KApply1(Lbl'_LT_'write'_GT_',(var_9_3612 :: [])) :: []),(KApply1(Lbl'_LT_'timer'_GT_',(var_10_3613 :: [])) :: []),(KApply1(Lbl'_LT_'indexes'_GT_',(KApply2(Lbl'Hash'indexes,(var_11_3614 :: []),(var_12_3615 :: [])) :: [])) :: [])) :: [])) config (-1)))) config (-1))),(var_14_3616 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'nstate'_GT_',((eval_'Plus'Int_((var_1_3589 :: []),((Lazy.force int2) :: [])) config (-1)))) :: []),(var_2_3590),(var_3_3591)) :: [])), (StepFunc step))| _ -> (stepElt 76))| _ -> (stepElt 76))
(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(Rest),_1,_2,_3,_4,_5,_6)),DotVar1)``),`<nstate>`(NS),_7,_8) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(#loopSep(N)~>Rest),_1,_2,_3,_4,_5,_6),`Map:lookup`(_9,_0))),#match(DotVar1,`_[_<-undef]`(_9,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isInt(NS),isInt(N)),isK(Rest)),`_>=Int__INT`(N,NS))) ensures #token("true","Bool") [UNIQUE_ID(012343b8845f6ae0f68af64e686020cf23e7c6d962c28d3501670117ad4320ea) contentStartColumn(6) contentStartLine(76) org.kframework.attributes.Location(Location(76,6,78,25)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K "requires" K)]|}*)
| (var_4_3617) as e99 when guard < 77 -> (let e = ((evalMap'Coln'lookup((var_0_3588),e99) config (-1))) in match e with 
| [Bottom] -> (stepElt 77)
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3618),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'loopSep,((Int _ as var_13_3619) :: [])) :: var_5_3620)) :: []),(var_6_3621),(var_7_3622),(var_8_3623),(var_9_3624),(var_10_3625),(var_11_3626)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3588),e99) config (-1))) in match e with 
| [Bottom] -> (stepElt 77)
| ((Map (SortStateCellMap,_,_) as var_12_3627) :: []) when ((((((true) && (true))) && (true))) && (((((((true) && (true))) && (true))) && ((isTrue (eval_'_GT_Eqls'Int__INT((var_13_3619 :: []),(var_1_3589 :: [])) config (-1))))))) && (((compare var_4_3617 var_4_3618) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e99,(KApply8(Lbl'_LT_'state'_GT_',e99,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3620)) :: []),(var_6_3621),(var_7_3622),(var_8_3623),(var_9_3624),(var_10_3625),(var_11_3626)) :: [])) config (-1))),(var_12_3627 :: [])) config (-1)))) :: []),(KApply1(Lbl'_LT_'nstate'_GT_',(var_1_3589 :: [])) :: []),(var_2_3590),(var_3_3591)) :: [])), (StepFunc step))| _ -> (stepElt 77))| _ -> (stepElt 77))
| _ -> (interned_bottom, (StepFunc step))) else (result, f) in stepElt guard) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 77) else (choice, f)| _ -> (lookups_step c config 77))
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_0_3628)) :: []),(var_1_3629),(KApply1(Lbl'_LT_'tmp'_GT_',((List (SortList, Lbl_List_, (var_2_3630 :: []) :: var_3_3631)) :: [])) :: []),(var_4_3632)) :: []) when guard < 78(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(B~>DotVar4),_1,_2,_3,_4,_5,_6)),DotVar2)``),_7,`<tmp>`(`` `_List_`(`ListItem`(B),DotVar1)=>`_List_`(`.List`(.KList),DotVar1)``),_8) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(#secondBranch(`.List{"_,__OSL-SYNTAX"}`(.KList))~>DotVar4),_1,_2,_3,_4,_5,_6),`Map:lookup`(_9,_0))),#match(DotVar2,`_[_<-undef]`(_9,_0))),isKItem(B)) ensures #token("true","Bool") [UNIQUE_ID(559e061ba495af3ae2f7053457741e8bb1a7a2fbc86b4abb8c07c7a8c0fee299) contentStartColumn(6) contentStartLine(16) org.kframework.attributes.Location(Location(16,6,17,43)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_0_3628) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> if result == interned_bottom then (match e with | (var_5_3633) as e100 -> (let e = ((evalMap'Coln'lookup((var_0_3628),e100) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| (KApply8(Lbl'_LT_'state'_GT_',(var_5_3634),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'secondBranch,(KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra') :: [])) :: var_6_3635)) :: []),(var_7_3636),(var_8_3637),(var_9_3638),(var_10_3639),(var_11_3640),(var_12_3641)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3628),e100) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| ((Map (SortStateCellMap,_,_) as var_13_3642) :: []) when ((((((true) && (true))) && (true))) && (true)) && (((compare var_5_3633 var_5_3634) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e100,(KApply8(Lbl'_LT_'state'_GT_',e100,(KApply1(Lbl'_LT_'k'_GT_',(var_2_3630 :: var_6_3635)) :: []),(var_7_3636),(var_8_3637),(var_9_3638),(var_10_3639),(var_11_3640),(var_12_3641)) :: [])) config (-1))),(var_13_3642 :: [])) config (-1)))) :: []),(var_1_3629),(KApply1(Lbl'_LT_'tmp'_GT_',((eval_List_(((Lazy.force const'Stop'List)),((List (SortList, Lbl_List_, var_3_3631)) :: [])) config (-1)))) :: []),(var_4_3632)) :: [])), (StepFunc step))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step))) else (result, f)) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 78) else (choice, f)| _ -> (lookups_step c config 78))
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_0_3643)) :: []),(var_1_3644),(KApply1(Lbl'_LT_'tmp'_GT_',((List (SortList,_,_) as var_2_3645) :: [])) :: []),(var_3_3646)) :: []) when guard < 79(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#secondBranch(Bs)~>DotVar4),_1,_2,_3,_4,_5,_6)),DotVar2)``),_7,`<tmp>`(``DotVar1=>`_List_`(`ListItem`(B),DotVar1)``),_8) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(#branch(`_,__OSL-SYNTAX`(B,Bs))~>DotVar4),_1,_2,_3,_4,_5,_6),`Map:lookup`(_9,_0))),#match(DotVar2,`_[_<-undef]`(_9,_0))),`_andBool_`(isBlock(B),isBlocks(Bs))) ensures #token("true","Bool") [UNIQUE_ID(2035f34c6e27b4f72cafe442a232e9951cb299a35b1b66ab5a328ffc1a79a29d) contentStartColumn(6) contentStartLine(12) org.kframework.attributes.Location(Location(12,6,13,43)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/control.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_0_3643) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> if result == interned_bottom then (match e with | (var_4_3647) as e101 -> (let e = ((evalMap'Coln'lookup((var_0_3643),e101) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3648),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'branch,(KApply2(Lbl_'Comm'__OSL'Hyph'SYNTAX,(var_14_3649 :: []),(var_5_3650 :: [])) :: [])) :: var_6_3651)) :: []),(var_7_3652),(var_8_3653),(var_9_3654),(var_10_3655),(var_11_3656),(var_12_3657)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3643),e101) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| ((Map (SortStateCellMap,_,_) as var_13_3658) :: []) when ((((((true) && (true))) && (true))) && ((((isTrue (evalisBlock((var_14_3649 :: [])) config (-1)))) && ((isTrue (evalisBlocks((var_5_3650 :: [])) config (-1))))))) && (((compare var_4_3647 var_4_3648) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e101,(KApply8(Lbl'_LT_'state'_GT_',e101,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'secondBranch,(var_5_3650 :: [])) :: var_6_3651)) :: []),(var_7_3652),(var_8_3653),(var_9_3654),(var_10_3655),(var_11_3656),(var_12_3657)) :: [])) config (-1))),(var_13_3658 :: [])) config (-1)))) :: []),(var_1_3644),(KApply1(Lbl'_LT_'tmp'_GT_',((eval_List_(((evalListItem((var_14_3649 :: [])) config (-1))),(var_2_3645 :: [])) config (-1)))) :: []),(var_3_3646)) :: [])), (StepFunc step))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step))) else (result, f)) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 79) else (choice, f)| _ -> (lookups_step c config 79))
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_0_3659)) :: []),(var_1_3660),(var_2_3661),(KApply1(Lbl'_LT_'funDefs'_GT_',(var_3_3662)) :: [])) :: []) when guard < 80(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#block(#bindParams(Ps,Es,SS))~>`#void_OSL-SYNTAX`(.KList)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,`<funDefs>`(_10)) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_9),#match(`<state>`(_0,`<k>`(#FnCall(F,Es)~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_9,_0))),#match(`<funDef>`(`<fname>`(F),`<fparams>`(Ps),DotVar5,`<fbody>`(#block(SS))),`Map:lookup`(_10,`<fname>`(F)))),#match(DotVar1,`_[_<-undef]`(_9,_0))),#match(DotVar4,`_[_<-undef]`(_10,`<fname>`(F)))),`_andBool_`(`_andBool_`(`_andBool_`(isExps(Es),isId(F)),isStmts(SS)),isParameters(Ps))) ensures #token("true","Bool") [UNIQUE_ID(0d4082e8ca976614d2aff35b40688c56c02f43f5ce14377a32a8d8605a40937d) contentStartColumn(6) contentStartLine(25) org.kframework.attributes.Location(Location(25,6,28,35)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/call.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_0_3659) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> if result == interned_bottom then (match e with | (var_4_3663) as e102 -> (let e = ((evalMap'Coln'lookup((var_0_3659),e102) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3664),(KApply1(Lbl'_LT_'k'_GT_',(KApply2(Lbl'Hash'FnCall,(var_16_3665 :: []),(var_6_3666 :: [])) :: var_8_3667)) :: []),(var_9_3668),(var_10_3669),(var_11_3670),(var_12_3671),(var_13_3672),(var_14_3673)) :: []) -> (let e = ((evalMap'Coln'lookup((var_3_3662),(KApply1(Lbl'_LT_'fname'_GT_',(var_16_3665 :: [])) :: [])) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| (KApply4(Lbl'_LT_'funDef'_GT_',(KApply1(Lbl'_LT_'fname'_GT_',(var_16_3674 :: [])) :: []),(KApply1(Lbl'_LT_'fparams'_GT_',(var_5_3675 :: [])) :: []),(var_17_3676),(KApply1(Lbl'_LT_'fbody'_GT_',(KApply1(Lbl'Hash'block,(var_7_3677 :: [])) :: [])) :: [])) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3659),e102) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| ((Map (SortStateCellMap,_,_) as var_15_3678) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_3_3662),(KApply1(Lbl'_LT_'fname'_GT_',(var_16_3665 :: [])) :: [])) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| ((Map (SortFunDefCellMap,_,_) as var_18_3679) :: []) when ((((((((((true) && (true))) && (true))) && (true))) && (true))) && ((((((((isTrue (evalisExps((var_6_3666 :: [])) config (-1)))) && ((isTrue (evalisId((var_16_3665 :: [])) config (-1)))))) && ((isTrue (evalisStmts((var_7_3677 :: [])) config (-1)))))) && ((isTrue (evalisParameters((var_5_3675 :: [])) config (-1))))))) && (((compare_kitem var_16_3665 var_16_3674) = 0) && ((compare var_4_3664 var_4_3663) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e102,(KApply8(Lbl'_LT_'state'_GT_',e102,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'block,((eval'Hash'bindParams((var_5_3675 :: []),(var_6_3666 :: []),(var_7_3677 :: [])) config (-1)))) :: const'Hash'void_OSL'Hyph'SYNTAX :: var_8_3667)) :: []),(var_9_3668),(var_10_3669),(var_11_3670),(var_12_3671),(var_13_3672),(var_14_3673)) :: [])) config (-1))),(var_15_3678 :: [])) config (-1)))) :: []),(var_1_3660),(var_2_3661),(KApply1(Lbl'_LT_'funDefs'_GT_',(var_3_3662)) :: [])) :: [])), (StepFunc step))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step))) else (result, f)) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 80) else (choice, f)| _ -> (lookups_step c config 80))
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_0_3680)) :: []),(var_1_3681),(var_2_3682),(KApply1(Lbl'_LT_'funDefs'_GT_',(var_3_3683)) :: [])) :: []) when guard < 81(*{| rule `<T>`(`<states>`(``_9=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,`<funDefs>`(``_10=>`_FunDefCellMap_`(`FunDefCellMapItem`(`<fname>`(F),`<funDef>`(`<fname>`(F),`<fparams>`(Ps),`<fret>`(T),`<fbody>`(B))),DotVar4)``)) requires `_andBool_`(`_andBool_`(`_andBool_`(`_andBool_`(#match(DotVar4,_10),#mapChoice(_0,_9)),#match(`<state>`(_0,`<k>`(`_;_OSL-SYNTAX`(#function(F,Ps,T,B))~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_9,_0))),#match(DotVar1,`_[_<-undef]`(_9,_0))),`_andBool_`(`_andBool_`(`_andBool_`(isBlock(B),isId(F)),isType(T)),isParameters(Ps))) ensures #token("true","Bool") [UNIQUE_ID(b66173926aa9bf5458fca11319b6799edb468829f9689a339c4a0a265c80d93b) contentStartColumn(6) contentStartLine(11) org.kframework.attributes.Location(Location(11,6,18,17)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/call.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (let e = (var_3_3683) in match e with 
| [Bottom] -> (lookups_step c config 81)
| ((Map (SortFunDefCellMap,_,_) as var_17_3684) :: []) -> (match (var_0_3680) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> if result == interned_bottom then (match e with | (var_4_3685) as e103 -> (let e = ((evalMap'Coln'lookup((var_0_3680),e103) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| (KApply8(Lbl'_LT_'state'_GT_',(var_4_3686),(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl_'SCln'_OSL'Hyph'SYNTAX,(KApply4(Lbl'Hash'function,(var_13_3687 :: []),(var_14_3688 :: []),(var_15_3689 :: []),(var_16_3690 :: [])) :: [])) :: var_5_3691)) :: []),(var_6_3692),(var_7_3693),(var_8_3694),(var_9_3695),(var_10_3696),(var_11_3697)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_0_3680),e103) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| ((Map (SortStateCellMap,_,_) as var_12_3698) :: []) when ((((((((true) && (true))) && (true))) && (true))) && ((((((((isTrue (evalisBlock((var_16_3690 :: [])) config (-1)))) && ((isTrue (evalisId((var_13_3687 :: [])) config (-1)))))) && ((isTrue (evalisType((var_15_3689 :: [])) config (-1)))))) && ((isTrue (evalisParameters((var_14_3688 :: [])) config (-1))))))) && (((compare var_4_3686 var_4_3685) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e103,(KApply8(Lbl'_LT_'state'_GT_',e103,(KApply1(Lbl'_LT_'k'_GT_',(var_5_3691)) :: []),(var_6_3692),(var_7_3693),(var_8_3694),(var_9_3695),(var_10_3696),(var_11_3697)) :: [])) config (-1))),(var_12_3698 :: [])) config (-1)))) :: []),(var_1_3681),(var_2_3682),(KApply1(Lbl'_LT_'funDefs'_GT_',((eval_FunDefCellMap_(((evalFunDefCellMapItem((KApply1(Lbl'_LT_'fname'_GT_',(var_13_3687 :: [])) :: []),(KApply4(Lbl'_LT_'funDef'_GT_',(KApply1(Lbl'_LT_'fname'_GT_',(var_13_3687 :: [])) :: []),(KApply1(Lbl'_LT_'fparams'_GT_',(var_14_3688 :: [])) :: []),(KApply1(Lbl'_LT_'fret'_GT_',(var_15_3689 :: [])) :: []),(KApply1(Lbl'_LT_'fbody'_GT_',(var_16_3690 :: [])) :: [])) :: [])) config (-1))),(var_17_3684 :: [])) config (-1)))) :: [])) :: [])), (StepFunc step))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step))) else (result, f)) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 81) else (choice, f)| _ -> (lookups_step c config 81))| _ -> (lookups_step c config 81))
| (KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',(var_10_3699)) :: []),(var_7_3700),(var_8_3701),(var_9_3702)) :: []) when guard < 82(*{| rule `<T>`(`<states>`(``_10=>`_StateCellMap_`(`StateCellMapItem`(_0,`<state>`(_0,`<k>`(#read(X)~>DotVar3),_1,_2,_3,_4,_5,_6)),DotVar1)``),_7,_8,_9) requires `_andBool_`(`_andBool_`(`_andBool_`(#mapChoice(_0,_10),#match(`<state>`(_0,`<k>`(X~>DotVar3),_1,_2,_3,_4,_5,_6),`Map:lookup`(_10,_0))),#match(DotVar1,`_[_<-undef]`(_10,_0))),isId(X)) ensures #token("true","Bool") [UNIQUE_ID(eb0440173ab96786ccc8e1d34b35497cdf7aaf288b45371e829649f8905e656a) contentStartColumn(6) contentStartLine(162) org.kframework.attributes.Location(Location(162,6,162,21)) org.kframework.attributes.Source(Source(/home/user/workspace/ownership-language-osl/model/osl.k)) org.kframework.definition.Production(syntax RuleContent ::= K)]|}*)
 -> (match (var_10_3699) with 
| [Map (_,_,collection)] -> let (choice, f) = (KMap.fold (fun e v (result, f) -> if result == interned_bottom then (match e with | (var_0_3703) as e104 -> (let e = ((evalMap'Coln'lookup((var_10_3699),e104) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| (KApply8(Lbl'_LT_'state'_GT_',(var_0_3704),(KApply1(Lbl'_LT_'k'_GT_',(varX_3705 :: varDotVar3_3706)) :: []),(var_1_3707),(var_2_3708),(var_3_3709),(var_4_3710),(var_5_3711),(var_6_3712)) :: []) -> (let e = ((eval_'LSqB'_'_LT_Hyph'undef'RSqB'((var_10_3699),e104) config (-1))) in match e with 
| [Bottom] -> (interned_bottom, (StepFunc step))
| ((Map (SortStateCellMap,_,_) as varDotVar1_3713) :: []) when ((((((true) && (true))) && (true))) && ((isTrue (evalisId((varX_3705 :: [])) config (-1))))) && (((compare var_0_3704 var_0_3703) = 0) && true) -> (((KApply4(Lbl'_LT_'T'_GT_',(KApply1(Lbl'_LT_'states'_GT_',((eval_StateCellMap_(((evalStateCellMapItem(e104,(KApply8(Lbl'_LT_'state'_GT_',e104,(KApply1(Lbl'_LT_'k'_GT_',(KApply1(Lbl'Hash'read,(varX_3705 :: [])) :: varDotVar3_3706)) :: []),(var_1_3707),(var_2_3708),(var_3_3709),(var_4_3710),(var_5_3711),(var_6_3712)) :: [])) config (-1))),(varDotVar1_3713 :: [])) config (-1)))) :: []),(var_7_3700),(var_8_3701),(var_9_3702)) :: [])), (StepFunc step))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step)))| _ -> (interned_bottom, (StepFunc step))) else (result, f)) collection (interned_bottom, (StepFunc step))) in if choice == interned_bottom then (lookups_step c config 82) else (choice, f)| _ -> (lookups_step c config 82))
| _ -> raise (Stuck c)
let make_stuck (config: k) : k = config
let make_unstuck (config: k) : k = config
let get_thread_set (config: k) : k = [Map(SortMap,Lbl_Map_,KMap.empty)]
let set_thread_set (config: k) (set: k) : k = config
end
let () = Plugin.the_definition := Some (module Def)
