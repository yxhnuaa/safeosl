type sort = 
|SortFbodyCellOpt
|SortFunDefCellFragment
|SortIndexItem
|SortTimerCellOpt
|SortK
|SortIndexesCell
|SortEnvCell
|SortStmt
|SortFparamsCellOpt
|SortMap
|SortFretCellOpt
|SortValue
|SortKItem
|SortIndexesCellOpt
|SortStackCellOpt
|SortProp
|SortLoopItem
|SortIndexCell
|SortWriteCell
|SortParameter
|SortStream
|SortTmpCell
|SortSeparator
|SortBlocks
|SortParameters
|SortString
|SortFloat
|SortTCellFragment
|SortStoreCellOpt
|SortSet
|SortBorrowItem
|SortFunDefsCellFragment
|SortStoreCell
|SortFretCell
|SortMInt
|SortStatesCellOpt
|SortCell
|SortTimerCell
|SortLOCK
|SortFunDefsCell
|SortStmts
|SortBlockItem
|SortFnameCell
|SortOItem
|SortBranchTmp
|SortFunDefsCellOpt
|SortEnvCellOpt
|SortNstateCell
|SortFunction
|SortBool
|SortKResult
|SortRItem
|SortTmpCellOpt
|SortDItem
|SortIndexCellOpt
|SortLifetime
|SortKCell
|SortBytes
|SortWriteCellOpt
|SortFparamsCell
|SortStatesCell
|SortWItem
|SortFnameCellOpt
|SortExps
|SortIOError
|SortStringBuffer
|SortType
|SortTCell
|SortNstateCellOpt
|SortInt
|SortStatesCellFragment
|SortStackCell
|SortBlock
|SortFunDefCellMap
|SortKConfigVar
|SortStateCellFragment
|SortKCellOpt
|SortFbodyCell
|SortProps
|SortIndexes
|SortStateCellMap
|SortId
|SortUninit
|SortFunDefCell
|SortList
|SortStateCell
|SortExp
type klabel = 
|Lbl'Hash'argv
|LblisFbodyCellOpt
|LblMap'Coln'lookup
|Lbl'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO
|LblsignExtendBitRangeInt
|Lbl_'EqlsEqls'Bool__BOOL
|LblisSet
|Lbltransfer__'SCln'_OSL'Hyph'SYNTAX
|LblinitNstateCell
|Lbl_'_LT_Eqls'Set__SET
|LblinitFparamsCell
|LblnoEnvCell
|Lbl'Apos'__OSL'Hyph'SYNTAX
|LblisIOError
|Lbl'Hash'parse
|Lbl'Hash'freezer'Hash'Transfer1_
|Lbl'Hash'EALREADY_K'Hyph'IO
|Lbl_'SCln'_OSL'Hyph'SYNTAX
|LblmakeList
|LblisSeparator
|LblinitIndexesCell
|Lbl'Hash'ESPIPE_K'Hyph'IO
|LblinitFbodyCell
|LblisFunDefCellFragment
|Lbl'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO
|Lbl'Hash'ENOENT_K'Hyph'IO
|LblisFnameCellOpt
|Lbl'Hash'mborrow
|LblisBlockItem
|Lbl_StateCellMap_
|Lbldestruct_'SCln'_OSL'Hyph'SYNTAX
|Lbl'Hash'ENOTTY_K'Hyph'IO
|Lbl'_LT_'states'_GT_'
|LblisStatesCellOpt
|Lbl'Hash'freezer'Hash'Read0_
|LblinitWriteCell
|Lbl'Hash'ENOTEMPTY_K'Hyph'IO
|Lbl'Hash'borrowmutck
|Lbl'Hash'branch
|LblisBorrowItem
|Lbl'Hash'EMSGSIZE_K'Hyph'IO
|LblisKConfigVar
|Lbl'Hash'ENETRESET_K'Hyph'IO
|Lbl'Hash'EAFNOSUPPORT_K'Hyph'IO
|Lbl'Hash'loc
|LblisFunDefsCellFragment
|Lbl'Hash'mutRef
|Lbl'Hash'lc
|LblisCell
|Lbl'Hash'ENOMEM_K'Hyph'IO
|Lblvalues
|Lbl'Hash'expStmt
|Lbl'Hash'own
|Lbl'Hash'borrowImmCK
|LblisRItem
|LblisStoreCellOpt
|Lbl'Hash'ENXIO_K'Hyph'IO
|Lbl_'_LT_'Int__INT
|LblisLoopItem
|LblisWItem
|Lbl'Hash'configuration_K'Hyph'REFLECTION
|LblisFunDefsCellOpt
|Lbl'Hash'Loc
|LblisFloat
|LblisOItem
|LblinitFretCell
|LblinitFunDefsCell
|LblchrChar
|Lbl_divInt__INT
|Lbl'Hash'EROFS_K'Hyph'IO
|Lbl'Hash'wv
|Lbl_'Plus'Int_
|Lbl'Hash'uninit_OSL'Hyph'SYNTAX
|Lbl_orBool__BOOL
|LblnoIndexCell
|LblnoTmpCell
|Lbl'Hash'ENFILE_K'Hyph'IO
|LblupdateMap
|LblInt2String
|Lbl'Hash'deallocate
|Lbl_'EqlsSlshEqls'K_
|LblFunDefCellMapItem
|Lbl'Stop'StateCellMap
|LblisTimerCellOpt
|Lbl_List_
|Lbl'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO
|Lbl'Hash'EOPNOTSUPP_K'Hyph'IO
|Lbl_'PipeHyph_GT_'_
|Lbl_'Hyph'Map__MAP
|Lbl'Hash'compareS
|Lblmut_OSL'Hyph'SYNTAX
|LblisIndexesCellOpt
|Lbl'Hash'EMLINK_K'Hyph'IO
|LblinitStoreCell
|Lbl'Hash'borrowMutCK
|Lbl'Hash'TransferMB
|Lbl'Hash'sort
|Lbl_'EqlsEqls'K_
|LblreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING
|Lbl'Hash'EOVERFLOW_K'Hyph'IO
|Lbl'Hash'FnCall
|Lbl'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO
|LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX
|Lbl'Hash'increaseIndex_OSL
|Lbl'Stop'Map
|LblisTmpCellOpt
|Lbl_'EqlsSlshEqls'String__STRING
|Lbl'Hash'EIO_K'Hyph'IO
|Lbl'Hash'Transfer
|LblisStateCellFragment
|LblisType
|LblisInt
|LblisBranchTmp
|Lbl_FunDefCellMap_
|Lbl'Hash'EFAULT_K'Hyph'IO
|Lbl'Hash'secondBranch
|Lbl'Hash'fresh
|Lbl'Hash'TransferIB
|Lbl_impliesBool__BOOL
|Lbl_'Star'Int__INT
|Lbl'Hash'repeat
|Lbl'_LT_'T'_GT_'
|Lbl'Hash'Thread
|Lbl_'Comm'__OSL'Hyph'SYNTAX
|LblmaxInt'LPar'_'Comm'_'RPar'_INT
|Lbl'Hash'EDEADLK_K'Hyph'IO
|Lbl'_LT_'tmp'_GT_'
|Lbl_'_LT_Eqls'String__STRING
|LblisStackCellOpt
|LblisStmt
|Lbl'Hash'ENOBUFS_K'Hyph'IO
|Lbl_Map_
|Lbl_'Hyph'Int__INT
|Lblproject'Coln'Stmts
|Lbl'Hash'EOF_K'Hyph'IO
|Lbl'Hash'compareA
|LblFloat2String
|LblisValue
|LblsizeList
|Lbl'Hash'EWOULDBLOCK_K'Hyph'IO
|LblString2Id
|Lbl'Hash'decompose
|Lbl_'EqlsSlshEqls'Bool__BOOL
|Lbl'Hash'EFBIG_K'Hyph'IO
|LblisTCell
|Lbl'Hash'EBADF_K'Hyph'IO
|Lbl'Hash'freezer'Hash'TransferIB1_
|Lbl'Hash'EPIPE_K'Hyph'IO
|LblnoStoreCell
|Lbl_'Xor_Perc'Int___INT
|Lbl'_LT_'funDef'_GT_'
|Lbl'Hash'Deallocate
|LblinitIndexCell
|Lbl'Hash'function
|LblisFunDefCellMap
|LblrfindString
|Lbl'Hash'ESOCKTNOSUPPORT_K'Hyph'IO
|Lbl'Hash'EINTR_K'Hyph'IO
|Lbl'_LT_'store'_GT_'
|Lbl'Hash'stat'LPar'_'RPar'_K'Hyph'IO
|LblupdateList
|Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra'
|Lbl'Hash'compare
|Lbl'_LT_'funDefs'_GT_Hyph'fragment
|Lbl'_LT_'timer'_GT_'
|Lbl'_LT_'stack'_GT_'
|LblcategoryChar
|LblSet'Coln'difference
|Lbl'Hash'EHOSTUNREACH_K'Hyph'IO
|Lbl'Hash'ECONNRESET_K'Hyph'IO
|LblisKCellOpt
|Lbl'Hash'ECHILD_K'Hyph'IO
|Lbl'Hash'indexes
|LblString2Float
|LblMap'Coln'lookupOrDefault
|Lbl'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL
|Lbl'Hash'ENOTCONN_K'Hyph'IO
|Lbl'Hash'stdout_K'Hyph'IO
|Lbl_'And'Int__INT
|Lbl'Hash'increaseTimer_OSL
|Lbl'Hash'borrowimmck
|Lbl'Hash'ENAMETOOLONG_K'Hyph'IO
|Lbllog2Int
|Lbl_'EqlsSlshEqls'Int__INT
|Lbl'Hash'declTy
|Lbl'Hash'stdin_K'Hyph'IO
|Lbl'Hash'freezer'Hash'Transfer0_
|Lbl_'_GT_Eqls'String__STRING
|Lbl'Stop'FunDefCellMap
|LblsizeMap
|LblnoWriteCell
|LblnoNstateCell
|LblisId
|LblsubstrString
|Lblsize
|LblisIndexes
|LblinitTimerCell
|Lbl'Hash'ENETUNREACH_K'Hyph'IO
|LblisFretCellOpt
|Lbl'Hash'EPROTOTYPE_K'Hyph'IO
|LblinitStatesCell
|Lbl'Hash'freezer'Hash'TransferMB1_
|Lbl'Hash'systemResult'LPar'_'Comm'_'Comm'_'RPar'_K'Hyph'IO
|LblisWriteCellOpt
|LblinitFunDefCell
|LblnoFnameCell
|LblsrandInt
|Lbl'Hash'EINVAL_K'Hyph'IO
|LblisKItem
|Lbl'Hash'ENODEV_K'Hyph'IO
|LblList'Coln'set
|LblString2Base
|LblnoStackCell
|LblisStatesCellFragment
|Lbl'Hash'noparse_K'Hyph'IO
|LblisFbodyCell
|Lbl'_LT_'write'_GT_'
|Lblkeys
|LblisLifetime
|Lbl'Hash'ESHUTDOWN_K'Hyph'IO
|LblisParameter
|LblisFparamsCell
|Lbl'Hash'Read
|Lbl'Hash'ENOTDIR_K'Hyph'IO
|LblisExp
|LblisLOCK
|LblisFparamsCellOpt
|Lbl'Hash'lv
|Lbl_'_LT_Eqls'Int__INT
|LblnotBool_
|Lbl'Hash'stderr_K'Hyph'IO
|LblnoKCell
|Lbl'Hash'EBUSY_K'Hyph'IO
|LblisTimerCell
|Lbl'Hash'getenv
|Lbl'_LT_'fparams'_GT_'
|LblintersectSet
|Lbl_in_keys'LPar'_'RPar'_MAP
|LblfindChar
|LblisStatesCell
|LblSet'Coln'in
|LblisK
|LblString2Int
|Lbl'Hash'ENETDOWN_K'Hyph'IO
|Lbl_'LSqB'_'_LT_Hyph'undef'RSqB'
|Lbl'Hash'Bottom
|Lbl'Hash'writev
|Lbl_'EqlsEqls'Int_
|Lbl_andThenBool__BOOL
|Lbl'Hash'parseInModule
|Lbl'Hash'writeCK
|Lbl'Hash'cint
|Lbl'Stop'List'LBraQuotHash'props'QuotRBra'
|LblnoIndexesCell
|LblnoFparamsCell
|Lbl'Hash'system
|LblisString
|LblisProp
|Lbl'Hash'parameter
|Lbl_'Perc'Int__INT
|Lbl_'_GT__GT_'Int__INT
|Lbl'Hash'decl
|LblisList
|LblisTmpCell
|Lbl'Hash'EPROTONOSUPPORT_K'Hyph'IO
|LblisFunction
|LblreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING
|Lbl'Hash'EDESTADDRREQ_K'Hyph'IO
|Lbl'Hash'EADDRINUSE_K'Hyph'IO
|LblisNstateCell
|Lbl_'Xor_'Int__INT
|LblfindString
|Lbl'_LT_'k'_GT_'
|Lbl'_LT_'funDefs'_GT_'
|Lbl'_LT_'fbody'_GT_'
|LblabsInt
|Lblval
|Lbl'Hash'EHOSTDOWN_K'Hyph'IO
|LblisEnvCell
|Lbl_'_GT_'String__STRING
|Lbl'_LT_'state'_GT_Hyph'fragment
|Lbl_'EqlsEqls'String__STRING
|Lbl'Hash'checkInit
|LblisProps
|LblisKResult
|LblList'Coln'get
|LblisStackCell
|Lbl'Hash'lstat'LPar'_'RPar'_K'Hyph'IO
|LblisIndexesCell
|Lbl'Hash'freezer'Hash'Deallocate0_
|LblinitFnameCell
|LblnoFretCell
|LblSetItem
|Lblcopy_OSL'Hyph'SYNTAX
|LblunsignedBytes
|LblStateCellMapItem
|Lbl'Stop'List
|Lbl'Hash'ENOLCK_K'Hyph'IO
|Lbl'Hash'ECONNABORTED_K'Hyph'IO
|LblrandInt
|Lbl'Hash'EXDEV_K'Hyph'IO
|Lbl'Hash'close'LPar'_'RPar'_K'Hyph'IO
|LblisUninit
|Lbl'Hash'props
|Lblkeys_list'LPar'_'RPar'_MAP
|LblfreshId
|Lbl_orElseBool__BOOL
|Lbl'Hash'EISDIR_K'Hyph'IO
|LblisIndexCell
|LblList'Coln'range
|LblisKCell
|Lbl'Hash'unknownIOError
|Lbl_'_GT_Eqls'Int__INT
|Lbl'Hash'voidTy_OSL'Hyph'SYNTAX
|LblinitEnvCell
|Lbl'Hash'freezer'Hash'expStmt0_
|Lbl'Hash'compareE
|Lbl'Hash'bindParams
|LblisNstateCellOpt
|Lbl_'Stop'__OSL'Hyph'SYNTAX
|Lbl'Hash'ENOSYS_K'Hyph'IO
|Lbl'Hash'ECONNREFUSED_K'Hyph'IO
|Lbl'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO
|Lbl'Hash'EADDRNOTAVAIL_K'Hyph'IO
|LblcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING
|Lbl_'_GT_'Int__INT
|LblfillList
|Lbl'_LT_'states'_GT_Hyph'fragment
|LblnoFunDefsCell
|LblisBlocks
|Lbl'Hash'removeState_CONTROL
|LblbitRangeInt
|Lbl_'_LT_'String__STRING
|Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra'
|Lbl'_LT_'funDef'_GT_Hyph'fragment
|Lbl'Hash'ThreadLocal
|Lbl'Hash'inProps
|Lbl_xorBool__BOOL
|Lbl'_LT_'fname'_GT_'
|LblisIndexCellOpt
|Lbl'Hash'open'LPar'_'RPar'_K'Hyph'IO
|Lbl'_LT_'env'_GT_'
|Lbl'Hash'ETOOMANYREFS_K'Hyph'IO
|Lbl'Hash'ENOSPC_K'Hyph'IO
|Lbl'Hash'br
|Lbl'Hash'logToFile
|Lbl'Hash'freezer'Hash'lvDref0_
|Lbl'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO
|Lbl'Hash'ref
|LblbigEndianBytes
|LblId2String
|LblMap'Coln'choice
|LblisFunDefsCell
|LblisExps
|Lbl'Hash'borrow
|Lbl___OSL'Hyph'SYNTAX
|Lbl_Set_
|Lbl'Hash'EEXIST_K'Hyph'IO
|Lbl'Hash'getc'LPar'_'RPar'_K'Hyph'IO
|LblinitTmpCell
|Lbl'Hash'unwrapInt
|Lbl'_LT_'state'_GT_'
|LblisBool
|Lbl'Tild'Int__INT
|Lbl'Hash'list2Set
|LblordChar
|Lbl_modInt__INT
|LblrfindChar
|Lbl'Hash'EAGAIN_K'Hyph'IO
|LblisIndexItem
|LbldirectionalityChar
|Lbl'Hash'opendir'LPar'_'RPar'_K'Hyph'IO
|LblnoTimerCell
|LblinitKCell
|Lbl'Hash'rs
|LblisDItem
|Lbl'Stop'Set
|LblisStateCell
|Lbl'Hash'EACCES_K'Hyph'IO
|Lbl'Hash'ELOOP_K'Hyph'IO
|Lbl'Hash'EDOM_K'Hyph'IO
|LblremoveAll
|LblisFunDefCell
|Lbl_andBool_
|Lbl'Hash'loopSep
|Lbl'_LT_'fret'_GT_'
|Lbl'Hash'TransferV
|Lbl'Hash'EPFNOSUPPORT_K'Hyph'IO
|LbllengthString
|LblinitStackCell
|Lbl'Hash'ERANGE_K'Hyph'IO
|LblinitTCell
|LblisFnameCell
|LblsignedBytes
|LblnoFbodyCell
|LblFloatFormat
|Lbl'Hash'ENOTSOCK_K'Hyph'IO
|Lbl_'Plus'String__STRING
|Lbl_'Pipe'Int__INT
|Lbl'Hash'Transferuninit
|Lbl'Hash'EISCONN_K'Hyph'IO
|Lbl_dividesInt__INT
|LblisStateCellMap
|Lbl'_LT_'T'_GT_Hyph'fragment
|LblSet'Coln'choice
|Lbl'Star'__OSL'Hyph'SYNTAX
|Lbl'Hash'buffer
|LblfreshInt
|Lbl'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO
|Lbl'Hash'ETIMEDOUT_K'Hyph'IO
|LblisBlock
|Lbl'Hash'transfer
|LblisParameters
|Lbl_xorInt__INT
|Lbl'Hash'EINPROGRESS_K'Hyph'IO
|LblinitStateCell
|Lbl'_LT_'indexes'_GT_'
|Lbl'Hash'ENOPROTOOPT_K'Hyph'IO
|LbllittleEndianBytes
|Lbl'Hash'EPERM_K'Hyph'IO
|Lbl_'_LT__LT_'Int__INT
|Lbl'Hash'block
|Lbl'Hash'freezer'Hash'Transferuninit0_
|Lbl'_LT_'index'_GT_'
|Lbl'Hash'immRef
|LblBase2String
|LblListItem
|LblisWriteCell
|LblisStream
|Lbl_'_LT_Eqls'Map__MAP
|LblnewUUID_STRING
|Lbl'_LT_'nstate'_GT_'
|Lbl'Hash'ESRCH_K'Hyph'IO
|Lbl'Hash'EMFILE_K'Hyph'IO
|Lblloop_'SCln'_OSL'Hyph'SYNTAX
|Lbl'Hash'unwrapVal
|Lbl'Hash'read
|Lbl'Hash'existRef
|Lbl_inList_
|Lbl'Hash'void_OSL'Hyph'SYNTAX
|Lbl'Hash'ENOEXEC_K'Hyph'IO
|Lbl'Hash'freezerval0_
|LblminInt'LPar'_'Comm'_'RPar'_INT
|LblisMap
|LblisStoreCell
|LblisStmts
|Lbl'Hash'lvDref
|LblisTCellFragment
|Lbl'Hash'uninitialize
|Lblreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING
|Lbl_'Slsh'Int__INT
|Lbl'Hash'blockend_BLOCK
|Lbl_'LSqB'_'_LT_Hyph'_'RSqB'_MAP
|Lbl'Hash'tell'LPar'_'RPar'_K'Hyph'IO
|LblgetKLabel
|LblisEnvCellOpt
|Lbl'Hash'E2BIG_K'Hyph'IO
|LblnoStatesCell
|Lbl'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO
|LblisFretCell
let print_sort(c: sort) : string = match c with 
|SortFbodyCellOpt -> "FbodyCellOpt"
|SortFunDefCellFragment -> "FunDefCellFragment"
|SortIndexItem -> "IndexItem"
|SortTimerCellOpt -> "TimerCellOpt"
|SortK -> "K"
|SortIndexesCell -> "IndexesCell"
|SortEnvCell -> "EnvCell"
|SortStmt -> "Stmt"
|SortFparamsCellOpt -> "FparamsCellOpt"
|SortMap -> "Map"
|SortFretCellOpt -> "FretCellOpt"
|SortValue -> "Value"
|SortKItem -> "KItem"
|SortIndexesCellOpt -> "IndexesCellOpt"
|SortStackCellOpt -> "StackCellOpt"
|SortProp -> "Prop"
|SortLoopItem -> "LoopItem"
|SortIndexCell -> "IndexCell"
|SortWriteCell -> "WriteCell"
|SortParameter -> "Parameter"
|SortStream -> "Stream"
|SortTmpCell -> "TmpCell"
|SortSeparator -> "Separator"
|SortBlocks -> "Blocks"
|SortParameters -> "Parameters"
|SortString -> "String"
|SortFloat -> "Float"
|SortTCellFragment -> "TCellFragment"
|SortStoreCellOpt -> "StoreCellOpt"
|SortSet -> "Set"
|SortBorrowItem -> "BorrowItem"
|SortFunDefsCellFragment -> "FunDefsCellFragment"
|SortStoreCell -> "StoreCell"
|SortFretCell -> "FretCell"
|SortMInt -> "MInt"
|SortStatesCellOpt -> "StatesCellOpt"
|SortCell -> "Cell"
|SortTimerCell -> "TimerCell"
|SortLOCK -> "LOCK"
|SortFunDefsCell -> "FunDefsCell"
|SortStmts -> "Stmts"
|SortBlockItem -> "BlockItem"
|SortFnameCell -> "FnameCell"
|SortOItem -> "OItem"
|SortBranchTmp -> "BranchTmp"
|SortFunDefsCellOpt -> "FunDefsCellOpt"
|SortEnvCellOpt -> "EnvCellOpt"
|SortNstateCell -> "NstateCell"
|SortFunction -> "Function"
|SortBool -> "Bool"
|SortKResult -> "KResult"
|SortRItem -> "RItem"
|SortTmpCellOpt -> "TmpCellOpt"
|SortDItem -> "DItem"
|SortIndexCellOpt -> "IndexCellOpt"
|SortLifetime -> "Lifetime"
|SortKCell -> "KCell"
|SortBytes -> "Bytes"
|SortWriteCellOpt -> "WriteCellOpt"
|SortFparamsCell -> "FparamsCell"
|SortStatesCell -> "StatesCell"
|SortWItem -> "WItem"
|SortFnameCellOpt -> "FnameCellOpt"
|SortExps -> "Exps"
|SortIOError -> "IOError"
|SortStringBuffer -> "StringBuffer"
|SortType -> "Type"
|SortTCell -> "TCell"
|SortNstateCellOpt -> "NstateCellOpt"
|SortInt -> "Int"
|SortStatesCellFragment -> "StatesCellFragment"
|SortStackCell -> "StackCell"
|SortBlock -> "Block"
|SortFunDefCellMap -> "FunDefCellMap"
|SortKConfigVar -> "KConfigVar"
|SortStateCellFragment -> "StateCellFragment"
|SortKCellOpt -> "KCellOpt"
|SortFbodyCell -> "FbodyCell"
|SortProps -> "Props"
|SortIndexes -> "Indexes"
|SortStateCellMap -> "StateCellMap"
|SortId -> "Id"
|SortUninit -> "Uninit"
|SortFunDefCell -> "FunDefCell"
|SortList -> "List"
|SortStateCell -> "StateCell"
|SortExp -> "Exp"
let print_klabel(c: klabel) : string = match c with 
|Lbl'Hash'argv -> "#argv"
|LblisFbodyCellOpt -> "isFbodyCellOpt"
|LblMap'Coln'lookup -> "`Map:lookup`"
|Lbl'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#seek(_,_)_K-IO`"
|LblsignExtendBitRangeInt -> "signExtendBitRangeInt"
|Lbl_'EqlsEqls'Bool__BOOL -> "`_==Bool__BOOL`"
|LblisSet -> "isSet"
|Lbltransfer__'SCln'_OSL'Hyph'SYNTAX -> "`transfer__;_OSL-SYNTAX`"
|LblinitNstateCell -> "initNstateCell"
|Lbl_'_LT_Eqls'Set__SET -> "`_<=Set__SET`"
|LblinitFparamsCell -> "initFparamsCell"
|LblnoEnvCell -> "noEnvCell"
|Lbl'Apos'__OSL'Hyph'SYNTAX -> "`'__OSL-SYNTAX`"
|LblisIOError -> "isIOError"
|Lbl'Hash'parse -> "#parse"
|Lbl'Hash'freezer'Hash'Transfer1_ -> "`#freezer#Transfer1_`"
|Lbl'Hash'EALREADY_K'Hyph'IO -> "`#EALREADY_K-IO`"
|Lbl_'SCln'_OSL'Hyph'SYNTAX -> "`_;_OSL-SYNTAX`"
|LblmakeList -> "makeList"
|LblisSeparator -> "isSeparator"
|LblinitIndexesCell -> "initIndexesCell"
|Lbl'Hash'ESPIPE_K'Hyph'IO -> "`#ESPIPE_K-IO`"
|LblinitFbodyCell -> "initFbodyCell"
|LblisFunDefCellFragment -> "isFunDefCellFragment"
|Lbl'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#unlock(_,_)_K-IO`"
|Lbl'Hash'ENOENT_K'Hyph'IO -> "`#ENOENT_K-IO`"
|LblisFnameCellOpt -> "isFnameCellOpt"
|Lbl'Hash'mborrow -> "#mborrow"
|LblisBlockItem -> "isBlockItem"
|Lbl_StateCellMap_ -> "`_StateCellMap_`"
|Lbldestruct_'SCln'_OSL'Hyph'SYNTAX -> "`destruct_;_OSL-SYNTAX`"
|Lbl'Hash'ENOTTY_K'Hyph'IO -> "`#ENOTTY_K-IO`"
|Lbl'_LT_'states'_GT_' -> "`<states>`"
|LblisStatesCellOpt -> "isStatesCellOpt"
|Lbl'Hash'freezer'Hash'Read0_ -> "`#freezer#Read0_`"
|LblinitWriteCell -> "initWriteCell"
|Lbl'Hash'ENOTEMPTY_K'Hyph'IO -> "`#ENOTEMPTY_K-IO`"
|Lbl'Hash'borrowmutck -> "#borrowmutck"
|Lbl'Hash'branch -> "#branch"
|LblisBorrowItem -> "isBorrowItem"
|Lbl'Hash'EMSGSIZE_K'Hyph'IO -> "`#EMSGSIZE_K-IO`"
|LblisKConfigVar -> "isKConfigVar"
|Lbl'Hash'ENETRESET_K'Hyph'IO -> "`#ENETRESET_K-IO`"
|Lbl'Hash'EAFNOSUPPORT_K'Hyph'IO -> "`#EAFNOSUPPORT_K-IO`"
|Lbl'Hash'loc -> "#loc"
|LblisFunDefsCellFragment -> "isFunDefsCellFragment"
|Lbl'Hash'mutRef -> "#mutRef"
|Lbl'Hash'lc -> "#lc"
|LblisCell -> "isCell"
|Lbl'Hash'ENOMEM_K'Hyph'IO -> "`#ENOMEM_K-IO`"
|Lblvalues -> "values"
|Lbl'Hash'expStmt -> "#expStmt"
|Lbl'Hash'own -> "#own"
|Lbl'Hash'borrowImmCK -> "#borrowImmCK"
|LblisRItem -> "isRItem"
|LblisStoreCellOpt -> "isStoreCellOpt"
|Lbl'Hash'ENXIO_K'Hyph'IO -> "`#ENXIO_K-IO`"
|Lbl_'_LT_'Int__INT -> "`_<Int__INT`"
|LblisLoopItem -> "isLoopItem"
|LblisWItem -> "isWItem"
|Lbl'Hash'configuration_K'Hyph'REFLECTION -> "`#configuration_K-REFLECTION`"
|LblisFunDefsCellOpt -> "isFunDefsCellOpt"
|Lbl'Hash'Loc -> "#Loc"
|LblisFloat -> "isFloat"
|LblisOItem -> "isOItem"
|LblinitFretCell -> "initFretCell"
|LblinitFunDefsCell -> "initFunDefsCell"
|LblchrChar -> "chrChar"
|Lbl_divInt__INT -> "`_divInt__INT`"
|Lbl'Hash'EROFS_K'Hyph'IO -> "`#EROFS_K-IO`"
|Lbl'Hash'wv -> "#wv"
|Lbl_'Plus'Int_ -> "`_+Int_`"
|Lbl'Hash'uninit_OSL'Hyph'SYNTAX -> "`#uninit_OSL-SYNTAX`"
|Lbl_orBool__BOOL -> "`_orBool__BOOL`"
|LblnoIndexCell -> "noIndexCell"
|LblnoTmpCell -> "noTmpCell"
|Lbl'Hash'ENFILE_K'Hyph'IO -> "`#ENFILE_K-IO`"
|LblupdateMap -> "updateMap"
|LblInt2String -> "`Int2String`"
|Lbl'Hash'deallocate -> "#deallocate"
|Lbl_'EqlsSlshEqls'K_ -> "`_=/=K_`"
|LblFunDefCellMapItem -> "`FunDefCellMapItem`"
|Lbl'Stop'StateCellMap -> "`.StateCellMap`"
|LblisTimerCellOpt -> "isTimerCellOpt"
|Lbl_List_ -> "`_List_`"
|Lbl'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#open(_,_)_K-IO`"
|Lbl'Hash'EOPNOTSUPP_K'Hyph'IO -> "`#EOPNOTSUPP_K-IO`"
|Lbl_'PipeHyph_GT_'_ -> "`_|->_`"
|Lbl_'Hyph'Map__MAP -> "`_-Map__MAP`"
|Lbl'Hash'compareS -> "#compareS"
|Lblmut_OSL'Hyph'SYNTAX -> "`mut_OSL-SYNTAX`"
|LblisIndexesCellOpt -> "isIndexesCellOpt"
|Lbl'Hash'EMLINK_K'Hyph'IO -> "`#EMLINK_K-IO`"
|LblinitStoreCell -> "initStoreCell"
|Lbl'Hash'borrowMutCK -> "#borrowMutCK"
|Lbl'Hash'TransferMB -> "#TransferMB"
|Lbl'Hash'sort -> "#sort"
|Lbl_'EqlsEqls'K_ -> "`_==K_`"
|LblreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING -> "`replaceFirst(_,_,_)_STRING`"
|Lbl'Hash'EOVERFLOW_K'Hyph'IO -> "`#EOVERFLOW_K-IO`"
|Lbl'Hash'FnCall -> "#FnCall"
|Lbl'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#putc(_,_)_K-IO`"
|LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX -> "`newResource(_)_OSL-SYNTAX`"
|Lbl'Hash'increaseIndex_OSL -> "`#increaseIndex_OSL`"
|Lbl'Stop'Map -> "`.Map`"
|LblisTmpCellOpt -> "isTmpCellOpt"
|Lbl_'EqlsSlshEqls'String__STRING -> "`_=/=String__STRING`"
|Lbl'Hash'EIO_K'Hyph'IO -> "`#EIO_K-IO`"
|Lbl'Hash'Transfer -> "#Transfer"
|LblisStateCellFragment -> "isStateCellFragment"
|LblisType -> "isType"
|LblisInt -> "isInt"
|LblisBranchTmp -> "isBranchTmp"
|Lbl_FunDefCellMap_ -> "`_FunDefCellMap_`"
|Lbl'Hash'EFAULT_K'Hyph'IO -> "`#EFAULT_K-IO`"
|Lbl'Hash'secondBranch -> "#secondBranch"
|Lbl'Hash'fresh -> "#fresh"
|Lbl'Hash'TransferIB -> "#TransferIB"
|Lbl_impliesBool__BOOL -> "`_impliesBool__BOOL`"
|Lbl_'Star'Int__INT -> "`_*Int__INT`"
|Lbl'Hash'repeat -> "#repeat"
|Lbl'_LT_'T'_GT_' -> "`<T>`"
|Lbl'Hash'Thread -> "#Thread"
|Lbl_'Comm'__OSL'Hyph'SYNTAX -> "`_,__OSL-SYNTAX`"
|LblmaxInt'LPar'_'Comm'_'RPar'_INT -> "`maxInt(_,_)_INT`"
|Lbl'Hash'EDEADLK_K'Hyph'IO -> "`#EDEADLK_K-IO`"
|Lbl'_LT_'tmp'_GT_' -> "`<tmp>`"
|Lbl_'_LT_Eqls'String__STRING -> "`_<=String__STRING`"
|LblisStackCellOpt -> "isStackCellOpt"
|LblisStmt -> "isStmt"
|Lbl'Hash'ENOBUFS_K'Hyph'IO -> "`#ENOBUFS_K-IO`"
|Lbl_Map_ -> "`_Map_`"
|Lbl_'Hyph'Int__INT -> "`_-Int__INT`"
|Lblproject'Coln'Stmts -> "`project:Stmts`"
|Lbl'Hash'EOF_K'Hyph'IO -> "`#EOF_K-IO`"
|Lbl'Hash'compareA -> "#compareA"
|LblFloat2String -> "`Float2String`"
|LblisValue -> "isValue"
|LblsizeList -> "sizeList"
|Lbl'Hash'EWOULDBLOCK_K'Hyph'IO -> "`#EWOULDBLOCK_K-IO`"
|LblString2Id -> "`String2Id`"
|Lbl'Hash'decompose -> "#decompose"
|Lbl_'EqlsSlshEqls'Bool__BOOL -> "`_=/=Bool__BOOL`"
|Lbl'Hash'EFBIG_K'Hyph'IO -> "`#EFBIG_K-IO`"
|LblisTCell -> "isTCell"
|Lbl'Hash'EBADF_K'Hyph'IO -> "`#EBADF_K-IO`"
|Lbl'Hash'freezer'Hash'TransferIB1_ -> "`#freezer#TransferIB1_`"
|Lbl'Hash'EPIPE_K'Hyph'IO -> "`#EPIPE_K-IO`"
|LblnoStoreCell -> "noStoreCell"
|Lbl_'Xor_Perc'Int___INT -> "`_^%Int___INT`"
|Lbl'_LT_'funDef'_GT_' -> "`<funDef>`"
|Lbl'Hash'Deallocate -> "#Deallocate"
|LblinitIndexCell -> "initIndexCell"
|Lbl'Hash'function -> "#function"
|LblisFunDefCellMap -> "isFunDefCellMap"
|LblrfindString -> "rfindString"
|Lbl'Hash'ESOCKTNOSUPPORT_K'Hyph'IO -> "`#ESOCKTNOSUPPORT_K-IO`"
|Lbl'Hash'EINTR_K'Hyph'IO -> "`#EINTR_K-IO`"
|Lbl'_LT_'store'_GT_' -> "`<store>`"
|Lbl'Hash'stat'LPar'_'RPar'_K'Hyph'IO -> "`#stat(_)_K-IO`"
|LblupdateList -> "updateList"
|Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra' -> "`.List{\"_,__OSL-SYNTAX\"}`"
|Lbl'Hash'compare -> "#compare"
|Lbl'_LT_'funDefs'_GT_Hyph'fragment -> "`<funDefs>-fragment`"
|Lbl'_LT_'timer'_GT_' -> "`<timer>`"
|Lbl'_LT_'stack'_GT_' -> "`<stack>`"
|LblcategoryChar -> "categoryChar"
|LblSet'Coln'difference -> "`Set:difference`"
|Lbl'Hash'EHOSTUNREACH_K'Hyph'IO -> "`#EHOSTUNREACH_K-IO`"
|Lbl'Hash'ECONNRESET_K'Hyph'IO -> "`#ECONNRESET_K-IO`"
|LblisKCellOpt -> "isKCellOpt"
|Lbl'Hash'ECHILD_K'Hyph'IO -> "`#ECHILD_K-IO`"
|Lbl'Hash'indexes -> "#indexes"
|LblString2Float -> "`String2Float`"
|LblMap'Coln'lookupOrDefault -> "`Map:lookupOrDefault`"
|Lbl'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL -> "`#if_#then_#else_#fi_K-EQUAL`"
|Lbl'Hash'ENOTCONN_K'Hyph'IO -> "`#ENOTCONN_K-IO`"
|Lbl'Hash'stdout_K'Hyph'IO -> "`#stdout_K-IO`"
|Lbl_'And'Int__INT -> "`_&Int__INT`"
|Lbl'Hash'increaseTimer_OSL -> "`#increaseTimer_OSL`"
|Lbl'Hash'borrowimmck -> "#borrowimmck"
|Lbl'Hash'ENAMETOOLONG_K'Hyph'IO -> "`#ENAMETOOLONG_K-IO`"
|Lbllog2Int -> "log2Int"
|Lbl_'EqlsSlshEqls'Int__INT -> "`_=/=Int__INT`"
|Lbl'Hash'declTy -> "#declTy"
|Lbl'Hash'stdin_K'Hyph'IO -> "`#stdin_K-IO`"
|Lbl'Hash'freezer'Hash'Transfer0_ -> "`#freezer#Transfer0_`"
|Lbl_'_GT_Eqls'String__STRING -> "`_>=String__STRING`"
|Lbl'Stop'FunDefCellMap -> "`.FunDefCellMap`"
|LblsizeMap -> "sizeMap"
|LblnoWriteCell -> "noWriteCell"
|LblnoNstateCell -> "noNstateCell"
|LblisId -> "isId"
|LblsubstrString -> "substrString"
|Lblsize -> "size"
|LblisIndexes -> "isIndexes"
|LblinitTimerCell -> "initTimerCell"
|Lbl'Hash'ENETUNREACH_K'Hyph'IO -> "`#ENETUNREACH_K-IO`"
|LblisFretCellOpt -> "isFretCellOpt"
|Lbl'Hash'EPROTOTYPE_K'Hyph'IO -> "`#EPROTOTYPE_K-IO`"
|LblinitStatesCell -> "initStatesCell"
|Lbl'Hash'freezer'Hash'TransferMB1_ -> "`#freezer#TransferMB1_`"
|Lbl'Hash'systemResult'LPar'_'Comm'_'Comm'_'RPar'_K'Hyph'IO -> "`#systemResult(_,_,_)_K-IO`"
|LblisWriteCellOpt -> "isWriteCellOpt"
|LblinitFunDefCell -> "initFunDefCell"
|LblnoFnameCell -> "noFnameCell"
|LblsrandInt -> "srandInt"
|Lbl'Hash'EINVAL_K'Hyph'IO -> "`#EINVAL_K-IO`"
|LblisKItem -> "isKItem"
|Lbl'Hash'ENODEV_K'Hyph'IO -> "`#ENODEV_K-IO`"
|LblList'Coln'set -> "`List:set`"
|LblString2Base -> "`String2Base`"
|LblnoStackCell -> "noStackCell"
|LblisStatesCellFragment -> "isStatesCellFragment"
|Lbl'Hash'noparse_K'Hyph'IO -> "`#noparse_K-IO`"
|LblisFbodyCell -> "isFbodyCell"
|Lbl'_LT_'write'_GT_' -> "`<write>`"
|Lblkeys -> "keys"
|LblisLifetime -> "isLifetime"
|Lbl'Hash'ESHUTDOWN_K'Hyph'IO -> "`#ESHUTDOWN_K-IO`"
|LblisParameter -> "isParameter"
|LblisFparamsCell -> "isFparamsCell"
|Lbl'Hash'Read -> "#Read"
|Lbl'Hash'ENOTDIR_K'Hyph'IO -> "`#ENOTDIR_K-IO`"
|LblisExp -> "isExp"
|LblisLOCK -> "isLOCK"
|LblisFparamsCellOpt -> "isFparamsCellOpt"
|Lbl'Hash'lv -> "#lv"
|Lbl_'_LT_Eqls'Int__INT -> "`_<=Int__INT`"
|LblnotBool_ -> "`notBool_`"
|Lbl'Hash'stderr_K'Hyph'IO -> "`#stderr_K-IO`"
|LblnoKCell -> "noKCell"
|Lbl'Hash'EBUSY_K'Hyph'IO -> "`#EBUSY_K-IO`"
|LblisTimerCell -> "isTimerCell"
|Lbl'Hash'getenv -> "#getenv"
|Lbl'_LT_'fparams'_GT_' -> "`<fparams>`"
|LblintersectSet -> "intersectSet"
|Lbl_in_keys'LPar'_'RPar'_MAP -> "`_in_keys(_)_MAP`"
|LblfindChar -> "findChar"
|LblisStatesCell -> "isStatesCell"
|LblSet'Coln'in -> "`Set:in`"
|LblisK -> "isK"
|LblString2Int -> "`String2Int`"
|Lbl'Hash'ENETDOWN_K'Hyph'IO -> "`#ENETDOWN_K-IO`"
|Lbl_'LSqB'_'_LT_Hyph'undef'RSqB' -> "`_[_<-undef]`"
|Lbl'Hash'Bottom -> "#Bottom"
|Lbl'Hash'writev -> "#writev"
|Lbl_'EqlsEqls'Int_ -> "`_==Int_`"
|Lbl_andThenBool__BOOL -> "`_andThenBool__BOOL`"
|Lbl'Hash'parseInModule -> "#parseInModule"
|Lbl'Hash'writeCK -> "#writeCK"
|Lbl'Hash'cint -> "#cint"
|Lbl'Stop'List'LBraQuotHash'props'QuotRBra' -> "`.List{\"#props\"}`"
|LblnoIndexesCell -> "noIndexesCell"
|LblnoFparamsCell -> "noFparamsCell"
|Lbl'Hash'system -> "#system"
|LblisString -> "isString"
|LblisProp -> "isProp"
|Lbl'Hash'parameter -> "#parameter"
|Lbl_'Perc'Int__INT -> "`_%Int__INT`"
|Lbl_'_GT__GT_'Int__INT -> "`_>>Int__INT`"
|Lbl'Hash'decl -> "#decl"
|LblisList -> "isList"
|LblisTmpCell -> "isTmpCell"
|Lbl'Hash'EPROTONOSUPPORT_K'Hyph'IO -> "`#EPROTONOSUPPORT_K-IO`"
|LblisFunction -> "isFunction"
|LblreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING -> "`replaceAll(_,_,_)_STRING`"
|Lbl'Hash'EDESTADDRREQ_K'Hyph'IO -> "`#EDESTADDRREQ_K-IO`"
|Lbl'Hash'EADDRINUSE_K'Hyph'IO -> "`#EADDRINUSE_K-IO`"
|LblisNstateCell -> "isNstateCell"
|Lbl_'Xor_'Int__INT -> "`_^Int__INT`"
|LblfindString -> "findString"
|Lbl'_LT_'k'_GT_' -> "`<k>`"
|Lbl'_LT_'funDefs'_GT_' -> "`<funDefs>`"
|Lbl'_LT_'fbody'_GT_' -> "`<fbody>`"
|LblabsInt -> "absInt"
|Lblval -> "val"
|Lbl'Hash'EHOSTDOWN_K'Hyph'IO -> "`#EHOSTDOWN_K-IO`"
|LblisEnvCell -> "isEnvCell"
|Lbl_'_GT_'String__STRING -> "`_>String__STRING`"
|Lbl'_LT_'state'_GT_Hyph'fragment -> "`<state>-fragment`"
|Lbl_'EqlsEqls'String__STRING -> "`_==String__STRING`"
|Lbl'Hash'checkInit -> "#checkInit"
|LblisProps -> "isProps"
|LblisKResult -> "isKResult"
|LblList'Coln'get -> "`List:get`"
|LblisStackCell -> "isStackCell"
|Lbl'Hash'lstat'LPar'_'RPar'_K'Hyph'IO -> "`#lstat(_)_K-IO`"
|LblisIndexesCell -> "isIndexesCell"
|Lbl'Hash'freezer'Hash'Deallocate0_ -> "`#freezer#Deallocate0_`"
|LblinitFnameCell -> "initFnameCell"
|LblnoFretCell -> "noFretCell"
|LblSetItem -> "`SetItem`"
|Lblcopy_OSL'Hyph'SYNTAX -> "`copy_OSL-SYNTAX`"
|LblunsignedBytes -> "unsignedBytes"
|LblStateCellMapItem -> "`StateCellMapItem`"
|Lbl'Stop'List -> "`.List`"
|Lbl'Hash'ENOLCK_K'Hyph'IO -> "`#ENOLCK_K-IO`"
|Lbl'Hash'ECONNABORTED_K'Hyph'IO -> "`#ECONNABORTED_K-IO`"
|LblrandInt -> "randInt"
|Lbl'Hash'EXDEV_K'Hyph'IO -> "`#EXDEV_K-IO`"
|Lbl'Hash'close'LPar'_'RPar'_K'Hyph'IO -> "`#close(_)_K-IO`"
|LblisUninit -> "isUninit"
|Lbl'Hash'props -> "#props"
|Lblkeys_list'LPar'_'RPar'_MAP -> "`keys_list(_)_MAP`"
|LblfreshId -> "freshId"
|Lbl_orElseBool__BOOL -> "`_orElseBool__BOOL`"
|Lbl'Hash'EISDIR_K'Hyph'IO -> "`#EISDIR_K-IO`"
|LblisIndexCell -> "isIndexCell"
|LblList'Coln'range -> "`List:range`"
|LblisKCell -> "isKCell"
|Lbl'Hash'unknownIOError -> "#unknownIOError"
|Lbl_'_GT_Eqls'Int__INT -> "`_>=Int__INT`"
|Lbl'Hash'voidTy_OSL'Hyph'SYNTAX -> "`#voidTy_OSL-SYNTAX`"
|LblinitEnvCell -> "initEnvCell"
|Lbl'Hash'freezer'Hash'expStmt0_ -> "`#freezer#expStmt0_`"
|Lbl'Hash'compareE -> "#compareE"
|Lbl'Hash'bindParams -> "#bindParams"
|LblisNstateCellOpt -> "isNstateCellOpt"
|Lbl_'Stop'__OSL'Hyph'SYNTAX -> "`_.__OSL-SYNTAX`"
|Lbl'Hash'ENOSYS_K'Hyph'IO -> "`#ENOSYS_K-IO`"
|Lbl'Hash'ECONNREFUSED_K'Hyph'IO -> "`#ECONNREFUSED_K-IO`"
|Lbl'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#lock(_,_)_K-IO`"
|Lbl'Hash'EADDRNOTAVAIL_K'Hyph'IO -> "`#EADDRNOTAVAIL_K-IO`"
|LblcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING -> "`countAllOccurrences(_,_)_STRING`"
|Lbl_'_GT_'Int__INT -> "`_>Int__INT`"
|LblfillList -> "fillList"
|Lbl'_LT_'states'_GT_Hyph'fragment -> "`<states>-fragment`"
|LblnoFunDefsCell -> "noFunDefsCell"
|LblisBlocks -> "isBlocks"
|Lbl'Hash'removeState_CONTROL -> "`#removeState_CONTROL`"
|LblbitRangeInt -> "bitRangeInt"
|Lbl_'_LT_'String__STRING -> "`_<String__STRING`"
|Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra' -> "`.List{\"___OSL-SYNTAX\"}`"
|Lbl'_LT_'funDef'_GT_Hyph'fragment -> "`<funDef>-fragment`"
|Lbl'Hash'ThreadLocal -> "#ThreadLocal"
|Lbl'Hash'inProps -> "#inProps"
|Lbl_xorBool__BOOL -> "`_xorBool__BOOL`"
|Lbl'_LT_'fname'_GT_' -> "`<fname>`"
|LblisIndexCellOpt -> "isIndexCellOpt"
|Lbl'Hash'open'LPar'_'RPar'_K'Hyph'IO -> "`#open(_)_K-IO`"
|Lbl'_LT_'env'_GT_' -> "`<env>`"
|Lbl'Hash'ETOOMANYREFS_K'Hyph'IO -> "`#ETOOMANYREFS_K-IO`"
|Lbl'Hash'ENOSPC_K'Hyph'IO -> "`#ENOSPC_K-IO`"
|Lbl'Hash'br -> "#br"
|Lbl'Hash'logToFile -> "#logToFile"
|Lbl'Hash'freezer'Hash'lvDref0_ -> "`#freezer#lvDref0_`"
|Lbl'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#read(_,_)_K-IO`"
|Lbl'Hash'ref -> "#ref"
|LblbigEndianBytes -> "bigEndianBytes"
|LblId2String -> "`Id2String`"
|LblMap'Coln'choice -> "`Map:choice`"
|LblisFunDefsCell -> "isFunDefsCell"
|LblisExps -> "isExps"
|Lbl'Hash'borrow -> "#borrow"
|Lbl___OSL'Hyph'SYNTAX -> "`___OSL-SYNTAX`"
|Lbl_Set_ -> "`_Set_`"
|Lbl'Hash'EEXIST_K'Hyph'IO -> "`#EEXIST_K-IO`"
|Lbl'Hash'getc'LPar'_'RPar'_K'Hyph'IO -> "`#getc(_)_K-IO`"
|LblinitTmpCell -> "initTmpCell"
|Lbl'Hash'unwrapInt -> "#unwrapInt"
|Lbl'_LT_'state'_GT_' -> "`<state>`"
|LblisBool -> "isBool"
|Lbl'Tild'Int__INT -> "`~Int__INT`"
|Lbl'Hash'list2Set -> "#list2Set"
|LblordChar -> "ordChar"
|Lbl_modInt__INT -> "`_modInt__INT`"
|LblrfindChar -> "rfindChar"
|Lbl'Hash'EAGAIN_K'Hyph'IO -> "`#EAGAIN_K-IO`"
|LblisIndexItem -> "isIndexItem"
|LbldirectionalityChar -> "directionalityChar"
|Lbl'Hash'opendir'LPar'_'RPar'_K'Hyph'IO -> "`#opendir(_)_K-IO`"
|LblnoTimerCell -> "noTimerCell"
|LblinitKCell -> "initKCell"
|Lbl'Hash'rs -> "#rs"
|LblisDItem -> "isDItem"
|Lbl'Stop'Set -> "`.Set`"
|LblisStateCell -> "isStateCell"
|Lbl'Hash'EACCES_K'Hyph'IO -> "`#EACCES_K-IO`"
|Lbl'Hash'ELOOP_K'Hyph'IO -> "`#ELOOP_K-IO`"
|Lbl'Hash'EDOM_K'Hyph'IO -> "`#EDOM_K-IO`"
|LblremoveAll -> "removeAll"
|LblisFunDefCell -> "isFunDefCell"
|Lbl_andBool_ -> "`_andBool_`"
|Lbl'Hash'loopSep -> "#loopSep"
|Lbl'_LT_'fret'_GT_' -> "`<fret>`"
|Lbl'Hash'TransferV -> "#TransferV"
|Lbl'Hash'EPFNOSUPPORT_K'Hyph'IO -> "`#EPFNOSUPPORT_K-IO`"
|LbllengthString -> "lengthString"
|LblinitStackCell -> "initStackCell"
|Lbl'Hash'ERANGE_K'Hyph'IO -> "`#ERANGE_K-IO`"
|LblinitTCell -> "initTCell"
|LblisFnameCell -> "isFnameCell"
|LblsignedBytes -> "signedBytes"
|LblnoFbodyCell -> "noFbodyCell"
|LblFloatFormat -> "`FloatFormat`"
|Lbl'Hash'ENOTSOCK_K'Hyph'IO -> "`#ENOTSOCK_K-IO`"
|Lbl_'Plus'String__STRING -> "`_+String__STRING`"
|Lbl_'Pipe'Int__INT -> "`_|Int__INT`"
|Lbl'Hash'Transferuninit -> "#Transferuninit"
|Lbl'Hash'EISCONN_K'Hyph'IO -> "`#EISCONN_K-IO`"
|Lbl_dividesInt__INT -> "`_dividesInt__INT`"
|LblisStateCellMap -> "isStateCellMap"
|Lbl'_LT_'T'_GT_Hyph'fragment -> "`<T>-fragment`"
|LblSet'Coln'choice -> "`Set:choice`"
|Lbl'Star'__OSL'Hyph'SYNTAX -> "`*__OSL-SYNTAX`"
|Lbl'Hash'buffer -> "#buffer"
|LblfreshInt -> "freshInt"
|Lbl'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#write(_,_)_K-IO`"
|Lbl'Hash'ETIMEDOUT_K'Hyph'IO -> "`#ETIMEDOUT_K-IO`"
|LblisBlock -> "isBlock"
|Lbl'Hash'transfer -> "#transfer"
|LblisParameters -> "isParameters"
|Lbl_xorInt__INT -> "`_xorInt__INT`"
|Lbl'Hash'EINPROGRESS_K'Hyph'IO -> "`#EINPROGRESS_K-IO`"
|LblinitStateCell -> "initStateCell"
|Lbl'_LT_'indexes'_GT_' -> "`<indexes>`"
|Lbl'Hash'ENOPROTOOPT_K'Hyph'IO -> "`#ENOPROTOOPT_K-IO`"
|LbllittleEndianBytes -> "littleEndianBytes"
|Lbl'Hash'EPERM_K'Hyph'IO -> "`#EPERM_K-IO`"
|Lbl_'_LT__LT_'Int__INT -> "`_<<Int__INT`"
|Lbl'Hash'block -> "#block"
|Lbl'Hash'freezer'Hash'Transferuninit0_ -> "`#freezer#Transferuninit0_`"
|Lbl'_LT_'index'_GT_' -> "`<index>`"
|Lbl'Hash'immRef -> "#immRef"
|LblBase2String -> "`Base2String`"
|LblListItem -> "`ListItem`"
|LblisWriteCell -> "isWriteCell"
|LblisStream -> "isStream"
|Lbl_'_LT_Eqls'Map__MAP -> "`_<=Map__MAP`"
|LblnewUUID_STRING -> "`newUUID_STRING`"
|Lbl'_LT_'nstate'_GT_' -> "`<nstate>`"
|Lbl'Hash'ESRCH_K'Hyph'IO -> "`#ESRCH_K-IO`"
|Lbl'Hash'EMFILE_K'Hyph'IO -> "`#EMFILE_K-IO`"
|Lblloop_'SCln'_OSL'Hyph'SYNTAX -> "`loop_;_OSL-SYNTAX`"
|Lbl'Hash'unwrapVal -> "#unwrapVal"
|Lbl'Hash'read -> "#read"
|Lbl'Hash'existRef -> "#existRef"
|Lbl_inList_ -> "`_inList_`"
|Lbl'Hash'void_OSL'Hyph'SYNTAX -> "`#void_OSL-SYNTAX`"
|Lbl'Hash'ENOEXEC_K'Hyph'IO -> "`#ENOEXEC_K-IO`"
|Lbl'Hash'freezerval0_ -> "`#freezerval0_`"
|LblminInt'LPar'_'Comm'_'RPar'_INT -> "`minInt(_,_)_INT`"
|LblisMap -> "isMap"
|LblisStoreCell -> "isStoreCell"
|LblisStmts -> "isStmts"
|Lbl'Hash'lvDref -> "#lvDref"
|LblisTCellFragment -> "isTCellFragment"
|Lbl'Hash'uninitialize -> "#uninitialize"
|Lblreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING -> "`replace(_,_,_,_)_STRING`"
|Lbl_'Slsh'Int__INT -> "`_/Int__INT`"
|Lbl'Hash'blockend_BLOCK -> "`#blockend_BLOCK`"
|Lbl_'LSqB'_'_LT_Hyph'_'RSqB'_MAP -> "`_[_<-_]_MAP`"
|Lbl'Hash'tell'LPar'_'RPar'_K'Hyph'IO -> "`#tell(_)_K-IO`"
|LblgetKLabel -> "getKLabel"
|LblisEnvCellOpt -> "isEnvCellOpt"
|Lbl'Hash'E2BIG_K'Hyph'IO -> "`#E2BIG_K-IO`"
|LblnoStatesCell -> "noStatesCell"
|Lbl'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "`#seekEnd(_,_)_K-IO`"
|LblisFretCell -> "isFretCell"
let print_klabel_string(c: klabel) : string = match c with 
|Lbl'Hash'argv -> "#argv"
|LblisFbodyCellOpt -> "isFbodyCellOpt"
|LblMap'Coln'lookup -> "Map:lookup"
|Lbl'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#seek(_,_)_K-IO"
|LblsignExtendBitRangeInt -> "signExtendBitRangeInt"
|Lbl_'EqlsEqls'Bool__BOOL -> "_==Bool__BOOL"
|LblisSet -> "isSet"
|Lbltransfer__'SCln'_OSL'Hyph'SYNTAX -> "transfer__;_OSL-SYNTAX"
|LblinitNstateCell -> "initNstateCell"
|Lbl_'_LT_Eqls'Set__SET -> "_<=Set__SET"
|LblinitFparamsCell -> "initFparamsCell"
|LblnoEnvCell -> "noEnvCell"
|Lbl'Apos'__OSL'Hyph'SYNTAX -> "'__OSL-SYNTAX"
|LblisIOError -> "isIOError"
|Lbl'Hash'parse -> "#parse"
|Lbl'Hash'freezer'Hash'Transfer1_ -> "#freezer#Transfer1_"
|Lbl'Hash'EALREADY_K'Hyph'IO -> "#EALREADY_K-IO"
|Lbl_'SCln'_OSL'Hyph'SYNTAX -> "_;_OSL-SYNTAX"
|LblmakeList -> "makeList"
|LblisSeparator -> "isSeparator"
|LblinitIndexesCell -> "initIndexesCell"
|Lbl'Hash'ESPIPE_K'Hyph'IO -> "#ESPIPE_K-IO"
|LblinitFbodyCell -> "initFbodyCell"
|LblisFunDefCellFragment -> "isFunDefCellFragment"
|Lbl'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#unlock(_,_)_K-IO"
|Lbl'Hash'ENOENT_K'Hyph'IO -> "#ENOENT_K-IO"
|LblisFnameCellOpt -> "isFnameCellOpt"
|Lbl'Hash'mborrow -> "#mborrow"
|LblisBlockItem -> "isBlockItem"
|Lbl_StateCellMap_ -> "_StateCellMap_"
|Lbldestruct_'SCln'_OSL'Hyph'SYNTAX -> "destruct_;_OSL-SYNTAX"
|Lbl'Hash'ENOTTY_K'Hyph'IO -> "#ENOTTY_K-IO"
|Lbl'_LT_'states'_GT_' -> "<states>"
|LblisStatesCellOpt -> "isStatesCellOpt"
|Lbl'Hash'freezer'Hash'Read0_ -> "#freezer#Read0_"
|LblinitWriteCell -> "initWriteCell"
|Lbl'Hash'ENOTEMPTY_K'Hyph'IO -> "#ENOTEMPTY_K-IO"
|Lbl'Hash'borrowmutck -> "#borrowmutck"
|Lbl'Hash'branch -> "#branch"
|LblisBorrowItem -> "isBorrowItem"
|Lbl'Hash'EMSGSIZE_K'Hyph'IO -> "#EMSGSIZE_K-IO"
|LblisKConfigVar -> "isKConfigVar"
|Lbl'Hash'ENETRESET_K'Hyph'IO -> "#ENETRESET_K-IO"
|Lbl'Hash'EAFNOSUPPORT_K'Hyph'IO -> "#EAFNOSUPPORT_K-IO"
|Lbl'Hash'loc -> "#loc"
|LblisFunDefsCellFragment -> "isFunDefsCellFragment"
|Lbl'Hash'mutRef -> "#mutRef"
|Lbl'Hash'lc -> "#lc"
|LblisCell -> "isCell"
|Lbl'Hash'ENOMEM_K'Hyph'IO -> "#ENOMEM_K-IO"
|Lblvalues -> "values"
|Lbl'Hash'expStmt -> "#expStmt"
|Lbl'Hash'own -> "#own"
|Lbl'Hash'borrowImmCK -> "#borrowImmCK"
|LblisRItem -> "isRItem"
|LblisStoreCellOpt -> "isStoreCellOpt"
|Lbl'Hash'ENXIO_K'Hyph'IO -> "#ENXIO_K-IO"
|Lbl_'_LT_'Int__INT -> "_<Int__INT"
|LblisLoopItem -> "isLoopItem"
|LblisWItem -> "isWItem"
|Lbl'Hash'configuration_K'Hyph'REFLECTION -> "#configuration_K-REFLECTION"
|LblisFunDefsCellOpt -> "isFunDefsCellOpt"
|Lbl'Hash'Loc -> "#Loc"
|LblisFloat -> "isFloat"
|LblisOItem -> "isOItem"
|LblinitFretCell -> "initFretCell"
|LblinitFunDefsCell -> "initFunDefsCell"
|LblchrChar -> "chrChar"
|Lbl_divInt__INT -> "_divInt__INT"
|Lbl'Hash'EROFS_K'Hyph'IO -> "#EROFS_K-IO"
|Lbl'Hash'wv -> "#wv"
|Lbl_'Plus'Int_ -> "_+Int_"
|Lbl'Hash'uninit_OSL'Hyph'SYNTAX -> "#uninit_OSL-SYNTAX"
|Lbl_orBool__BOOL -> "_orBool__BOOL"
|LblnoIndexCell -> "noIndexCell"
|LblnoTmpCell -> "noTmpCell"
|Lbl'Hash'ENFILE_K'Hyph'IO -> "#ENFILE_K-IO"
|LblupdateMap -> "updateMap"
|LblInt2String -> "Int2String"
|Lbl'Hash'deallocate -> "#deallocate"
|Lbl_'EqlsSlshEqls'K_ -> "_=/=K_"
|LblFunDefCellMapItem -> "FunDefCellMapItem"
|Lbl'Stop'StateCellMap -> ".StateCellMap"
|LblisTimerCellOpt -> "isTimerCellOpt"
|Lbl_List_ -> "_List_"
|Lbl'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#open(_,_)_K-IO"
|Lbl'Hash'EOPNOTSUPP_K'Hyph'IO -> "#EOPNOTSUPP_K-IO"
|Lbl_'PipeHyph_GT_'_ -> "_|->_"
|Lbl_'Hyph'Map__MAP -> "_-Map__MAP"
|Lbl'Hash'compareS -> "#compareS"
|Lblmut_OSL'Hyph'SYNTAX -> "mut_OSL-SYNTAX"
|LblisIndexesCellOpt -> "isIndexesCellOpt"
|Lbl'Hash'EMLINK_K'Hyph'IO -> "#EMLINK_K-IO"
|LblinitStoreCell -> "initStoreCell"
|Lbl'Hash'borrowMutCK -> "#borrowMutCK"
|Lbl'Hash'TransferMB -> "#TransferMB"
|Lbl'Hash'sort -> "#sort"
|Lbl_'EqlsEqls'K_ -> "_==K_"
|LblreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING -> "replaceFirst(_,_,_)_STRING"
|Lbl'Hash'EOVERFLOW_K'Hyph'IO -> "#EOVERFLOW_K-IO"
|Lbl'Hash'FnCall -> "#FnCall"
|Lbl'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#putc(_,_)_K-IO"
|LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX -> "newResource(_)_OSL-SYNTAX"
|Lbl'Hash'increaseIndex_OSL -> "#increaseIndex_OSL"
|Lbl'Stop'Map -> ".Map"
|LblisTmpCellOpt -> "isTmpCellOpt"
|Lbl_'EqlsSlshEqls'String__STRING -> "_=/=String__STRING"
|Lbl'Hash'EIO_K'Hyph'IO -> "#EIO_K-IO"
|Lbl'Hash'Transfer -> "#Transfer"
|LblisStateCellFragment -> "isStateCellFragment"
|LblisType -> "isType"
|LblisInt -> "isInt"
|LblisBranchTmp -> "isBranchTmp"
|Lbl_FunDefCellMap_ -> "_FunDefCellMap_"
|Lbl'Hash'EFAULT_K'Hyph'IO -> "#EFAULT_K-IO"
|Lbl'Hash'secondBranch -> "#secondBranch"
|Lbl'Hash'fresh -> "#fresh"
|Lbl'Hash'TransferIB -> "#TransferIB"
|Lbl_impliesBool__BOOL -> "_impliesBool__BOOL"
|Lbl_'Star'Int__INT -> "_*Int__INT"
|Lbl'Hash'repeat -> "#repeat"
|Lbl'_LT_'T'_GT_' -> "<T>"
|Lbl'Hash'Thread -> "#Thread"
|Lbl_'Comm'__OSL'Hyph'SYNTAX -> "_,__OSL-SYNTAX"
|LblmaxInt'LPar'_'Comm'_'RPar'_INT -> "maxInt(_,_)_INT"
|Lbl'Hash'EDEADLK_K'Hyph'IO -> "#EDEADLK_K-IO"
|Lbl'_LT_'tmp'_GT_' -> "<tmp>"
|Lbl_'_LT_Eqls'String__STRING -> "_<=String__STRING"
|LblisStackCellOpt -> "isStackCellOpt"
|LblisStmt -> "isStmt"
|Lbl'Hash'ENOBUFS_K'Hyph'IO -> "#ENOBUFS_K-IO"
|Lbl_Map_ -> "_Map_"
|Lbl_'Hyph'Int__INT -> "_-Int__INT"
|Lblproject'Coln'Stmts -> "project:Stmts"
|Lbl'Hash'EOF_K'Hyph'IO -> "#EOF_K-IO"
|Lbl'Hash'compareA -> "#compareA"
|LblFloat2String -> "Float2String"
|LblisValue -> "isValue"
|LblsizeList -> "sizeList"
|Lbl'Hash'EWOULDBLOCK_K'Hyph'IO -> "#EWOULDBLOCK_K-IO"
|LblString2Id -> "String2Id"
|Lbl'Hash'decompose -> "#decompose"
|Lbl_'EqlsSlshEqls'Bool__BOOL -> "_=/=Bool__BOOL"
|Lbl'Hash'EFBIG_K'Hyph'IO -> "#EFBIG_K-IO"
|LblisTCell -> "isTCell"
|Lbl'Hash'EBADF_K'Hyph'IO -> "#EBADF_K-IO"
|Lbl'Hash'freezer'Hash'TransferIB1_ -> "#freezer#TransferIB1_"
|Lbl'Hash'EPIPE_K'Hyph'IO -> "#EPIPE_K-IO"
|LblnoStoreCell -> "noStoreCell"
|Lbl_'Xor_Perc'Int___INT -> "_^%Int___INT"
|Lbl'_LT_'funDef'_GT_' -> "<funDef>"
|Lbl'Hash'Deallocate -> "#Deallocate"
|LblinitIndexCell -> "initIndexCell"
|Lbl'Hash'function -> "#function"
|LblisFunDefCellMap -> "isFunDefCellMap"
|LblrfindString -> "rfindString"
|Lbl'Hash'ESOCKTNOSUPPORT_K'Hyph'IO -> "#ESOCKTNOSUPPORT_K-IO"
|Lbl'Hash'EINTR_K'Hyph'IO -> "#EINTR_K-IO"
|Lbl'_LT_'store'_GT_' -> "<store>"
|Lbl'Hash'stat'LPar'_'RPar'_K'Hyph'IO -> "#stat(_)_K-IO"
|LblupdateList -> "updateList"
|Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra' -> ".List{\"_,__OSL-SYNTAX\"}"
|Lbl'Hash'compare -> "#compare"
|Lbl'_LT_'funDefs'_GT_Hyph'fragment -> "<funDefs>-fragment"
|Lbl'_LT_'timer'_GT_' -> "<timer>"
|Lbl'_LT_'stack'_GT_' -> "<stack>"
|LblcategoryChar -> "categoryChar"
|LblSet'Coln'difference -> "Set:difference"
|Lbl'Hash'EHOSTUNREACH_K'Hyph'IO -> "#EHOSTUNREACH_K-IO"
|Lbl'Hash'ECONNRESET_K'Hyph'IO -> "#ECONNRESET_K-IO"
|LblisKCellOpt -> "isKCellOpt"
|Lbl'Hash'ECHILD_K'Hyph'IO -> "#ECHILD_K-IO"
|Lbl'Hash'indexes -> "#indexes"
|LblString2Float -> "String2Float"
|LblMap'Coln'lookupOrDefault -> "Map:lookupOrDefault"
|Lbl'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL -> "#if_#then_#else_#fi_K-EQUAL"
|Lbl'Hash'ENOTCONN_K'Hyph'IO -> "#ENOTCONN_K-IO"
|Lbl'Hash'stdout_K'Hyph'IO -> "#stdout_K-IO"
|Lbl_'And'Int__INT -> "_&Int__INT"
|Lbl'Hash'increaseTimer_OSL -> "#increaseTimer_OSL"
|Lbl'Hash'borrowimmck -> "#borrowimmck"
|Lbl'Hash'ENAMETOOLONG_K'Hyph'IO -> "#ENAMETOOLONG_K-IO"
|Lbllog2Int -> "log2Int"
|Lbl_'EqlsSlshEqls'Int__INT -> "_=/=Int__INT"
|Lbl'Hash'declTy -> "#declTy"
|Lbl'Hash'stdin_K'Hyph'IO -> "#stdin_K-IO"
|Lbl'Hash'freezer'Hash'Transfer0_ -> "#freezer#Transfer0_"
|Lbl_'_GT_Eqls'String__STRING -> "_>=String__STRING"
|Lbl'Stop'FunDefCellMap -> ".FunDefCellMap"
|LblsizeMap -> "sizeMap"
|LblnoWriteCell -> "noWriteCell"
|LblnoNstateCell -> "noNstateCell"
|LblisId -> "isId"
|LblsubstrString -> "substrString"
|Lblsize -> "size"
|LblisIndexes -> "isIndexes"
|LblinitTimerCell -> "initTimerCell"
|Lbl'Hash'ENETUNREACH_K'Hyph'IO -> "#ENETUNREACH_K-IO"
|LblisFretCellOpt -> "isFretCellOpt"
|Lbl'Hash'EPROTOTYPE_K'Hyph'IO -> "#EPROTOTYPE_K-IO"
|LblinitStatesCell -> "initStatesCell"
|Lbl'Hash'freezer'Hash'TransferMB1_ -> "#freezer#TransferMB1_"
|Lbl'Hash'systemResult'LPar'_'Comm'_'Comm'_'RPar'_K'Hyph'IO -> "#systemResult(_,_,_)_K-IO"
|LblisWriteCellOpt -> "isWriteCellOpt"
|LblinitFunDefCell -> "initFunDefCell"
|LblnoFnameCell -> "noFnameCell"
|LblsrandInt -> "srandInt"
|Lbl'Hash'EINVAL_K'Hyph'IO -> "#EINVAL_K-IO"
|LblisKItem -> "isKItem"
|Lbl'Hash'ENODEV_K'Hyph'IO -> "#ENODEV_K-IO"
|LblList'Coln'set -> "List:set"
|LblString2Base -> "String2Base"
|LblnoStackCell -> "noStackCell"
|LblisStatesCellFragment -> "isStatesCellFragment"
|Lbl'Hash'noparse_K'Hyph'IO -> "#noparse_K-IO"
|LblisFbodyCell -> "isFbodyCell"
|Lbl'_LT_'write'_GT_' -> "<write>"
|Lblkeys -> "keys"
|LblisLifetime -> "isLifetime"
|Lbl'Hash'ESHUTDOWN_K'Hyph'IO -> "#ESHUTDOWN_K-IO"
|LblisParameter -> "isParameter"
|LblisFparamsCell -> "isFparamsCell"
|Lbl'Hash'Read -> "#Read"
|Lbl'Hash'ENOTDIR_K'Hyph'IO -> "#ENOTDIR_K-IO"
|LblisExp -> "isExp"
|LblisLOCK -> "isLOCK"
|LblisFparamsCellOpt -> "isFparamsCellOpt"
|Lbl'Hash'lv -> "#lv"
|Lbl_'_LT_Eqls'Int__INT -> "_<=Int__INT"
|LblnotBool_ -> "notBool_"
|Lbl'Hash'stderr_K'Hyph'IO -> "#stderr_K-IO"
|LblnoKCell -> "noKCell"
|Lbl'Hash'EBUSY_K'Hyph'IO -> "#EBUSY_K-IO"
|LblisTimerCell -> "isTimerCell"
|Lbl'Hash'getenv -> "#getenv"
|Lbl'_LT_'fparams'_GT_' -> "<fparams>"
|LblintersectSet -> "intersectSet"
|Lbl_in_keys'LPar'_'RPar'_MAP -> "_in_keys(_)_MAP"
|LblfindChar -> "findChar"
|LblisStatesCell -> "isStatesCell"
|LblSet'Coln'in -> "Set:in"
|LblisK -> "isK"
|LblString2Int -> "String2Int"
|Lbl'Hash'ENETDOWN_K'Hyph'IO -> "#ENETDOWN_K-IO"
|Lbl_'LSqB'_'_LT_Hyph'undef'RSqB' -> "_[_<-undef]"
|Lbl'Hash'Bottom -> "#Bottom"
|Lbl'Hash'writev -> "#writev"
|Lbl_'EqlsEqls'Int_ -> "_==Int_"
|Lbl_andThenBool__BOOL -> "_andThenBool__BOOL"
|Lbl'Hash'parseInModule -> "#parseInModule"
|Lbl'Hash'writeCK -> "#writeCK"
|Lbl'Hash'cint -> "#cint"
|Lbl'Stop'List'LBraQuotHash'props'QuotRBra' -> ".List{\"#props\"}"
|LblnoIndexesCell -> "noIndexesCell"
|LblnoFparamsCell -> "noFparamsCell"
|Lbl'Hash'system -> "#system"
|LblisString -> "isString"
|LblisProp -> "isProp"
|Lbl'Hash'parameter -> "#parameter"
|Lbl_'Perc'Int__INT -> "_%Int__INT"
|Lbl_'_GT__GT_'Int__INT -> "_>>Int__INT"
|Lbl'Hash'decl -> "#decl"
|LblisList -> "isList"
|LblisTmpCell -> "isTmpCell"
|Lbl'Hash'EPROTONOSUPPORT_K'Hyph'IO -> "#EPROTONOSUPPORT_K-IO"
|LblisFunction -> "isFunction"
|LblreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING -> "replaceAll(_,_,_)_STRING"
|Lbl'Hash'EDESTADDRREQ_K'Hyph'IO -> "#EDESTADDRREQ_K-IO"
|Lbl'Hash'EADDRINUSE_K'Hyph'IO -> "#EADDRINUSE_K-IO"
|LblisNstateCell -> "isNstateCell"
|Lbl_'Xor_'Int__INT -> "_^Int__INT"
|LblfindString -> "findString"
|Lbl'_LT_'k'_GT_' -> "<k>"
|Lbl'_LT_'funDefs'_GT_' -> "<funDefs>"
|Lbl'_LT_'fbody'_GT_' -> "<fbody>"
|LblabsInt -> "absInt"
|Lblval -> "val"
|Lbl'Hash'EHOSTDOWN_K'Hyph'IO -> "#EHOSTDOWN_K-IO"
|LblisEnvCell -> "isEnvCell"
|Lbl_'_GT_'String__STRING -> "_>String__STRING"
|Lbl'_LT_'state'_GT_Hyph'fragment -> "<state>-fragment"
|Lbl_'EqlsEqls'String__STRING -> "_==String__STRING"
|Lbl'Hash'checkInit -> "#checkInit"
|LblisProps -> "isProps"
|LblisKResult -> "isKResult"
|LblList'Coln'get -> "List:get"
|LblisStackCell -> "isStackCell"
|Lbl'Hash'lstat'LPar'_'RPar'_K'Hyph'IO -> "#lstat(_)_K-IO"
|LblisIndexesCell -> "isIndexesCell"
|Lbl'Hash'freezer'Hash'Deallocate0_ -> "#freezer#Deallocate0_"
|LblinitFnameCell -> "initFnameCell"
|LblnoFretCell -> "noFretCell"
|LblSetItem -> "SetItem"
|Lblcopy_OSL'Hyph'SYNTAX -> "copy_OSL-SYNTAX"
|LblunsignedBytes -> "unsignedBytes"
|LblStateCellMapItem -> "StateCellMapItem"
|Lbl'Stop'List -> ".List"
|Lbl'Hash'ENOLCK_K'Hyph'IO -> "#ENOLCK_K-IO"
|Lbl'Hash'ECONNABORTED_K'Hyph'IO -> "#ECONNABORTED_K-IO"
|LblrandInt -> "randInt"
|Lbl'Hash'EXDEV_K'Hyph'IO -> "#EXDEV_K-IO"
|Lbl'Hash'close'LPar'_'RPar'_K'Hyph'IO -> "#close(_)_K-IO"
|LblisUninit -> "isUninit"
|Lbl'Hash'props -> "#props"
|Lblkeys_list'LPar'_'RPar'_MAP -> "keys_list(_)_MAP"
|LblfreshId -> "freshId"
|Lbl_orElseBool__BOOL -> "_orElseBool__BOOL"
|Lbl'Hash'EISDIR_K'Hyph'IO -> "#EISDIR_K-IO"
|LblisIndexCell -> "isIndexCell"
|LblList'Coln'range -> "List:range"
|LblisKCell -> "isKCell"
|Lbl'Hash'unknownIOError -> "#unknownIOError"
|Lbl_'_GT_Eqls'Int__INT -> "_>=Int__INT"
|Lbl'Hash'voidTy_OSL'Hyph'SYNTAX -> "#voidTy_OSL-SYNTAX"
|LblinitEnvCell -> "initEnvCell"
|Lbl'Hash'freezer'Hash'expStmt0_ -> "#freezer#expStmt0_"
|Lbl'Hash'compareE -> "#compareE"
|Lbl'Hash'bindParams -> "#bindParams"
|LblisNstateCellOpt -> "isNstateCellOpt"
|Lbl_'Stop'__OSL'Hyph'SYNTAX -> "_.__OSL-SYNTAX"
|Lbl'Hash'ENOSYS_K'Hyph'IO -> "#ENOSYS_K-IO"
|Lbl'Hash'ECONNREFUSED_K'Hyph'IO -> "#ECONNREFUSED_K-IO"
|Lbl'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#lock(_,_)_K-IO"
|Lbl'Hash'EADDRNOTAVAIL_K'Hyph'IO -> "#EADDRNOTAVAIL_K-IO"
|LblcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING -> "countAllOccurrences(_,_)_STRING"
|Lbl_'_GT_'Int__INT -> "_>Int__INT"
|LblfillList -> "fillList"
|Lbl'_LT_'states'_GT_Hyph'fragment -> "<states>-fragment"
|LblnoFunDefsCell -> "noFunDefsCell"
|LblisBlocks -> "isBlocks"
|Lbl'Hash'removeState_CONTROL -> "#removeState_CONTROL"
|LblbitRangeInt -> "bitRangeInt"
|Lbl_'_LT_'String__STRING -> "_<String__STRING"
|Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra' -> ".List{\"___OSL-SYNTAX\"}"
|Lbl'_LT_'funDef'_GT_Hyph'fragment -> "<funDef>-fragment"
|Lbl'Hash'ThreadLocal -> "#ThreadLocal"
|Lbl'Hash'inProps -> "#inProps"
|Lbl_xorBool__BOOL -> "_xorBool__BOOL"
|Lbl'_LT_'fname'_GT_' -> "<fname>"
|LblisIndexCellOpt -> "isIndexCellOpt"
|Lbl'Hash'open'LPar'_'RPar'_K'Hyph'IO -> "#open(_)_K-IO"
|Lbl'_LT_'env'_GT_' -> "<env>"
|Lbl'Hash'ETOOMANYREFS_K'Hyph'IO -> "#ETOOMANYREFS_K-IO"
|Lbl'Hash'ENOSPC_K'Hyph'IO -> "#ENOSPC_K-IO"
|Lbl'Hash'br -> "#br"
|Lbl'Hash'logToFile -> "#logToFile"
|Lbl'Hash'freezer'Hash'lvDref0_ -> "#freezer#lvDref0_"
|Lbl'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#read(_,_)_K-IO"
|Lbl'Hash'ref -> "#ref"
|LblbigEndianBytes -> "bigEndianBytes"
|LblId2String -> "Id2String"
|LblMap'Coln'choice -> "Map:choice"
|LblisFunDefsCell -> "isFunDefsCell"
|LblisExps -> "isExps"
|Lbl'Hash'borrow -> "#borrow"
|Lbl___OSL'Hyph'SYNTAX -> "___OSL-SYNTAX"
|Lbl_Set_ -> "_Set_"
|Lbl'Hash'EEXIST_K'Hyph'IO -> "#EEXIST_K-IO"
|Lbl'Hash'getc'LPar'_'RPar'_K'Hyph'IO -> "#getc(_)_K-IO"
|LblinitTmpCell -> "initTmpCell"
|Lbl'Hash'unwrapInt -> "#unwrapInt"
|Lbl'_LT_'state'_GT_' -> "<state>"
|LblisBool -> "isBool"
|Lbl'Tild'Int__INT -> "~Int__INT"
|Lbl'Hash'list2Set -> "#list2Set"
|LblordChar -> "ordChar"
|Lbl_modInt__INT -> "_modInt__INT"
|LblrfindChar -> "rfindChar"
|Lbl'Hash'EAGAIN_K'Hyph'IO -> "#EAGAIN_K-IO"
|LblisIndexItem -> "isIndexItem"
|LbldirectionalityChar -> "directionalityChar"
|Lbl'Hash'opendir'LPar'_'RPar'_K'Hyph'IO -> "#opendir(_)_K-IO"
|LblnoTimerCell -> "noTimerCell"
|LblinitKCell -> "initKCell"
|Lbl'Hash'rs -> "#rs"
|LblisDItem -> "isDItem"
|Lbl'Stop'Set -> ".Set"
|LblisStateCell -> "isStateCell"
|Lbl'Hash'EACCES_K'Hyph'IO -> "#EACCES_K-IO"
|Lbl'Hash'ELOOP_K'Hyph'IO -> "#ELOOP_K-IO"
|Lbl'Hash'EDOM_K'Hyph'IO -> "#EDOM_K-IO"
|LblremoveAll -> "removeAll"
|LblisFunDefCell -> "isFunDefCell"
|Lbl_andBool_ -> "_andBool_"
|Lbl'Hash'loopSep -> "#loopSep"
|Lbl'_LT_'fret'_GT_' -> "<fret>"
|Lbl'Hash'TransferV -> "#TransferV"
|Lbl'Hash'EPFNOSUPPORT_K'Hyph'IO -> "#EPFNOSUPPORT_K-IO"
|LbllengthString -> "lengthString"
|LblinitStackCell -> "initStackCell"
|Lbl'Hash'ERANGE_K'Hyph'IO -> "#ERANGE_K-IO"
|LblinitTCell -> "initTCell"
|LblisFnameCell -> "isFnameCell"
|LblsignedBytes -> "signedBytes"
|LblnoFbodyCell -> "noFbodyCell"
|LblFloatFormat -> "FloatFormat"
|Lbl'Hash'ENOTSOCK_K'Hyph'IO -> "#ENOTSOCK_K-IO"
|Lbl_'Plus'String__STRING -> "_+String__STRING"
|Lbl_'Pipe'Int__INT -> "_|Int__INT"
|Lbl'Hash'Transferuninit -> "#Transferuninit"
|Lbl'Hash'EISCONN_K'Hyph'IO -> "#EISCONN_K-IO"
|Lbl_dividesInt__INT -> "_dividesInt__INT"
|LblisStateCellMap -> "isStateCellMap"
|Lbl'_LT_'T'_GT_Hyph'fragment -> "<T>-fragment"
|LblSet'Coln'choice -> "Set:choice"
|Lbl'Star'__OSL'Hyph'SYNTAX -> "*__OSL-SYNTAX"
|Lbl'Hash'buffer -> "#buffer"
|LblfreshInt -> "freshInt"
|Lbl'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#write(_,_)_K-IO"
|Lbl'Hash'ETIMEDOUT_K'Hyph'IO -> "#ETIMEDOUT_K-IO"
|LblisBlock -> "isBlock"
|Lbl'Hash'transfer -> "#transfer"
|LblisParameters -> "isParameters"
|Lbl_xorInt__INT -> "_xorInt__INT"
|Lbl'Hash'EINPROGRESS_K'Hyph'IO -> "#EINPROGRESS_K-IO"
|LblinitStateCell -> "initStateCell"
|Lbl'_LT_'indexes'_GT_' -> "<indexes>"
|Lbl'Hash'ENOPROTOOPT_K'Hyph'IO -> "#ENOPROTOOPT_K-IO"
|LbllittleEndianBytes -> "littleEndianBytes"
|Lbl'Hash'EPERM_K'Hyph'IO -> "#EPERM_K-IO"
|Lbl_'_LT__LT_'Int__INT -> "_<<Int__INT"
|Lbl'Hash'block -> "#block"
|Lbl'Hash'freezer'Hash'Transferuninit0_ -> "#freezer#Transferuninit0_"
|Lbl'_LT_'index'_GT_' -> "<index>"
|Lbl'Hash'immRef -> "#immRef"
|LblBase2String -> "Base2String"
|LblListItem -> "ListItem"
|LblisWriteCell -> "isWriteCell"
|LblisStream -> "isStream"
|Lbl_'_LT_Eqls'Map__MAP -> "_<=Map__MAP"
|LblnewUUID_STRING -> "newUUID_STRING"
|Lbl'_LT_'nstate'_GT_' -> "<nstate>"
|Lbl'Hash'ESRCH_K'Hyph'IO -> "#ESRCH_K-IO"
|Lbl'Hash'EMFILE_K'Hyph'IO -> "#EMFILE_K-IO"
|Lblloop_'SCln'_OSL'Hyph'SYNTAX -> "loop_;_OSL-SYNTAX"
|Lbl'Hash'unwrapVal -> "#unwrapVal"
|Lbl'Hash'read -> "#read"
|Lbl'Hash'existRef -> "#existRef"
|Lbl_inList_ -> "_inList_"
|Lbl'Hash'void_OSL'Hyph'SYNTAX -> "#void_OSL-SYNTAX"
|Lbl'Hash'ENOEXEC_K'Hyph'IO -> "#ENOEXEC_K-IO"
|Lbl'Hash'freezerval0_ -> "#freezerval0_"
|LblminInt'LPar'_'Comm'_'RPar'_INT -> "minInt(_,_)_INT"
|LblisMap -> "isMap"
|LblisStoreCell -> "isStoreCell"
|LblisStmts -> "isStmts"
|Lbl'Hash'lvDref -> "#lvDref"
|LblisTCellFragment -> "isTCellFragment"
|Lbl'Hash'uninitialize -> "#uninitialize"
|Lblreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING -> "replace(_,_,_,_)_STRING"
|Lbl_'Slsh'Int__INT -> "_/Int__INT"
|Lbl'Hash'blockend_BLOCK -> "#blockend_BLOCK"
|Lbl_'LSqB'_'_LT_Hyph'_'RSqB'_MAP -> "_[_<-_]_MAP"
|Lbl'Hash'tell'LPar'_'RPar'_K'Hyph'IO -> "#tell(_)_K-IO"
|LblgetKLabel -> "getKLabel"
|LblisEnvCellOpt -> "isEnvCellOpt"
|Lbl'Hash'E2BIG_K'Hyph'IO -> "#E2BIG_K-IO"
|LblnoStatesCell -> "noStatesCell"
|Lbl'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO -> "#seekEnd(_,_)_K-IO"
|LblisFretCell -> "isFretCell"
let parse_sort(c: string) : sort = match c with 
|"FbodyCellOpt" -> SortFbodyCellOpt
|"FunDefCellFragment" -> SortFunDefCellFragment
|"IndexItem" -> SortIndexItem
|"TimerCellOpt" -> SortTimerCellOpt
|"K" -> SortK
|"IndexesCell" -> SortIndexesCell
|"EnvCell" -> SortEnvCell
|"Stmt" -> SortStmt
|"FparamsCellOpt" -> SortFparamsCellOpt
|"Map" -> SortMap
|"FretCellOpt" -> SortFretCellOpt
|"Value" -> SortValue
|"KItem" -> SortKItem
|"IndexesCellOpt" -> SortIndexesCellOpt
|"StackCellOpt" -> SortStackCellOpt
|"Prop" -> SortProp
|"LoopItem" -> SortLoopItem
|"IndexCell" -> SortIndexCell
|"WriteCell" -> SortWriteCell
|"Parameter" -> SortParameter
|"Stream" -> SortStream
|"TmpCell" -> SortTmpCell
|"Separator" -> SortSeparator
|"Blocks" -> SortBlocks
|"Parameters" -> SortParameters
|"String" -> SortString
|"Float" -> SortFloat
|"TCellFragment" -> SortTCellFragment
|"StoreCellOpt" -> SortStoreCellOpt
|"Set" -> SortSet
|"BorrowItem" -> SortBorrowItem
|"FunDefsCellFragment" -> SortFunDefsCellFragment
|"StoreCell" -> SortStoreCell
|"FretCell" -> SortFretCell
|"MInt" -> SortMInt
|"StatesCellOpt" -> SortStatesCellOpt
|"Cell" -> SortCell
|"TimerCell" -> SortTimerCell
|"LOCK" -> SortLOCK
|"FunDefsCell" -> SortFunDefsCell
|"Stmts" -> SortStmts
|"BlockItem" -> SortBlockItem
|"FnameCell" -> SortFnameCell
|"OItem" -> SortOItem
|"BranchTmp" -> SortBranchTmp
|"FunDefsCellOpt" -> SortFunDefsCellOpt
|"EnvCellOpt" -> SortEnvCellOpt
|"NstateCell" -> SortNstateCell
|"Function" -> SortFunction
|"Bool" -> SortBool
|"KResult" -> SortKResult
|"RItem" -> SortRItem
|"TmpCellOpt" -> SortTmpCellOpt
|"DItem" -> SortDItem
|"IndexCellOpt" -> SortIndexCellOpt
|"Lifetime" -> SortLifetime
|"KCell" -> SortKCell
|"Bytes" -> SortBytes
|"WriteCellOpt" -> SortWriteCellOpt
|"FparamsCell" -> SortFparamsCell
|"StatesCell" -> SortStatesCell
|"WItem" -> SortWItem
|"FnameCellOpt" -> SortFnameCellOpt
|"Exps" -> SortExps
|"IOError" -> SortIOError
|"StringBuffer" -> SortStringBuffer
|"Type" -> SortType
|"TCell" -> SortTCell
|"NstateCellOpt" -> SortNstateCellOpt
|"Int" -> SortInt
|"StatesCellFragment" -> SortStatesCellFragment
|"StackCell" -> SortStackCell
|"Block" -> SortBlock
|"FunDefCellMap" -> SortFunDefCellMap
|"KConfigVar" -> SortKConfigVar
|"StateCellFragment" -> SortStateCellFragment
|"KCellOpt" -> SortKCellOpt
|"FbodyCell" -> SortFbodyCell
|"Props" -> SortProps
|"Indexes" -> SortIndexes
|"StateCellMap" -> SortStateCellMap
|"Id" -> SortId
|"Uninit" -> SortUninit
|"FunDefCell" -> SortFunDefCell
|"List" -> SortList
|"StateCell" -> SortStateCell
|"Exp" -> SortExp
| _ -> invalid_arg ("parse_sort: " ^ c)
let parse_klabel(c: string) : klabel = match c with 
|"#argv" -> Lbl'Hash'argv
|"isFbodyCellOpt" -> LblisFbodyCellOpt
|"Map:lookup" -> LblMap'Coln'lookup
|"#seek(_,_)_K-IO" -> Lbl'Hash'seek'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"signExtendBitRangeInt" -> LblsignExtendBitRangeInt
|"_==Bool__BOOL" -> Lbl_'EqlsEqls'Bool__BOOL
|"isSet" -> LblisSet
|"transfer__;_OSL-SYNTAX" -> Lbltransfer__'SCln'_OSL'Hyph'SYNTAX
|"initNstateCell" -> LblinitNstateCell
|"_<=Set__SET" -> Lbl_'_LT_Eqls'Set__SET
|"initFparamsCell" -> LblinitFparamsCell
|"noEnvCell" -> LblnoEnvCell
|"'__OSL-SYNTAX" -> Lbl'Apos'__OSL'Hyph'SYNTAX
|"isIOError" -> LblisIOError
|"#parse" -> Lbl'Hash'parse
|"#freezer#Transfer1_" -> Lbl'Hash'freezer'Hash'Transfer1_
|"#EALREADY_K-IO" -> Lbl'Hash'EALREADY_K'Hyph'IO
|"_;_OSL-SYNTAX" -> Lbl_'SCln'_OSL'Hyph'SYNTAX
|"makeList" -> LblmakeList
|"isSeparator" -> LblisSeparator
|"initIndexesCell" -> LblinitIndexesCell
|"#ESPIPE_K-IO" -> Lbl'Hash'ESPIPE_K'Hyph'IO
|"initFbodyCell" -> LblinitFbodyCell
|"isFunDefCellFragment" -> LblisFunDefCellFragment
|"#unlock(_,_)_K-IO" -> Lbl'Hash'unlock'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"#ENOENT_K-IO" -> Lbl'Hash'ENOENT_K'Hyph'IO
|"isFnameCellOpt" -> LblisFnameCellOpt
|"#mborrow" -> Lbl'Hash'mborrow
|"isBlockItem" -> LblisBlockItem
|"_StateCellMap_" -> Lbl_StateCellMap_
|"destruct_;_OSL-SYNTAX" -> Lbldestruct_'SCln'_OSL'Hyph'SYNTAX
|"#ENOTTY_K-IO" -> Lbl'Hash'ENOTTY_K'Hyph'IO
|"<states>" -> Lbl'_LT_'states'_GT_'
|"isStatesCellOpt" -> LblisStatesCellOpt
|"#freezer#Read0_" -> Lbl'Hash'freezer'Hash'Read0_
|"initWriteCell" -> LblinitWriteCell
|"#ENOTEMPTY_K-IO" -> Lbl'Hash'ENOTEMPTY_K'Hyph'IO
|"#borrowmutck" -> Lbl'Hash'borrowmutck
|"#branch" -> Lbl'Hash'branch
|"isBorrowItem" -> LblisBorrowItem
|"#EMSGSIZE_K-IO" -> Lbl'Hash'EMSGSIZE_K'Hyph'IO
|"isKConfigVar" -> LblisKConfigVar
|"#ENETRESET_K-IO" -> Lbl'Hash'ENETRESET_K'Hyph'IO
|"#EAFNOSUPPORT_K-IO" -> Lbl'Hash'EAFNOSUPPORT_K'Hyph'IO
|"#loc" -> Lbl'Hash'loc
|"isFunDefsCellFragment" -> LblisFunDefsCellFragment
|"#mutRef" -> Lbl'Hash'mutRef
|"#lc" -> Lbl'Hash'lc
|"isCell" -> LblisCell
|"#ENOMEM_K-IO" -> Lbl'Hash'ENOMEM_K'Hyph'IO
|"values" -> Lblvalues
|"#expStmt" -> Lbl'Hash'expStmt
|"#own" -> Lbl'Hash'own
|"#borrowImmCK" -> Lbl'Hash'borrowImmCK
|"isRItem" -> LblisRItem
|"isStoreCellOpt" -> LblisStoreCellOpt
|"#ENXIO_K-IO" -> Lbl'Hash'ENXIO_K'Hyph'IO
|"_<Int__INT" -> Lbl_'_LT_'Int__INT
|"isLoopItem" -> LblisLoopItem
|"isWItem" -> LblisWItem
|"#configuration_K-REFLECTION" -> Lbl'Hash'configuration_K'Hyph'REFLECTION
|"isFunDefsCellOpt" -> LblisFunDefsCellOpt
|"#Loc" -> Lbl'Hash'Loc
|"isFloat" -> LblisFloat
|"isOItem" -> LblisOItem
|"initFretCell" -> LblinitFretCell
|"initFunDefsCell" -> LblinitFunDefsCell
|"chrChar" -> LblchrChar
|"_divInt__INT" -> Lbl_divInt__INT
|"#EROFS_K-IO" -> Lbl'Hash'EROFS_K'Hyph'IO
|"#wv" -> Lbl'Hash'wv
|"_+Int_" -> Lbl_'Plus'Int_
|"#uninit_OSL-SYNTAX" -> Lbl'Hash'uninit_OSL'Hyph'SYNTAX
|"_orBool__BOOL" -> Lbl_orBool__BOOL
|"noIndexCell" -> LblnoIndexCell
|"noTmpCell" -> LblnoTmpCell
|"#ENFILE_K-IO" -> Lbl'Hash'ENFILE_K'Hyph'IO
|"updateMap" -> LblupdateMap
|"Int2String" -> LblInt2String
|"#deallocate" -> Lbl'Hash'deallocate
|"_=/=K_" -> Lbl_'EqlsSlshEqls'K_
|"FunDefCellMapItem" -> LblFunDefCellMapItem
|".StateCellMap" -> Lbl'Stop'StateCellMap
|"isTimerCellOpt" -> LblisTimerCellOpt
|"_List_" -> Lbl_List_
|"#open(_,_)_K-IO" -> Lbl'Hash'open'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"#EOPNOTSUPP_K-IO" -> Lbl'Hash'EOPNOTSUPP_K'Hyph'IO
|"_|->_" -> Lbl_'PipeHyph_GT_'_
|"_-Map__MAP" -> Lbl_'Hyph'Map__MAP
|"#compareS" -> Lbl'Hash'compareS
|"mut_OSL-SYNTAX" -> Lblmut_OSL'Hyph'SYNTAX
|"isIndexesCellOpt" -> LblisIndexesCellOpt
|"#EMLINK_K-IO" -> Lbl'Hash'EMLINK_K'Hyph'IO
|"initStoreCell" -> LblinitStoreCell
|"#borrowMutCK" -> Lbl'Hash'borrowMutCK
|"#TransferMB" -> Lbl'Hash'TransferMB
|"#sort" -> Lbl'Hash'sort
|"_==K_" -> Lbl_'EqlsEqls'K_
|"replaceFirst(_,_,_)_STRING" -> LblreplaceFirst'LPar'_'Comm'_'Comm'_'RPar'_STRING
|"#EOVERFLOW_K-IO" -> Lbl'Hash'EOVERFLOW_K'Hyph'IO
|"#FnCall" -> Lbl'Hash'FnCall
|"#putc(_,_)_K-IO" -> Lbl'Hash'putc'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"newResource(_)_OSL-SYNTAX" -> LblnewResource'LPar'_'RPar'_OSL'Hyph'SYNTAX
|"#increaseIndex_OSL" -> Lbl'Hash'increaseIndex_OSL
|".Map" -> Lbl'Stop'Map
|"isTmpCellOpt" -> LblisTmpCellOpt
|"_=/=String__STRING" -> Lbl_'EqlsSlshEqls'String__STRING
|"#EIO_K-IO" -> Lbl'Hash'EIO_K'Hyph'IO
|"#Transfer" -> Lbl'Hash'Transfer
|"isStateCellFragment" -> LblisStateCellFragment
|"isType" -> LblisType
|"isInt" -> LblisInt
|"isBranchTmp" -> LblisBranchTmp
|"_FunDefCellMap_" -> Lbl_FunDefCellMap_
|"#EFAULT_K-IO" -> Lbl'Hash'EFAULT_K'Hyph'IO
|"#secondBranch" -> Lbl'Hash'secondBranch
|"#fresh" -> Lbl'Hash'fresh
|"#TransferIB" -> Lbl'Hash'TransferIB
|"_impliesBool__BOOL" -> Lbl_impliesBool__BOOL
|"_*Int__INT" -> Lbl_'Star'Int__INT
|"#repeat" -> Lbl'Hash'repeat
|"<T>" -> Lbl'_LT_'T'_GT_'
|"#Thread" -> Lbl'Hash'Thread
|"_,__OSL-SYNTAX" -> Lbl_'Comm'__OSL'Hyph'SYNTAX
|"maxInt(_,_)_INT" -> LblmaxInt'LPar'_'Comm'_'RPar'_INT
|"#EDEADLK_K-IO" -> Lbl'Hash'EDEADLK_K'Hyph'IO
|"<tmp>" -> Lbl'_LT_'tmp'_GT_'
|"_<=String__STRING" -> Lbl_'_LT_Eqls'String__STRING
|"isStackCellOpt" -> LblisStackCellOpt
|"isStmt" -> LblisStmt
|"#ENOBUFS_K-IO" -> Lbl'Hash'ENOBUFS_K'Hyph'IO
|"_Map_" -> Lbl_Map_
|"_-Int__INT" -> Lbl_'Hyph'Int__INT
|"project:Stmts" -> Lblproject'Coln'Stmts
|"#EOF_K-IO" -> Lbl'Hash'EOF_K'Hyph'IO
|"#compareA" -> Lbl'Hash'compareA
|"Float2String" -> LblFloat2String
|"isValue" -> LblisValue
|"sizeList" -> LblsizeList
|"#EWOULDBLOCK_K-IO" -> Lbl'Hash'EWOULDBLOCK_K'Hyph'IO
|"String2Id" -> LblString2Id
|"#decompose" -> Lbl'Hash'decompose
|"_=/=Bool__BOOL" -> Lbl_'EqlsSlshEqls'Bool__BOOL
|"#EFBIG_K-IO" -> Lbl'Hash'EFBIG_K'Hyph'IO
|"isTCell" -> LblisTCell
|"#EBADF_K-IO" -> Lbl'Hash'EBADF_K'Hyph'IO
|"#freezer#TransferIB1_" -> Lbl'Hash'freezer'Hash'TransferIB1_
|"#EPIPE_K-IO" -> Lbl'Hash'EPIPE_K'Hyph'IO
|"noStoreCell" -> LblnoStoreCell
|"_^%Int___INT" -> Lbl_'Xor_Perc'Int___INT
|"<funDef>" -> Lbl'_LT_'funDef'_GT_'
|"#Deallocate" -> Lbl'Hash'Deallocate
|"initIndexCell" -> LblinitIndexCell
|"#function" -> Lbl'Hash'function
|"isFunDefCellMap" -> LblisFunDefCellMap
|"rfindString" -> LblrfindString
|"#ESOCKTNOSUPPORT_K-IO" -> Lbl'Hash'ESOCKTNOSUPPORT_K'Hyph'IO
|"#EINTR_K-IO" -> Lbl'Hash'EINTR_K'Hyph'IO
|"<store>" -> Lbl'_LT_'store'_GT_'
|"#stat(_)_K-IO" -> Lbl'Hash'stat'LPar'_'RPar'_K'Hyph'IO
|"updateList" -> LblupdateList
|".List{\"_,__OSL-SYNTAX\"}" -> Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra'
|"#compare" -> Lbl'Hash'compare
|"<funDefs>-fragment" -> Lbl'_LT_'funDefs'_GT_Hyph'fragment
|"<timer>" -> Lbl'_LT_'timer'_GT_'
|"<stack>" -> Lbl'_LT_'stack'_GT_'
|"categoryChar" -> LblcategoryChar
|"Set:difference" -> LblSet'Coln'difference
|"#EHOSTUNREACH_K-IO" -> Lbl'Hash'EHOSTUNREACH_K'Hyph'IO
|"#ECONNRESET_K-IO" -> Lbl'Hash'ECONNRESET_K'Hyph'IO
|"isKCellOpt" -> LblisKCellOpt
|"#ECHILD_K-IO" -> Lbl'Hash'ECHILD_K'Hyph'IO
|"#indexes" -> Lbl'Hash'indexes
|"String2Float" -> LblString2Float
|"Map:lookupOrDefault" -> LblMap'Coln'lookupOrDefault
|"#if_#then_#else_#fi_K-EQUAL" -> Lbl'Hash'if_'Hash'then_'Hash'else_'Hash'fi_K'Hyph'EQUAL
|"#ENOTCONN_K-IO" -> Lbl'Hash'ENOTCONN_K'Hyph'IO
|"#stdout_K-IO" -> Lbl'Hash'stdout_K'Hyph'IO
|"_&Int__INT" -> Lbl_'And'Int__INT
|"#increaseTimer_OSL" -> Lbl'Hash'increaseTimer_OSL
|"#borrowimmck" -> Lbl'Hash'borrowimmck
|"#ENAMETOOLONG_K-IO" -> Lbl'Hash'ENAMETOOLONG_K'Hyph'IO
|"log2Int" -> Lbllog2Int
|"_=/=Int__INT" -> Lbl_'EqlsSlshEqls'Int__INT
|"#declTy" -> Lbl'Hash'declTy
|"#stdin_K-IO" -> Lbl'Hash'stdin_K'Hyph'IO
|"#freezer#Transfer0_" -> Lbl'Hash'freezer'Hash'Transfer0_
|"_>=String__STRING" -> Lbl_'_GT_Eqls'String__STRING
|".FunDefCellMap" -> Lbl'Stop'FunDefCellMap
|"sizeMap" -> LblsizeMap
|"noWriteCell" -> LblnoWriteCell
|"noNstateCell" -> LblnoNstateCell
|"isId" -> LblisId
|"substrString" -> LblsubstrString
|"size" -> Lblsize
|"isIndexes" -> LblisIndexes
|"initTimerCell" -> LblinitTimerCell
|"#ENETUNREACH_K-IO" -> Lbl'Hash'ENETUNREACH_K'Hyph'IO
|"isFretCellOpt" -> LblisFretCellOpt
|"#EPROTOTYPE_K-IO" -> Lbl'Hash'EPROTOTYPE_K'Hyph'IO
|"initStatesCell" -> LblinitStatesCell
|"#freezer#TransferMB1_" -> Lbl'Hash'freezer'Hash'TransferMB1_
|"#systemResult(_,_,_)_K-IO" -> Lbl'Hash'systemResult'LPar'_'Comm'_'Comm'_'RPar'_K'Hyph'IO
|"isWriteCellOpt" -> LblisWriteCellOpt
|"initFunDefCell" -> LblinitFunDefCell
|"noFnameCell" -> LblnoFnameCell
|"srandInt" -> LblsrandInt
|"#EINVAL_K-IO" -> Lbl'Hash'EINVAL_K'Hyph'IO
|"isKItem" -> LblisKItem
|"#ENODEV_K-IO" -> Lbl'Hash'ENODEV_K'Hyph'IO
|"List:set" -> LblList'Coln'set
|"String2Base" -> LblString2Base
|"noStackCell" -> LblnoStackCell
|"isStatesCellFragment" -> LblisStatesCellFragment
|"#noparse_K-IO" -> Lbl'Hash'noparse_K'Hyph'IO
|"isFbodyCell" -> LblisFbodyCell
|"<write>" -> Lbl'_LT_'write'_GT_'
|"keys" -> Lblkeys
|"isLifetime" -> LblisLifetime
|"#ESHUTDOWN_K-IO" -> Lbl'Hash'ESHUTDOWN_K'Hyph'IO
|"isParameter" -> LblisParameter
|"isFparamsCell" -> LblisFparamsCell
|"#Read" -> Lbl'Hash'Read
|"#ENOTDIR_K-IO" -> Lbl'Hash'ENOTDIR_K'Hyph'IO
|"isExp" -> LblisExp
|"isLOCK" -> LblisLOCK
|"isFparamsCellOpt" -> LblisFparamsCellOpt
|"#lv" -> Lbl'Hash'lv
|"_<=Int__INT" -> Lbl_'_LT_Eqls'Int__INT
|"notBool_" -> LblnotBool_
|"#stderr_K-IO" -> Lbl'Hash'stderr_K'Hyph'IO
|"noKCell" -> LblnoKCell
|"#EBUSY_K-IO" -> Lbl'Hash'EBUSY_K'Hyph'IO
|"isTimerCell" -> LblisTimerCell
|"#getenv" -> Lbl'Hash'getenv
|"<fparams>" -> Lbl'_LT_'fparams'_GT_'
|"intersectSet" -> LblintersectSet
|"_in_keys(_)_MAP" -> Lbl_in_keys'LPar'_'RPar'_MAP
|"findChar" -> LblfindChar
|"isStatesCell" -> LblisStatesCell
|"Set:in" -> LblSet'Coln'in
|"isK" -> LblisK
|"String2Int" -> LblString2Int
|"#ENETDOWN_K-IO" -> Lbl'Hash'ENETDOWN_K'Hyph'IO
|"_[_<-undef]" -> Lbl_'LSqB'_'_LT_Hyph'undef'RSqB'
|"#Bottom" -> Lbl'Hash'Bottom
|"#writev" -> Lbl'Hash'writev
|"_==Int_" -> Lbl_'EqlsEqls'Int_
|"_andThenBool__BOOL" -> Lbl_andThenBool__BOOL
|"#parseInModule" -> Lbl'Hash'parseInModule
|"#writeCK" -> Lbl'Hash'writeCK
|"#cint" -> Lbl'Hash'cint
|".List{\"#props\"}" -> Lbl'Stop'List'LBraQuotHash'props'QuotRBra'
|"noIndexesCell" -> LblnoIndexesCell
|"noFparamsCell" -> LblnoFparamsCell
|"#system" -> Lbl'Hash'system
|"isString" -> LblisString
|"isProp" -> LblisProp
|"#parameter" -> Lbl'Hash'parameter
|"_%Int__INT" -> Lbl_'Perc'Int__INT
|"_>>Int__INT" -> Lbl_'_GT__GT_'Int__INT
|"#decl" -> Lbl'Hash'decl
|"isList" -> LblisList
|"isTmpCell" -> LblisTmpCell
|"#EPROTONOSUPPORT_K-IO" -> Lbl'Hash'EPROTONOSUPPORT_K'Hyph'IO
|"isFunction" -> LblisFunction
|"replaceAll(_,_,_)_STRING" -> LblreplaceAll'LPar'_'Comm'_'Comm'_'RPar'_STRING
|"#EDESTADDRREQ_K-IO" -> Lbl'Hash'EDESTADDRREQ_K'Hyph'IO
|"#EADDRINUSE_K-IO" -> Lbl'Hash'EADDRINUSE_K'Hyph'IO
|"isNstateCell" -> LblisNstateCell
|"_^Int__INT" -> Lbl_'Xor_'Int__INT
|"findString" -> LblfindString
|"<k>" -> Lbl'_LT_'k'_GT_'
|"<funDefs>" -> Lbl'_LT_'funDefs'_GT_'
|"<fbody>" -> Lbl'_LT_'fbody'_GT_'
|"absInt" -> LblabsInt
|"val" -> Lblval
|"#EHOSTDOWN_K-IO" -> Lbl'Hash'EHOSTDOWN_K'Hyph'IO
|"isEnvCell" -> LblisEnvCell
|"_>String__STRING" -> Lbl_'_GT_'String__STRING
|"<state>-fragment" -> Lbl'_LT_'state'_GT_Hyph'fragment
|"_==String__STRING" -> Lbl_'EqlsEqls'String__STRING
|"#checkInit" -> Lbl'Hash'checkInit
|"isProps" -> LblisProps
|"isKResult" -> LblisKResult
|"List:get" -> LblList'Coln'get
|"isStackCell" -> LblisStackCell
|"#lstat(_)_K-IO" -> Lbl'Hash'lstat'LPar'_'RPar'_K'Hyph'IO
|"isIndexesCell" -> LblisIndexesCell
|"#freezer#Deallocate0_" -> Lbl'Hash'freezer'Hash'Deallocate0_
|"initFnameCell" -> LblinitFnameCell
|"noFretCell" -> LblnoFretCell
|"SetItem" -> LblSetItem
|"copy_OSL-SYNTAX" -> Lblcopy_OSL'Hyph'SYNTAX
|"unsignedBytes" -> LblunsignedBytes
|"StateCellMapItem" -> LblStateCellMapItem
|".List" -> Lbl'Stop'List
|"#ENOLCK_K-IO" -> Lbl'Hash'ENOLCK_K'Hyph'IO
|"#ECONNABORTED_K-IO" -> Lbl'Hash'ECONNABORTED_K'Hyph'IO
|"randInt" -> LblrandInt
|"#EXDEV_K-IO" -> Lbl'Hash'EXDEV_K'Hyph'IO
|"#close(_)_K-IO" -> Lbl'Hash'close'LPar'_'RPar'_K'Hyph'IO
|"isUninit" -> LblisUninit
|"#props" -> Lbl'Hash'props
|"keys_list(_)_MAP" -> Lblkeys_list'LPar'_'RPar'_MAP
|"freshId" -> LblfreshId
|"_orElseBool__BOOL" -> Lbl_orElseBool__BOOL
|"#EISDIR_K-IO" -> Lbl'Hash'EISDIR_K'Hyph'IO
|"isIndexCell" -> LblisIndexCell
|"List:range" -> LblList'Coln'range
|"isKCell" -> LblisKCell
|"#unknownIOError" -> Lbl'Hash'unknownIOError
|"_>=Int__INT" -> Lbl_'_GT_Eqls'Int__INT
|"#voidTy_OSL-SYNTAX" -> Lbl'Hash'voidTy_OSL'Hyph'SYNTAX
|"initEnvCell" -> LblinitEnvCell
|"#freezer#expStmt0_" -> Lbl'Hash'freezer'Hash'expStmt0_
|"#compareE" -> Lbl'Hash'compareE
|"#bindParams" -> Lbl'Hash'bindParams
|"isNstateCellOpt" -> LblisNstateCellOpt
|"_.__OSL-SYNTAX" -> Lbl_'Stop'__OSL'Hyph'SYNTAX
|"#ENOSYS_K-IO" -> Lbl'Hash'ENOSYS_K'Hyph'IO
|"#ECONNREFUSED_K-IO" -> Lbl'Hash'ECONNREFUSED_K'Hyph'IO
|"#lock(_,_)_K-IO" -> Lbl'Hash'lock'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"#EADDRNOTAVAIL_K-IO" -> Lbl'Hash'EADDRNOTAVAIL_K'Hyph'IO
|"countAllOccurrences(_,_)_STRING" -> LblcountAllOccurrences'LPar'_'Comm'_'RPar'_STRING
|"_>Int__INT" -> Lbl_'_GT_'Int__INT
|"fillList" -> LblfillList
|"<states>-fragment" -> Lbl'_LT_'states'_GT_Hyph'fragment
|"noFunDefsCell" -> LblnoFunDefsCell
|"isBlocks" -> LblisBlocks
|"#removeState_CONTROL" -> Lbl'Hash'removeState_CONTROL
|"bitRangeInt" -> LblbitRangeInt
|"_<String__STRING" -> Lbl_'_LT_'String__STRING
|".List{\"___OSL-SYNTAX\"}" -> Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra'
|"<funDef>-fragment" -> Lbl'_LT_'funDef'_GT_Hyph'fragment
|"#ThreadLocal" -> Lbl'Hash'ThreadLocal
|"#inProps" -> Lbl'Hash'inProps
|"_xorBool__BOOL" -> Lbl_xorBool__BOOL
|"<fname>" -> Lbl'_LT_'fname'_GT_'
|"isIndexCellOpt" -> LblisIndexCellOpt
|"#open(_)_K-IO" -> Lbl'Hash'open'LPar'_'RPar'_K'Hyph'IO
|"<env>" -> Lbl'_LT_'env'_GT_'
|"#ETOOMANYREFS_K-IO" -> Lbl'Hash'ETOOMANYREFS_K'Hyph'IO
|"#ENOSPC_K-IO" -> Lbl'Hash'ENOSPC_K'Hyph'IO
|"#br" -> Lbl'Hash'br
|"#logToFile" -> Lbl'Hash'logToFile
|"#freezer#lvDref0_" -> Lbl'Hash'freezer'Hash'lvDref0_
|"#read(_,_)_K-IO" -> Lbl'Hash'read'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"#ref" -> Lbl'Hash'ref
|"bigEndianBytes" -> LblbigEndianBytes
|"Id2String" -> LblId2String
|"Map:choice" -> LblMap'Coln'choice
|"isFunDefsCell" -> LblisFunDefsCell
|"isExps" -> LblisExps
|"#borrow" -> Lbl'Hash'borrow
|"___OSL-SYNTAX" -> Lbl___OSL'Hyph'SYNTAX
|"_Set_" -> Lbl_Set_
|"#EEXIST_K-IO" -> Lbl'Hash'EEXIST_K'Hyph'IO
|"#getc(_)_K-IO" -> Lbl'Hash'getc'LPar'_'RPar'_K'Hyph'IO
|"initTmpCell" -> LblinitTmpCell
|"#unwrapInt" -> Lbl'Hash'unwrapInt
|"<state>" -> Lbl'_LT_'state'_GT_'
|"isBool" -> LblisBool
|"~Int__INT" -> Lbl'Tild'Int__INT
|"#list2Set" -> Lbl'Hash'list2Set
|"ordChar" -> LblordChar
|"_modInt__INT" -> Lbl_modInt__INT
|"rfindChar" -> LblrfindChar
|"#EAGAIN_K-IO" -> Lbl'Hash'EAGAIN_K'Hyph'IO
|"isIndexItem" -> LblisIndexItem
|"directionalityChar" -> LbldirectionalityChar
|"#opendir(_)_K-IO" -> Lbl'Hash'opendir'LPar'_'RPar'_K'Hyph'IO
|"noTimerCell" -> LblnoTimerCell
|"initKCell" -> LblinitKCell
|"#rs" -> Lbl'Hash'rs
|"isDItem" -> LblisDItem
|".Set" -> Lbl'Stop'Set
|"isStateCell" -> LblisStateCell
|"#EACCES_K-IO" -> Lbl'Hash'EACCES_K'Hyph'IO
|"#ELOOP_K-IO" -> Lbl'Hash'ELOOP_K'Hyph'IO
|"#EDOM_K-IO" -> Lbl'Hash'EDOM_K'Hyph'IO
|"removeAll" -> LblremoveAll
|"isFunDefCell" -> LblisFunDefCell
|"_andBool_" -> Lbl_andBool_
|"#loopSep" -> Lbl'Hash'loopSep
|"<fret>" -> Lbl'_LT_'fret'_GT_'
|"#TransferV" -> Lbl'Hash'TransferV
|"#EPFNOSUPPORT_K-IO" -> Lbl'Hash'EPFNOSUPPORT_K'Hyph'IO
|"lengthString" -> LbllengthString
|"initStackCell" -> LblinitStackCell
|"#ERANGE_K-IO" -> Lbl'Hash'ERANGE_K'Hyph'IO
|"initTCell" -> LblinitTCell
|"isFnameCell" -> LblisFnameCell
|"signedBytes" -> LblsignedBytes
|"noFbodyCell" -> LblnoFbodyCell
|"FloatFormat" -> LblFloatFormat
|"#ENOTSOCK_K-IO" -> Lbl'Hash'ENOTSOCK_K'Hyph'IO
|"_+String__STRING" -> Lbl_'Plus'String__STRING
|"_|Int__INT" -> Lbl_'Pipe'Int__INT
|"#Transferuninit" -> Lbl'Hash'Transferuninit
|"#EISCONN_K-IO" -> Lbl'Hash'EISCONN_K'Hyph'IO
|"_dividesInt__INT" -> Lbl_dividesInt__INT
|"isStateCellMap" -> LblisStateCellMap
|"<T>-fragment" -> Lbl'_LT_'T'_GT_Hyph'fragment
|"Set:choice" -> LblSet'Coln'choice
|"*__OSL-SYNTAX" -> Lbl'Star'__OSL'Hyph'SYNTAX
|"#buffer" -> Lbl'Hash'buffer
|"freshInt" -> LblfreshInt
|"#write(_,_)_K-IO" -> Lbl'Hash'write'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"#ETIMEDOUT_K-IO" -> Lbl'Hash'ETIMEDOUT_K'Hyph'IO
|"isBlock" -> LblisBlock
|"#transfer" -> Lbl'Hash'transfer
|"isParameters" -> LblisParameters
|"_xorInt__INT" -> Lbl_xorInt__INT
|"#EINPROGRESS_K-IO" -> Lbl'Hash'EINPROGRESS_K'Hyph'IO
|"initStateCell" -> LblinitStateCell
|"<indexes>" -> Lbl'_LT_'indexes'_GT_'
|"#ENOPROTOOPT_K-IO" -> Lbl'Hash'ENOPROTOOPT_K'Hyph'IO
|"littleEndianBytes" -> LbllittleEndianBytes
|"#EPERM_K-IO" -> Lbl'Hash'EPERM_K'Hyph'IO
|"_<<Int__INT" -> Lbl_'_LT__LT_'Int__INT
|"#block" -> Lbl'Hash'block
|"#freezer#Transferuninit0_" -> Lbl'Hash'freezer'Hash'Transferuninit0_
|"<index>" -> Lbl'_LT_'index'_GT_'
|"#immRef" -> Lbl'Hash'immRef
|"Base2String" -> LblBase2String
|"ListItem" -> LblListItem
|"isWriteCell" -> LblisWriteCell
|"isStream" -> LblisStream
|"_<=Map__MAP" -> Lbl_'_LT_Eqls'Map__MAP
|"newUUID_STRING" -> LblnewUUID_STRING
|"<nstate>" -> Lbl'_LT_'nstate'_GT_'
|"#ESRCH_K-IO" -> Lbl'Hash'ESRCH_K'Hyph'IO
|"#EMFILE_K-IO" -> Lbl'Hash'EMFILE_K'Hyph'IO
|"loop_;_OSL-SYNTAX" -> Lblloop_'SCln'_OSL'Hyph'SYNTAX
|"#unwrapVal" -> Lbl'Hash'unwrapVal
|"#read" -> Lbl'Hash'read
|"#existRef" -> Lbl'Hash'existRef
|"_inList_" -> Lbl_inList_
|"#void_OSL-SYNTAX" -> Lbl'Hash'void_OSL'Hyph'SYNTAX
|"#ENOEXEC_K-IO" -> Lbl'Hash'ENOEXEC_K'Hyph'IO
|"#freezerval0_" -> Lbl'Hash'freezerval0_
|"minInt(_,_)_INT" -> LblminInt'LPar'_'Comm'_'RPar'_INT
|"isMap" -> LblisMap
|"isStoreCell" -> LblisStoreCell
|"isStmts" -> LblisStmts
|"#lvDref" -> Lbl'Hash'lvDref
|"isTCellFragment" -> LblisTCellFragment
|"#uninitialize" -> Lbl'Hash'uninitialize
|"replace(_,_,_,_)_STRING" -> Lblreplace'LPar'_'Comm'_'Comm'_'Comm'_'RPar'_STRING
|"_/Int__INT" -> Lbl_'Slsh'Int__INT
|"#blockend_BLOCK" -> Lbl'Hash'blockend_BLOCK
|"_[_<-_]_MAP" -> Lbl_'LSqB'_'_LT_Hyph'_'RSqB'_MAP
|"#tell(_)_K-IO" -> Lbl'Hash'tell'LPar'_'RPar'_K'Hyph'IO
|"getKLabel" -> LblgetKLabel
|"isEnvCellOpt" -> LblisEnvCellOpt
|"#E2BIG_K-IO" -> Lbl'Hash'E2BIG_K'Hyph'IO
|"noStatesCell" -> LblnoStatesCell
|"#seekEnd(_,_)_K-IO" -> Lbl'Hash'seekEnd'LPar'_'Comm'_'RPar'_K'Hyph'IO
|"isFretCell" -> LblisFretCell
| _ -> invalid_arg ("parse_klabel: " ^ c)
let collection_for (c: klabel) : klabel = match c with 
|LblSetItem -> Lbl_Set_
|Lbl'Stop'Set -> Lbl_Set_
|Lbl_FunDefCellMap_ -> Lbl_FunDefCellMap_
|Lbl'_LT_'funDef'_GT_' -> Lbl_FunDefCellMap_
|LblStateCellMapItem -> Lbl_StateCellMap_
|LblFunDefCellMapItem -> Lbl_FunDefCellMap_
|LblListItem -> Lbl_List_
|Lbl_Map_ -> Lbl_Map_
|Lbl'Stop'FunDefCellMap -> Lbl_FunDefCellMap_
|Lbl'Stop'StateCellMap -> Lbl_StateCellMap_
|Lbl'Stop'List -> Lbl_List_
|Lbl_Set_ -> Lbl_Set_
|Lbl_List_ -> Lbl_List_
|Lbl'Stop'Map -> Lbl_Map_
|Lbl_'PipeHyph_GT_'_ -> Lbl_Map_
|Lbl_StateCellMap_ -> Lbl_StateCellMap_
|Lbl'_LT_'state'_GT_' -> Lbl_StateCellMap_
| _ -> invalid_arg "collection_for"
let unit_for (c: klabel) : klabel = match c with 
|Lbl_Set_ -> Lbl'Stop'Set
|Lbl_List_ -> Lbl'Stop'List
|Lbl_FunDefCellMap_ -> Lbl'Stop'FunDefCellMap
|Lbl_StateCellMap_ -> Lbl'Stop'StateCellMap
|Lbl_Map_ -> Lbl'Stop'Map
| _ -> invalid_arg "unit_for"
let el_for (c: klabel) : klabel = match c with 
|Lbl_Set_ -> LblSetItem
|Lbl_List_ -> LblListItem
|Lbl_FunDefCellMap_ -> LblFunDefCellMapItem
|Lbl_StateCellMap_ -> LblStateCellMapItem
|Lbl_Map_ -> Lbl_'PipeHyph_GT_'_
| _ -> invalid_arg "el_for"
let unit_for_array (c: sort) : klabel = match c with 
| _ -> invalid_arg "unit_for_array"
let el_for_array (c: sort) : klabel = match c with 
| _ -> invalid_arg "el_for_array"
module Dynarray : sig
  type 'a t
  val make : int -> 'a -> 'a t
  val length : 'a t -> int
  val get : 'a t -> int -> 'a
  val set : 'a t -> int -> 'a -> unit
  val compare : ('a list -> 'a list -> int) -> 'a t -> 'a t -> int
  val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a
  val fold_right : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val iteri : (int -> 'a -> unit) -> 'a t -> unit
end = struct
  type 'a t = { size: int;                mutable arr: 'a array;                default: 'a }
  let make size default = { size=size; arr=Array.make (min size 10) default; default=default}
  let length arr = arr.size
  let get arr idx = if idx >= Array.length arr.arr && idx < arr.size then arr.default else Array.get arr.arr idx
  let calc_size arr at_least = let double = Array.length arr.arr * 2 in
    let at_most = if double > arr.size then arr.size else double in
    if at_least > at_most then at_least else at_most
  let upgrade_size arr size = let old = arr.arr in    arr.arr <- Array.make size arr.default;    Array.blit old 0 arr.arr 0 (Array.length old)
  let set arr idx value= if idx >= Array.length arr.arr && idx < arr.size then upgrade_size arr (calc_size arr (idx + 1));
    Array.set arr.arr idx value
  let compare_arr f a b = let smaller,larger = if Array.length a.arr < Array.length b.arr then a,b else b,a in
  upgrade_size smaller (Array.length larger.arr);
  f (Array.to_list a.arr) (Array.to_list b.arr)
  let compare f a b = let v = Pervasives.compare a.size b.size in if v = 0 then compare_arr f a b else v
  let fold_left f init arr = snd (Array.fold_left (fun (i,x) a -> if i > 0 then (i - 1, f x a) else (0,x))  (arr.size,init) arr.arr)
  let fold_right f arr init = snd (Array.fold_right (fun a (i,x) -> if i > 0 then (i - 1, x) else (0, f a x)) arr.arr (Array.length arr.arr - arr.size, init))
  let iteri f arr = Array.iteri (fun i a -> if i < arr.size then f i a else ()) arr.arr
end


module type S =
sig
  type m
  type s
  type t = kitem list
  and kitem = KToken of sort * string
            | InjectedKLabel of klabel
            | Map of sort * klabel * m
            | List of sort * klabel * t list
            | Set of sort * klabel * s
            | Array of sort * t * t Dynarray.t
            | Int of Z.t
            | Float of Gmp.FR.t * int * int
            | String of string
            | Bytes of bytes
            | StringBuffer of Buffer.t
            | Bool of bool
            | MInt of int * Z.t
            | ThreadLocal
            | Thread of t * t * t * t
            | Bottom
            | KApply0 of klabel
            | KApply1 of klabel * t
            | KApply2 of klabel * t * t
            | KApply3 of klabel * t * t * t
            | KApply4 of klabel * t * t * t * t
            | KApply5 of klabel * t * t * t * t * t
            | KApply8 of klabel * t * t * t * t * t * t * t * t
  val compare : t -> t -> int
  val compare_kitem : kitem -> kitem -> int
  val compare_klist : t list -> t list -> int
  val equal_k : t -> t -> bool
  val hash_k : t -> int
  val hash_k_param : int -> t -> int
end
module rec K : (S with type m = K.t Map.Make(K).t and type s = Set.Make(K).t)  =
struct
  module KMap = Map.Make(K)
  module KSet = Set.Make(K)
  type m = K.t KMap.t
  and s = KSet.t
  and t = kitem list
  and kitem = KToken of sort * string
            | InjectedKLabel of klabel
            | Map of sort * klabel * m
            | List of sort * klabel * t list
            | Set of sort * klabel * s
            | Array of sort * t * t Dynarray.t
            | Int of Z.t
            | Float of Gmp.FR.t * int * int
            | String of string
            | Bytes of bytes
            | StringBuffer of Buffer.t
            | Bool of bool
            | MInt of int * Z.t
            | ThreadLocal
            | Thread of t * t * t * t
            | Bottom
            | KApply0 of klabel
            | KApply1 of klabel * t
            | KApply2 of klabel * t * t
            | KApply3 of klabel * t * t * t
            | KApply4 of klabel * t * t * t * t
            | KApply5 of klabel * t * t * t * t * t
            | KApply8 of klabel * t * t * t * t * t * t * t * t
let rec hash_k c = match c with
    | [] -> 1
    | hd :: tl -> (hash_k tl) * 31 + hash_kitem hd
  and hash_kitem c = match c with
    | KApply0(lbl) -> (Hashtbl.hash lbl)
    | KApply1(lbl,k0) -> ((Hashtbl.hash lbl)) * 37 + hash_k k0
    | KApply2(lbl,k0,k1) -> (((Hashtbl.hash lbl)) * 37 + hash_k k0) * 37 + hash_k k1
    | KApply3(lbl,k0,k1,k2) -> ((((Hashtbl.hash lbl)) * 37 + hash_k k0) * 37 + hash_k k1) * 37 + hash_k k2
    | KApply4(lbl,k0,k1,k2,k3) -> (((((Hashtbl.hash lbl)) * 37 + hash_k k0) * 37 + hash_k k1) * 37 + hash_k k2) * 37 + hash_k k3
    | KApply5(lbl,k0,k1,k2,k3,k4) -> ((((((Hashtbl.hash lbl)) * 37 + hash_k k0) * 37 + hash_k k1) * 37 + hash_k k2) * 37 + hash_k k3) * 37 + hash_k k4
    | KApply8(lbl,k0,k1,k2,k3,k4,k5,k6,k7) -> (((((((((Hashtbl.hash lbl)) * 37 + hash_k k0) * 37 + hash_k k1) * 37 + hash_k k2) * 37 + hash_k k3) * 37 + hash_k k4) * 37 + hash_k k5) * 37 + hash_k k6) * 37 + hash_k k7
    | KToken(s, st) -> Hashtbl.hash s * 41 + Hashtbl.hash st
    | InjectedKLabel kl -> Hashtbl.hash kl
    | Map(_,k,m) -> Hashtbl.hash k * 43 + KMap.fold (fun k v accum -> accum + (hash_k k lxor hash_k v)) m 0
    | List(_,k,l) -> Hashtbl.hash k * 47 + hash_klist l
    | Set(_,k,s) -> Hashtbl.hash k * 53 + KSet.fold (fun k accum -> accum + hash_k k) s 0
    | Array(k,_,l) -> Hashtbl.hash k * 61 + (Dynarray.length l)
    | Int i -> Z.hash i
    | Float (f,_,_) -> Hashtbl.hash (Gmp.FR.to_float f)
    | String s -> Hashtbl.hash s
    | StringBuffer s -> Hashtbl.hash (Buffer.contents s)
    | Bytes b -> Hashtbl.hash b
    | Bool b -> Hashtbl.hash b
    | MInt (w,i) -> Hashtbl.hash w * 67 + Z.hash i
    | Bottom -> 1
    | ThreadLocal -> 2
    | Thread(k1,k2,k3,k4) -> ((((Hashtbl.hash Lbl'Hash'Thread) * 37 + hash_k k1) * 37 + hash_k k2) * 37 + hash_k k3) * 36 + hash_k k4
  and hash_klist c = match c with
    | [] -> 1
    | hd :: tl -> (hash_klist tl) * 59 + hash_k hd
let rec hash_k_param_fld ((l,max) as lmax) = function 
  | [] -> lmax
  | h::t -> if max < 0 then lmax else hash_k_param_fld (h::l,max-1) t

let hash_k_param_add_kitem k max =
  hash_k_param_fld max k

let rec qfld l1 h max = match l1 with 
  | [] -> let (l2,max) = max in if l2 = [] then h else qfld l2 h ([],max)
  | ki :: kq -> match ki with 
    | KApply0(lbl) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        max)
    | KApply1(lbl,k0) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        hash_k_param_add_kitem k0 (
        max))
    | KApply2(lbl,k0,k1) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        hash_k_param_add_kitem k0 (
        hash_k_param_add_kitem k1 (
        max)))
    | KApply3(lbl,k0,k1,k2) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        hash_k_param_add_kitem k0 (
        hash_k_param_add_kitem k1 (
        hash_k_param_add_kitem k2 (
        max))))
    | KApply4(lbl,k0,k1,k2,k3) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        hash_k_param_add_kitem k0 (
        hash_k_param_add_kitem k1 (
        hash_k_param_add_kitem k2 (
        hash_k_param_add_kitem k3 (
        max)))))
    | KApply5(lbl,k0,k1,k2,k3,k4) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        hash_k_param_add_kitem k0 (
        hash_k_param_add_kitem k1 (
        hash_k_param_add_kitem k2 (
        hash_k_param_add_kitem k3 (
        hash_k_param_add_kitem k4 (
        max))))))
    | KApply8(lbl,k0,k1,k2,k3,k4,k5,k6,k7) -> 
        qfld kq (31*h + Hashtbl.hash lbl) (
        hash_k_param_add_kitem k0 (
        hash_k_param_add_kitem k1 (
        hash_k_param_add_kitem k2 (
        hash_k_param_add_kitem k3 (
        hash_k_param_add_kitem k4 (
        hash_k_param_add_kitem k5 (
        hash_k_param_add_kitem k6 (
        hash_k_param_add_kitem k7 (
        max)))))))))
    | KToken(s, st) ->
        qfld kq (31*h + Hashtbl.hash s * 41 + Hashtbl.hash st) (
        max)
    | InjectedKLabel lbl ->
        qfld kq (31*h + Hashtbl.hash lbl) (
        max)
    | Map(_,lbl,m) -> 
        qfld kq (31*h + 43 * Hashtbl.hash lbl) (
        KMap.fold (fun k v max -> hash_k_param_add_kitem v (hash_k_param_add_kitem k max)) m max)
    | List(_,lbl,l) ->
        qfld kq (31*h + 47 * Hashtbl.hash lbl) (
        List.fold_left (fun max k -> hash_k_param_add_kitem k max) max l)
    | Set(_,lbl,s) ->
        qfld kq (31*h + 53 * Hashtbl.hash lbl) (
        KSet.fold (fun k max -> hash_k_param_add_kitem k max) s max)
    | Array(lbl,_,l) -> 
        qfld kq (31*h + 61 * Hashtbl.hash lbl + Dynarray.length l) (
        max)
    | Int i -> qfld kq (31*h + Z.hash i) (
        max)
    | Float (f,_,_) -> qfld kq (31*h + Hashtbl.hash (Gmp.FR.to_float f)) (
        max)
    | String s -> qfld kq (31*h + Hashtbl.hash s) (
        max)
    | Bytes b -> qfld kq (31*h + Hashtbl.hash b) (
        max)
    | StringBuffer s -> qfld kq (31*h + Hashtbl.hash (Buffer.contents s)) (
        max)
    | Bool b -> qfld kq (31*h + Hashtbl.hash b) (
        max)
    | MInt (w,i) -> qfld kq ((31*h + (Hashtbl.hash w))*67 + Z.hash i) (
        max)
    | Bottom -> qfld kq (31*h + 1) (
        max)
    | ThreadLocal -> qfld kq (31*h + 2) (
        max)
    | Thread(k1,k2,k3,k4) -> qfld kq (31*h + Hashtbl.hash Lbl'Hash'Thread) (hash_k_param_add_kitem k1 (hash_k_param_add_kitem k2 (hash_k_param_add_kitem k3 (hash_k_param_add_kitem k4 max))))

let hash_k_param max k = 
    qfld [] 0 (hash_k_param_add_kitem k ([],max))

  let rec equal_k c1 c2 = if c1 == c2 then true else match (c1, c2) with
    | [], [] -> true
    | (hd1 :: tl1), (hd2 :: tl2) -> equal_kitem hd1 hd2 && equal_k tl1 tl2
    | _ -> false
  and equal_kitem c1 c2 = if c1 == c2 then true else match (c1, c2) with
    | KApply0(lbl1),KApply0(lbl2) -> lbl1 = lbl2
    | KApply1(lbl1,k0_1),KApply1(lbl2,k0_2) -> lbl1 = lbl2 && equal_k k0_1 k0_2
    | KApply2(lbl1,k0_1,k1_1),KApply2(lbl2,k0_2,k1_2) -> lbl1 = lbl2 && equal_k k0_1 k0_2 && equal_k k1_1 k1_2
    | KApply3(lbl1,k0_1,k1_1,k2_1),KApply3(lbl2,k0_2,k1_2,k2_2) -> lbl1 = lbl2 && equal_k k0_1 k0_2 && equal_k k1_1 k1_2 && equal_k k2_1 k2_2
    | KApply4(lbl1,k0_1,k1_1,k2_1,k3_1),KApply4(lbl2,k0_2,k1_2,k2_2,k3_2) -> lbl1 = lbl2 && equal_k k0_1 k0_2 && equal_k k1_1 k1_2 && equal_k k2_1 k2_2 && equal_k k3_1 k3_2
    | KApply5(lbl1,k0_1,k1_1,k2_1,k3_1,k4_1),KApply5(lbl2,k0_2,k1_2,k2_2,k3_2,k4_2) -> lbl1 = lbl2 && equal_k k0_1 k0_2 && equal_k k1_1 k1_2 && equal_k k2_1 k2_2 && equal_k k3_1 k3_2 && equal_k k4_1 k4_2
    | KApply8(lbl1,k0_1,k1_1,k2_1,k3_1,k4_1,k5_1,k6_1,k7_1),KApply8(lbl2,k0_2,k1_2,k2_2,k3_2,k4_2,k5_2,k6_2,k7_2) -> lbl1 = lbl2 && equal_k k0_1 k0_2 && equal_k k1_1 k1_2 && equal_k k2_1 k2_2 && equal_k k3_1 k3_2 && equal_k k4_1 k4_2 && equal_k k5_1 k5_2 && equal_k k6_1 k6_2 && equal_k k7_1 k7_2
    | (KToken(s1, st1)), (KToken(s2, st2)) -> s1 = s2 && st1 = st2
    | (InjectedKLabel kl1), (InjectedKLabel kl2) -> kl1 = kl2
    | (Map (_,k1,m1)), (Map (_,k2,m2)) -> k1 = k2 && KMap.cardinal m1 = KMap.cardinal m2 && (KMap.equal) (equal_k) m1 m2
    | (List (_,k1,l1)), (List (_,k2,l2)) -> k1 = k2 && equal_klist l1 l2
    | (Set (_,k1,s1)), (Set (_,k2,s2)) -> k1 = k2 && KSet.cardinal s1 = KSet.cardinal s2 && (KSet.equal) s1 s2
    | (Array (s1,k1,l1)), (Array (s2,k2,l2)) -> s1 = s2 && equal_k k1 k2 && l1 == l2
    | (Int i1), (Int i2) -> Z.equal i1 i2
    | (Float (f1,e1,p1)), (Float (f2,e2,p2)) -> e1 = e2 && p1 = p2 && Gmp.FR.compare f1 f2 = 0
    | (String s1), (String s2) -> s1 = s2
    | (Bytes b1), (Bytes b2) -> b1 == b2
    | (StringBuffer s1), (StringBuffer s2) -> s1 == s2
    | (Bool b1), (Bool b2) -> b1 = b2
    | (MInt (w1,i1)), (MInt (w2,i2)) -> w1 = w2 && Z.equal i1 i2
    | Bottom, Bottom -> true
    | _ -> false
  and equal_klist c1 c2 = if c1 == c2 then true else match (c1, c2) with
    | [], [] -> true
    | (hd1 :: tl1), (hd2 :: tl2) -> equal_k hd1 hd2 && equal_klist tl1 tl2
    | _ -> false
  let rec compare c1 c2 = if c1 == c2 then 0 else match (c1, c2) with
    | [], [] -> 0
    | (hd1 :: tl1), (hd2 :: tl2) -> let v = compare_kitem hd1 hd2 in if v = 0 then compare tl1 tl2 else v
    | (_ :: _), _ -> -1
    | _ -> 1
  and compare_kitem c1 c2 = if c1 == c2 then 0 else match (c1, c2) with
    | KApply0(lbl1),KApply0(lbl2) -> Pervasives.compare lbl1 lbl2
    | KApply1(lbl1,k0_1),KApply1(lbl2,k0_2) -> (let v = Pervasives.compare lbl1 lbl2 in if v = 0 then compare k0_1 k0_2
 else v)
    | KApply2(lbl1,k0_1,k1_1),KApply2(lbl2,k0_2,k1_2) -> (let v = Pervasives.compare lbl1 lbl2 in if v = 0 then (let v = compare k0_1 k0_2 in if v = 0 then compare k1_1 k1_2
 else v)
 else v)
    | KApply3(lbl1,k0_1,k1_1,k2_1),KApply3(lbl2,k0_2,k1_2,k2_2) -> (let v = Pervasives.compare lbl1 lbl2 in if v = 0 then (let v = compare k0_1 k0_2 in if v = 0 then (let v = compare k1_1 k1_2 in if v = 0 then compare k2_1 k2_2
 else v)
 else v)
 else v)
    | KApply4(lbl1,k0_1,k1_1,k2_1,k3_1),KApply4(lbl2,k0_2,k1_2,k2_2,k3_2) -> (let v = Pervasives.compare lbl1 lbl2 in if v = 0 then (let v = compare k0_1 k0_2 in if v = 0 then (let v = compare k1_1 k1_2 in if v = 0 then (let v = compare k2_1 k2_2 in if v = 0 then compare k3_1 k3_2
 else v)
 else v)
 else v)
 else v)
    | KApply5(lbl1,k0_1,k1_1,k2_1,k3_1,k4_1),KApply5(lbl2,k0_2,k1_2,k2_2,k3_2,k4_2) -> (let v = Pervasives.compare lbl1 lbl2 in if v = 0 then (let v = compare k0_1 k0_2 in if v = 0 then (let v = compare k1_1 k1_2 in if v = 0 then (let v = compare k2_1 k2_2 in if v = 0 then (let v = compare k3_1 k3_2 in if v = 0 then compare k4_1 k4_2
 else v)
 else v)
 else v)
 else v)
 else v)
    | KApply8(lbl1,k0_1,k1_1,k2_1,k3_1,k4_1,k5_1,k6_1,k7_1),KApply8(lbl2,k0_2,k1_2,k2_2,k3_2,k4_2,k5_2,k6_2,k7_2) -> (let v = Pervasives.compare lbl1 lbl2 in if v = 0 then (let v = compare k0_1 k0_2 in if v = 0 then (let v = compare k1_1 k1_2 in if v = 0 then (let v = compare k2_1 k2_2 in if v = 0 then (let v = compare k3_1 k3_2 in if v = 0 then (let v = compare k4_1 k4_2 in if v = 0 then (let v = compare k5_1 k5_2 in if v = 0 then (let v = compare k6_1 k6_2 in if v = 0 then compare k7_1 k7_2
 else v)
 else v)
 else v)
 else v)
 else v)
 else v)
 else v)
 else v)
    | (KToken(s1, st1)), (KToken(s2, st2)) -> let v = Pervasives.compare s1 s2 in if v = 0 then Pervasives.compare st1 st2 else v
    | (InjectedKLabel kl1), (InjectedKLabel kl2) -> Pervasives.compare kl1 kl2
    | (Map (_,k1,m1)), (Map (_,k2,m2)) -> let v = Pervasives.compare k1 k2 in if v = 0 then (KMap.compare) compare m1 m2 else v
    | (List (_,k1,l1)), (List (_,k2,l2)) -> let v = Pervasives.compare k1 k2 in if v = 0 then compare_klist l1 l2 else v
    | (Set (_,k1,s1)), (Set (_,k2,s2)) -> let v = Pervasives.compare k1 k2 in if v = 0 then (KSet.compare) s1 s2 else v
    | (Array (s1,k1,l1)), (Array (s2,k2,l2)) -> let v = Pervasives.compare s1 s2 in if v = 0 then let v = compare k1 k2 in if v = 0 then Dynarray.compare compare_klist l1 l2 else v else v
    | (Int i1), (Int i2) -> Z.compare i1 i2
    | (Float (f1,e1,p1)), (Float (f2,e2,p2)) -> let v = e2 - e1 in if v = 0 then let v2 = p2 - p1 in if v2 = 0 then Gmp.FR.compare f1 f2 else v2 else v
    | (String s1), (String s2) -> Pervasives.compare s1 s2
    | (Bytes b1), (Bytes b2) -> Pervasives.compare b1 b2
    | (StringBuffer s1), (StringBuffer s2) -> Pervasives.compare (Buffer.contents s1) (Buffer.contents s2)
    | (Bool b1), (Bool b2) -> if b1 = b2 then 0 else if b1 then -1 else 1
    | (MInt (w1,i1)), (MInt (w2,i2)) -> let v = Pervasives.compare w1 w2 in if v = 0 then Z.compare i1 i2 else v
    | Bottom, Bottom -> 0
    | ThreadLocal, ThreadLocal -> 0
    | Thread (k11, k12, k13, k14), Thread (k21, k22, k23, k24) -> let v = compare k11 k21 in if v = 0 then let v = compare k12 k22 in if v = 0 then let v = compare k13 k23 in if v = 0 then compare k14 k24 else v else v else v
    | KApply0 _, _ -> -1
    | _, KApply0 _ -> 1
    | KApply1 _, _ -> -1
    | _, KApply1 _ -> 1
    | KApply2 _, _ -> -1
    | _, KApply2 _ -> 1
    | KApply3 _, _ -> -1
    | _, KApply3 _ -> 1
    | KApply4 _, _ -> -1
    | _, KApply4 _ -> 1
    | KApply5 _, _ -> -1
    | _, KApply5 _ -> 1
    | KApply8 _, _ -> -1
    | _, KApply8 _ -> 1
    | KToken(_, _), _ -> -1
    | _, KToken(_, _) -> 1
    | InjectedKLabel(_), _ -> -1
    | _, InjectedKLabel(_) -> 1
    | Map(_), _ -> -1
    | _, Map(_) -> 1
    | List(_), _ -> -1
    | _, List(_) -> 1
    | Set(_), _ -> -1
    | _, Set(_) -> 1
    | Array(_), _ -> -1
    | _, Array(_) -> 1
    | Int(_), _ -> -1
    | _, Int(_) -> 1
    | Float(_), _ -> -1
    | _, Float(_) -> 1
    | String(_), _ -> -1
    | _, String(_) -> 1
    | Bytes(_), _ -> -1
    | _, Bytes(_) -> 1
    | StringBuffer(_), _ -> -1
    | _, StringBuffer(_) -> 1
    | Bool(_), _ -> -1
    | _, Bool(_) -> 1
    | MInt _, _ -> -1
    | _, MInt _ -> 1
    | Bottom, _ -> -1
    | _, Bottom -> 1
    | ThreadLocal, _ -> -1
    | _, ThreadLocal -> 1
  and compare_klist c1 c2 = match (c1, c2) with
    | [], [] -> 0
    | (hd1 :: tl1), (hd2 :: tl2) -> let v = compare hd1 hd2 in if v = 0 then compare_klist tl1 tl2 else v
    | (_ :: _), _ -> -1
    | _ -> 1
end
type normal_kitem = KApply of klabel * K.t list
                  | KItem of K.kitem
open K
let normalize (k: kitem) : normal_kitem = match k with 
  | KApply0(lbl) -> KApply (lbl, [])
  | KApply1(lbl,k0) -> KApply (lbl, [k0])
  | KApply2(lbl,k0,k1) -> KApply (lbl, [k0; k1])
  | KApply3(lbl,k0,k1,k2) -> KApply (lbl, [k0; k1; k2])
  | KApply4(lbl,k0,k1,k2,k3) -> KApply (lbl, [k0; k1; k2; k3])
  | KApply5(lbl,k0,k1,k2,k3,k4) -> KApply (lbl, [k0; k1; k2; k3; k4])
  | KApply8(lbl,k0,k1,k2,k3,k4,k5,k6,k7) -> KApply (lbl, [k0; k1; k2; k3; k4; k5; k6; k7])
| v -> KItem v
let denormalize (k: normal_kitem) : kitem = match k with 
  | KApply (lbl, []) -> KApply0(lbl)
  | KApply (lbl, [k0]) -> KApply1(lbl,k0)
  | KApply (lbl, [k0; k1]) -> KApply2(lbl,k0,k1)
  | KApply (lbl, [k0; k1; k2]) -> KApply3(lbl,k0,k1,k2)
  | KApply (lbl, [k0; k1; k2; k3]) -> KApply4(lbl,k0,k1,k2,k3)
  | KApply (lbl, [k0; k1; k2; k3; k4]) -> KApply5(lbl,k0,k1,k2,k3,k4)
  | KApply (lbl, [k0; k1; k2; k3; k4; k5; k6; k7]) -> KApply8(lbl,k0,k1,k2,k3,k4,k5,k6,k7)
| KItem v -> v
| KApply (_, _) -> invalid_arg "denormalize"
type k = K.t
let denormalize0  (c: unit) : k list = match c with () -> []
let normalize0 (c: k list) = match c with [] -> ()
| _ -> invalid_arg "normalize0"
let denormalize1  (c: k) : k list = match c with (k0) -> [k0]
let normalize1 (c: k list) = match c with [k0] -> (k0)
| _ -> invalid_arg "normalize1"
let denormalize2  (c: k * k) : k list = match c with (k0,k1) -> [k0; k1]
let normalize2 (c: k list) = match c with [k0; k1] -> (k0,k1)
| _ -> invalid_arg "normalize2"
let denormalize3  (c: k * k * k) : k list = match c with (k0,k1,k2) -> [k0; k1; k2]
let normalize3 (c: k list) = match c with [k0; k1; k2] -> (k0,k1,k2)
| _ -> invalid_arg "normalize3"
let denormalize4  (c: k * k * k * k) : k list = match c with (k0,k1,k2,k3) -> [k0; k1; k2; k3]
let normalize4 (c: k list) = match c with [k0; k1; k2; k3] -> (k0,k1,k2,k3)
| _ -> invalid_arg "normalize4"
let denormalize5  (c: k * k * k * k * k) : k list = match c with (k0,k1,k2,k3,k4) -> [k0; k1; k2; k3; k4]
let normalize5 (c: k list) = match c with [k0; k1; k2; k3; k4] -> (k0,k1,k2,k3,k4)
| _ -> invalid_arg "normalize5"
let denormalize8  (c: k * k * k * k * k * k * k * k) : k list = match c with (k0,k1,k2,k3,k4,k5,k6,k7) -> [k0; k1; k2; k3; k4; k5; k6; k7]
let normalize8 (c: k list) = match c with [k0; k1; k2; k3; k4; k5; k6; k7] -> (k0,k1,k2,k3,k4,k5,k6,k7)
| _ -> invalid_arg "normalize8"
let int0 = lazy (Int (Z.of_string "0"))
let int1 = lazy (Int (Z.of_string "1"))
let int2 = lazy (Int (Z.of_string "2"))
let int'Hyph'1 = lazy (Int (Z.of_string "-1"))
let constinitIndexCell = KApply0(LblinitIndexCell)
let const'Hash'EHOSTDOWN_K'Hyph'IO = KApply0(Lbl'Hash'EHOSTDOWN_K'Hyph'IO)
let constnoKCell = KApply0(LblnoKCell)
let constinitNstateCell = KApply0(LblinitNstateCell)
let const'Hash'EINTR_K'Hyph'IO = KApply0(Lbl'Hash'EINTR_K'Hyph'IO)
let constnoFbodyCell = KApply0(LblnoFbodyCell)
let constnoTimerCell = KApply0(LblnoTimerCell)
let const'Hash'freezer'Hash'lvDref0_ = KApply0(Lbl'Hash'freezer'Hash'lvDref0_)
let const'Stop'FunDefCellMap = KApply0(Lbl'Stop'FunDefCellMap)
let const'Hash'EOVERFLOW_K'Hyph'IO = KApply0(Lbl'Hash'EOVERFLOW_K'Hyph'IO)
let constinitFbodyCell = KApply0(LblinitFbodyCell)
let const'Hash'ENETDOWN_K'Hyph'IO = KApply0(Lbl'Hash'ENETDOWN_K'Hyph'IO)
let const'Hash'removeState_CONTROL = KApply0(Lbl'Hash'removeState_CONTROL)
let const'Hash'EIO_K'Hyph'IO = KApply0(Lbl'Hash'EIO_K'Hyph'IO)
let const'Hash'Bottom = KApply0(Lbl'Hash'Bottom)
let const'Hash'EISCONN_K'Hyph'IO = KApply0(Lbl'Hash'EISCONN_K'Hyph'IO)
let const'Hash'EDOM_K'Hyph'IO = KApply0(Lbl'Hash'EDOM_K'Hyph'IO)
let const'Stop'Map = KApply0(Lbl'Stop'Map)
let const'Hash'EPROTOTYPE_K'Hyph'IO = KApply0(Lbl'Hash'EPROTOTYPE_K'Hyph'IO)
let constnoStackCell = KApply0(LblnoStackCell)
let constmut_OSL'Hyph'SYNTAX = KApply0(Lblmut_OSL'Hyph'SYNTAX)
let constnoIndexesCell = KApply0(LblnoIndexesCell)
let constinitFunDefsCell = KApply0(LblinitFunDefsCell)
let const'Hash'EADDRINUSE_K'Hyph'IO = KApply0(Lbl'Hash'EADDRINUSE_K'Hyph'IO)
let constinitTmpCell = KApply0(LblinitTmpCell)
let constinitWriteCell = KApply0(LblinitWriteCell)
let const'Hash'void_OSL'Hyph'SYNTAX = KApply0(Lbl'Hash'void_OSL'Hyph'SYNTAX)
let const'Hash'EACCES_K'Hyph'IO = KApply0(Lbl'Hash'EACCES_K'Hyph'IO)
let const'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra' = KApply0(Lbl'Stop'List'LBraQuot'_'Comm'__OSL'Hyph'SYNTAX'QuotRBra')
let constnoFretCell = KApply0(LblnoFretCell)
let constnoIndexCell = KApply0(LblnoIndexCell)
let constnewUUID_STRING = KApply0(LblnewUUID_STRING)
let const'Hash'ENOSPC_K'Hyph'IO = KApply0(Lbl'Hash'ENOSPC_K'Hyph'IO)
let constnoTmpCell = KApply0(LblnoTmpCell)
let const'Hash'EMLINK_K'Hyph'IO = KApply0(Lbl'Hash'EMLINK_K'Hyph'IO)
let const'Hash'EINVAL_K'Hyph'IO = KApply0(Lbl'Hash'EINVAL_K'Hyph'IO)
let const'Hash'freezer'Hash'Read0_ = KApply0(Lbl'Hash'freezer'Hash'Read0_)
let const'Hash'ENOPROTOOPT_K'Hyph'IO = KApply0(Lbl'Hash'ENOPROTOOPT_K'Hyph'IO)
let const'Hash'EPERM_K'Hyph'IO = KApply0(Lbl'Hash'EPERM_K'Hyph'IO)
let const'Hash'EWOULDBLOCK_K'Hyph'IO = KApply0(Lbl'Hash'EWOULDBLOCK_K'Hyph'IO)
let constinitFretCell = KApply0(LblinitFretCell)
let const'Stop'Set = KApply0(Lbl'Stop'Set)
let const'Hash'ENETUNREACH_K'Hyph'IO = KApply0(Lbl'Hash'ENETUNREACH_K'Hyph'IO)
let const'Stop'List = KApply0(Lbl'Stop'List)
let const'Hash'ENAMETOOLONG_K'Hyph'IO = KApply0(Lbl'Hash'ENAMETOOLONG_K'Hyph'IO)
let const'Hash'ECONNRESET_K'Hyph'IO = KApply0(Lbl'Hash'ECONNRESET_K'Hyph'IO)
let const'Hash'ENXIO_K'Hyph'IO = KApply0(Lbl'Hash'ENXIO_K'Hyph'IO)
let constinitFnameCell = KApply0(LblinitFnameCell)
let const'Hash'EALREADY_K'Hyph'IO = KApply0(Lbl'Hash'EALREADY_K'Hyph'IO)
let const'Hash'ENOTCONN_K'Hyph'IO = KApply0(Lbl'Hash'ENOTCONN_K'Hyph'IO)
let const'Hash'freezer'Hash'Deallocate0_ = KApply0(Lbl'Hash'freezer'Hash'Deallocate0_)
let const'Hash'blockend_BLOCK = KApply0(Lbl'Hash'blockend_BLOCK)
let const'Hash'ETOOMANYREFS_K'Hyph'IO = KApply0(Lbl'Hash'ETOOMANYREFS_K'Hyph'IO)
let const'Hash'configuration_K'Hyph'REFLECTION = KApply0(Lbl'Hash'configuration_K'Hyph'REFLECTION)
let constnoStoreCell = KApply0(LblnoStoreCell)
let const'Hash'EMSGSIZE_K'Hyph'IO = KApply0(Lbl'Hash'EMSGSIZE_K'Hyph'IO)
let constnoEnvCell = KApply0(LblnoEnvCell)
let const'Hash'increaseTimer_OSL = KApply0(Lbl'Hash'increaseTimer_OSL)
let const'Hash'EDEADLK_K'Hyph'IO = KApply0(Lbl'Hash'EDEADLK_K'Hyph'IO)
let const'Hash'ENOTSOCK_K'Hyph'IO = KApply0(Lbl'Hash'ENOTSOCK_K'Hyph'IO)
let const'Hash'EAGAIN_K'Hyph'IO = KApply0(Lbl'Hash'EAGAIN_K'Hyph'IO)
let const'Hash'ESHUTDOWN_K'Hyph'IO = KApply0(Lbl'Hash'ESHUTDOWN_K'Hyph'IO)
let const'Hash'ERANGE_K'Hyph'IO = KApply0(Lbl'Hash'ERANGE_K'Hyph'IO)
let const'Hash'E2BIG_K'Hyph'IO = KApply0(Lbl'Hash'E2BIG_K'Hyph'IO)
let const'Hash'ECONNREFUSED_K'Hyph'IO = KApply0(Lbl'Hash'ECONNREFUSED_K'Hyph'IO)
let const'Hash'ENOSYS_K'Hyph'IO = KApply0(Lbl'Hash'ENOSYS_K'Hyph'IO)
let constnoFnameCell = KApply0(LblnoFnameCell)
let constinitIndexesCell = KApply0(LblinitIndexesCell)
let constnoFunDefsCell = KApply0(LblnoFunDefsCell)
let const'Hash'ENOTDIR_K'Hyph'IO = KApply0(Lbl'Hash'ENOTDIR_K'Hyph'IO)
let constnoStatesCell = KApply0(LblnoStatesCell)
let constinitStackCell = KApply0(LblinitStackCell)
let const'Hash'ECONNABORTED_K'Hyph'IO = KApply0(Lbl'Hash'ECONNABORTED_K'Hyph'IO)
let const'Hash'EBUSY_K'Hyph'IO = KApply0(Lbl'Hash'EBUSY_K'Hyph'IO)
let const'Hash'EOPNOTSUPP_K'Hyph'IO = KApply0(Lbl'Hash'EOPNOTSUPP_K'Hyph'IO)
let const'Hash'ESRCH_K'Hyph'IO = KApply0(Lbl'Hash'ESRCH_K'Hyph'IO)
let const'Hash'ENOMEM_K'Hyph'IO = KApply0(Lbl'Hash'ENOMEM_K'Hyph'IO)
let const'Hash'ESPIPE_K'Hyph'IO = KApply0(Lbl'Hash'ESPIPE_K'Hyph'IO)
let constnoFparamsCell = KApply0(LblnoFparamsCell)
let const'Hash'ENOENT_K'Hyph'IO = KApply0(Lbl'Hash'ENOENT_K'Hyph'IO)
let const'Hash'freezerval0_ = KApply0(Lbl'Hash'freezerval0_)
let const'Stop'StateCellMap = KApply0(Lbl'Stop'StateCellMap)
let constinitEnvCell = KApply0(LblinitEnvCell)
let const'Hash'stdout_K'Hyph'IO = KApply0(Lbl'Hash'stdout_K'Hyph'IO)
let const'Hash'ENODEV_K'Hyph'IO = KApply0(Lbl'Hash'ENODEV_K'Hyph'IO)
let const'Hash'EXDEV_K'Hyph'IO = KApply0(Lbl'Hash'EXDEV_K'Hyph'IO)
let const'Hash'ENOLCK_K'Hyph'IO = KApply0(Lbl'Hash'ENOLCK_K'Hyph'IO)
let const'Hash'argv = KApply0(Lbl'Hash'argv)
let constinitTimerCell = KApply0(LblinitTimerCell)
let const'Hash'ENOTEMPTY_K'Hyph'IO = KApply0(Lbl'Hash'ENOTEMPTY_K'Hyph'IO)
let const'Hash'EMFILE_K'Hyph'IO = KApply0(Lbl'Hash'EMFILE_K'Hyph'IO)
let const'Hash'freezer'Hash'expStmt0_ = KApply0(Lbl'Hash'freezer'Hash'expStmt0_)
let const'Hash'ELOOP_K'Hyph'IO = KApply0(Lbl'Hash'ELOOP_K'Hyph'IO)
let const'Hash'stderr_K'Hyph'IO = KApply0(Lbl'Hash'stderr_K'Hyph'IO)
let const'Hash'EPIPE_K'Hyph'IO = KApply0(Lbl'Hash'EPIPE_K'Hyph'IO)
let const'Hash'ENETRESET_K'Hyph'IO = KApply0(Lbl'Hash'ENETRESET_K'Hyph'IO)
let constinitStoreCell = KApply0(LblinitStoreCell)
let const'Hash'EPFNOSUPPORT_K'Hyph'IO = KApply0(Lbl'Hash'EPFNOSUPPORT_K'Hyph'IO)
let const'Hash'EEXIST_K'Hyph'IO = KApply0(Lbl'Hash'EEXIST_K'Hyph'IO)
let const'Hash'stdin_K'Hyph'IO = KApply0(Lbl'Hash'stdin_K'Hyph'IO)
let const'Hash'EADDRNOTAVAIL_K'Hyph'IO = KApply0(Lbl'Hash'EADDRNOTAVAIL_K'Hyph'IO)
let const'Hash'EHOSTUNREACH_K'Hyph'IO = KApply0(Lbl'Hash'EHOSTUNREACH_K'Hyph'IO)
let const'Hash'increaseIndex_OSL = KApply0(Lbl'Hash'increaseIndex_OSL)
let const'Hash'ECHILD_K'Hyph'IO = KApply0(Lbl'Hash'ECHILD_K'Hyph'IO)
let const'Hash'EOF_K'Hyph'IO = KApply0(Lbl'Hash'EOF_K'Hyph'IO)
let const'Hash'EDESTADDRREQ_K'Hyph'IO = KApply0(Lbl'Hash'EDESTADDRREQ_K'Hyph'IO)
let const'Hash'EFBIG_K'Hyph'IO = KApply0(Lbl'Hash'EFBIG_K'Hyph'IO)
let constcopy_OSL'Hyph'SYNTAX = KApply0(Lblcopy_OSL'Hyph'SYNTAX)
let const'Hash'EBADF_K'Hyph'IO = KApply0(Lbl'Hash'EBADF_K'Hyph'IO)
let const'Hash'ETIMEDOUT_K'Hyph'IO = KApply0(Lbl'Hash'ETIMEDOUT_K'Hyph'IO)
let const'Hash'ESOCKTNOSUPPORT_K'Hyph'IO = KApply0(Lbl'Hash'ESOCKTNOSUPPORT_K'Hyph'IO)
let const'Hash'EISDIR_K'Hyph'IO = KApply0(Lbl'Hash'EISDIR_K'Hyph'IO)
let const'Hash'voidTy_OSL'Hyph'SYNTAX = KApply0(Lbl'Hash'voidTy_OSL'Hyph'SYNTAX)
let const'Hash'ENOTTY_K'Hyph'IO = KApply0(Lbl'Hash'ENOTTY_K'Hyph'IO)
let constinitFparamsCell = KApply0(LblinitFparamsCell)
let const'Hash'EFAULT_K'Hyph'IO = KApply0(Lbl'Hash'EFAULT_K'Hyph'IO)
let constnoWriteCell = KApply0(LblnoWriteCell)
let const'Hash'EROFS_K'Hyph'IO = KApply0(Lbl'Hash'EROFS_K'Hyph'IO)
let const'Hash'noparse_K'Hyph'IO = KApply0(Lbl'Hash'noparse_K'Hyph'IO)
let const'Hash'ENFILE_K'Hyph'IO = KApply0(Lbl'Hash'ENFILE_K'Hyph'IO)
let const'Hash'ThreadLocal = KApply0(Lbl'Hash'ThreadLocal)
let constinitFunDefCell = KApply0(LblinitFunDefCell)
let const'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra' = KApply0(Lbl'Stop'List'LBraQuot'___OSL'Hyph'SYNTAX'QuotRBra')
let const'Hash'EAFNOSUPPORT_K'Hyph'IO = KApply0(Lbl'Hash'EAFNOSUPPORT_K'Hyph'IO)
let const'Hash'ENOEXEC_K'Hyph'IO = KApply0(Lbl'Hash'ENOEXEC_K'Hyph'IO)
let const'Hash'uninit_OSL'Hyph'SYNTAX = KApply0(Lbl'Hash'uninit_OSL'Hyph'SYNTAX)
let const'Hash'EPROTONOSUPPORT_K'Hyph'IO = KApply0(Lbl'Hash'EPROTONOSUPPORT_K'Hyph'IO)
let constnoNstateCell = KApply0(LblnoNstateCell)
let const'Hash'ENOBUFS_K'Hyph'IO = KApply0(Lbl'Hash'ENOBUFS_K'Hyph'IO)
let const'Stop'List'LBraQuotHash'props'QuotRBra' = KApply0(Lbl'Stop'List'LBraQuotHash'props'QuotRBra')
let const'Hash'EINPROGRESS_K'Hyph'IO = KApply0(Lbl'Hash'EINPROGRESS_K'Hyph'IO)
let val_for (c: klabel) (k : k) (v : k) : normal_kitem = match c with
|_ -> KApply((el_for c), [k;v])
